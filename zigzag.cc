/***************************************************************************
Copyright (c) 2016-2017, Guillaume Baud-Berthier, SafeRiver/LaBRI

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
IN THE SOFTWARE.
***************************************************************************/

#include "zigzag.h"

unsigned act_init;

dll_list<idef>* dll_ands_zz;
dll_list<idef>* dll_inputs_zz;
dll_list<idef>* dll_latches_zz;

SimpSolver* solver_zz;
vec<Lit>    assumptions_zz;

std::map<unsigned, ands_def*> map_ands_zz;
std::vector<idef*> mapdef_zz;
std::vector<dll_list<ands_def>*> list_ands_zz;

unsigned vars_zz = 0;

// Simple path (loop free) constraints on demand
bool simple_path_od(int time, int bad, bool verbose_) {

  vec<Lit> lits;
  idef* tmp_def;
  dll_elem<idef> *tmp_e;
  bool same_state;
  bool run_solver_again = 1;

  while (run_solver_again) {
    run_solver_again = 0;

    for(int i = 0; i < time/*(time - 1)*/ && !run_solver_again; i++) {
      for(int j = i+1; j <= time && !run_solver_again; j++) {
	
	same_state = 1;

	tmp_e = dll_latches_zz->head;
	while(tmp_e != NULL && same_state) {
	  tmp_def = tmp_e->def;

	  assert(tmp_def->lhs_ts[i] > 1);
	  assert(tmp_def->lhs_ts[j] > 1);

	  if (solver_zz->model[tmp_def->lhs_ts[i] >> 1] != solver_zz->model[tmp_def->lhs_ts[j] >> 1])
	    same_state = 0;

	  tmp_e = tmp_e->next;
	}

	if (same_state) {
	  // Add constraints
	  lits.clear();
	  tmp_e = dll_latches_zz->head;
	  while(tmp_e != NULL && same_state) {
	    tmp_def = tmp_e->def;


	    assert(tmp_def->lhs_ts[i] > 1);
	    assert(tmp_def->lhs_ts[j] > 1);


	    unsigned new_v = new_var(&vars_zz) >> 1;

	    while (new_v >= solver_zz->nVars()) solver_zz->newVar();

	    assert(!aiger_sign(tmp_def->lhs_ts[i]) && !aiger_sign(tmp_def->lhs_ts[j]));

	    lits.push(mkLit(new_v));

	    tcl(solver_zz, -new_v, -(tmp_def->lhs_ts[i] >> 1), -(tmp_def->lhs_ts[j] >> 1));
	    tcl(solver_zz, -new_v,  (tmp_def->lhs_ts[i] >> 1),  (tmp_def->lhs_ts[j] >> 1));
	    tcl(solver_zz,  new_v,  (tmp_def->lhs_ts[i] >> 1), -(tmp_def->lhs_ts[j] >> 1));
	    tcl(solver_zz,  new_v, -(tmp_def->lhs_ts[i] >> 1),  (tmp_def->lhs_ts[j] >> 1));

	    tmp_e = tmp_e->next;
	  }
	
	  solver_zz->addClause_(lits);
	  run_solver_again = 1;
	}
      }
    }

    if (run_solver_again) {
      assume(solver_zz, assumptions_zz, bad); // Assume: -P(x)
      if (check_sat(solver_zz, assumptions_zz, verbose_) == l_False)
	return 1;
    }
  }

  return 0;
}

dll_list<ands_def>* unroll_kind(int time) {
  int j;
  unsigned tmp_rhs;
  unsigned tmp_rhs0, tmp_rhs1;

  dll_list<ands_def> *tmp_list_ands;
  tmp_list_ands = new dll_list<ands_def>();
  dll_init<ands_def>(tmp_list_ands);
  
  idef* tmp_def;
  dll_elem<idef> *tmp_e;
  std::list<unsigned>::iterator it_rhs;
  std::set<unsigned>::iterator it_tmp_rhs;


  // Inputs
  tmp_e = dll_inputs_zz->head;
  while(tmp_e != NULL) {
    tmp_def = tmp_e->def;
    
    tmp_def->lhs_ts.push_back(new_var(&vars_zz));

    tmp_e = tmp_e->next;
  }  
  
  // Latches
  tmp_e = dll_latches_zz->head;
  while(tmp_e != NULL) {
    tmp_def = tmp_e->def;

    if (!time) { // Init
      tmp_def->lhs_ts.push_back(new_var(&vars_zz));

      bcl(solver_zz, -(tmp_def->lhs_ts[0] >> 1), -act_init);

    } else {
      assert(tmp_def->rhs.size() == 1);
      
      if (tmp_def->rhs.front() > 1) {
	assert(tmp_def->lhs_ts[time-1] != -1);

	assert(tmp_def->lhs_ts.size() == time);
	tmp_def->lhs_ts.push_back(new_var(&vars_zz));

	if (aiger_sign(tmp_def->rhs.front()) != aiger_sign(mapdef_zz[aiger_lit2var(tmp_def->rhs.front())]->lhs_ts[time-1])) {
	  bcl(solver_zz,  (tmp_def->lhs_ts[time] >> 1),  (mapdef_zz[aiger_lit2var(tmp_def->rhs.front())]->lhs_ts[time-1] >> 1));
	  bcl(solver_zz, -(tmp_def->lhs_ts[time] >> 1), -(mapdef_zz[aiger_lit2var(tmp_def->rhs.front())]->lhs_ts[time-1] >> 1));
	} else {
	  bcl(solver_zz,  (tmp_def->lhs_ts[time] >> 1), -(mapdef_zz[aiger_lit2var(tmp_def->rhs.front())]->lhs_ts[time-1] >> 1));
	  bcl(solver_zz, -(tmp_def->lhs_ts[time] >> 1),   mapdef_zz[aiger_lit2var(tmp_def->rhs.front())]->lhs_ts[time-1] >> 1);
	}
	

      } else { // Cst latch
	tmp_def->lhs_ts.push_back(new_var(&vars_zz));
	
	assert(tmp_def->rhs.front() == 1);
	ucl(solver_zz, tmp_def->lhs_ts[time] >> 1);	
      }
    }
    tmp_e = tmp_e->next;
  }
  
  // Ands
  tmp_e = dll_ands_zz->head;
  while(tmp_e != NULL) {
    tmp_def = tmp_e->def;

    prop_tr_un *tpu = unroll_prop_tr(mapdef_zz, map_ands_zz, tmp_def->rhs_tr, time, 0);

    assert(tpu->lit == UNDEF_LIT);
    assert(tpu->op != VAR);

    tmp_def->lhs_ts.push_back(new_var(&vars_zz));

    ands_def *tmp_ands = new ands_def(tmp_def->lhs_ts[time], tpu, tmp_def, time);

    map_ands_zz.insert(std::pair<unsigned, ands_def*>(aiger_lit2var(tmp_ands->lhs), tmp_ands));
      
    tmp_ands->dllade = dll_insert_last(tmp_list_ands, aiger_lit2var(tmp_def->lhs_ts[time]), tmp_ands);

    tmp_e = tmp_e->next;
  }

  return tmp_list_ands;
}



int zigzag(int start, int stop, aiger *model, bool elim_hl, int simple, bool verbose_, int *status) {
  int i, j;

  //int status = -1;
  dll_inputs_zz = new dll_list<idef>();
  dll_latches_zz = new dll_list<idef>();
  dll_ands_zz = new dll_list<idef>();
  unsigned bad_lit = init_model(model, dll_ands_zz, dll_inputs_zz, dll_latches_zz, mapdef_zz, 0, verbose_);
  aiger_reset(model);
  solver_zz = new SimpSolver();
  init_solver(solver_zz, 0, verbose_);

  if (elim_hl)
    hl_ve(mapdef_zz, dll_ands_zz, verbose_);

  act_init = new_var(&vars_zz) >> 1;

  for(i = 0; i <= stop; i++) {

    if (verbose_) printf("[zigzag]  Encoding %d..\n", i);

    list_ands_zz.push_back(unroll_kind(i));

    iter_flatten_ptu(list_ands_zz, i);

    ands2clauses(solver_zz, list_ands_zz, i, 0);

    assert(mapdef_zz[aiger_lit2var(bad_lit)]->lhs_ts[i] > 1);
    
    int bad = mapdef_zz[aiger_lit2var(bad_lit)]->lhs_ts[i];
    bad = aiger_sign(bad_lit) == aiger_sign(bad) ? (bad >> 1) : -(bad >> 1);

    lbool res_check;
    if (verbose_) printf("[zigzag]  Solving %d-induction..\n", i);
      
    assume(solver_zz, assumptions_zz, bad); // Assume: -P(x)

    res_check = check_sat(solver_zz, assumptions_zz,  verbose_);
    if (res_check == l_False) {
      *status = 0;
      break;
    } else if (i > 1 && simple == 2) {
      if (simple_path_od(i, bad, verbose_)) {
	*status = 0;
	break;
      }
    }
    

    if (verbose_) printf("[zigzag]  Solving BMC %d..\n", i);

    assume(solver_zz, assumptions_zz, bad);              // Assume: -P(x)
    assume(solver_zz, assumptions_zz, act_init);       // Assume:  I(x)

    res_check = check_sat(solver_zz, assumptions_zz, verbose_);
    if (res_check == l_True) {
      *status = 1;
      break;
    }


    printf("u%d\n", i);
    fflush(stdout);

    ucl(solver_zz, -bad);                // Add: P(x)

  }

  //printf("%d\nb0\n.\n", *status);
  //fflush(stdout);

  if (verbose_)
    printf("Vars %d (%d - %d)| Clauses %d\n", solver_zz->nVars() - solver_zz->eliminated_vars,
	   solver_zz->nVars(), solver_zz->eliminated_vars, solver_zz->nClauses());

  destroy_model(dll_ands_zz, dll_inputs_zz, dll_latches_zz, list_ands_zz, mapdef_zz, map_ands_zz, verbose_);
  return 0;
}


