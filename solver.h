/***************************************************************************
Copyright (c) 2016-2017, Guillaume Baud-Berthier, SafeRiver/LaBRI

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
IN THE SOFTWARE.
***************************************************************************/

#ifndef solver_h
#define solver_h

#include "SimpSolver.h"
#include "utils.h"
#include "un_simp.h"

using namespace Glucose;

//extern SimpSolver* solver;
//extern vec<Lit>    assumptions;

/* ========================================================================== */
/*                             Solver functions                               */
/* ========================================================================== */
void init_solver(SimpSolver* solver, bool elim_ll, bool verbose);
void ands2clauses(SimpSolver* solver, std::vector<dll_list<ands_def>*> &list_ands, int time, bool elim_ll);
lbool check_sat(SimpSolver* solver, vec<Lit> &assumptions, bool verb = 1, bool do_simp = 1);
//lbool check_sat_with_restart(SimpSolver* solver, bool verb);
void assume(SimpSolver* solver, vec<Lit> &assumptions, int lit);
void ucl(SimpSolver* solver, int a);
void bcl(SimpSolver* solver, int a, int b);
void tcl(SimpSolver* solver, int a, int b, int c);
void cl_lhs2rhs(SimpSolver* solver, unsigned lhs, std::list<unsigned> &rhs);
void cl_rhs2lhs(SimpSolver* solver, unsigned lhs, std::list<unsigned> &rhs);
void bigAnd2Solver(SimpSolver* solver, unsigned lhs, prop_tr_un_fl *ptuf);
void propagate_solver();

void ands2clauses_coi(SimpSolver* solver, std::vector<dll_list<ands_def>*> &list_ands, std::vector<idef*> &mapdef, std::map<unsigned, ands_def*> &map_ands, int bad_lit, int time, bool verbose);

bool import_unit_from_solver(SimpSolver* solver, std::map<unsigned, ands_def*> &map_ands, bool verbose);
bool import_binary_from_solver(SimpSolver* solver, std::map<unsigned, ands_def*> &map_ands, bool verbose);
//bool import_binary_from_solver_bwd(bool verbose);

std::pair<std::list<std::list<unsigned>>, std::list<std::list<unsigned>>> pt2cl(prop_tr_un_fl *ptuf);

#endif
