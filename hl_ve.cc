/***************************************************************************
Copyright (c) 2016-2017, Guillaume Baud-Berthier, SafeRiver/LaBRI

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
IN THE SOFTWARE.
***************************************************************************/
#include "hl_ve.h"

int HL_VE_CL_GROW = 0;
int HL_VE_CL_MAX_SZ = 20;

bool REVERSE_ORDER = 0;
bool exact_nb_of_cls = 1;

int stats_hl_ve_cl = 0;

/* ========================================================================== */
/*                          High level VE functions                           */
/* ========================================================================== */
std::pair<std::pair<int, int>, std::pair<int, int>> compute_nb_cls_gen(prop_tr *pt) {
  if (pt->op == VAR) {
    return std::pair<std::pair<int, int>, std::pair<int, int>>(std::pair<int, int>(1, 1), std::pair<int, int>(1, 1));
  } else if (pt->op == AND) {
    std::pair<std::pair<int, int>, std::pair<int, int>> l = compute_nb_cls_gen(pt->l);
    std::pair<std::pair<int, int>, std::pair<int, int>> r = compute_nb_cls_gen(pt->r);
    return std::pair<std::pair<int, int>, std::pair<int, int>>(std::pair<int, int>(l.first.first * r.first.first, l.first.second + r.first.second),
							       std::pair<int, int>(l.second.first + r.second.first,
										   (l.second.second > r.second.second) ?
										   l.second.second : r.second.second));
  } else {
    assert(pt->op == OR);
    std::pair<std::pair<int, int>, std::pair<int, int>> l = compute_nb_cls_gen(pt->l);
    std::pair<std::pair<int, int>, std::pair<int, int>> r = compute_nb_cls_gen(pt->r);
    return std::pair<std::pair<int, int>, std::pair<int, int>>(std::pair<int, int>(l.first.first + r.first.first, l.first.second * r.first.second),
							       std::pair<int, int>((l.second.first > r.second.first) ?
										   l.second.first : r.second.first,
										   l.second.second + r.second.second));
  }
}

std::pair<std::pair<int, int>, std::pair<int, int>> compute_nb_cls_gen_ve(prop_tr *pt, int var_to_elim,
					  std::pair<std::pair<int, int>, std::pair<int, int>> cl_gen_var, bool sign) {
  if (pt->op == VAR) {
    if (pt->var == var_to_elim)
      if (sign)
	return std::pair<std::pair<int, int>, std::pair<int, int>>(std::pair<int, int>(cl_gen_var.first.second, cl_gen_var.first.first),
								   std::pair<int, int>(cl_gen_var.second.second, cl_gen_var.second.first));
      else
	return cl_gen_var;
    else
      return std::pair<std::pair<int, int>, std::pair<int, int>>(std::pair<int, int>(1, 1), std::pair<int, int>(1, 1));
  } else if (pt->op == AND) {
    std::pair<std::pair<int, int>, std::pair<int, int>> l = compute_nb_cls_gen_ve(pt->l, var_to_elim, cl_gen_var, pt->l_sign);
    std::pair<std::pair<int, int>, std::pair<int, int>> r = compute_nb_cls_gen_ve(pt->r, var_to_elim, cl_gen_var, pt->r_sign);
    return std::pair<std::pair<int, int>, std::pair<int, int>>(std::pair<int, int>(l.first.first * r.first.first, l.first.second + r.first.second),
							       std::pair<int, int>(l.second.first + r.second.first,
										   (l.second.second > r.second.second) ?
										   l.second.second : r.second.second));
  } else {
    assert(pt->op == OR);
    std::pair<std::pair<int, int>, std::pair<int, int>> l = compute_nb_cls_gen_ve(pt->l, var_to_elim, cl_gen_var, pt->l_sign);
    std::pair<std::pair<int, int>, std::pair<int, int>> r = compute_nb_cls_gen_ve(pt->r, var_to_elim, cl_gen_var, pt->r_sign);
    return std::pair<std::pair<int, int>, std::pair<int, int>>(std::pair<int, int>(l.first.first + r.first.first, l.first.second * r.first.second),
							       std::pair<int, int>((l.second.first > r.second.first) ?
										   l.second.first : r.second.first,
										   l.second.second + r.second.second));
  }
}


/***********************************************************************/
std::pair<std::list<std::list<int>>, std::list<std::list<int>>> compute_cls_gen_exact(prop_tr *pt, bool sign) {
  std::list<int> li;
  std::list<std::list<int>> lli_f;
  std::list<std::list<int>> lli_s;
  
  if (pt->op == VAR) {
    sign ? li.push_back((pt->var << 1) + 1) : li.push_back(pt->var << 1);
    lli_f.push_back(li);
    lli_s.push_back(li);

  } else {
    std::list<std::pair<std::list<std::list<int>>, std::list<std::list<int>>>> lli_pl;
    
    std::pair<std::list<std::list<int>>, std::list<std::list<int>>> lli_pl_tmp = compute_cls_gen_exact(pt->l, pt->l_sign);
    std::pair<std::list<std::list<int>>, std::list<std::list<int>>> lli_pr_tmp = compute_cls_gen_exact(pt->r, pt->r_sign);
    lli_pl.push_back(lli_pl_tmp);
    lli_pl.push_back(lli_pr_tmp);
    
    std::list<std::pair<std::list<std::list<int>>, std::list<std::list<int>>>>::iterator it_llip;
    std::list<std::list<int>>::iterator it_lli;
    std::list<std::list<int>>::iterator it_lli2;
    std::list<int>::iterator it_li;

    if (pt->op == AND) {
      
      for( it_llip = lli_pl.begin(); it_llip != lli_pl.end(); it_llip++) {
	for (it_lli = it_llip->first.begin(); it_lli != it_llip->first.end(); it_lli++) {
	  lli_f.push_back(*it_lli);
	}
      }
      
      std::list<std::list<int>> lli_op1_s = lli_pl.front().second;
      lli_pl.pop_front();
      while (!lli_pl.empty()) {
	std::list<std::list<int>> lli_op2_s = lli_pl.front().second;
	lli_pl.pop_front();
	for (it_lli = lli_op1_s.begin(); it_lli != lli_op1_s.end(); it_lli++) {
	  for (it_lli2 = lli_op2_s.begin(); it_lli2 != lli_op2_s.end(); it_lli2++) {
	    li.clear();
	    
	    for (it_li = it_lli->begin(); it_li != it_lli->end(); it_li++) {
	      li.push_back(*it_li);
	    }
	    for (it_li = it_lli2->begin(); it_li != it_lli2->end(); it_li++) {
	      li.push_back(*it_li);
	    }
	    lli_s.push_back(li);
	  }
	}
	lli_op1_s = lli_s;
	lli_s.clear();
      }
      lli_s = lli_op1_s;
      
    } else {

      for( it_llip = lli_pl.begin(); it_llip != lli_pl.end(); it_llip++) {
	for (it_lli = it_llip->second.begin(); it_lli != it_llip->second.end(); it_lli++) {
	  lli_s.push_back(*it_lli);
	}

      }
      
      std::list<std::list<int>> lli_op1_f = lli_pl.front().first;
      lli_pl.pop_front();
      while (!lli_pl.empty()) {
	std::list<std::list<int>> lli_op2_f = lli_pl.front().first;
	lli_pl.pop_front();
	for (it_lli = lli_op1_f.begin(); it_lli != lli_op1_f.end(); it_lli++) {
	  for (it_lli2 = lli_op2_f.begin(); it_lli2 != lli_op2_f.end(); it_lli2++) {
	    li.clear();
	    
	    for (it_li = it_lli->begin(); it_li != it_lli->end(); it_li++) {
	      li.push_back(*it_li);
	    }
	    for (it_li = it_lli2->begin(); it_li != it_lli2->end(); it_li++) {
	      li.push_back(*it_li);
	    }
	    lli_f.push_back(li);
	  }
	}
	lli_op1_f = lli_f;
	lli_f.clear();
      }
      lli_f = lli_op1_f;
      
    }
  }
  return std::pair<std::list<std::list<int>>, std::list<std::list<int>>>(lli_f, lli_s);
}

std::pair<std::pair<int, int>, std::pair<int, int>> compute_nb_cls_gen_exact(prop_tr *pt, bool sign) {
  std::pair<std::pair<int, int>, std::pair<int, int>> ret = std::pair<std::pair<int, int>, std::pair<int, int>>(std::pair<int, int>(0, 0), std::pair<int, int>(0,0));
  std::set<int> set_cl;
  std::pair<std::list<std::list<int>>, std::list<std::list<int>>> list_of_cls = compute_cls_gen_exact(pt, sign);
  std::list<std::list<int>> cls = list_of_cls.first;
  int nb_of_cls = 0;
  for (std::list<std::list<int>>::iterator it = cls.begin(); it != cls.end(); it++) {
    bool trivial_cl = 0;
    for (std::list<int>::iterator it_lit = (*it).begin(); it_lit != (*it).end(); it_lit++) {
      set_cl.insert(*it_lit);
    }
    ret.second.second = ret.second.second > set_cl.size() ? ret.second.second : set_cl.size();
    for (std::list<int>::iterator it_lit = (*it).begin(); !trivial_cl && it_lit != --(*it).end();) {
      int current = *it_lit;
      it_lit++;
      if (aiger_lit2var(current) == aiger_lit2var(*it_lit))
	trivial_cl = 1;
    }
    if (!trivial_cl)
      nb_of_cls++;
    set_cl.clear();
  }
  ret.first.second = nb_of_cls;

  cls = list_of_cls.second;
  nb_of_cls = 0;
  for (std::list<std::list<int>>::iterator it = cls.begin(); it != cls.end(); it++) {
    bool trivial_cl = 0;
    for (std::list<int>::iterator it_lit = (*it).begin(); it_lit != (*it).end(); it_lit++) {
      set_cl.insert(*it_lit);
    }
    ret.second.first = ret.second.first > set_cl.size() ? ret.second.first : set_cl.size();
    for (std::list<int>::iterator it_lit = (*it).begin(); !trivial_cl && it_lit != --(*it).end();) {
      int current = *it_lit;
      it_lit++;
      if (aiger_lit2var(current) == aiger_lit2var(*it_lit))
	trivial_cl = 1;
    }
    if (!trivial_cl)
      nb_of_cls++;
    set_cl.clear();
  }
  ret.first.first = nb_of_cls;
  return ret;
}



std::pair<std::list<std::list<int>>, std::list<std::list<int>>> compute_cls_gen_ve_exact(
        prop_tr *pt, int var_to_elim, std::pair<std::list<std::list<int>>, std::list<std::list<int>>> cl_gen_var, bool sign) {
  std::list<int> li;
  std::list<std::list<int>> lli_f;
  std::list<std::list<int>> lli_s;
  
  if (pt->op == VAR) {
    if (pt->var == var_to_elim)
      if (sign)
	return std::pair<std::list<std::list<int>>, std::list<std::list<int>>>(cl_gen_var.second, cl_gen_var.first);
      else
	return cl_gen_var;
    else {
      sign ? li.push_back((pt->var << 1) + 1) : li.push_back(pt->var << 1);
      lli_f.push_back(li);
      lli_s.push_back(li);
    }
  } else {
    std::list<std::pair<std::list<std::list<int>>, std::list<std::list<int>>>> lli_pl;
    
    std::pair<std::list<std::list<int>>, std::list<std::list<int>>> lli_pl_tmp = compute_cls_gen_ve_exact(pt->l, var_to_elim, cl_gen_var, pt->l_sign);
    std::pair<std::list<std::list<int>>, std::list<std::list<int>>> lli_pr_tmp = compute_cls_gen_ve_exact(pt->r, var_to_elim, cl_gen_var, pt->r_sign);
    lli_pl.push_back(lli_pl_tmp);
    lli_pl.push_back(lli_pr_tmp);
    
    std::list<std::pair<std::list<std::list<int>>, std::list<std::list<int>>>>::iterator it_llip;
    std::list<std::list<int>>::iterator it_lli;
    std::list<std::list<int>>::iterator it_lli2;
    std::list<int>::iterator it_li;

    if (pt->op == AND) {
      
      for( it_llip = lli_pl.begin(); it_llip != lli_pl.end(); it_llip++) {
	for (it_lli = it_llip->first.begin(); it_lli != it_llip->first.end(); it_lli++) {
	  lli_f.push_back(*it_lli);
	}
      }
      
      std::list<std::list<int>> lli_op1_s = lli_pl.front().second;
      lli_pl.pop_front();
      while (!lli_pl.empty()) {
	std::list<std::list<int>> lli_op2_s = lli_pl.front().second;
	lli_pl.pop_front();
	for (it_lli = lli_op1_s.begin(); it_lli != lli_op1_s.end(); it_lli++) {
	  for (it_lli2 = lli_op2_s.begin(); it_lli2 != lli_op2_s.end(); it_lli2++) {
	    li.clear();
	    
	    for (it_li = it_lli->begin(); it_li != it_lli->end(); it_li++) {
	      li.push_back(*it_li);
	    }
	    for (it_li = it_lli2->begin(); it_li != it_lli2->end(); it_li++) {
	      li.push_back(*it_li);
	    }
	    lli_s.push_back(li);
	  }
	}
	lli_op1_s = lli_s;
	lli_s.clear();
      }
      lli_s = lli_op1_s;
      
    } else {

      for( it_llip = lli_pl.begin(); it_llip != lli_pl.end(); it_llip++) {
	for (it_lli = it_llip->second.begin(); it_lli != it_llip->second.end(); it_lli++) {
	  lli_s.push_back(*it_lli);
	}

      }
      
      std::list<std::list<int>> lli_op1_f = lli_pl.front().first;
      lli_pl.pop_front();
      while (!lli_pl.empty()) {
	std::list<std::list<int>> lli_op2_f = lli_pl.front().first;
	lli_pl.pop_front();
	for (it_lli = lli_op1_f.begin(); it_lli != lli_op1_f.end(); it_lli++) {
	  for (it_lli2 = lli_op2_f.begin(); it_lli2 != lli_op2_f.end(); it_lli2++) {
	    li.clear();
	    
	    for (it_li = it_lli->begin(); it_li != it_lli->end(); it_li++) {
	      li.push_back(*it_li);
	    }
	    for (it_li = it_lli2->begin(); it_li != it_lli2->end(); it_li++) {
	      li.push_back(*it_li);
	    }
	    lli_f.push_back(li);
	  }
	}
	lli_op1_f = lli_f;
	lli_f.clear();
      }
      lli_f = lli_op1_f;
      
    }
  }
  return std::pair<std::list<std::list<int>>, std::list<std::list<int>>>(lli_f, lli_s);
}

std::pair<std::pair<int, int>, std::pair<int, int>> compute_nb_cls_gen_ve_exact(prop_tr *pt, int var_to_elim, prop_tr *pt_repBy) {
  std::pair<std::pair<int, int>, std::pair<int, int>> ret = std::pair<std::pair<int, int>, std::pair<int, int>>(std::pair<int, int>(0, 0), std::pair<int, int>(0,0));
  std::set<int> set_cl;
  std::pair<std::list<std::list<int>>, std::list<std::list<int>>> cl_gen_var = compute_cls_gen_exact(pt_repBy, 0);
  std::pair<std::list<std::list<int>>, std::list<std::list<int>>> list_of_cls = compute_cls_gen_ve_exact(pt, var_to_elim, cl_gen_var, 0);
  std::list<std::list<int>> cls = list_of_cls.first;
  int nb_of_cls = 0;
  for (std::list<std::list<int>>::iterator it = cls.begin(); it != cls.end(); it++) {
    bool trivial_cl = 0;
    for (std::list<int>::iterator it_lit = (*it).begin(); it_lit != (*it).end(); it_lit++) {
      set_cl.insert(*it_lit);
    }
    ret.second.second = ret.second.second > set_cl.size() ? ret.second.second : set_cl.size();
    for (std::list<int>::iterator it_lit = (*it).begin(); !trivial_cl && it_lit != --(*it).end();) {
      int current = *it_lit;
      it_lit++;
      if (aiger_lit2var(current) == aiger_lit2var(*it_lit))
	trivial_cl = 1;
    }
    if (!trivial_cl)
      nb_of_cls++;
    set_cl.clear();
  }
  ret.first.second = nb_of_cls;

  cls = list_of_cls.second;
  nb_of_cls = 0;
  for (std::list<std::list<int>>::iterator it = cls.begin(); it != cls.end(); it++) {
    bool trivial_cl = 0;
    for (std::list<int>::iterator it_lit = (*it).begin(); it_lit != (*it).end(); it_lit++) {
      set_cl.insert(*it_lit);
    }
    ret.second.first = ret.second.first > set_cl.size() ? ret.second.first : set_cl.size();
    for (std::list<int>::iterator it_lit = (*it).begin(); !trivial_cl && it_lit != --(*it).end();) {
      int current = *it_lit;
      it_lit++;
      if (aiger_lit2var(current) == aiger_lit2var(*it_lit))
	trivial_cl = 1;
    }
    if (!trivial_cl)
      nb_of_cls++;
    set_cl.clear();
  }
  ret.first.first = nb_of_cls;
  return ret;
}


/* ========================================================================== */
/*                             Prop_tr functions                              */
/* ========================================================================== */
prop_tr* build_not_prop_tr(prop_tr *pt) {
  assert(pt->op != VAR);
  prop_tr *not_prop_tr = new prop_tr();

  if (pt->op == AND)
    not_prop_tr->op = OR;
  else if (pt->op == OR)
    not_prop_tr->op = AND;
  
  not_prop_tr->var = pt->var;
  
  if (pt->l->op == VAR) {
    not_prop_tr->l_sign = !pt->l_sign;
    not_prop_tr->l = pt->l;
  } else {
    assert(!pt->l_sign);
    not_prop_tr->l_sign = 0;
    not_prop_tr->l = build_not_prop_tr(pt->l);
  }

  if (pt->r->op == VAR) {
    not_prop_tr->r_sign = !pt->r_sign;
    not_prop_tr->r = pt->r;
  } else {
    assert(!pt->r_sign);
    not_prop_tr->r_sign = 0;
    not_prop_tr->r = build_not_prop_tr(pt->r);
  }

  return not_prop_tr;
}

void elim_prop_tr_rec(prop_tr *pt, prop_tr *pt2elim) {
  assert(pt->l != NULL);
  assert(pt->r != NULL);

  // LEFT NODE
  if (pt->l->l == NULL && pt->l->r == NULL && pt->l->var == pt2elim->var) {
    if (pt->l_sign) {
      pt->l = build_not_prop_tr(pt2elim);
      pt->l_sign = 0;
    } else {
      pt->l = pt2elim;
    }

  } 
  if (pt->r->l == NULL && pt->r->r == NULL && pt->r->var == pt2elim->var) {  
    // RIGHT NODE
    if (pt->r_sign) {
      pt->r = build_not_prop_tr(pt2elim);
      pt->r_sign = 0;
    } else {
      pt->r = pt2elim;
    }
    
  } 
  
  if (pt->l->op != VAR)
    elim_prop_tr_rec(pt->l, pt2elim);
  if (pt->r->op != VAR)    
    elim_prop_tr_rec(pt->r, pt2elim);
}


void update_refby_hl_ve(std::vector<idef*> &mapdef, prop_tr *pt, idef *idef_to_del, idef *idef_to_add) {
  assert(idef_to_del != idef_to_add);
  if (pt->op == VAR) {
    idef *tmp_def = mapdef[pt->var];
    tmp_def->refBy.erase(idef_to_del);
    tmp_def->refBy.insert(idef_to_add);
  } else {
    update_refby_hl_ve(mapdef, pt->l, idef_to_del,  idef_to_add);
    update_refby_hl_ve(mapdef, pt->r, idef_to_del,  idef_to_add);
  }
}


void hl_ve_ClausesBased(std::vector<idef*> &mapdef, dll_list<idef>* dll_ands) {
  idef *tmp_def, *def_refBy;
  dll_elem<idef> *tmp_e;
  std::list<unsigned>::iterator it_rhs;
  std::list<unsigned>::iterator it_rhs2;
  std::set<idef*>::iterator it_ref;
  bool op_found;
  prop_tr *tmp_prop_tr;

 if (REVERSE_ORDER)
    tmp_e = dll_ands->last;
  else
    tmp_e = dll_ands->head;

  while(tmp_e != NULL) {
    tmp_def = tmp_e->def;
    assert(tmp_def->rhs_tr->op == AND);

    if (tmp_def->apply_ve && tmp_def->refBy.size()) {
      std::pair<std::pair<int, int>, std::pair<int, int>> cl_nb_and_sz_tmp_def;
      if (exact_nb_of_cls)
	cl_nb_and_sz_tmp_def = compute_nb_cls_gen_exact(tmp_def->rhs_tr, 0);
      else
	cl_nb_and_sz_tmp_def = compute_nb_cls_gen(tmp_def->rhs_tr);

      std::pair<int, int> nb_of_cl_tmp_def = cl_nb_and_sz_tmp_def.first;
      std::pair<int, int> cl_sz_tmp_def = cl_nb_and_sz_tmp_def.second;

      int current_nb_of_cls = nb_of_cl_tmp_def.first + nb_of_cl_tmp_def.second;

      for (it_ref = tmp_def->refBy.begin(); it_ref != tmp_def->refBy.end(); ++it_ref) {
	def_refBy = *it_ref;

	std::pair<std::pair<int, int>, std::pair<int, int>> cl_nb_and_sz_ref;
	if (exact_nb_of_cls)
	  cl_nb_and_sz_ref = compute_nb_cls_gen_exact(def_refBy->rhs_tr, 0);
	else
	  cl_nb_and_sz_ref = compute_nb_cls_gen(def_refBy->rhs_tr);
	std::pair<int, int> nb_of_cl_ref = cl_nb_and_sz_ref.first;
	current_nb_of_cls += nb_of_cl_ref.first + nb_of_cl_ref.second;
      }

      int future_nb_of_cls = 0;
      assert(tmp_def->refBy.size());
      
      bool to_elim = 1;
      for (it_ref = tmp_def->refBy.begin(); to_elim && it_ref != tmp_def->refBy.end(); ++it_ref) {
	def_refBy = *it_ref;
	std::pair<std::pair<int, int>, std::pair<int, int>> cl_nb_and_sz_refBy;
	if (exact_nb_of_cls)
	  cl_nb_and_sz_refBy = compute_nb_cls_gen_ve_exact(def_refBy->rhs_tr,
							   tmp_def->lhs, tmp_def->rhs_tr);
	else
	  cl_nb_and_sz_refBy = compute_nb_cls_gen_ve(def_refBy->rhs_tr,
						     tmp_def->lhs, cl_nb_and_sz_tmp_def, 0);

	std::pair<int, int> nb_of_cl_def_refBy = cl_nb_and_sz_refBy.first;
	std::pair<int, int> cl_sz_refBy = cl_nb_and_sz_refBy.second;

	int max_sz_cl = cl_sz_refBy.first > cl_sz_refBy.second ? cl_sz_refBy.first+1 : cl_sz_refBy.second+1;

	future_nb_of_cls += nb_of_cl_def_refBy.first + nb_of_cl_def_refBy.second;
	if (future_nb_of_cls > current_nb_of_cls + HL_VE_CL_GROW ||
	    max_sz_cl > HL_VE_CL_MAX_SZ)
	  to_elim = 0;
      }

      if (to_elim) {
	for (it_ref = tmp_def->refBy.begin(); it_ref != tmp_def->refBy.end();) {

	  def_refBy = *it_ref;

	  assert(def_refBy->type == idef_and);

	  elim_prop_tr_rec(def_refBy->rhs_tr, tmp_def->rhs_tr);

	  update_refby_hl_ve(mapdef, def_refBy->rhs_tr, tmp_def, def_refBy);

	  it_ref = tmp_def->refBy.erase(it_ref);

	}

	assert(!tmp_def->refBy.size());
	dll_remove_elem(tmp_e);

	stats_hl_ve_cl++;
      }
    }

    if (REVERSE_ORDER)
      tmp_e = tmp_e->prev;
    else
      tmp_e = tmp_e->next;
  }
}



void hl_ve(std::vector<idef*> &mapdef, dll_list<idef>* dll_ands, bool verbose) {
  clock_t start = clock();

  hl_ve_ClausesBased(mapdef, dll_ands);

  if (verbose) printf("[MC]  High level VE : %d (%.2fs)\n",
		      stats_hl_ve_cl, ((double) (clock() - start)) / CLOCKS_PER_SEC);
 }
