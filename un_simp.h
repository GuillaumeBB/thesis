/***************************************************************************
Copyright (c) 2016-2017, Guillaume Baud-Berthier, SafeRiver/LaBRI

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
IN THE SOFTWARE.
***************************************************************************/

#ifndef un_simp_h
#define un_simp_h

#include "utils.h"
#include "solver.h"

using namespace Glucose;

/* ========================================================================== */
/*                 Structural equivalence helper functions                    */
/* ========================================================================== */
unsigned hash_lit(unsigned x);
unsigned hash_ptuf(prop_tr_un_fl *ptuf);
bool equal_ptuf(prop_tr_un_fl *ptuf1, prop_tr_un_fl *ptuf2);
ands_def* check_struct_equiv(prop_tr_un_fl *ptuf, unsigned struct_hash);
ands_def* check_struct_equiv_global(prop_tr_un_fl *ptuf, unsigned struct_hash);
bool struct_equiv_global(SimpSolver* solver, ands_def *ad, unsigned hash, int time);
bool struct_equiv(SimpSolver* solver, ands_def *ad, unsigned hash, int time);
void clear_global_hash();

/* ========================================================================== */
/*                    Structural diff helper functions                        */
/* ========================================================================== */
unsigned hash_not_ptuf(prop_tr_un_fl *ptuf);
bool equal_not_ptuf(prop_tr_un_fl *ptuf1, prop_tr_un_fl *ptuf2);
ands_def* check_struct_diff(prop_tr_un_fl *ptuf, unsigned struct_hash);
ands_def* check_struct_diff_global(prop_tr_un_fl *ptuf, unsigned struct_hash);
bool struct_diff_global(SimpSolver* solver, ands_def *ad, unsigned hash_not, int time);
bool struct_diff(SimpSolver* solver, ands_def *ad, unsigned hash_not, int time);

/* ========================================================================== */
/*                 Functional equivalence helper functions                    */
/* ========================================================================== */
unsigned hash_func_ad(ands_def *ad);
bool equal_func_eq(ands_def *ad, ands_def *ad_);
ands_def* check_func_equiv(ands_def *ad, unsigned struct_hash);
ands_def* check_func_equiv_global(ands_def *ad, unsigned struct_hash);
bool func_equiv_global(ands_def *ad, unsigned hash_func, int time);
bool func_equiv(ands_def *ad, unsigned hash_func, int time);

/* ========================================================================== */
/*                 Functional diff helper functions                           */
/* ========================================================================== */
unsigned hash_func_diff_ad(ands_def *ad);
bool equal_func_diff_eq(ands_def *ad, ands_def *ad_);
ands_def* check_func_diff(ands_def *ad, unsigned struct_hash);
ands_def* check_func_diff_global(ands_def *ad, unsigned struct_hash);
bool func_diff_global(ands_def *ad, unsigned hash_func_not, int time);
bool func_diff(ands_def *ad, unsigned hash_func_not, int time);

/* ========================================================================== */
/*                           Optimisation functions                           */
/* ========================================================================== */
bool propagate_cst(prop_tr_un_fl *ptuf);
bool check_cst_ptuf_and(prop_tr_un_fl *ptuf);
bool check_cst_ptuf_or(prop_tr_un_fl *ptuf);
bool check_cst(SimpSolver* solver, dll_list<ands_def> *l, ands_def *ad, int time);
bool simp_prop_and_ptuf(std::list<unsigned> lit2prop_, prop_tr_un_fl *ptuf, bool is_and);
bool simp_prop_and(dll_list<ands_def> *l, ands_def *ad, int time);
bool simp_prop_or_ptuf(std::list<unsigned> lit2prop_, prop_tr_un_fl *ptuf, bool is_or);
bool simp_prop_or(dll_list<ands_def> *l, ands_def *ad, int time);
bool simp_prop(dll_list<ands_def> *l, ands_def *ad, int time);

void build_funcA_cst(ands_def *ad, prop_tr_un_fl *ptuf);
void build_funcO_cst(ands_def *ad, prop_tr_un_fl *ptuf);
void build_funcEq(ands_def *ad, prop_tr_un_fl *ptuf);
bool check_func_contra(ands_def *ad);
bool check_func_tauto(ands_def *ad);

bool simp_funcO_subs(prop_tr_un_fl *ptuf, std::set<unsigned> dom);
bool simp_funcA_subs(prop_tr_un_fl *ptuf, std::set<unsigned> dom);

bool simp_adv_cst(ands_def *ad);
bool simp_adv(SimpSolver* solver, ands_def *ad, int time);

bool propagate_def_cst(SimpSolver* solver, ands_def* ad, unsigned cst, bool fb);
bool propagate_def_alias(SimpSolver* solver, std::map<unsigned, ands_def*> &map_ands, ands_def* ad, unsigned cst, bool fb);

void simp_ands(SimpSolver* solver, std::vector<dll_list<ands_def>*> &list_ands, std::vector<idef*> &mapdef, std::map<unsigned, ands_def*> &map_ands, int time, unsigned bad_lit, bool bwd, bool verbose, bool elim_ll);

#endif
