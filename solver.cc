/***************************************************************************
Copyright (c) 2016-2017, Guillaume Baud-Berthier, SafeRiver/LaBRI

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
IN THE SOFTWARE.
***************************************************************************/

#include "solver.h"

using namespace Glucose;
/* ========================================================================== */
/*                             Solver functions                               */
/* ========================================================================== */
void init_solver(SimpSolver* solver, bool elim_ll, bool verbose) {
  solver->parsing = 0;
  solver->verbosity = 0;
  solver->verbEveryConflicts = 10000;
  solver->showModel = true;
  solver->certifiedUNSAT = false;
  //solver->setIncrementalMode();
  //solver->certifiedOutput = fopen("unsat_proof", "w+");
  solver->use_elim = elim_ll;
  solver->verb_mc = verbose;

  //assumptions.clear();
}

lbool check_sat(SimpSolver* solver, vec<Lit> &assumptions, bool verb, bool do_simp) {
  clock_t clk_diff, clk;
  
  if (verb)
    printf("[glucose] Conflicts : %8lu | Clauses : %8d | Learnts : %8d | "
	   "Unary : %8lu | Binary : %8lu\n",
	   solver->conflicts, solver->nClauses(), solver->nLearnts(),
	   solver->nbUn, solver->nbBin);
  
  clk = clock();
  lbool ret = solver->solveLimited(assumptions, do_simp);
  clk_diff = clock() - clk;
  assumptions.clear();
  
  if (verb) {
    printf(ret == l_True ? "[glucose] SAT (%6.2f sec)\n" :
	                   (ret == l_False ? "[glucose] UNSAT (%6.2f sec)\n" :
			                     "[glucose] INDETERMINATE (%6.2f sec)\n"),
	   (float)(clk_diff)/(float)(CLOCKS_PER_SEC));
  }
  return ret;
}

void assume(SimpSolver* solver, vec<Lit> &assumptions, int lit) {
  int var;
  var = abs(lit);
  if (var >= solver->nVars()) {
    printf("assume | unknown var\n");
    exit(-1);

  } else {
    assumptions.push( (lit > 0) ? mkLit(var) : ~mkLit(var) );
  }
}

void ucl(SimpSolver* solver, int a) {
  vec<Lit> lits;
  int var;
  var = abs(a);
  while (var >= solver->nVars()) solver->newVar();
  lits.push( (a > 0) ? mkLit(var) : ~mkLit(var) );

  assert(!solver->isEliminated(abs(a)));

  solver->addClause_(lits);
}

void bcl(SimpSolver* solver, int a, int b) {
  vec<Lit> lits;
  int var;
  var = abs(a);
  while (var >= solver->nVars()) solver->newVar();
  lits.push( (a > 0) ? mkLit(var) : ~mkLit(var) );

  var = abs(b);
  while (var >= solver->nVars()) solver->newVar();
  lits.push( (b > 0) ? mkLit(var) : ~mkLit(var) );
  assert(!solver->isEliminated(abs(a)));
  assert(!solver->isEliminated(abs(b)));

  solver->addClause_(lits);
}



void tcl(SimpSolver* solver, int a, int b, int c) {
  vec<Lit> lits;
  int var;
  var = abs(a);
  while (var >= solver->nVars()) solver->newVar();
  lits.push( (a > 0) ? mkLit(var) : ~mkLit(var) );

  var = abs(b);
  while (var >= solver->nVars()) solver->newVar();
  lits.push( (b > 0) ? mkLit(var) : ~mkLit(var) );

  var = abs(c);
  while (var >= solver->nVars()) solver->newVar();
  lits.push( (c > 0) ? mkLit(var) : ~mkLit(var) );

  assert(!solver->isEliminated(abs(a)));
  assert(!solver->isEliminated(abs(b)));
  assert(!solver->isEliminated(abs(c)));

  solver->addClause_(lits);
}

void cl_lhs2rhs(SimpSolver* solver, unsigned lhs, std::list<unsigned> &rhs) {
  //assert(!aiger_sign(lhs));
  assert(lhs > 1);
  
  std::list<unsigned>::iterator it_rhs;
  vec<Lit> lits;
  int var = lhs >> 1;

  while (var >= solver->nVars()) solver->newVar();
  aiger_sign(lhs) ? lits.push(mkLit(var)) : lits.push(~mkLit(var));
  assert(!solver->isEliminated(var));

  for (it_rhs = rhs.begin(); it_rhs != rhs.end(); ++it_rhs) {
    assert(*it_rhs > 1);
    var = *it_rhs >> 1;
    while (var >= solver->nVars()) solver->newVar();
    assert(var < solver->nVars());
    assert(!solver->isEliminated(var));
    lits.push( aiger_sign(*it_rhs) ? ~mkLit(var) : mkLit(var) );
  }

  solver->addClause_(lits);
}

void cl_rhs2lhs(SimpSolver* solver, unsigned lhs, std::list<unsigned> &rhs) {
  //assert(!aiger_sign(lhs));
  assert(lhs > 1);
  
  std::list<unsigned>::iterator it_rhs;
  vec<Lit> lits;
  int var = lhs >> 1;

  while (var >= solver->nVars()) solver->newVar();
  aiger_sign(lhs) ? lits.push(~mkLit(var)) : lits.push(mkLit(var));
  assert(!solver->isEliminated(var));

  for (it_rhs = rhs.begin(); it_rhs != rhs.end(); ++it_rhs) {
    assert(*it_rhs > 1);
    var = *it_rhs >> 1;
    while (var >= solver->nVars()) solver->newVar();
    assert(var < solver->nVars());
    assert(!solver->isEliminated(var));
    lits.push( aiger_sign(*it_rhs) ? mkLit(var) : ~mkLit(var) );
  }

  solver->addClause_(lits);
}


void bigAnd2Solver(SimpSolver* solver, unsigned lhs, prop_tr_un_fl *ptuf) {
  assert(ptuf->op == AND);
  vec<Lit> lits_tmp;
  vec<Lit> lits;

  int var_tmp;
  int var = lhs >> 1;
  while (var >= solver->nVars()) solver->newVar();
  assert(!solver->isEliminated(var));

  list_ptuf_e *it = ptuf->lp->head;
  
  while(it != NULL) {
    assert(it->ptuf->op == VAR);
    lits_tmp.clear();

    assert(it->ptuf->lit > 1);
    var_tmp = it->ptuf->lit >> 1;
    while (var_tmp >= solver->nVars()) solver->newVar();
    assert(var_tmp < solver->nVars());
    assert(!solver->isEliminated(var_tmp));
    lits_tmp.push( aiger_sign(it->ptuf->lit) ? ~mkLit(var_tmp) : mkLit(var_tmp));
    lits_tmp.push( aiger_sign(lhs) ? mkLit(var) : ~mkLit(var));
    solver->addClause_(lits_tmp);

    lits.push( aiger_sign(it->ptuf->lit) ? mkLit(var_tmp) : ~mkLit(var_tmp));

    it = it->next;
  }
  
  lits.push( aiger_sign(lhs) ? ~mkLit(var) : mkLit(var));
  solver->addClause_(lits);
}



std::pair<std::list<std::list<unsigned>>, std::list<std::list<unsigned>>> pt2cl(prop_tr_un_fl *ptuf) {
  std::list<unsigned> li;
  std::list<std::list<unsigned>> lli_f;
  std::list<std::list<unsigned>> lli_s;
  
  if (ptuf->op == VAR) {
    li.push_back(ptuf->lit);
    lli_f.push_back(li);
    lli_s.push_back(li);

  } else if (ptuf->lp->size == 1) {
    assert(0);
  } else {
    assert(ptuf->lp->size > 1);
    std::list<std::pair<std::list<std::list<unsigned>>, std::list<std::list<unsigned>>>> lli_pl;
    
    list_ptuf_e *it = ptuf->lp->head;
    
    while(it != NULL) {
      std::pair<std::list<std::list<unsigned>>, std::list<std::list<unsigned>>> lli_p = pt2cl(it->ptuf);
      lli_pl.push_back(lli_p);
      
      it = it->next;
    }
    
    std::list<std::pair<std::list<std::list<unsigned>>, std::list<std::list<unsigned>>>>::iterator it_llip;
    std::list<std::list<unsigned>>::iterator it_lli;
    std::list<std::list<unsigned>>::iterator it_lli2;
    std::list<unsigned>::iterator it_li;

    if (ptuf->op == AND) {
      
      for( it_llip = lli_pl.begin(); it_llip != lli_pl.end(); it_llip++) {
	//std::list<std::list<unsigned>> lli_op_f = it_llip->first;
	for (it_lli = it_llip->first.begin(); it_lli != it_llip->first.end(); it_lli++) {
	  lli_f.push_back(*it_lli);
	}
      }
      
      std::list<std::list<unsigned>> lli_op1_s = lli_pl.front().second;
      lli_pl.pop_front();
      while (!lli_pl.empty()) {
	std::list<std::list<unsigned>> lli_op2_s = lli_pl.front().second;
	lli_pl.pop_front();
	for (it_lli = lli_op1_s.begin(); it_lli != lli_op1_s.end(); it_lli++) {
	  for (it_lli2 = lli_op2_s.begin(); it_lli2 != lli_op2_s.end(); it_lli2++) {
	    li.clear();
	    
	    for (it_li = it_lli->begin(); it_li != it_lli->end(); it_li++) {
	      li.push_back(*it_li);
	    }
	    for (it_li = it_lli2->begin(); it_li != it_lli2->end(); it_li++) {
	      li.push_back(*it_li);
	    }
	    lli_s.push_back(li);
	  }
	}
	lli_op1_s = lli_s;
	lli_s.clear();
      }
      lli_s = lli_op1_s;
      
    } else {

      for( it_llip = lli_pl.begin(); it_llip != lli_pl.end(); it_llip++) {
	//std::list<std::list<unsigned>> lli_op_s = it_llip->second;
	for (it_lli = it_llip->second.begin(); it_lli != it_llip->second.end(); it_lli++) {
	  lli_s.push_back(*it_lli);
	}

      }
      
      std::list<std::list<unsigned>> lli_op1_f = lli_pl.front().first;
      lli_pl.pop_front();
      while (!lli_pl.empty()) {
	std::list<std::list<unsigned>> lli_op2_f = lli_pl.front().first;
	lli_pl.pop_front();
	for (it_lli = lli_op1_f.begin(); it_lli != lli_op1_f.end(); it_lli++) {
	  for (it_lli2 = lli_op2_f.begin(); it_lli2 != lli_op2_f.end(); it_lli2++) {
	    li.clear();
	    
	    for (it_li = it_lli->begin(); it_li != it_lli->end(); it_li++) {
	      li.push_back(*it_li);
	    }
	    for (it_li = it_lli2->begin(); it_li != it_lli2->end(); it_li++) {
	      li.push_back(*it_li);
	    }
	    lli_f.push_back(li);
	  }
	}
	lli_op1_f = lli_f;
	lli_f.clear();
      }
      lli_f = lli_op1_f;
      
    }
  }
  return std::pair<std::list<std::list<unsigned>>, std::list<std::list<unsigned>>>(lli_f, lli_s);
}




void and2clauses_coi_norec(prop_tr_un_fl *ptuf, std::stack<ands_def*> &stack_ad) {
  list_ptuf_e *it = ptuf->lp->head;
  while(it != NULL) {
    if (it->ptuf->op == VAR) {
      if (it->ptuf->lit_ref != NULL) {
	if (!it->ptuf->lit_ref->added2solver) {
	  stack_ad.push(it->ptuf->lit_ref);
	  it->ptuf->lit_ref->added2solver = 1;
	}
      }

    } else {
      and2clauses_coi_norec(it->ptuf, stack_ad);
    }
    
    it = it->next;
  }
}

int and2clauses_coi_norec(SimpSolver* solver, ands_def *ad_) {
  std::stack<ands_def*> stack_ad;
  stack_ad.push(ad_);
  ands_def *ad;
  int stats_coi_solv = 0;

  while(!stack_ad.empty()) {
    ad = stack_ad.top();
    stack_ad.pop();
    stats_coi_solv++;

    list_ptuf_e *it = ad->rhs_tr_fl->lp->head;
    while(it != NULL) {
      if (it->ptuf->op == VAR) {
	if (it->ptuf->lit_ref != NULL) {
	  if (!it->ptuf->lit_ref->added2solver) {
	    stack_ad.push(it->ptuf->lit_ref);
	    it->ptuf->lit_ref->added2solver = 1;
	  }
	}

      } else {
	and2clauses_coi_norec(it->ptuf, stack_ad);
      }
    
      it = it->next;
    }

    // Add to solver
    if (ad->rhs_tr_fl->op == AND && ad->rhs_tr_fl->lp->tail->ptuf->op == VAR) {
      // Optimized big and def to clauses
      bigAnd2Solver(solver, ad->lhs, ad->rhs_tr_fl);
      
    } else {
      std::pair<std::list<std::list<unsigned>>, std::list<std::list<unsigned>>> lli_p =
	pt2cl(ad->rhs_tr_fl);
  
      std::list<std::list<unsigned>>::iterator it_lli;
  
      for (it_lli = lli_p.first.begin(); it_lli != lli_p.first.end(); it_lli++) {
	cl_lhs2rhs(solver, ad->lhs, *it_lli);
      }
  
      for (it_lli = lli_p.second.begin(); it_lli != lli_p.second.end(); it_lli++) {
	cl_rhs2lhs(solver, ad->lhs, *it_lli);
      }
    }
  }
  return stats_coi_solv;
}


int stats_coi = 0;
int stats_total_def = 0;
void ands2clauses_coi(SimpSolver* solver, std::vector<dll_list<ands_def>*> &list_ands, std::vector<idef*> &mapdef, std::map<unsigned, ands_def*> &map_ands, int bad_lit, int time, bool verbose) {
  std::map<unsigned, ands_def*>::iterator it_lit_ref;
  unsigned var_po = mapdef[aiger_lit2var(bad_lit)]->lhs_ts[time];

  if (var_po > 1) {

    it_lit_ref = map_ands.find(aiger_lit2var(var_po));
    
    assert(it_lit_ref != map_ands.end()); // Might be false if po <=> input
    assert(it_lit_ref->second->cst == -1);
    if (it_lit_ref->second->cst == -1) {
      if (!it_lit_ref->second->added2solver) {
	stats_coi += and2clauses_coi_norec(solver, it_lit_ref->second);
	it_lit_ref->second->added2solver = 1;
      }
    }
  }
  
  if (verbose) {
    stats_total_def += dll_size(list_ands[time]);
    printf("[MC]  COI : %d/%d\n", stats_coi, stats_total_def);
  }
}







void ands2clauses(SimpSolver* solver, std::vector<dll_list<ands_def>*> &list_ands, int time, bool elim_ll) {
  ands_def* tmp_def;
  dll_elem<ands_def> *tmp_e;

  // Ands def
  tmp_e = list_ands[time]->head;
  while(tmp_e != NULL) {
    tmp_def = tmp_e->def;
    
    //assert(!aiger_sign(tmp_def->lhs));
    assert(tmp_def->lhs > 1);

    //print_ad_ptuf(tmp_def);

    if (tmp_def->rhs_tr_fl->op == AND && tmp_def->rhs_tr_fl->lp->tail->ptuf->op == VAR) {
      // Optimized big and def to clauses
      bigAnd2Solver(solver, tmp_def->lhs, tmp_def->rhs_tr_fl);
      
     } else {
      std::pair<std::list<std::list<unsigned>>, std::list<std::list<unsigned>>> lli_p =
	pt2cl(tmp_def->rhs_tr_fl);

      std::list<std::list<unsigned>>::iterator it_lli;

      for (it_lli = lli_p.first.begin(); it_lli != lli_p.first.end(); it_lli++) {
	cl_lhs2rhs(solver, tmp_def->lhs, *it_lli);
      }

      for (it_lli = lli_p.second.begin(); it_lli != lli_p.second.end(); it_lli++) {
	cl_rhs2lhs(solver, tmp_def->lhs, *it_lli);
      }
    }

    if (elim_ll) {
      //delete tmp_def->rhs_tr;
      //destroy_ptuf(tmp_def->rhs_tr_fl);
      tmp_def->funcA_cst.clear();
      tmp_def->funcO_cst.clear();
      tmp_def->func_eq.clear();
      tmp_def->refBy.clear();
    }
    tmp_e = tmp_e->next;
  }
}

int saved_index_unit  = 0;
int stats_import_unit = 0;

bool import_unit_from_solver(SimpSolver* solver, std::map<unsigned, ands_def*> &map_ands, bool verbose) {
  std::map<unsigned, ands_def*>::iterator it_lit_ref;
  int old_import_nb = stats_import_unit;
  for (int i = saved_index_unit; i < solver->unit_clauses_tr.size(); i++) {
    it_lit_ref = map_ands.find(aiger_lit2var(solver->unit_clauses_tr[i].l_tr.x));
    if (it_lit_ref == map_ands.end()) {
      // TODO: unit clause is an input or a latch.. do nothing ?
      
    } else {
      if (propagate_def_cst(solver, it_lit_ref->second,
			    aiger_sign(it_lit_ref->second->lhs) ?
			    aiger_sign(solver->unit_clauses_tr[i].l_tr.x) :
			    aiger_not(aiger_sign(solver->unit_clauses_tr[i].l_tr.x)),
			    0))
	stats_import_unit++;
    }
  }
  
  saved_index_unit = solver->unit_clauses_tr.size();
  
  if (verbose) 
    printf("[solver]  Propagation from unit learnt clauses : %d/%d (Unit : %lu | Propa : %d)\n",
	   stats_import_unit, solver->unit_clauses_tr.size(),
	   solver->nbUn, solver->stats_propagation);

  //assert(solver->unit_clauses_tr.size() >= solver->nbUn);
  if (old_import_nb < stats_import_unit)
    return 1;
  return 0;
}

inline unsigned hash_2unsigned(unsigned a, unsigned b) {
  return a * 111 + b;
}

int saved_index_bin  = 0;
int stats_import_bin = 0;
std::unordered_multimap<unsigned, std::pair<unsigned, unsigned>> hash_binCL;

bool import_binary_from_solver(SimpSolver* solver, std::map<unsigned, ands_def*> &map_ands, bool verbose) {
  
  // printf("learnts : %d | originals : %d\n", solver->learnts.size(), solver->clauses.size());
  // solver->removeSatisfied(solver->learnts);
  // solver->removeSatisfied(solver->clauses);
  // printf("learnts : %d | originals : %d\n", solver->learnts.size(), solver->clauses.size());
  // printf("TEST %d\n", solver->decisionLevel());
  // for (int i = 0; i < solver->learnts.size(); i++) {
  //   Clause& c = solver->ca[solver->learnts[i]];
  //   int cpt = 0;
  //   bin_tr tmp_bt;
  //   Lit lc0;
  //   Lit lc1;
  //   for (int j = 0; j < c.size(); j++) {
  //     if (solver->value(c[j]) == l_Undef) {
  // 	if (cpt == 0)
  // 	  lc0 = c[j];
  // 	if (cpt == 1)
  // 	  lc1 = c[j];
  // 	cpt++;
  //     }
  //   }

  //   if (c.size() > 2 && cpt == 2) {
  //     // solver->printClause(solver->learnts[i]);
  //     // printf("\n");

  //     tmp_bt.l1_tr = lc0 < lc1 ? lc0 : lc1;
  //     tmp_bt.l2_tr = lc0 < lc1 ? lc1 : lc0;
  //     // Check if it is already in the vector
  //     bool already_in = 0;
  //     for(int i = 0; i < solver->bin_clauses_tr.size() && !already_in; i++) {
  // 	if (tmp_bt.l1_tr.x == solver->bin_clauses_tr[i].l1_tr.x &&
  // 	    tmp_bt.l2_tr.x == solver->bin_clauses_tr[i].l2_tr.x)
  // 	  already_in = 1;
  //     }
  //     if (!already_in) {
  // 	solver->bin_clauses_tr.push(tmp_bt);
  //     }

  //   }
  // }
  // for (int i = 0; i < solver->clauses.size(); i++) {
  //   Clause& c = solver->ca[solver->clauses[i]];
  //   int cpt = 0;
  //   bin_tr tmp_bt;
  //   Lit lc0;
  //   Lit lc1;
  //   for (int j = 0; j < c.size(); j++) {
  //     if (solver->value(c[j]) == l_Undef) {
  // 	if (cpt == 0)
  // 	  lc0 = c[j];
  // 	if (cpt == 1)
  // 	  lc1 = c[j];
  // 	cpt++;
  //     }
  //   }

  //   if (c.size() > 2 && cpt == 2) {
  //     // solver->printClause(solver->clauses[i]);
  //     // printf("\n");

  //     tmp_bt.l1_tr = lc0 < lc1 ? lc0 : lc1;
  //     tmp_bt.l2_tr = lc0 < lc1 ? lc1 : lc0;
  //     // Check if it is already in the vector
  //     bool already_in = 0;
  //     for(int i = 0; i < solver->bin_clauses_tr.size() && !already_in; i++) {
  // 	if (tmp_bt.l1_tr.x == solver->bin_clauses_tr[i].l1_tr.x &&
  // 	    tmp_bt.l2_tr.x == solver->bin_clauses_tr[i].l2_tr.x)
  // 	  already_in = 1;
  //     }
  //     if (!already_in) {
  // 	solver->bin_clauses_tr.push(tmp_bt);
  //     }
  //   }
  // }
  // printf("E TEST\n");
  

  std::map<unsigned, ands_def*>::iterator it_lit_ref;
  std::map<unsigned, ands_def*>::iterator it_lit_ref2;
  std::set<std::pair<int, int>> indexes_set;
  std::unordered_multimap<unsigned, std::pair<unsigned, unsigned>>::iterator it_binCL;
  bool found_equiv;

  for(int i = saved_index_bin; i < solver->bin_clauses_tr.size(); i++) {
    std::pair<std::unordered_multimap<unsigned, std::pair<unsigned, unsigned>>::iterator,
	      std::unordered_multimap<unsigned, std::pair<unsigned, unsigned>>::iterator> range;
    range = hash_binCL.equal_range(hash_2unsigned(solver->bin_clauses_tr[i].l1_tr.x,
						  solver->bin_clauses_tr[i].l2_tr.x));

    found_equiv = 0;

    if(range.first != hash_binCL.end()) { 
      // Conflict or equivalent!
      for(it_binCL = range.first; it_binCL != range.second; it_binCL++) {
	
	if (it_binCL->second.first  == solver->bin_clauses_tr[i].l1_tr.x &&
	    it_binCL->second.second == solver->bin_clauses_tr[i].l2_tr.x) {

	  assert(solver->bin_clauses_tr[i].l1_tr.x < solver->bin_clauses_tr[i].l2_tr.x);
	  
	  if (aiger_sign(solver->bin_clauses_tr[i].l1_tr.x) ==
	      aiger_sign(solver->bin_clauses_tr[i].l2_tr.x)) {
	    // ~a  # ~b
	    //  a  #  b
	    // OR
	    //  a  #  b
	    // ~a  # ~b
	    it_lit_ref  = map_ands.find(aiger_lit2var(solver->bin_clauses_tr[i].l1_tr.x));
	    it_lit_ref2 = map_ands.find(aiger_lit2var(solver->bin_clauses_tr[i].l2_tr.x));

	    if (it_lit_ref != map_ands.end() && it_lit_ref2 != map_ands.end()) {
	      // a and b are AND defs 
	      if (it_lit_ref->second->cycle == it_lit_ref2->second->cycle) {
		//if (it_lit_ref->second->pos > it_lit_ref2->second->pos) {
		if (it_lit_ref->second->lhs < it_lit_ref2->second->lhs) {
		  // => b = ~a
		  if (propagate_def_alias(solver, map_ands, it_lit_ref2->second,
					  aiger_sign(it_lit_ref2->second->lhs) ?
					  aiger_strip(solver->bin_clauses_tr[i].l1_tr.x) :
					  aiger_not(aiger_strip(solver->bin_clauses_tr[i].l1_tr.x)), 0))
		    stats_import_bin++;
		} else {
		  // => a = ~b
		  if (propagate_def_alias(solver, map_ands, it_lit_ref->second,
					  aiger_sign(it_lit_ref->second->lhs) ?
					  aiger_strip(solver->bin_clauses_tr[i].l2_tr.x) :
					  aiger_not(aiger_strip(solver->bin_clauses_tr[i].l2_tr.x)), 0))
		    stats_import_bin++;
		}
	      } else if (it_lit_ref->second->cycle > it_lit_ref2->second->cycle) {
		// => a = ~b
		if (propagate_def_alias(solver, map_ands, it_lit_ref->second,
					aiger_sign(it_lit_ref->second->lhs) ?
					aiger_strip(solver->bin_clauses_tr[i].l2_tr.x) :
					aiger_not(aiger_strip(solver->bin_clauses_tr[i].l2_tr.x)), 0))
		  stats_import_bin++;
	      } else {
		// => b = ~a
		if (propagate_def_alias(solver, map_ands, it_lit_ref2->second,
					aiger_sign(it_lit_ref2->second->lhs) ?
					aiger_strip(solver->bin_clauses_tr[i].l1_tr.x) :
					aiger_not(aiger_strip(solver->bin_clauses_tr[i].l1_tr.x)), 0))
		  stats_import_bin++;
	      }
	    } else if (it_lit_ref != map_ands.end()) {
	      // a is an AND def, b is an input or a latch (for now)

	    } else if (it_lit_ref2 != map_ands.end()) {
	      // b is an AND def, a is an input or a latch (for now)

	    } else {
	      // a and are inputs or latches (for now)

	    } 

	  } else {
	    // ~a  #  b
	    //  a  # ~b
	    // OR
	    //  a  # ~b
	    // ~a  #  b
	    it_lit_ref  = map_ands.find(aiger_lit2var(solver->bin_clauses_tr[i].l1_tr.x));
	    it_lit_ref2 = map_ands.find(aiger_lit2var(solver->bin_clauses_tr[i].l2_tr.x));

	    if (it_lit_ref != map_ands.end() && it_lit_ref2 != map_ands.end()) {
	      // a and b are AND defs
	      if (it_lit_ref->second->cycle == it_lit_ref2->second->cycle) {
		//if (it_lit_ref->second->pos > it_lit_ref2->second->pos) {
		if (it_lit_ref->second->lhs < it_lit_ref2->second->lhs) {
		  // => b = a
		  if (propagate_def_alias(solver, map_ands, it_lit_ref2->second,
					  aiger_sign(it_lit_ref2->second->lhs) ?
					  aiger_not(aiger_strip(solver->bin_clauses_tr[i].l1_tr.x)) :
					  aiger_strip(solver->bin_clauses_tr[i].l1_tr.x), 0))
		    stats_import_bin++;
		} else {
		  // => a = b
		  if (propagate_def_alias(solver, map_ands, it_lit_ref->second,
					  aiger_sign(it_lit_ref->second->lhs) ?
					  aiger_not(aiger_strip(solver->bin_clauses_tr[i].l2_tr.x)) :
					  aiger_strip(solver->bin_clauses_tr[i].l2_tr.x), 0))
		    stats_import_bin++;
		}
	      } else if (it_lit_ref->second->cycle > it_lit_ref2->second->cycle) {
		// => a = b
		if (propagate_def_alias(solver, map_ands, it_lit_ref->second,
					aiger_sign(it_lit_ref->second->lhs) ?
					aiger_not(aiger_strip(solver->bin_clauses_tr[i].l2_tr.x)) :
					aiger_strip(solver->bin_clauses_tr[i].l2_tr.x), 0))
		  stats_import_bin++;
	      } else {
		// => b = a
		if (propagate_def_alias(solver, map_ands, it_lit_ref2->second,
					aiger_sign(it_lit_ref2->second->lhs) ?
					aiger_not(aiger_strip(solver->bin_clauses_tr[i].l1_tr.x)) :
					aiger_strip(solver->bin_clauses_tr[i].l1_tr.x), 0))
		  stats_import_bin++;
	      }


	    } else if (it_lit_ref != map_ands.end()) {
	      // a is an AND def, b is an input or a latch (for now)

	    } else if (it_lit_ref2 != map_ands.end()) {
	      // b is an AND def, a is an input or a latch (for now)

	    } else {
	      // a and are inputs or latches (for now)

	    } 
	  } 
	  found_equiv = 1;

	}
      }
    }

    if (!found_equiv) {
      hash_binCL.insert
  	(std::pair<unsigned, std::pair<unsigned, unsigned>>
	 (hash_2unsigned(aiger_not(solver->bin_clauses_tr[i].l1_tr.x),
			 aiger_not(solver->bin_clauses_tr[i].l2_tr.x)),
	  std::pair<unsigned, unsigned>(aiger_not(solver->bin_clauses_tr[i].l1_tr.x),
					aiger_not(solver->bin_clauses_tr[i].l2_tr.x))));
    }
  }

  saved_index_bin = solver->bin_clauses_tr.size();

  if (verbose)
    printf("[solver]  Propagation from binary clauses : %d/%d \n",
	   stats_import_bin, solver->bin_clauses_tr.size());

  return 0;
}

// bool import_binary_from_solver_bwd(bool verbose) {
//   std::map<unsigned, ands_def*>::iterator it_lit_ref;
//   std::map<unsigned, ands_def*>::iterator it_lit_ref2;
//   std::set<std::pair<int, int>> indexes_set;
//   std::unordered_multimap<unsigned, std::pair<unsigned, unsigned>>::iterator it_binCL;
//   bool found_equiv;

//   for(int i = saved_index_bin; i < solver->bin_clauses_tr.size(); i++) {
//     std::pair<std::unordered_multimap<unsigned, std::pair<unsigned, unsigned>>::iterator,
// 	      std::unordered_multimap<unsigned, std::pair<unsigned, unsigned>>::iterator> range;
//     range = hash_binCL.equal_range(hash_2unsigned(solver->bin_clauses_tr[i].l1_tr.x,
// 						  solver->bin_clauses_tr[i].l2_tr.x));

//     found_equiv = 0;

//     if(range.first != hash_binCL.end()) { 
//       // Conflict or equivalent!
//       for(it_binCL = range.first; it_binCL != range.second; it_binCL++) {
	
// 	if (it_binCL->second.first  == solver->bin_clauses_tr[i].l1_tr.x &&
// 	    it_binCL->second.second == solver->bin_clauses_tr[i].l2_tr.x) {

// 	  assert(solver->bin_clauses_tr[i].l1_tr.x < solver->bin_clauses_tr[i].l2_tr.x);
	  
// 	  if (aiger_sign(solver->bin_clauses_tr[i].l1_tr.x) ==
// 	      aiger_sign(solver->bin_clauses_tr[i].l2_tr.x)) {
// 	    // ~a  # ~b
// 	    //  a  #  b
// 	    // OR
// 	    //  a  #  b
// 	    // ~a  # ~b
// 	    it_lit_ref  = map_ands.find(aiger_lit2var(solver->bin_clauses_tr[i].l1_tr.x));
// 	    it_lit_ref2 = map_ands.find(aiger_lit2var(solver->bin_clauses_tr[i].l2_tr.x));

// 	    if (it_lit_ref != map_ands.end() && it_lit_ref2 != map_ands.end()) {
// 	      // a and b are AND defs 
// 	      if (it_lit_ref->second->cycle == it_lit_ref2->second->cycle) {
// 		//if (it_lit_ref->second->pos < it_lit_ref2->second->pos) {
// 		if (it_lit_ref->second->lhs > it_lit_ref2->second->lhs) {
// 		  // => b = ~a
// 		  if (propagate_def_alias(it_lit_ref2->second,
// 					  aiger_sign(it_lit_ref2->second->lhs) ?
// 					  aiger_strip(solver->bin_clauses_tr[i].l1_tr.x) :
// 					  aiger_not(aiger_strip(solver->bin_clauses_tr[i].l1_tr.x)), 0))
// 		    stats_import_bin++;
// 		} else {
// 		  // => a = ~b
// 		  if (propagate_def_alias(it_lit_ref->second,
// 					  aiger_sign(it_lit_ref->second->lhs) ?
// 					  aiger_strip(solver->bin_clauses_tr[i].l2_tr.x) :
// 					  aiger_not(aiger_strip(solver->bin_clauses_tr[i].l2_tr.x)), 0))
// 		    stats_import_bin++;
// 		}
// 	      } else if (it_lit_ref->second->cycle < it_lit_ref2->second->cycle) {
// 		// => a = ~b
// 		if (propagate_def_alias(it_lit_ref->second,
// 					aiger_sign(it_lit_ref->second->lhs) ?
// 					aiger_strip(solver->bin_clauses_tr[i].l2_tr.x) :
// 					aiger_not(aiger_strip(solver->bin_clauses_tr[i].l2_tr.x)), 0))
// 		  stats_import_bin++;
// 	      } else {
// 		// => b = ~a
// 		if (propagate_def_alias(it_lit_ref2->second,
// 					aiger_sign(it_lit_ref2->second->lhs) ?
// 					aiger_strip(solver->bin_clauses_tr[i].l1_tr.x) :
// 					aiger_not(aiger_strip(solver->bin_clauses_tr[i].l1_tr.x)), 0))
// 		  stats_import_bin++;
// 	      }
// 	    } else if (it_lit_ref != map_ands.end()) {
// 	      // a is an AND def, b is an input or a latch (for now)
// 	      assert(0);

// 	    } else if (it_lit_ref2 != map_ands.end()) {
// 	      // b is an AND def, a is an input or a latch (for now)
// 	      assert(0);

// 	    } else {
// 	      // a and are inputs or latches (for now)

// 	    } 

// 	  } else {
// 	    // ~a  #  b
// 	    //  a  # ~b
// 	    // OR
// 	    //  a  # ~b
// 	    // ~a  #  b
// 	    it_lit_ref  = map_ands.find(aiger_lit2var(solver->bin_clauses_tr[i].l1_tr.x));
// 	    it_lit_ref2 = map_ands.find(aiger_lit2var(solver->bin_clauses_tr[i].l2_tr.x));

// 	    if (it_lit_ref != map_ands.end() && it_lit_ref2 != map_ands.end()) {
// 	      // a and b are AND defs
// 	      if (it_lit_ref->second->cycle == it_lit_ref2->second->cycle) {
// 		//if (it_lit_ref->second->pos < it_lit_ref2->second->pos) {
// 		if (it_lit_ref->second->lhs > it_lit_ref2->second->lhs) {
// 		  // => b = a
// 		  if (propagate_def_alias(it_lit_ref2->second,
// 					  aiger_sign(it_lit_ref2->second->lhs) ?
// 					  aiger_not(aiger_strip(solver->bin_clauses_tr[i].l1_tr.x)) :
// 					  aiger_strip(solver->bin_clauses_tr[i].l1_tr.x), 0))
// 		    stats_import_bin++;
// 		} else {
// 		  // => a = b
// 		  if (propagate_def_alias(it_lit_ref->second,
// 					  aiger_sign(it_lit_ref->second->lhs) ?
// 					  aiger_not(aiger_strip(solver->bin_clauses_tr[i].l2_tr.x)) :
// 					  aiger_strip(solver->bin_clauses_tr[i].l2_tr.x), 0))
// 		    stats_import_bin++;
// 		}
// 	      } else if (it_lit_ref->second->cycle < it_lit_ref2->second->cycle) {
// 		// => a = b
// 		if (propagate_def_alias(it_lit_ref->second,
// 					aiger_sign(it_lit_ref->second->lhs) ?
// 					aiger_not(aiger_strip(solver->bin_clauses_tr[i].l2_tr.x)) :
// 					aiger_strip(solver->bin_clauses_tr[i].l2_tr.x), 0))
// 		  stats_import_bin++;
// 	      } else {
// 		// => b = a
// 		if (propagate_def_alias(it_lit_ref2->second,
// 					aiger_sign(it_lit_ref2->second->lhs) ?
// 					aiger_not(aiger_strip(solver->bin_clauses_tr[i].l1_tr.x)) :
// 					aiger_strip(solver->bin_clauses_tr[i].l1_tr.x), 0))
// 		  stats_import_bin++;
// 	      }


// 	    } else if (it_lit_ref != map_ands.end()) {
// 	      // a is an AND def, b is an input or a latch (for now)
// 	      assert(0);

// 	    } else if (it_lit_ref2 != map_ands.end()) {
// 	      // b is an AND def, a is an input or a latch (for now)
// 	      assert(0);

// 	    } else {
// 	      // a and are inputs or latches (for now)

// 	    } 
// 	  } 
// 	  found_equiv = 1;

// 	}
//       }
//     }

//     if (!found_equiv) {
//       hash_binCL.insert
//   	(std::pair<unsigned, std::pair<unsigned, unsigned>>
// 	 (hash_2unsigned(aiger_not(solver->bin_clauses_tr[i].l1_tr.x),
// 			 aiger_not(solver->bin_clauses_tr[i].l2_tr.x)),
// 	  std::pair<unsigned, unsigned>(aiger_not(solver->bin_clauses_tr[i].l1_tr.x),
// 					aiger_not(solver->bin_clauses_tr[i].l2_tr.x))));
//     }
//   }

//   saved_index_bin = solver->bin_clauses_tr.size();

//   if (verbose)
//     printf("[solver]  Propagation from binary clauses : %d/%d \n",
// 	   stats_import_bin, solver->bin_clauses_tr.size());

//   return 0;
// }
