/***************************************************************************
Copyright (c) 2016-2017, Guillaume Baud-Berthier, SafeRiver/LaBRI

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
IN THE SOFTWARE.
***************************************************************************/

#include "bmc.h"

using namespace Glucose;

float enco_cumul_time = 0;

dll_list<idef>* dll_ands_bmc;
dll_list<idef>* dll_inputs_bmc;
dll_list<idef>* dll_latches_bmc;

SimpSolver* solver_bmc;
vec<Lit>    assumptions_bmc;

std::map<unsigned, ands_def*> map_ands_bmc;
std::vector<idef*> mapdef_bmc;
std::vector<dll_list<ands_def>*> list_ands_bmc;

unsigned vars_bmc = 0;

/* ========================================================================== */
/*                           BMC algorithm functions                          */
/* ========================================================================== */

int bmc(int start, int stop, aiger *model, bool elim_hl, bool inprep_hl,
	bool cex, int nb_latches, bool verbose_, int *status) {
  int i, j;
  clock_t clk;

  if (verbose_) clk = clock();

  if (verbose_) printf("Mem: %d MB\n", get_mem());

  dll_inputs_bmc = new dll_list<idef>();
  dll_latches_bmc = new dll_list<idef>();
  dll_ands_bmc = new dll_list<idef>();
  unsigned bad_def = init_model(model, dll_ands_bmc, dll_inputs_bmc, dll_latches_bmc, mapdef_bmc, /*elim_ll*/0, verbose_);
  //aiger_reset(model);
  solver_bmc = new SimpSolver();
  init_solver(solver_bmc, /*elim_ll*/0, verbose_);
  solver_bmc->reduceDB_type = 0;

  if (elim_hl)
    hl_ve(mapdef_bmc, dll_ands_bmc, verbose_);

  if (verbose_) {
    clk = clock() - clk;
    enco_cumul_time += (float)(clk)/(float)(CLOCKS_PER_SEC);
  }

  for(i = 0; i <= stop; i++) {

    if (verbose_) printf("Mem: %d MB\n", get_mem());
    if (verbose_) printf("[bmc]  Encoding %d..\n", i);
    if (verbose_) clk = clock();


    if (inprep_hl /*&& !elim_ll*/) {
      bool simp_prev = import_unit_from_solver(solver_bmc, map_ands_bmc, verbose_);
      simp_prev |= import_binary_from_solver(solver_bmc, map_ands_bmc, verbose_);
      if (simp_prev) {
	clear_global_hash();
	for (int k = 0; k < i; k++) {
	  simp_ands(solver_bmc, list_ands_bmc, mapdef_bmc, map_ands_bmc, k, bad_def, 0, verbose_, /*elim_ll*/0);
	}
      }
    } else if (inprep_hl && /*elim_ll*/0 && verbose_) {
      printf("[bmc]  Warning : High level inprocessing is disabled because of varelim!\n");
    }

    list_ands_bmc.push_back(unroll(dll_ands_bmc, dll_inputs_bmc, dll_latches_bmc, mapdef_bmc, map_ands_bmc, &vars_bmc, i));
    
    iter_flatten_ptu(list_ands_bmc, i);

    simp_ands(solver_bmc, list_ands_bmc, mapdef_bmc, map_ands_bmc, i, bad_def, 0, verbose_, /*elim_ll*/0);

    // if (elim_ll) {
    //   if (verbose_) printf("[bmc]  Warning : k-COI is disabled because of varelim!\n");
    //   ands2clauses(solver_bmc, list_ands_bmc, i, elim_ll);
    // } else {
    ands2clauses_coi(solver_bmc, list_ands_bmc, mapdef_bmc, map_ands_bmc, bad_def, i, verbose_);
    //}

    if (verbose_) {
      clk = clock() - clk;
      enco_cumul_time += (float)(clk)/(float)(CLOCKS_PER_SEC);
    }
    
    if (verbose_) printf("[bmc]  Solving..\n");

    if ((!mapdef_bmc[aiger_lit2var(bad_def)]->lhs_ts[i] &&
	 !aiger_sign(bad_def)) ||
	(mapdef_bmc[aiger_lit2var(bad_def)]->lhs_ts[i] == 1 &&
	 aiger_sign(bad_def))) {
      
      if (verbose_) printf("[bmc]  UNSAT (Solved by simplification)\n");
      printf("u%d\n", i);
      fflush(stdout);

      if (!dll_size(dll_latches_bmc)) {
	*status = 0;
	break;
      }

    } else if ((!mapdef_bmc[aiger_lit2var(bad_def)]->lhs_ts[i] &&
		aiger_sign(bad_def)) ||
	       (mapdef_bmc[aiger_lit2var(bad_def)]->lhs_ts[i] == 1 &&
		!aiger_sign(bad_def))) {

      if (verbose_) printf("[bmc]  CEX (Solved by simplification)\n");
      *status = 1;
      break;
	
    } else {

      if (verbose_) clk = clock();

      unsigned var_po = mapdef_bmc[aiger_lit2var(bad_def)]->lhs_ts[i];
      assert(var_po > 1);
      std::map<unsigned, ands_def*>::iterator it_lit_ref = map_ands_bmc.find(aiger_lit2var(var_po));

      assert(it_lit_ref != map_ands_bmc.end()); // Might be false if po <=> input
      assert(it_lit_ref->second->cst == -1);
      
      //int lit_bad = addPO(solver_bmc, mapdef_bmc, &vars_bmc, bad_def, i);
      int lit_bad = mapdef_bmc[aiger_lit2var(bad_def)]->lhs_ts[i];
      if (aiger_sign(lit_bad))
	lit_bad = aiger_sign(bad_def) ?  (lit_bad >> 1) : -(lit_bad >> 1);
      else
	lit_bad = aiger_sign(bad_def) ? -(lit_bad >> 1) :  (lit_bad >> 1);
      
      assume(solver_bmc, assumptions_bmc, lit_bad);
      //assume(solver_bmc, assumptions_bmc, mapdef_bmc[0]->lhs_ts.back() >> 1);



      /* Freeze latch variables */
      // if (elim_ll) {
      // 	dll_elem<idef> *tmp_e = dll_latches_bmc->head;
      // 	idef* tmp_def;
      // 	while(tmp_e != NULL) {
      // 	  tmp_def = tmp_e->def;

      // 	  assert(tmp_def->rhs.size() == 1);
      // 	  if (tmp_def->rhs.front() > 1) {
      // 	    int next_lit = (mapdef_bmc[aiger_lit2var(tmp_def->rhs.front())]->lhs_ts.back());

      // 	    /* Temporary fix.. */
      // 	    it_lit_ref = map_ands_bmc.find(aiger_lit2var(next_lit));
      // 	    if (it_lit_ref != map_ands_bmc.end()) {
      // 	      if (it_lit_ref->second->cst != -1) {
      // 		next_lit = it_lit_ref->second->cst;
      // 		if (it_lit_ref->second->cst_ref != NULL)
      // 		  assert(it_lit_ref->second->cst_ref->cst == -1); // Could be false.. [TODO]
      // 	      }
      // 	    }
      // 	    /*******/

      // 	    if (next_lit > 1) {
      // 	      solver_bmc->frozen[next_lit >> 1] = true;
      // 	    }
      // 	  }

      // 	  tmp_e = tmp_e->next;
      // 	}
      // }
      /**************************/

      if (verbose_) {
	clk = clock() - clk;
	enco_cumul_time += (float)(clk)/(float)(CLOCKS_PER_SEC);
      }
      
      lbool res_check;
      res_check = check_sat(solver_bmc, assumptions_bmc, verbose_);
      if (res_check == l_True) {
	*status = 1;
	break;
      } else if (res_check == l_Undef) {
	assert(0);
      }	


      printf("u%d\n", i);
      fflush(stdout);

      if (!dll_size(dll_latches_bmc)) {
	*status = 0;
	break;
      }      
      
      /* Unfreeze latch variables */
      // if (elim_ll)
      // 	for(int l = 0; l < solver_bmc->frozen.size(); l++)
      // 	  solver_bmc->frozen[l] = false;      
      /**************************/ 
      
      
      //ucl(solver_bmc, -(mapdef_bmc[0]->lhs_ts.back() >> 1));
      ucl(solver_bmc, -lit_bad);
    }
  }

  
  //printf("%d\nb0\n", status);
  //fflush(stdout);

  if (cex && *status == 1) {
    idef* tmp_def;

    for (int i = 0; i < nb_latches; i++)
      printf("0");
    printf("\n");

    for(j = 0; j <= i; j++) {
      dll_elem<idef> *tmp_e = dll_inputs_bmc->head;
      while(tmp_e != NULL) {
	tmp_def = tmp_e->def;

	assert(tmp_def->lhs_ts[j] > 1);

	if (solver_bmc->modelValue(tmp_def->lhs_ts[j] >> 1) == l_True)
	  printf("1");
	else if (solver_bmc->modelValue(tmp_def->lhs_ts[j] >> 1) == l_False)
	  printf("0");
	else
	  printf("x");

	tmp_e = tmp_e->next;
      }
      printf("\n");
    }
    printf(".\n");
  }

  if (verbose_) {
    printf("Encoding %.2fs | Simplification %.2fs | Solving %.2fs\n", enco_cumul_time,
	   solver_bmc->simp_cumul_time, solver_bmc->solv_cumul_time);
    // if (elim_ll)
    //   printf("Vars %d (%d - %d)| Clauses %d\n",
    // 	     solver_bmc->nVars() - solver_bmc->eliminated_vars, solver_bmc->nVars(), solver_bmc->eliminated_vars, solver_bmc->nClauses());
    // else
      printf("Vars %d (%d - %d)| Clauses %d\n",
	     solver_bmc->nVars() - solver_bmc->eliminated_vars, solver_bmc->nVars(), solver_bmc->eliminated_vars, solver_bmc->nClauses());
  }
  fflush(stdout);

  if (verbose_) printf("Mem: %d MB\n", get_mem());

  destroy_model(dll_ands_bmc, dll_inputs_bmc, dll_latches_bmc, list_ands_bmc, mapdef_bmc, map_ands_bmc, verbose_);

  return *status;
}


