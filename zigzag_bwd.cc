/***************************************************************************
Copyright (c) 2016-2017, Guillaume Baud-Berthier, SafeRiver/LaBRI

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
IN THE SOFTWARE.
***************************************************************************/

#include "zigzag_bwd.h"

std::vector<int>   act_init_bwd;

dll_list<idef>* dll_ands_zzb;
dll_list<idef>* dll_inputs_zzb;
dll_list<idef>* dll_latches_zzb;

SimpSolver* solver_zzb;
vec<Lit>    assumptions_zzb;

std::map<unsigned, ands_def*> map_ands_zzb;
std::vector<idef*> mapdef_zzb;
std::vector<dll_list<ands_def>*> list_ands_zzb;

unsigned vars_zzb = 0;

// Simple path constraints on demand
bool simple_path_od_bwd(int time, bool verbose) {
  vec<Lit> lits;
  idef* tmp_def;
  dll_elem<idef> *tmp_e;
  bool same_state;
  bool run_solver_again = 1;
  
  while (run_solver_again) {
    run_solver_again = 0;
     
    for(int i = 0; i < time && !run_solver_again; i++) {
      for(int j = i + 1; j <= time && !run_solver_again; j++) {
	
	same_state = 1;
	
	tmp_e = dll_latches_zzb->head;
	while(tmp_e != NULL && same_state) {
	  tmp_def = tmp_e->def;

	  assert(tmp_def->lhs_ts[i] > 1);
	  assert(tmp_def->lhs_ts[j] > 1);
	  assert(solver_zzb->model[tmp_def->lhs_ts[i] >> 1] != l_Undef);
	  assert(solver_zzb->model[tmp_def->lhs_ts[j] >> 1] != l_Undef);

	  if (solver_zzb->model[tmp_def->lhs_ts[i] >> 1] == l_Undef ||
	      solver_zzb->model[tmp_def->lhs_ts[j] >> 1] == l_Undef ||
	      solver_zzb->model[tmp_def->lhs_ts[i] >> 1] != solver_zzb->model[tmp_def->lhs_ts[j] >> 1] ||
	      ((tmp_def->lhs_ts[i] >> 1 == tmp_def->lhs_ts[j] >> 1) &&
	       (tmp_def->lhs_ts[i] >> 1 != tmp_def->lhs_ts[j])) )
	    same_state = 0;
	  
	  tmp_e = tmp_e->next;
	}
	
	if (same_state) {
	  // Add constraints
	  lits.clear();
	  tmp_e = dll_latches_zzb->head;
	  while(tmp_e != NULL && same_state) {
	    tmp_def = tmp_e->def;

	    assert(tmp_def->lhs_ts[i] > 1);
	    assert(tmp_def->lhs_ts[j] > 1);
	    
	    unsigned new_v = new_var(&vars_zzb) >> 1;

	    while (new_v >= solver_zzb->nVars()) solver_zzb->newVar();

	    assert(aiger_sign(tmp_def->lhs_ts[i]) == aiger_sign(tmp_def->lhs_ts[j]));
	    lits.push(mkLit(new_v));

	    tcl(solver_zzb, -new_v, -(tmp_def->lhs_ts[i] >> 1), -(tmp_def->lhs_ts[j] >> 1));
	    tcl(solver_zzb, -new_v,  (tmp_def->lhs_ts[i] >> 1),  (tmp_def->lhs_ts[j] >> 1));
	    tcl(solver_zzb,  new_v,  (tmp_def->lhs_ts[i] >> 1), -(tmp_def->lhs_ts[j] >> 1));
	    tcl(solver_zzb,  new_v, -(tmp_def->lhs_ts[i] >> 1),  (tmp_def->lhs_ts[j] >> 1));

	    tmp_e = tmp_e->next;
	  }
	  
	  solver_zzb->addClause_(lits);
	  run_solver_again = 1;
	}
      }
    }

    if (run_solver_again) {
      if (check_sat(solver_zzb, assumptions_zzb, verbose) == l_False)
	return 1;
    }

  }

  return 0;
}


#if 0

dll_list<ands_def>* unroll_kind_bwd(int time) {
  dll_list<ands_def> *tmp_list_ands;
  tmp_list_ands = new dll_list<ands_def>();
  dll_init<ands_def>(tmp_list_ands);
  
  idef* tmp_def;
  dll_elem<idef> *tmp_e;

  // Latches
  tmp_e = dll_latches->head;
  while(tmp_e != NULL) {
    tmp_def = tmp_e->def;
    
    tmp_def->lhs_ts.push_back(new_var(&vars_zzb));

    bcl(solver_zzb, -(tmp_def->lhs_ts[time] >> 1), -act_init_bwd[time]);

    tmp_e = tmp_e->next;
  }

  // Inputs
  tmp_e = dll_inputs->head;
  while(tmp_e != NULL) {
    tmp_def = tmp_e->def;

    tmp_def->lhs_ts.push_back(new_var(&vars_zzb));

    tmp_e = tmp_e->next;
  }


  // Ands
  tmp_e = dll_ands->head;
  while(tmp_e != NULL) {
    tmp_def = tmp_e->def;

    prop_tr_un *tpu = unroll_prop_tr(mapdef_zzb, map_ands_zzb, tmp_def->rhs_tr, time, 0);

    assert(tpu->lit == UNDEF_LIT);
    assert(tpu->op != VAR);

    tmp_def->lhs_ts.push_back(new_var(&vars_zzb));

    ands_def *tmp_ands = new ands_def(tmp_def->lhs_ts[time], tpu, tmp_def, time);

    map_ands.insert(std::pair<unsigned, ands_def*>(aiger_lit2var(tmp_ands->lhs), tmp_ands));
    
    tmp_ands->dllade = dll_insert_last(tmp_list_ands, aiger_lit2var(tmp_def->lhs_ts[time]), tmp_ands);

    tmp_e = tmp_e->next;
  }

  // Add relation constraints
  tmp_e = dll_latches->head;
  while(tmp_e != NULL) {
    tmp_def = tmp_e->def;
    
    int lit_latch = tmp_def->lhs_ts[time-1];
    if (tmp_def->rhs.front() > 1) {
      int lit_def   = mapdef_zzb[aiger_lit2var(tmp_def->rhs.front())]->lhs_ts[time];
      if ((!aiger_sign(lit_latch) && !aiger_sign(lit_def)) ||
	  ( aiger_sign(lit_latch) &&  aiger_sign(lit_def))) {
	if (aiger_sign(tmp_def->rhs.front())) {
	  bcl(solver_zzb,   lit_latch >> 1,    lit_def >> 1);
	  bcl(solver_zzb, -(lit_latch >> 1), -(lit_def >> 1));
	} else {
	  bcl(solver_zzb,   lit_latch >> 1,  -(lit_def >> 1));
	  bcl(solver_zzb, -(lit_latch >> 1),   lit_def >> 1);
	}
      } else {
	if (aiger_sign(tmp_def->rhs.front())) {
	  bcl(solver_zzb,   lit_latch >> 1,  -(lit_def >> 1));
	  bcl(solver_zzb, -(lit_latch >> 1),   lit_def >> 1);
	} else {
	  bcl(solver_zzb,   lit_latch >> 1,    lit_def >> 1);
	  bcl(solver_zzb, -(lit_latch >> 1), -(lit_def >> 1));
	}
      }
    } else {
      ucl(solver_zzb,  tmp_def->rhs.front() ? lit_latch >> 1 : -(lit_latch >> 1));
    }

    tmp_e = tmp_e->next;
  }

  return tmp_list_ands;
}

#else

dll_list<ands_def>* unroll_kind_bwd(int time) {
  dll_list<ands_def> *tmp_list_ands;
  tmp_list_ands = new dll_list<ands_def>();
  dll_init<ands_def>(tmp_list_ands);
  
  idef* tmp_def;
  dll_elem<idef> *tmp_e;

  // Latches
  tmp_e = dll_latches_zzb->last;
  while(tmp_e != NULL) {
    tmp_def = tmp_e->def;
    
    if (tmp_def->lhs == aiger_lit2var(tmp_def->rhs.front())) {
      // Special case x := 0, ~x
      assert(aiger_sign(tmp_def->rhs.front()));

      if (!time) {
	assert(!tmp_def->lhs_ts.size());
	tmp_def->lhs_ts.push_back(new_var(&vars_zzb));

	bcl(solver_zzb, -(tmp_def->lhs_ts[time] >> 1), -act_init_bwd[time]);

      } else {
	if (tmp_def->lhs_ts.size() == time) {
	  tmp_def->lhs_ts.push_back(aiger_not(tmp_def->lhs_ts[time-1]));
	  if (aiger_sign(tmp_def->lhs_ts[time]))
	    bcl(solver_zzb, (tmp_def->lhs_ts[time] >> 1), -act_init_bwd[time]);
	  else
	    bcl(solver_zzb, -(tmp_def->lhs_ts[time] >> 1), -act_init_bwd[time]);

	} else {
	  int lit_latch  = tmp_def->lhs_ts[time-1];
	  int lit_latch_ = tmp_def->lhs_ts[time];
	  if (aiger_sign(lit_latch_)) {
	    bcl(solver_zzb,   lit_latch >> 1,  -(lit_latch_ >> 1));
	    bcl(solver_zzb, -(lit_latch >> 1),   lit_latch_ >> 1);
	    bcl(solver_zzb,   lit_latch_ >> 1, -act_init_bwd[time]);
	  } else {
	    bcl(solver_zzb,   lit_latch >> 1,    lit_latch_ >> 1);
	    bcl(solver_zzb, -(lit_latch >> 1), -(lit_latch_ >> 1));
	    bcl(solver_zzb, -(lit_latch_ >> 1), -act_init_bwd[time]);
	  }
	}
      }

    } else {

      if (tmp_def->lhs_ts.size() == time) {
	tmp_def->lhs_ts.push_back(new_var(&vars_zzb));
      } else {
	assert(tmp_def->lhs_ts.size() == time + 1);
      }

      if (aiger_sign(tmp_def->lhs_ts[time]))
	bcl(solver_zzb, (tmp_def->lhs_ts[time] >> 1), -act_init_bwd[time]);
      else
	bcl(solver_zzb, -(tmp_def->lhs_ts[time] >> 1), -act_init_bwd[time]);
    
      if (time) {
	assert(tmp_def->rhs.size() == 1);
      
	if (tmp_def->rhs.front() > 1) {

	  if (mapdef_zzb[aiger_lit2var(tmp_def->rhs.front())]->lhs_ts.size() > time) {
	    // Special case to consider => Cross reference between latches, e.g. :
	    // x := 0, ~y;
	    // y := 0,  x;
	    // Or 
	    // x := 0,  a;
	    // y := 0,  x;
	    // z := 0, ~x;
	    // We have to add constraints..
	    int lit_latch = tmp_def->lhs_ts[time-1];
	    int lit_def   = mapdef_zzb[aiger_lit2var(tmp_def->rhs.front())]->lhs_ts[time];
	    if ((!aiger_sign(lit_latch) && !aiger_sign(lit_def)) ||
		( aiger_sign(lit_latch) &&  aiger_sign(lit_def))) {
	      if (aiger_sign(tmp_def->rhs.front())) {
		bcl(solver_zzb,   lit_latch >> 1,    lit_def >> 1);
		bcl(solver_zzb, -(lit_latch >> 1), -(lit_def >> 1));
	      } else {
		bcl(solver_zzb,   lit_latch >> 1,  -(lit_def >> 1));
		bcl(solver_zzb, -(lit_latch >> 1),   lit_def >> 1);
	      }
	    } else {
	      if (aiger_sign(tmp_def->rhs.front())) {
		bcl(solver_zzb,   lit_latch >> 1,  -(lit_def >> 1));
		bcl(solver_zzb, -(lit_latch >> 1),   lit_def >> 1);
	      } else {
		bcl(solver_zzb,   lit_latch >> 1,    lit_def >> 1);
		bcl(solver_zzb, -(lit_latch >> 1), -(lit_def >> 1));
	      }
	    }

	  } else {
	    if (aiger_sign(tmp_def->rhs.front()))
	      mapdef_zzb[aiger_lit2var(tmp_def->rhs.front())]->lhs_ts.push_back(aiger_not(tmp_def->lhs_ts[time-1]));
	    else
	      mapdef_zzb[aiger_lit2var(tmp_def->rhs.front())]->lhs_ts.push_back(tmp_def->lhs_ts[time-1]);
	  }

	} else { // Cst latch
	  if ((tmp_def->rhs.front() && !aiger_sign(tmp_def->lhs_ts[time-1])) ||
	      (!tmp_def->rhs.front() && aiger_sign(tmp_def->lhs_ts[time-1])))
	    ucl(solver_zzb, tmp_def->lhs_ts[time-1] >> 1);
	  else
	    ucl(solver_zzb, -(tmp_def->lhs_ts[time-1] >> 1));
	}
      }
    }

    tmp_e = tmp_e->prev;
  }

  // Inputs
  tmp_e = dll_inputs_zzb->head;
  while(tmp_e != NULL) {
    tmp_def = tmp_e->def;

    if (tmp_def->lhs_ts.size() <= time)
      tmp_def->lhs_ts.push_back(new_var(&vars_zzb));
    else
      assert(tmp_def->lhs_ts.size() == time + 1);

    tmp_e = tmp_e->next;
  }

  // Ands
  tmp_e = dll_ands_zzb->head;
  while(tmp_e != NULL) {
    tmp_def = tmp_e->def;

    prop_tr_un *tpu = unroll_prop_tr(mapdef_zzb, map_ands_zzb, tmp_def->rhs_tr, time, 0);

    assert(tpu->lit == UNDEF_LIT);
    assert(tpu->op != VAR);

    if (tmp_def->lhs_ts.size() <= time)
      tmp_def->lhs_ts.push_back(new_var(&vars_zzb));
    else
      assert(tmp_def->lhs_ts.size() == time + 1);

    ands_def *tmp_ands = new ands_def(tmp_def->lhs_ts[time], tpu, tmp_def, time);
    map_ands_zzb.insert(std::pair<unsigned, ands_def*>(aiger_lit2var(tmp_ands->lhs), tmp_ands));
      
    tmp_ands->dllade = dll_insert_last(tmp_list_ands, aiger_lit2var(tmp_def->lhs_ts[time]), tmp_ands);


    tmp_e = tmp_e->next;
  }

  return tmp_list_ands;
}

#endif


/**************************************************************************************/
int zigzag_bwd(int start, int stop, aiger *model, bool elim_hl, int simple, bool verbose_, int *status) {
  int i, j;

  dll_inputs_zzb = new dll_list<idef>();
  dll_latches_zzb = new dll_list<idef>();
  dll_ands_zzb = new dll_list<idef>();
  unsigned bad_lit = init_model(model, dll_ands_zzb, dll_inputs_zzb, dll_latches_zzb, mapdef_zzb, 0, verbose_);
  aiger_reset(model);
  solver_zzb = new SimpSolver();
  init_solver(solver_zzb, 0, verbose_);
  
  if (elim_hl)
    hl_ve(mapdef_zzb, dll_ands_zzb, verbose_);


  for(i = 0; i <= stop; i++) {
    act_init_bwd.push_back(new_var(&vars_zzb) >> 1);

    if (verbose_) printf("[zigzag]  Encoding %d..\n", i);

    list_ands_zzb.push_back(unroll_kind_bwd(i));

    iter_flatten_ptu(list_ands_zzb, i);

    ands2clauses(solver_zzb, list_ands_zzb, i, 0);

    assert(mapdef_zzb[aiger_lit2var(bad_lit)]->lhs_ts[i] > 1);
    
    // Add Bad cls
    int lit_bad;
    lit_bad = mapdef_zzb[aiger_lit2var(bad_lit)]->lhs_ts[i];
    std::map<unsigned, ands_def*>::iterator lit_bad_ref = map_ands_zzb.find(aiger_lit2var(lit_bad));
    if (lit_bad_ref != map_ands_zzb.end())
      assert(lit_bad_ref->second->cst == -1);
      
    if (aiger_sign(lit_bad))
      lit_bad = aiger_sign(bad_lit) ?  (lit_bad >> 1) : -(lit_bad >> 1);
    else
      lit_bad = aiger_sign(bad_lit) ? -(lit_bad >> 1) :  (lit_bad >> 1);

    if (i) 
      ucl(solver_zzb, -lit_bad); // Assert:  P(x_i);
    else
      ucl(solver_zzb, lit_bad);  // Assert: -P(x_0);

    if (verbose_) printf("[zigzag]  Solving %d-induction..\n", i);

    lbool res_check = check_sat(solver_zzb, assumptions_zzb, verbose_); 
    if (res_check == l_False) {
      *status = 0;
      break;
    } else if (i > 1 && simple == 2) {
      if (simple_path_od_bwd(i, verbose_)) {
	*status = 0;
	break;
      }
    }

    if (verbose_) printf("[zigzag]  Solving BMC %d..\n", i);

    assume(solver_zzb, assumptions_zzb, act_init_bwd[i]);           // Assume:  I(x_i)

    res_check = check_sat(solver_zzb, assumptions_zzb, verbose_); 
    if (res_check == l_True) {
      *status = 1;
      break;
    }


    printf("u%d\n", i);
    fflush(stdout);
  }

  //printf("%d\nb0\n.\n", *status);
  //fflush(stdout);

  if (verbose_)
    printf("Vars %d (%d - %d)| Clauses %d\n",
	   solver_zzb->nVars() - solver_zzb->eliminated_vars, solver_zzb->nVars(), solver_zzb->eliminated_vars, solver_zzb->nClauses());
  destroy_model(dll_ands_zzb, dll_inputs_zzb, dll_latches_zzb, list_ands_zzb, mapdef_zzb, map_ands_zzb, verbose_);
  return 0;
}

