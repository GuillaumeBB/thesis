/***************************************************************************
Copyright (c) 2016-2017, Guillaume Baud-Berthier, SafeRiver/LaBRI

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
IN THE SOFTWARE.
***************************************************************************/
#ifndef aigprep_h
#define aigprep_h

#include <list>
#include <map>
#include <set>
#include "utils.h"

struct aiger_def;

/* Doubly linked list for def id */
/* struct dl_elem */
/* { */
/*   unsigned id; */
/*   aiger_def* def; */
/*   dl_elem *next; */
/*   dl_elem *prev; */
/* }; */

/* struct dl_list */
/* { */
/*   unsigned sz; */
/*   dl_elem *head; */
/*   dl_elem *last; */
/* }; */

/* void dl_init(dl_list* l); */
/* void dl_delete(dl_list* l); */
/* //dl_elem* dl_insert_first(dl_list* l, unsigned ind); */
/* //dl_elem* dl_insert_last(dl_list* l, unsigned ind); */
/* dl_elem* dl_insert_last(dl_list* l, unsigned ind, aiger_def* def); */
/* //dl_elem* dl_insert_after(dl_list* l, dl_elem* e, unsigned ind); */
/* void dl_remove_elem(dl_list* l, dl_elem* e); */
/* unsigned dl_size(dl_list* l); */



enum aiger_def_type
{
  aiger_def_input = 0,
  aiger_def_latch = 1,
  aiger_def_and   = 2,
  aiger_def_cst   = 3
};

enum sim_val
  {
    v0 = 0,
    v1 = 1,
    vX = 2

  };

struct aiger_def
{
  aiger_def_type type;

  unsigned var_lhs;

  unsigned op1;           /* reset  for latches                */
                          /* rhs0   for ands                   */
                          /* 0      for inputs                 */
                          /* 0      for bad                    */
                          /* undef  for cst                    */

  unsigned op2;           /* next   for latches                */
                          /* rhs1   for ands                   */
                          /* 0      for inputs                 */
                          /* 0      for bad                    */
                          /* cst    for cst                    */
  aiger_def* op1_ref;
  aiger_def* op2_ref;

  bool mark_del;          /* true if this def has been deleted */

  bool coi;               /* true if inside coi                */

  bool update;            /* */

  sim_val prev_val;       /* Useful for latches */
  sim_val val;

  //dl_elem* dle;
  dll_elem<aiger_def> *dle;

  std::set<unsigned> refBy;
  
  std::set<unsigned> func_cst;

  std::set<unsigned> func;

aiger_def(): op1_ref(NULL), op2_ref(NULL), mark_del(0), coi(0), update(1), prev_val(v0), val(v0), dle(NULL){};
  ~aiger_def() { refBy.clear(); func_cst.clear(); func.clear(); };
};


extern bool balance_aig_OPT;

void init_prep (aiger * model, bool cex, int verb);
void destroy_prep();
aiger* rebuild_aig (bool bad);

/* COI */
void compute_coi(bool cex);
void coi(bool cex);
void reset_coi();
void reset_coi_latches();
void reset_coi_ands();
void reset_coi_inputs();


/* Main simplification loop */
void iter_defs(bool cex);
void iter_defs_simplifications();
void iter_defs_constraints();

/* Balance AIG */
unsigned max_aig_lvl();
bool     iter_balance_def(aiger_def *def);

/* ANDs simplification loop */
void iter_combi_propagate_cst      (unsigned id, aiger_def *def);
bool iter_combi_check_cst          (unsigned id, aiger_def *def);
bool iter_combi_trivia             (unsigned id, aiger_def *def);
bool iter_combi_simp               (unsigned id, aiger_def *def);
bool iter_combi_struct_equiv       (unsigned id, aiger_def *def, unsigned key);
void iter_combi_build_func         (unsigned id, aiger_def *def);
bool iter_combi_func_equiv_cst     (unsigned id, aiger_def *def);
bool iter_combi_func_subsume       (unsigned id, aiger_def *def);
bool iter_combi_func_equiv         (unsigned id, aiger_def *def, unsigned key);
bool iter_combi_func_equiv_diff1   (unsigned id, aiger_def *def);
bool iter_combi_func_equiv_diff1_s3(unsigned id, aiger_def *def);

/* Latches simplification loop */
void iter_latch_propagate_cst (unsigned id, aiger_def *def);
bool iter_latch_check_cst     (unsigned id, aiger_def *def);
bool iter_latch_struct_equiv  (unsigned id, aiger_def *def);

int ternary_simulation();

/* Bad state */
void update_bad_state();


/* Constraints learning helper functions */
void simple_constraints(aiger_def* def);

std::list<std::pair<unsigned, unsigned>> get_simple_ctr();
std::list<std::tuple<unsigned, unsigned, unsigned>> get_double_ctr();

/* Utils */
       void     remove_def        (aiger_def* def);
       void     print_set         (std::set<unsigned> &s);
       void     print_def         (aiger_def* def);
       void     set_update_flag   (std::set<unsigned> &refBy);
       unsigned hash_set          (std::set<unsigned> &f);
       unsigned hash_set_diff1    (std::set<unsigned> &f, unsigned ind);
       unsigned equal_set_diff1   (std::set<unsigned> &f1, std::set<unsigned> &f2, unsigned ind);
       void     add_set2set       (std::set<unsigned> &src, std::set<unsigned> &dest);
       void     update_level      (aiger_def* def);
       unsigned hash_uint         (int x);
inline void     order_ops         (aiger_def* def);
inline bool     equal_set         (std::set<unsigned> &f1, std::set<unsigned> &f2);
inline unsigned hash_def          (unsigned op1, unsigned op2);
inline bool     equal_def         (aiger_def* def1, aiger_def* def2);

#endif
