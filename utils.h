/***************************************************************************
Copyright (c) 2016-2017, Guillaume Baud-Berthier, SafeRiver/LaBRI

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
IN THE SOFTWARE.
***************************************************************************/

#ifndef utils_h
#define utils_h

extern "C" {
  #include "aiger.h"
}
#include <limits.h>
#include <cstdlib>
#include <assert.h>
#include <utility>
#include <vector>
#include <list>
#include <set>
#include <map>
#include <fstream>
#include <iostream>
#include <stack>
#include <unordered_map>
#include <unistd.h>

/* ========================================================================== */
/* Doubly linked list */
template <typename T> struct dll_list;

template <typename T> struct dll_elem
{
  unsigned id;
  dll_list<T> *l;
  T *def;
  dll_elem<T> *next;
  dll_elem<T> *prev;
};

template <typename T> struct dll_list
{
  unsigned sz;
  dll_elem<T> *head;
  dll_elem<T> *last;
};

/* ========================================================================== */
/*                      Doubly linked list functions                          */
/* ========================================================================== */
template <typename T> void dll_init(dll_list<T>* l) {
  l->sz = 0;
  l->head = NULL;
  l->last = NULL;
}

template <typename T> void dll_delete(dll_list<T>* l) {
  delete l;
}

template <typename T> dll_elem<T>* dll_insert_last(dll_list<T>* l, unsigned ind, T *def) {
  dll_elem<T>* e = new dll_elem<T>();
  e->id = ind;
  e->l = l;
  e->def = def;
  e->next = NULL;
  e->prev = l->last;
  
  if (!l->sz)
    l->head = e;
  else 
    l->last->next = e;
  
  l->last = e;
  l->sz++;
  return e;
}

template <typename T> dll_elem<T>* dll_insert_before(dll_list<T>* l, dll_elem<T> *e_b, unsigned ind, T *def) {
  assert(l->sz);
  dll_elem<T>* e = new dll_elem<T>();
  e->id = ind;
  e->l = l;
  e->def = def;
  e->next = e_b;
  e->prev = e_b->prev;
  
  e_b->prev = e;

  if (e_b == l->head)
    l->head = e;
  else 
    e->prev->next = e;

  l->sz++;
  return e;
}

template <typename T> void dll_remove_elem(dll_elem<T>* e) {
  dll_list<T>* l = e->l;
  if (e == NULL) {
    printf("Trying to remove an element that has already been removed (or NULL element)!\n"); exit(-1);
  }
  if (!l->sz) {
    printf("Trying to remove an element of an empty dll list!\n"); exit(-1);
  } else if (l->sz == 1) {
    l->sz = 0;
    l->head = NULL;
    l->last = NULL;
  } else {
    l->sz--;
    if (e->prev == NULL) {
      l->head = e->next;
      l->head->prev = NULL;
    } else
      e->prev->next = e->next;

    if (e->next == NULL) {
      l->last = e->prev;
      l->last->next = NULL;
    } else
      e->next->prev = e->prev;
  }
  /* delete e; */
  /* e = NULL; */
}

template <typename T> unsigned dll_size(dll_list<T>* l) {
  return l->sz;
}



/* ========================================================================== */



struct idef;
struct ands_def;
struct prop_tr_un_fl;//list_ptuf; 


enum prop_op{ AND, OR, VAR };

/* Propositional tree */
struct prop_tr
{
  prop_op op;
  unsigned var;
  bool l_sign;
  bool r_sign;
  prop_tr *l;
  prop_tr *r;

prop_tr(): op(VAR), var(0), l_sign(0), r_sign(0), l(NULL), r(NULL) {};
prop_tr(prop_tr *pt): op(pt->op), var(pt->var), l_sign(pt->l_sign), r_sign(pt->r_sign), l(pt->l), r(pt->r) {};
prop_tr(unsigned v): op(VAR), var(v), l_sign(0), r_sign(0), l(NULL), r(NULL) {};
  ~prop_tr() { if (l != NULL) delete l; if (r != NULL) delete r; };
};

/* Propositional tree unrolled */
struct prop_tr_un
{
  prop_op op;
  unsigned lit;
  ands_def *lit_ref;
  prop_tr_un *l;
  prop_tr_un *r;

prop_tr_un(): op(VAR), lit(0), lit_ref(NULL), l(NULL), r(NULL) {};
  ~prop_tr_un() { if (l != NULL) delete l; if (r != NULL) delete r; };
};


/* Special list for ptuf */
struct list_ptuf_e
{
  prop_tr_un_fl *ptuf;
  list_ptuf_e *prev;
  list_ptuf_e *next;

list_ptuf_e(): ptuf(NULL), prev(NULL), next(NULL) {};
};



struct list_ptuf
{
  int size;
  list_ptuf_e *head;
  list_ptuf_e *tail;

list_ptuf(): size(0), head(NULL), tail(NULL) {};
  ~list_ptuf() {};
};

/* Propositional tree unrolled and flattened */
struct prop_tr_un_fl
{
  prop_op op;
  unsigned lit;
  ands_def *lit_ref;
  list_ptuf *lp;

prop_tr_un_fl(): op(VAR), lit(0), lit_ref(NULL), lp(NULL) {};
  ~prop_tr_un_fl() {};
};

void destroy_ptuf(prop_tr_un_fl *ptuf);


/* ========================================================================== */
enum idef_type
{
  idef_input  = 0,
  idef_latch  = 1,
  idef_and    = 2,
  idef_assume = 3,
  idef_cst    = 4
};

enum idef_hl_type{ UNK, BIG_AND, XOR, EQUIV, ITE, UNBAL };

typedef struct idef
{
  idef_type type;
  
  idef_hl_type hl_type;
  
  unsigned lhs;
  
  std::vector<int> lhs_ts;
  
  prop_tr *rhs_tr;
  
  int pt_comp;
  int pt_var;
  
  std::list<unsigned> rhs; // Inputs: empty
                           // Latches: 1 element
                           // Ands: at least 2 elements
                           // PO : empty
  std::set<idef*> refBy;
  
  dll_elem<idef>* dlle;

  bool apply_ve;
  bool update_elim;
  int cost;
  dll_elem<idef>* it_ve;

} idef;

enum ad_type{ undef_t, and_t, or_t, xor_t, equiv_t, ite_t };

typedef struct ands_def
{
  unsigned lhs;
  
  //std::set<unsigned> rhs;
  
  prop_tr_un *rhs_tr;
  prop_tr_un_fl *rhs_tr_fl;

  idef *d;

  int cst;
  ands_def *cst_ref;

  bool added2solver;

  dll_elem<ands_def>* dllade;

ands_def(unsigned l, prop_tr_un *r, idef* d_, int c): lhs(l), rhs_tr(r), d(d_), cst(-1),
    cst_ref(NULL), added2solver(0), dllade(NULL), coi(0), simp_solver_check(0), I(0), T(0), E(0),
    cycle(c), unit_learnt_cycle(-1), apply_ve(1) {};

  //  ~ands_def() { delete rhs_tr; rhs_tr = NULL; destroy_ptuf(rhs_tr_fl);}; 

  std::set<unsigned> funcA_cst;
  std::set<unsigned> funcO_cst;

  bool op_and;
  std::set<unsigned> func_eq;

  ad_type type;
  unsigned I;
  ands_def *I_ref;
  unsigned T;
  ands_def *T_ref;
  unsigned E;
  ands_def *E_ref;

  bool coi;
  bool simp_solver_check;

  int level;
  int cycle;
  int pos;
  int unit_learnt_cycle;

  std::set<ands_def*> refBy;
  bool apply_ve;
  int nb_op;

} ands_def;

#define UNDEF_LIT INT_MAX
//unsigned UNDEF_LIT = INT_MAX;
//std::map<unsigned, ands_def*> map_ands;
//std::vector<idef*> mapdef;
//extern std::vector<dll_list<ands_def>*> list_ands;

//bool terminate_flag();
//void set_terminate_flag();


int new_var(unsigned *v);

/* ========================================================================== */
/*                               Debug functions                              */
/* ========================================================================== */
void print_idef(idef *d);
void print_pt(prop_tr *pt);
void print_ptu(prop_tr_un *pt);
void print_lp(list_ptuf *lp);
void print_ptuf(prop_tr_un_fl *pt);
void print_ptuf_bis(prop_tr_un_fl *pt);
void print_ad_ptu(ands_def *ad);
void print_ad_ptuf(ands_def *ad);
void print_ad_type(ands_def *ad);
void print_ad_ptuf_bis(ands_def *ad);
void print_ad_ptuf_f(ands_def *ad);
void print_ad_ite(ands_def *ad);
void check_def_consistency(prop_tr_un_fl *ptuf);
int get_mem();

/* ========================================================================== */
/*                      List prop_tr_un_fl function                           */
/* ========================================================================== */
void insert_last(list_ptuf *lp, prop_tr_un_fl *ptuf);
int compare_ptuf(prop_tr_un_fl *ptuf1, prop_tr_un_fl *ptuf2);
void insert_ordered(list_ptuf *lp, prop_tr_un_fl *ptuf);
void remove(list_ptuf *lp, list_ptuf_e *e);
void reorder_lp(list_ptuf* lp, list_ptuf_e *e);


inline unsigned hash_lit(unsigned x) {
  unsigned ret = ((x >> 16) ^ x) * 0x45d9f3b;
  ret = ((ret >> 16) ^ ret) * 0x45d9f3b;
  ret = (ret >> 16) ^ ret;
  return ret;
}

#endif
