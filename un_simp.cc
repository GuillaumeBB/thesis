/***************************************************************************
Copyright (c) 2016-2017, Guillaume Baud-Berthier, SafeRiver/LaBRI

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
IN THE SOFTWARE.
***************************************************************************/

#include "un_simp.h"

bool solver_fb_OPT            = 1;
bool fb2solver_OPT            = 1;
bool simp_adv_OPT             = 1;
bool struct_equiv_global_OPT  = 1;
bool struct_diff_global_OPT   = 1;
bool struct_equiv_OPT         = 1;
bool struct_diff_OPT          = 1;

int stats_fb2solver           = 0;
int stats_simp_unroll         = 0;
int stats_simp_adv_cst        = 0;
int stats_simp_adv_funcA_subs = 0;
int stats_simp_adv_funcO_subs = 0;
int stats_simp_adv_contra     = 0;
int stats_simp_adv_tauto      = 0;
int stats_simp_hl_contra      = 0;

int stats_simp_prop = 0;

int stats_struct_eq = 0;
int stats_struct_eq_global = 0;
int stats_struct_diff = 0;
int stats_struct_diff_global = 0;

int bigAO_size = 30;

std::unordered_multimap<unsigned, ands_def*> hash_struct_equiv;
std::unordered_multimap<unsigned, ands_def*> hash_struct_equiv_global;

std::map<unsigned, ands_def*> map_bigA;
std::map<unsigned, ands_def*> map_bigO;
/* ========================================================================== */
/*                 Structural equivalence helper functions                    */
/* ========================================================================== */
void clear_global_hash() {
  hash_struct_equiv_global.clear();
}

unsigned hash_ptuf(prop_tr_un_fl *ptuf) {
  unsigned hash = 0;
  if (ptuf->op == VAR) {
    hash = hash_lit(ptuf->lit);

  } else {
    list_ptuf_e *it = ptuf->lp->head;
    while(it != NULL) {
      
      hash += hash_ptuf(it->ptuf);
      it = it->next;
    }
    hash += ptuf->op == AND ?  1 : 2;
  }
  return hash;
}

bool equal_ptuf(prop_tr_un_fl *ptuf1, prop_tr_un_fl *ptuf2) {
  assert(ptuf1->op != VAR);
  assert(ptuf2->op != VAR);
  if (ptuf1->lp->size != ptuf2->lp->size || ptuf1->op != ptuf2->op)
    return 0;

  else {
    list_ptuf_e *e1 = ptuf1->lp->head;
    list_ptuf_e *e2 = ptuf2->lp->head;
    while (e1 != NULL) {
      if (e1->ptuf->op != e2->ptuf->op)
	return 0;
      if (e1->ptuf->op == VAR && e2->ptuf->op == VAR) {
	if (e1->ptuf->lit != e2->ptuf->lit)
	  return 0;
      } else {
	if (!equal_ptuf(e1->ptuf, e2->ptuf))
	  return 0;
      }
      e1 = e1->next;
      e2 = e2->next;
    }
    return 1;
  }
}

ands_def* check_struct_equiv(prop_tr_un_fl *ptuf, unsigned struct_hash) {
  std::unordered_multimap<unsigned, ands_def*>::iterator elem;
  std::pair< std::unordered_multimap< unsigned, ands_def* >::iterator,
	     std::unordered_multimap< unsigned, ands_def* >::iterator> range;
  range = hash_struct_equiv.equal_range(struct_hash);
  
  if(range.first != hash_struct_equiv.end()) { 
    assert(hash_struct_equiv.count(struct_hash) < 50);
    // Conflict or structurally equivalent!
    for(elem = range.first; elem != range.second; elem++) {
      if (equal_ptuf(ptuf, elem->second->rhs_tr_fl))
	return elem->second;
    } 
  }
  return NULL;
}

ands_def* check_struct_equiv_global(prop_tr_un_fl *ptuf, unsigned struct_hash) {
  std::unordered_multimap<unsigned, ands_def*>::iterator elem;
  std::pair< std::unordered_multimap< unsigned, ands_def* >::iterator,
	     std::unordered_multimap< unsigned, ands_def* >::iterator> range;
  range = hash_struct_equiv_global.equal_range(struct_hash);
  
  if(range.first != hash_struct_equiv_global.end()) {
    assert(hash_struct_equiv_global.count(struct_hash) < 50);
    // Conflict or structurally equivalent!
    for(elem = range.first; elem != range.second; elem++) {
      if (equal_ptuf(ptuf, elem->second->rhs_tr_fl))
	return elem->second;
    }
  }
  return NULL;
}


/* ========================================================================== */
/*                    Structural diff helper functions                        */
/* ========================================================================== */
unsigned hash_not_ptuf(prop_tr_un_fl *ptuf) {
  unsigned hash = 0;
  if (ptuf->op == VAR) {
    hash = hash_lit(aiger_not(ptuf->lit));

  } else {
    list_ptuf_e *it = ptuf->lp->head;
    while(it != NULL) {
      hash += hash_not_ptuf(it->ptuf);
      it = it->next;
    }
    hash += ptuf->op == AND ?  2 : 1;
  }
  return hash;
}

bool equal_not_ptuf(prop_tr_un_fl *ptuf1, prop_tr_un_fl *ptuf2) {
  assert(ptuf1->op != VAR);
  assert(ptuf2->op != VAR);
  if (ptuf1->lp->size != ptuf2->lp->size || ptuf1->op == ptuf2->op)
    return 0;

  else {
    list_ptuf_e *e1 = ptuf1->lp->head;
    list_ptuf_e *e2 = ptuf2->lp->head;
    while (e1 != NULL) {
      if ((e1->ptuf->op == AND  && e2->ptuf->op == AND) ||
	  (e1->ptuf->op == OR   && e2->ptuf->op == OR)  ||
	  (e1->ptuf->op == VAR  && e2->ptuf->op != VAR) ||
	  (e1->ptuf->op != VAR  && e2->ptuf->op == VAR))
	return 0;
      if (e1->ptuf->op == VAR && e2->ptuf->op == VAR) {
	if (aiger_not(e1->ptuf->lit) != e2->ptuf->lit)
	  return 0;
      } else {
	assert((e1->ptuf->op == AND && e2->ptuf->op == OR) || 
	       (e1->ptuf->op == OR  && e2->ptuf->op == AND));
	if (!equal_not_ptuf(e1->ptuf, e2->ptuf))
	  return 0;
      }
      e1 = e1->next;
      e2 = e2->next;
    }
    return 1;
  }
}

ands_def* check_struct_diff(prop_tr_un_fl *ptuf, unsigned struct_hash) {
  std::unordered_multimap<unsigned, ands_def*>::iterator elem;
  std::pair< std::unordered_multimap< unsigned, ands_def* >::iterator,
	     std::unordered_multimap< unsigned, ands_def* >::iterator> range;
  range = hash_struct_equiv.equal_range(struct_hash);
  
  if(range.first != hash_struct_equiv.end()) { 
    assert(hash_struct_equiv.count(struct_hash) < 50);
    // Conflict or structurally equivalent!
    for(elem = range.first; elem != range.second; elem++) {
      if (equal_not_ptuf(ptuf, elem->second->rhs_tr_fl)) {
	return elem->second;
      }
    } 
  }
  return NULL;
}

ands_def* check_struct_diff_global(prop_tr_un_fl *ptuf, unsigned struct_hash) {
  std::unordered_multimap<unsigned, ands_def*>::iterator elem;
  std::pair< std::unordered_multimap< unsigned, ands_def* >::iterator,
	     std::unordered_multimap< unsigned, ands_def* >::iterator> range;
  range = hash_struct_equiv_global.equal_range(struct_hash);
  
  if(range.first != hash_struct_equiv_global.end()) {
    assert(hash_struct_equiv_global.count(struct_hash) < 50);
    // Conflict or structurally equivalent!
    for(elem = range.first; elem != range.second; elem++) {
      if (equal_not_ptuf(ptuf, elem->second->rhs_tr_fl)) {
	return elem->second;
      }
    } 
  }
  return NULL;
}




/* ========================================================================== */
/*                           Optimisation functions                           */
/* ========================================================================== */
bool propagate_cst(prop_tr_un_fl *ptuf) {
  if (ptuf->op == VAR) {
    if (ptuf->lit_ref != NULL && ptuf->lit_ref->cst != -1) {
      while(ptuf->lit_ref != NULL && ptuf->lit_ref->cst != -1) {
	//printf("%d %d\n", ptuf->lit, ptuf->lit_ref->cst);

	if (aiger_sign(ptuf->lit) == aiger_sign(ptuf->lit_ref->lhs)) 
	  ptuf->lit = ptuf->lit_ref->cst;
	else
	  ptuf->lit = aiger_not(ptuf->lit_ref->cst);

	// if (aiger_sign(ptuf->lit)) {
	//   ptuf->lit = aiger_not(ptuf->lit_ref->cst);
	// } else {
	//   ptuf->lit = ptuf->lit_ref->cst;
	// }

	ptuf->lit_ref = ptuf->lit_ref->cst_ref;

	//if (ptuf->lit_ref != NULL)
	//printf("%d %d (%d)\n", ptuf->lit, ptuf->lit_ref->cst, ptuf->lit_ref->lhs);
	
	assert(ptuf->lit_ref == NULL || ptuf->lit_ref->cst == -1 || solver_fb_OPT);
      }

      return 1;
    } else
      return 0;

  } else {
    
    list_ptuf_e *it = ptuf->lp->head;
    while(it != NULL) {
      if (propagate_cst(it->ptuf))
	reorder_lp(ptuf->lp, it);
      it = it->next;
    }
    return 0;
  }
}

bool check_cst_ptuf_or(prop_tr_un_fl *ptuf);

bool check_cst_ptuf_and(prop_tr_un_fl *ptuf) {
  assert(ptuf->op == AND);
  unsigned prevLit = UNDEF_LIT;
  list_ptuf_e *it = ptuf->lp->head;
  while(it != NULL) {
    if (it->ptuf->op == VAR) {
      if (it->ptuf->lit == 0) {
	ptuf->lp->head->ptuf = new prop_tr_un_fl();
	return 1;
	
      } else if (it->ptuf->lit == 1) {
	remove(ptuf->lp, it);

      } else if (prevLit != UNDEF_LIT) {
	if (aiger_lit2var(prevLit) == aiger_lit2var(it->ptuf->lit)) {
	  if (prevLit == it->ptuf->lit) {
	    remove(ptuf->lp, it);

	  } else {
	    ptuf->lp->head->ptuf = new prop_tr_un_fl();
	    return 1;
	  }
	} else {
	  prevLit = it->ptuf->lit;
	}
      } else {
	prevLit = it->ptuf->lit;
      }

      it = it->next;

    } else {
      assert(it->ptuf->op == OR);
      prevLit = UNDEF_LIT;

      if(check_cst_ptuf_or(it->ptuf)) {
	if (!it->ptuf->lp->size) {
	  ptuf->lp->head->ptuf = new prop_tr_un_fl();
	  return 1;
	  
	} else if (it->ptuf->lp->head->ptuf->op == VAR) {
	  it->ptuf->op = VAR;
	  it->ptuf->lit = it->ptuf->lp->head->ptuf->lit;
	  it->ptuf->lit_ref = it->ptuf->lp->head->ptuf->lit_ref;

	  reorder_lp(ptuf->lp, it);

	  it = ptuf->lp->head;

	} else if (it->ptuf->lp->head->ptuf->op == AND) {
	  remove(ptuf->lp, it);
	  list_ptuf_e *it_tmp = it->ptuf->lp->head->ptuf->lp->head;
	  while(it_tmp != NULL) {
	    insert_ordered(ptuf->lp, it_tmp->ptuf);
	    it_tmp = it_tmp->next;
	  }
	  it = ptuf->lp->head;

	} else {
	  assert(0);
	}

      } else {
	it = it->next;
      }
    }
  }

  if (ptuf->lp->size <= 1) {
    // TODO: if empty    => true
    //       if size = 1 => alias
    if (!ptuf->lp->size) {
      return 1;

    } else if (ptuf->lp->head->ptuf->op == VAR) {
      return 1;
 
    } else {
      assert(ptuf->lp->head->ptuf->op == OR);
      return 1;
    }
  }
  return 0;
}

bool check_cst_ptuf_or(prop_tr_un_fl *ptuf) {
  assert(ptuf->op == OR);
  unsigned prevLit = UNDEF_LIT;
  list_ptuf_e *it = ptuf->lp->head;
  while(it != NULL) {
    if (it->ptuf->op == VAR) {
      if (it->ptuf->lit == 0) {
	remove(ptuf->lp, it);
	
      } else if (it->ptuf->lit == 1) {
	ptuf->lp->head->ptuf = new prop_tr_un_fl();
	ptuf->lp->head->ptuf->lit = 1;
	return 1;

      } else if (prevLit != UNDEF_LIT) {
	if (aiger_lit2var(prevLit) == aiger_lit2var(it->ptuf->lit)) {
	  if (prevLit == it->ptuf->lit) {
	    remove(ptuf->lp, it);

	  } else {
	    ptuf->lp->head->ptuf = new prop_tr_un_fl();
	    ptuf->lp->head->ptuf->lit = 1;
	    return 1;
	  }
	} else {
	  prevLit = it->ptuf->lit;
	}
      } else {
	prevLit = it->ptuf->lit;
      }
      it = it->next;

    } else {
      assert(it->ptuf->op == AND);
      prevLit = UNDEF_LIT;

      if(check_cst_ptuf_and(it->ptuf)) { 
	if (!it->ptuf->lp->size) {
	  ptuf->lp->head->ptuf = new prop_tr_un_fl();
	  ptuf->lp->head->ptuf->lit = 1;
	  return 1;
	  
	} else if (it->ptuf->lp->head->ptuf->op == VAR) {
	  it->ptuf->op = VAR;
	  it->ptuf->lit = it->ptuf->lp->head->ptuf->lit;
	  it->ptuf->lit_ref = it->ptuf->lp->head->ptuf->lit_ref;

	  reorder_lp(ptuf->lp, it);

	  it = ptuf->lp->head;
	  

	} else if (it->ptuf->lp->head->ptuf->op == OR) {
	  remove(ptuf->lp, it);
	  list_ptuf_e *it_tmp = it->ptuf->lp->head->ptuf->lp->head;
	  while(it_tmp != NULL) {
	    insert_ordered(ptuf->lp, it_tmp->ptuf);
	    it_tmp = it_tmp->next;
	  }
	  it = ptuf->lp->head;

	} else {
	  assert(0);
	}
	
      } else {

	it = it->next;
      }
    }    
  }

  if (ptuf->lp->size <= 1) {
    // TODO: if empty    => false
    //       if size = 1 => alias
    if (!ptuf->lp->size) {
      return 1;
      
    } else if (ptuf->lp->head->ptuf->op == VAR) {
      return 1;
 
    } else {
      assert(ptuf->lp->head->ptuf->op == AND);
      return 1;
    }
  }
  return 0;
}

bool check_cst(SimpSolver* solver, ands_def *ad, int time) {
  assert(ad->rhs_tr_fl->op != VAR);
  if (ad->rhs_tr_fl->op == AND) {
    
    if (check_cst_ptuf_and(ad->rhs_tr_fl)) {
      assert(ad->d->lhs_ts[time] == ad->lhs);
      
      if (!ad->rhs_tr_fl->lp->size) {

	if (fb2solver_OPT && ad->added2solver) {
	  aiger_sign(ad->d->lhs_ts[time]) ? ucl(solver, -(ad->d->lhs_ts[time] >> 1)) : ucl(solver, ad->d->lhs_ts[time] >> 1);
	  //ucl(ad->d->lhs_ts[time] >> 1);
	  stats_fb2solver++;
	}

	ad->d->lhs_ts[time] = 1;
	ad->cst = 1;
	ad->cst_ref = NULL;
	//dll_remove_elem(l, ad->dllade);
	dll_remove_elem(ad->dllade);

      } else if (ad->rhs_tr_fl->lp->head->ptuf->op == VAR) {

	if (fb2solver_OPT && ad->added2solver) {

	  stats_fb2solver++;
	  if (ad->rhs_tr_fl->lp->head->ptuf->lit == 0) {
	    aiger_sign(ad->d->lhs_ts[time]) ? ucl(solver, ad->d->lhs_ts[time] >> 1) : ucl(solver, -(ad->d->lhs_ts[time] >> 1));
	    //ucl(-(ad->d->lhs_ts[time] >> 1));

	  } else {
	    assert(ad->rhs_tr_fl->lp->head->ptuf->lit > 1);

	    if (!aiger_sign(ad->d->lhs_ts[time])) { //
	      if (aiger_sign(ad->rhs_tr_fl->lp->head->ptuf->lit)) {
		bcl(solver,   ad->d->lhs_ts[time] >> 1 ,   ad->rhs_tr_fl->lp->head->ptuf->lit >> 1);
		bcl(solver, -(ad->d->lhs_ts[time] >> 1), -(ad->rhs_tr_fl->lp->head->ptuf->lit >> 1));
	      } else {
		bcl(solver,   ad->d->lhs_ts[time] >> 1 , -(ad->rhs_tr_fl->lp->head->ptuf->lit >> 1));
		bcl(solver, -(ad->d->lhs_ts[time] >> 1),   ad->rhs_tr_fl->lp->head->ptuf->lit >> 1);
	      }
	    } else { //
	      if (!aiger_sign(ad->rhs_tr_fl->lp->head->ptuf->lit)) { //
		bcl(solver,   ad->d->lhs_ts[time] >> 1 ,   ad->rhs_tr_fl->lp->head->ptuf->lit >> 1); //
		bcl(solver, -(ad->d->lhs_ts[time] >> 1), -(ad->rhs_tr_fl->lp->head->ptuf->lit >> 1)); //
	      } else { //
		bcl(solver,   ad->d->lhs_ts[time] >> 1 , -(ad->rhs_tr_fl->lp->head->ptuf->lit >> 1)); //
		bcl(solver, -(ad->d->lhs_ts[time] >> 1),   ad->rhs_tr_fl->lp->head->ptuf->lit >> 1); //
	      } //
	    } //
	  }
	}
	
	ad->d->lhs_ts[time] = ad->rhs_tr_fl->lp->head->ptuf->lit;
	ad->cst = ad->rhs_tr_fl->lp->head->ptuf->lit;
	ad->cst_ref = ad->rhs_tr_fl->lp->head->ptuf->lit_ref;
	//dll_remove_elem(l, ad->dllade);
	dll_remove_elem(ad->dllade);

      } else if (ad->rhs_tr_fl->lp->head->ptuf->op == OR) {
	assert(ad->rhs_tr_fl->lp->head->ptuf->op == OR);
	assert(ad->rhs_tr_fl->lp->head->ptuf->lit == UNDEF_LIT);
	assert(ad->rhs_tr_fl->lp->head->ptuf->lit_ref == NULL);
	ad->rhs_tr_fl->op = OR;
	ad->rhs_tr_fl->lit = UNDEF_LIT;
	ad->rhs_tr_fl->lit_ref = NULL;
	
	ad->rhs_tr_fl->lp = ad->rhs_tr_fl->lp->head->ptuf->lp;

      } else {
	assert(0);
      }

      stats_simp_unroll++;
      return 1;
    }
    return 0;

  } else {
    if (check_cst_ptuf_or(ad->rhs_tr_fl)) {
      assert(ad->d->lhs_ts[time] == ad->lhs);

      if (!ad->rhs_tr_fl->lp->size) {

	if (fb2solver_OPT && ad->added2solver) {
	  aiger_sign(ad->d->lhs_ts[time]) ? ucl(solver, ad->d->lhs_ts[time] >> 1) : ucl(solver, -(ad->d->lhs_ts[time] >> 1));
	  //ucl(-(ad->d->lhs_ts[time] >> 1));
	  stats_fb2solver++;
	}

	ad->d->lhs_ts[time] = 0;
	ad->cst = 0;
	ad->cst_ref = NULL;
	//dll_remove_elem(l, ad->dllade);
	dll_remove_elem(ad->dllade);

      } else if (ad->rhs_tr_fl->lp->head->ptuf->op == VAR) {

	if (fb2solver_OPT && ad->added2solver) {

	  stats_fb2solver++;
	  if (ad->rhs_tr_fl->lp->head->ptuf->lit == 1) {
	    aiger_sign(ad->d->lhs_ts[time]) ? ucl(solver, -(ad->d->lhs_ts[time] >> 1)) : ucl(solver, ad->d->lhs_ts[time] >> 1);
	    //ucl(ad->d->lhs_ts[time] >> 1);
	    
	  } else {
	    assert(ad->rhs_tr_fl->lp->head->ptuf->lit > 1);

	    if (!aiger_sign(ad->d->lhs_ts[time])) { //
	      if (aiger_sign(ad->rhs_tr_fl->lp->head->ptuf->lit)) {
		bcl(solver,   ad->d->lhs_ts[time] >> 1 ,   ad->rhs_tr_fl->lp->head->ptuf->lit >> 1);
		bcl(solver, -(ad->d->lhs_ts[time] >> 1), -(ad->rhs_tr_fl->lp->head->ptuf->lit >> 1));
	      } else {
		bcl(solver,   ad->d->lhs_ts[time] >> 1 , -(ad->rhs_tr_fl->lp->head->ptuf->lit >> 1));
		bcl(solver, -(ad->d->lhs_ts[time] >> 1),   ad->rhs_tr_fl->lp->head->ptuf->lit >> 1);
	      }
	    } else { //
	      if (!aiger_sign(ad->rhs_tr_fl->lp->head->ptuf->lit)) { // 
		bcl(solver,   ad->d->lhs_ts[time] >> 1 ,   ad->rhs_tr_fl->lp->head->ptuf->lit >> 1);//
		bcl(solver, -(ad->d->lhs_ts[time] >> 1), -(ad->rhs_tr_fl->lp->head->ptuf->lit >> 1));//
	      } else {//
		bcl(solver,   ad->d->lhs_ts[time] >> 1 , -(ad->rhs_tr_fl->lp->head->ptuf->lit >> 1));//
		bcl(solver, -(ad->d->lhs_ts[time] >> 1),   ad->rhs_tr_fl->lp->head->ptuf->lit >> 1);//
	      }//
	    }//
	  }
	}

	ad->d->lhs_ts[time] = ad->rhs_tr_fl->lp->head->ptuf->lit;
	ad->cst = ad->rhs_tr_fl->lp->head->ptuf->lit;
	ad->cst_ref = ad->rhs_tr_fl->lp->head->ptuf->lit_ref;

	//dll_remove_elem(l, ad->dllade);
	dll_remove_elem(ad->dllade);

      } else if (ad->rhs_tr_fl->lp->head->ptuf->op == AND) {
	assert(ad->rhs_tr_fl->lp->head->ptuf->op == AND);
	assert(ad->rhs_tr_fl->lp->head->ptuf->lit == UNDEF_LIT);
	assert(ad->rhs_tr_fl->lp->head->ptuf->lit_ref == NULL);
	ad->rhs_tr_fl->op = AND;
	ad->rhs_tr_fl->lit = UNDEF_LIT;
	ad->rhs_tr_fl->lit_ref = NULL;
	
	ad->rhs_tr_fl->lp = ad->rhs_tr_fl->lp->head->ptuf->lp;

      } else {
	assert(0);
      }

      stats_simp_unroll++;
      return 1;
    }
    return 0;
  }
}

bool simp_prop_or_ptuf(std::list<unsigned> lit2prop_, prop_tr_un_fl *ptuf, bool is_or);

bool simp_prop_and_ptuf(std::list<unsigned> lit2prop_, prop_tr_un_fl *ptuf, bool is_and) {
  std::list<unsigned> lit2prop(lit2prop_);
  std::list<unsigned>::iterator it_list;
  bool simplified;
  bool prop = 0;
  list_ptuf_e *it = ptuf->lp->head;
  while(it != NULL) {
    if (it->ptuf->op == VAR) {
      simplified = 0;

      for(it_list = lit2prop_.begin(); it_list != lit2prop_.end(); ++it_list) {
	if (aiger_lit2var(*it_list) == aiger_lit2var(it->ptuf->lit)) {
	  if (*it_list == it->ptuf->lit)
	    it->ptuf->lit = 1;
	  else
	    it->ptuf->lit = 0;
	  it->ptuf->lit_ref = NULL;
	  simplified = 1;
	  prop = 1;
	}
      }
      if (is_and && !simplified)
	lit2prop.push_back(it->ptuf->lit);

    } else if (it->ptuf->op == AND) {
      assert(!is_and);
      prop |= simp_prop_and_ptuf(lit2prop, it->ptuf, 1);

    } else {
      assert(is_and);
      prop |= simp_prop_and_ptuf(lit2prop, it->ptuf, 0);
    }
    
    it = it->next;
  }
  return prop;
}


bool simp_prop_and(dll_list<ands_def> *l, ands_def *ad, int time) {
  // Prop:
  // a_1 && .. && a_n && (~a_i || .. || ~a_j || b_1 || .. || b_m) && .. 
  // .. && (~a_k || .. || ~a_l || b_1 || .. || b_p)
  // [[1 <= i < j <= n and 1 <= k < l <= n ]]
  // =>
  // a_1 && .. && a_n && (b_1 || .. || b_m) && .. && (b_1 || .. || b_p)
  //
  // Simp:
  // a_1 && .. && a_n && (a_i || b_1 || .. || b_m) && .. 
  // .. && (a_j || b_1 || .. || b_p)
  // [[1 <= i <= n and 1 <= j <= n ]]
  // =>
  // a_1 && .. && a_n
  bool ret = simp_prop_and_ptuf(std::list<unsigned>(), ad->rhs_tr_fl, 1);
  ret |= simp_prop_or_ptuf(std::list<unsigned>(), ad->rhs_tr_fl, 0);
  return ret;
}

bool simp_prop_or_ptuf(std::list<unsigned> lit2prop_, prop_tr_un_fl *ptuf, bool is_or) {
  std::list<unsigned> lit2prop(lit2prop_);
  std::list<unsigned>::iterator it_list;
  bool simplified;
  bool prop = 0;
  list_ptuf_e *it = ptuf->lp->head;
  while(it != NULL) {
    if (it->ptuf->op == VAR) {
      simplified = 0;

      for(it_list = lit2prop_.begin(); it_list != lit2prop_.end(); ++it_list) {
	if (aiger_lit2var(*it_list) == aiger_lit2var(it->ptuf->lit)) {
	  if (*it_list == it->ptuf->lit) {
	    it->ptuf->lit = 0;
	    it->ptuf->lit_ref = NULL;
	    simplified = 1;
	    prop = 1;
	  } else {
	    it->ptuf->lit = 1;
	    it->ptuf->lit_ref = NULL;
	    simplified = 1;
	    prop = 1;
	    //printf("%d <> %d\n", *it_list, it->ptuf->lit);
	    //assert(0);
	  }
	} 
      }
      if (is_or && !simplified)
	lit2prop.push_back(it->ptuf->lit);

    } else if (it->ptuf->op == AND) {
      assert(is_or);
      prop |= simp_prop_or_ptuf(lit2prop, it->ptuf, 0);

    } else {
      assert(!is_or);
      prop |= simp_prop_or_ptuf(lit2prop, it->ptuf, 1);
    }
    
    it = it->next;
  }
  return prop;
}

bool simp_prop_or(dll_list<ands_def> *l, ands_def *ad, int time) {
  bool ret = simp_prop_or_ptuf(std::list<unsigned>(), ad->rhs_tr_fl, 1);
  ret |= simp_prop_and_ptuf(std::list<unsigned>(), ad->rhs_tr_fl, 0);
  return ret;
}

bool simp_prop(dll_list<ands_def> *l, ands_def *ad, int time) {
  bool simp_p = 0;
  // simp AND
  if (ad->rhs_tr_fl->op == AND) {
    simp_p |= simp_prop_and(l, ad, time);
  }

  // simp OR
  if (ad->rhs_tr_fl->op == OR) {
    simp_p |= simp_prop_or(l, ad, time);
  }
  return simp_p;
}



void build_funcA_cst(ands_def *ad, prop_tr_un_fl *ptuf) {
  if (ptuf->op == VAR) {
    ad->funcA_cst.insert(ptuf->lit);
    if (ptuf->lit_ref != NULL) {
      if (aiger_sign(ptuf->lit) != aiger_sign(ptuf->lit_ref->lhs)) {
	if (ptuf->lit_ref->rhs_tr_fl->op == OR)
	  for(std::set<unsigned>::iterator it_or = ptuf->lit_ref->funcO_cst.begin();
	      it_or != ptuf->lit_ref->funcO_cst.end(); it_or++)
	    ad->funcA_cst.insert(aiger_not(*it_or));
      } else {
	ad->funcA_cst.insert(ptuf->lit_ref->funcA_cst.begin(), ptuf->lit_ref->funcA_cst.end());
      }
    }	
  } else if (ptuf->op == AND) {
    list_ptuf_e *it = ptuf->lp->head;
    while(it != NULL) {
      build_funcA_cst(ad, it->ptuf);
      it = it->next;
    }
  } else {
    assert(ptuf->op == OR);
    // Do nothing..
  }
}

void build_funcO_cst(ands_def *ad, prop_tr_un_fl *ptuf) {
  if (ptuf->op == VAR) {
    ad->funcO_cst.insert(ptuf->lit);
    if (ptuf->lit_ref != NULL) {
      if (aiger_sign(ptuf->lit) != aiger_sign(ptuf->lit_ref->lhs)) {
	if (ptuf->lit_ref->rhs_tr_fl->op == AND)
	  for(std::set<unsigned>::iterator it_and = ptuf->lit_ref->funcA_cst.begin();
	      it_and != ptuf->lit_ref->funcA_cst.end(); it_and++)
	    ad->funcO_cst.insert(aiger_not(*it_and));
      } else {	
	ad->funcO_cst.insert(ptuf->lit_ref->funcO_cst.begin(), ptuf->lit_ref->funcO_cst.end());
      }
    }
  } else if (ptuf->op == OR) {
    list_ptuf_e *it = ptuf->lp->head;
    while(it != NULL) {
      build_funcO_cst(ad, it->ptuf);
      it = it->next;
    }
  } else {
    assert(ptuf->op == AND);
    // Do nothing..
  }
}

void build_funcEq(ands_def *ad, prop_tr_un_fl *ptuf) {
  if (ptuf->op == AND) {
    ad->op_and = 1;
  } else if (ptuf->op == OR) {
    ad->op_and = 0;
  }

  list_ptuf_e *it = ptuf->lp->head;
  while(it != NULL) {
    if (it->ptuf->op == VAR) {
      if (it->ptuf->lit_ref != NULL &&
	  it->ptuf->lit_ref->func_eq.size() > 0 && 
	  ((aiger_sign(it->ptuf->lit) && it->ptuf->lit_ref->op_and != ad->op_and) ||
	   (!aiger_sign(it->ptuf->lit) && it->ptuf->lit_ref->op_and == ad->op_and))
	  ) {
	ad->func_eq.insert(it->ptuf->lit_ref->func_eq.begin(), it->ptuf->lit_ref->func_eq.end());
      } else {
	ad->func_eq.insert(it->ptuf->lit);
      }
      
      it = it->next;
    } else {
      ad->func_eq.clear();
      it = NULL;
    }
  }
}


bool check_func_contra(ands_def *ad) {
  bool is_cst = 0;
  if (ad->funcA_cst.size() > 1) {
    unsigned current;
    std::set<unsigned>::iterator it_ad;
    for(it_ad = ad->funcA_cst.begin(); !is_cst && it_ad != (--ad->funcA_cst.end());) {
      current = *it_ad;
      it_ad++;
      if (aiger_lit2var(*it_ad) == aiger_lit2var(current)) {
	is_cst = 1;

	assert(ad->rhs_tr_fl->op == AND);
	assert(ad->rhs_tr_fl->lp->head->ptuf->op == VAR);
	ad->rhs_tr_fl->lp->head->ptuf->lit = 0;
	ad->rhs_tr_fl->lp->head->ptuf->lit_ref = NULL;

	stats_simp_adv_contra++;
      }
    }
  }
  return is_cst;
}

bool check_func_tauto(ands_def *ad) {
  bool is_cst = 0;
  if (ad->funcO_cst.size() > 1) {
    unsigned current;
    std::set<unsigned>::iterator it_ad;
    for(it_ad = ad->funcO_cst.begin(); !is_cst && it_ad != (--ad->funcO_cst.end());) {
      current = *it_ad;
      it_ad++;
      if (aiger_lit2var(*it_ad) == aiger_lit2var(current)) {
	is_cst = 1;      

	assert(ad->rhs_tr_fl->op == OR);
	assert(ad->rhs_tr_fl->lp->head->ptuf->op == VAR);
	list_ptuf_e *it = ad->rhs_tr_fl->lp->head;
	ad->rhs_tr_fl->lp->head->ptuf->lit = 1;
	ad->rhs_tr_fl->lp->head->ptuf->lit_ref = NULL;

	stats_simp_adv_tauto++;
      }
    }
  }
  return is_cst;
}



bool simp_funcO_subs(prop_tr_un_fl *ptuf, std::set<unsigned> dom) {
  bool ret = 0;
  if (dom.size() > 0) {
    // Check if there is any simplification that can be applied with current dominators list
    list_ptuf_e *it = ptuf->lp->head;
    while(it != NULL) {
      if (it->ptuf->op == VAR) {

	// Check if lit belongs to dominators
	std::set<unsigned>::iterator it_dom = dom.find(it->ptuf->lit);
	if (it_dom != dom.end()) {
	    it->ptuf->lit = 0;
	    it->ptuf->lit_ref = NULL;
	  if (ptuf->op == AND)
	    return 1;
	  ret = 1;
	} 
	it_dom = dom.find(aiger_not(it->ptuf->lit));
	if (it_dom != dom.end()) {
	  it->ptuf->lit = 1;
	  it->ptuf->lit_ref = NULL;
	  if (ptuf->op == OR)
	    return 1;
	  ret = 1;
	}

	if (it->ptuf->lit_ref != NULL) {
	  // Check if lit and its func belongs to dominators

	  std::set<unsigned>::iterator it_dom = dom.begin();
	  std::set<unsigned>::iterator it_op;
	  std::set<unsigned>::iterator it_op_end;


	  if (it->ptuf->lit_ref->rhs_tr_fl->op == AND) {
	    it_op = it->ptuf->lit_ref->funcA_cst.begin();
	    it_op_end = it->ptuf->lit_ref->funcA_cst.end();
	  } else {
	    assert(it->ptuf->lit_ref->rhs_tr_fl->op == OR);
	    it_op = it->ptuf->lit_ref->funcO_cst.begin();
	    it_op_end = it->ptuf->lit_ref->funcO_cst.end();
	  }

	  while(it_dom != dom.end() && it_op != it_op_end) {
	    if (aiger_lit2var(*it_dom) == aiger_lit2var(*it_op)) {
	      if (*it_dom != *it_op) {
		if (it->ptuf->lit_ref->rhs_tr_fl->op == AND) {
		  // Do nothing..
		  it_dom++;
		  it_op++;

		} else {
		  if (aiger_sign(it->ptuf->lit)) {
		    it->ptuf->lit = 0;
		    it->ptuf->lit_ref = NULL;
		    if (ptuf->op == AND)
		      return 1;
		    ret = 1;
		    it_dom = dom.end();

		  } else {
		    it->ptuf->lit = 1;
		    it->ptuf->lit_ref = NULL;
		    if (ptuf->op == OR)
		      return 1;
		    ret = 1;
		    it_dom = dom.end();
		  }
		}
	      } else {
		if (it->ptuf->lit_ref->rhs_tr_fl->op == AND) {
		  if (aiger_sign(it->ptuf->lit)) {
		    it->ptuf->lit = 1;
		    it->ptuf->lit_ref = NULL;
		    if (ptuf->op == OR)
		      return 1;
		    ret = 1;
		    it_dom = dom.end();

		  } else {
		    it->ptuf->lit = 0;
		    it->ptuf->lit_ref = NULL;
		    if (ptuf->op == AND)
		      return 1;
		    ret = 1;
		    it_dom = dom.end();
		  }
		} else {
		  // Do nothing..
		  it_dom++;
		  it_op++;
		}
	      }
	    } else if (aiger_lit2var(*it_dom) < aiger_lit2var(*it_op))
	      it_dom++;
	    else
	      it_op++;
	  }
	}
      }
      it = it->next;
    }
  }
  
  
  
  if (ptuf->op == OR) {
    // Build next dominators list
    list_ptuf_e *it = ptuf->lp->head;
    while(it != NULL) {
      assert(it->ptuf->op != OR);
      if (it->ptuf->op == VAR) {
	dom.insert(it->ptuf->lit);
	if (it->ptuf->lit_ref != NULL) {
	  if (aiger_sign(it->ptuf->lit) != aiger_sign(it->ptuf->lit_ref->lhs)) {
	    if (it->ptuf->lit_ref->rhs_tr_fl->op == AND)
	      for(std::set<unsigned>::iterator it_and = it->ptuf->lit_ref->funcA_cst.begin();
		  it_and != it->ptuf->lit_ref->funcA_cst.end(); it_and++) 
		dom.insert(aiger_not(*it_and));

	  } else
	    dom.insert(it->ptuf->lit_ref->funcO_cst.begin(), it->ptuf->lit_ref->funcO_cst.end());
	}
      }      
      it = it->next;
    }
    
    // Call recursively on the AND nodes
    it = ptuf->lp->head;
    while(it != NULL) {
      if (it->ptuf->op == AND) {
	ret |= simp_funcO_subs(it->ptuf, dom);
      } else {
	assert(it->ptuf->op == VAR);
      }
      it = it->next;
    }
    
    
  } else if (ptuf->op == AND) {
    // Call recursively on the OR nodes
    list_ptuf_e *it = ptuf->lp->head;
    while(it != NULL) {
      if (it->ptuf->op == OR) {
	ret |= simp_funcO_subs(it->ptuf, dom);
      } else {
	assert(it->ptuf->op == VAR);
      }
      it = it->next;
    }
  }
  return ret;
}


bool simp_funcA_subs(prop_tr_un_fl *ptuf, std::set<unsigned> dom) {
  bool ret = 0;
  if (dom.size() > 0) {
    // Check if there is any simplification that can be applied with current dominators list
    list_ptuf_e *it = ptuf->lp->head;
    while(it != NULL) {
      if (it->ptuf->op == VAR) {
	
	// Check if lit belongs to dominators
	std::set<unsigned>::iterator it_dom = dom.find(it->ptuf->lit);
	if (it_dom != dom.end()) {
	  it->ptuf->lit = 1;
	  it->ptuf->lit_ref = NULL;
	  if (ptuf->op == OR)
	    return 1;
	  ret = 1;
	} 
	it_dom = dom.find(aiger_not(it->ptuf->lit));
	if (it_dom != dom.end()) {
	  it->ptuf->lit = 0;
	  it->ptuf->lit_ref = NULL;
	  if (ptuf->op == AND)
	    return 1;
	  ret = 1;
	  
	}

	if (it->ptuf->lit_ref != NULL) {
	  // Check if lit and its func belongs to dominators

	  std::set<unsigned>::iterator it_dom = dom.begin();
	  std::set<unsigned>::iterator it_op;
	  std::set<unsigned>::iterator it_op_end;
	  

	  if (it->ptuf->lit_ref->rhs_tr_fl->op == AND) {
	    it_op = it->ptuf->lit_ref->funcA_cst.begin();
	    it_op_end = it->ptuf->lit_ref->funcA_cst.end();
	  } else {
	    assert(it->ptuf->lit_ref->rhs_tr_fl->op == OR);
	    it_op = it->ptuf->lit_ref->funcO_cst.begin();
	    it_op_end = it->ptuf->lit_ref->funcO_cst.end();
	  }

	  while(it_dom != dom.end() && it_op != it_op_end) {
	    if (aiger_lit2var(*it_dom) == aiger_lit2var(*it_op)) {
	      if (*it_dom != *it_op) {
		if (it->ptuf->lit_ref->rhs_tr_fl->op == AND) {
		  if (aiger_sign(it->ptuf->lit)) {
		    it->ptuf->lit = 1;
		    it->ptuf->lit_ref = NULL;
		    if (ptuf->op == OR)
		      return 1;
		    ret = 1;
		    it_dom = dom.end();

		  } else {
		    it->ptuf->lit = 0;
		    it->ptuf->lit_ref = NULL;
		    if (ptuf->op == AND)
		      return 1;
		    ret = 1;
		    it_dom = dom.end();
		  }
		} else {
		  // Do nothing..
		  it_dom++;
		  it_op++;
		}
	      } else {
		if (it->ptuf->lit_ref->rhs_tr_fl->op == AND) {
		  // Do nothing..
		  it_dom++;
		  it_op++;
		} else {
		  if (aiger_sign(it->ptuf->lit)) {
		    it->ptuf->lit = 0;
		    it->ptuf->lit_ref = NULL;
		    if (ptuf->op == AND)
		      return 1;
		    ret = 1;
		    it_dom = dom.end();

		  } else {
		    it->ptuf->lit = 1;
		    it->ptuf->lit_ref = NULL;
		    if (ptuf->op == OR)
		      return 1;
		    ret = 1;
		    it_dom = dom.end();
		  }
		}
	      }
	    } else if (aiger_lit2var(*it_dom) < aiger_lit2var(*it_op))
	      it_dom++;
	    else
	      it_op++;
	  }
	}
      }
      it = it->next;
    }
  }
  
  
  
  if (ptuf->op == AND) {
    // Build next dominators list
    list_ptuf_e *it = ptuf->lp->head;
    while(it != NULL) {
      assert(it->ptuf->op != AND);
      if (it->ptuf->op == VAR) {
	dom.insert(it->ptuf->lit);
	if (it->ptuf->lit_ref != NULL) {
	  if (aiger_sign(it->ptuf->lit) != aiger_sign(it->ptuf->lit_ref->lhs)) {
	    if (it->ptuf->lit_ref->rhs_tr_fl->op == OR)
	      for(std::set<unsigned>::iterator it_or = it->ptuf->lit_ref->funcO_cst.begin();
		  it_or != it->ptuf->lit_ref->funcO_cst.end(); it_or++)
		dom.insert(aiger_not(*it_or));
	  
	  } else
	    dom.insert(it->ptuf->lit_ref->funcA_cst.begin(), it->ptuf->lit_ref->funcA_cst.end());
	}
      }
      it = it->next;
    }
    
    // Call recursively on the OR nodes
    it = ptuf->lp->head;
    while(it != NULL) {
      if (it->ptuf->op == OR) {
	ret |= simp_funcA_subs(it->ptuf, dom);
      } else {
	assert(it->ptuf->op == VAR);
      }
      it = it->next;
    }
    
    
  } else if (ptuf->op == OR) {
    // Call recursively on the AND nodes
    list_ptuf_e *it = ptuf->lp->head;
    while(it != NULL) {
      if (it->ptuf->op == AND) {
	ret |= simp_funcA_subs(it->ptuf, dom);
      } else {
	assert(it->ptuf->op == VAR);
      }
      it = it->next;
    }
  }
  return ret;
}

bool simp_adv_cst(ands_def *ad) {
  if (ad->rhs_tr_fl->lp->size == 2) {
    list_ptuf_e *it = ad->rhs_tr_fl->lp->head;
    list_ptuf_e *it_next = it->next;
    if (it->ptuf->op == it_next->ptuf->op && it_next->ptuf->op != VAR) {
      if (it->ptuf->lp->size == 2 && it_next->ptuf->lp->size == 2) {
	if (it->ptuf->lp->head->ptuf->op == VAR && it_next->ptuf->lp->head->ptuf->op == VAR) {
	  if (it->ptuf->lp->head->next->ptuf->op == VAR && it_next->ptuf->lp->head->next->ptuf->op == VAR) {
	    unsigned it_op1 = it->ptuf->lp->head->ptuf->lit;
	    unsigned it_op2 = it->ptuf->lp->head->next->ptuf->lit;
	    unsigned it_next_op1 = it_next->ptuf->lp->head->ptuf->lit;
	    unsigned it_next_op2 = it_next->ptuf->lp->head->next->ptuf->lit;

	    if (it_op1 == it_next_op1 && it_op2 == it_next_op2) {
	      remove(ad->rhs_tr_fl->lp, it_next);
	      return 1;
	      
	    } else if (it_op1 == it_next_op1 && aiger_lit2var(it_op2) == aiger_lit2var(it_next_op2)) {
	      it->ptuf->lp->head->next->ptuf->lit = it_op1;
	      it->ptuf->lp->head->next->ptuf->lit_ref = it->ptuf->lp->head->ptuf->lit_ref;
	      it_next->ptuf->lp->head->next->ptuf->lit = it_op1;
	      it_next->ptuf->lp->head->next->ptuf->lit_ref = it->ptuf->lp->head->ptuf->lit_ref;
	      return 1;

	    } else if (it_op2 == it_next_op2 && aiger_lit2var(it_op1) == aiger_lit2var(it_next_op1)) {
	      it->ptuf->lp->head->ptuf->lit = it_op2;
	      it->ptuf->lp->head->ptuf->lit_ref = it->ptuf->lp->head->next->ptuf->lit_ref;
	      it_next->ptuf->lp->head->ptuf->lit = it_op2;
	      it_next->ptuf->lp->head->ptuf->lit_ref = it->ptuf->lp->head->next->ptuf->lit_ref;
	      return 1;

	    }
	  }
	}
      }
    }
  }
  return 0;
}

bool simp_adv(SimpSolver* solver, ands_def *ad, int time) {
  bool cont = 0;
  if (simp_adv_cst(ad)) {
    cont = 1;
    check_cst(solver, ad, time);
    stats_simp_adv_cst++;
  }

  if (ad->cst == -1) {
    ad->funcA_cst.clear();
    ad->funcO_cst.clear();
    //ad->func_eq.clear();
    build_funcA_cst(ad, ad->rhs_tr_fl);
    build_funcO_cst(ad, ad->rhs_tr_fl);
    //build_funcEq(ad, ad->rhs_tr_fl);
    
    //if (ad->funcA_cst.size() > bigAO_size)
    //map_bigA.insert(std::pair<unsigned, ands_def*>(ad->lhs, ad));
	    
    //if (ad->funcO_cst.size() > bigAO_size)
    //map_bigO.insert(std::pair<unsigned, ands_def*>(ad->lhs, ad));

    if (check_func_contra(ad)) {
      cont = 1;
      check_cst(solver, ad, time);
    }

    if (ad->cst == -1) {
      if (check_func_tauto(ad)) {
	cont = 1;
	check_cst(solver, ad, time);
      }
  
      // if (ad->cst == -1) {
      // 	if(simp_funcA_subs(ad->rhs_tr_fl, std::set<unsigned>())) {
      // 	  cont = 1;
      // 	  stats_simp_adv_funcA_subs++;
      // 	  check_cst(ad, time);
      // 	}
		
      // 	if (ad->cst == -1) {
      // 	  if(simp_funcO_subs(ad->rhs_tr_fl, std::set<unsigned>())) {
      // 	    cont = 1;
      // 	    stats_simp_adv_funcO_subs++;
      // 	    check_cst(ad, time);
      // 	  }
      // 	}
      // }
    }
  }
  //if (cont) assert(0);
  return cont;
}



bool struct_equiv_global(SimpSolver* solver, ands_def *ad, unsigned hash, int time) {
  ands_def *struct_equiv = check_struct_equiv_global(ad->rhs_tr_fl, hash);

  if (struct_equiv != NULL) { 
    assert(struct_equiv->cst == -1);
    assert(struct_equiv != ad);
    assert(ad->d->lhs_ts[time] == ad->lhs);
	  
    if (fb2solver_OPT && ad->added2solver) {
      stats_fb2solver++;
      if (aiger_sign(ad->d->lhs_ts[time]) == aiger_sign(struct_equiv->lhs)) { //
	bcl(solver,   ad->d->lhs_ts[time] >> 1 , -(struct_equiv->lhs >> 1));
	bcl(solver, -(ad->d->lhs_ts[time] >> 1),   struct_equiv->lhs >> 1);
      } else { //
	bcl(solver,   ad->d->lhs_ts[time] >> 1 ,   struct_equiv->lhs >> 1); //
	bcl(solver, -(ad->d->lhs_ts[time] >> 1), -(struct_equiv->lhs >> 1)); //
      } //
    }

    ad->d->lhs_ts[time] = struct_equiv->lhs;
    ad->cst = struct_equiv->lhs;
    ad->cst_ref = struct_equiv;
    //dll_remove_elem(list_ands[time], ad->dllade);
    dll_remove_elem(ad->dllade);

    stats_struct_eq_global++;
    return 1;
  }

  return 0;
}

bool struct_equiv(SimpSolver* solver, ands_def *ad, unsigned hash, int time) {
  ands_def *struct_equiv = check_struct_equiv(ad->rhs_tr_fl, hash);

  if (struct_equiv != NULL) { 
    assert(struct_equiv->cst == -1);
    assert(struct_equiv != ad);
    assert(ad->d->lhs_ts[time] == ad->lhs);

    if (fb2solver_OPT && ad->added2solver) {
      stats_fb2solver++;
      if (aiger_sign(ad->d->lhs_ts[time]) == aiger_sign(struct_equiv->lhs)) { //
	bcl(solver,   ad->d->lhs_ts[time] >> 1 , -(struct_equiv->lhs >> 1));
	bcl(solver, -(ad->d->lhs_ts[time] >> 1),   struct_equiv->lhs >> 1);
      } else { //
	bcl(solver,   ad->d->lhs_ts[time] >> 1 ,   struct_equiv->lhs >> 1); //
	bcl(solver, -(ad->d->lhs_ts[time] >> 1), -(struct_equiv->lhs >> 1)); //
      } //
    }

    ad->d->lhs_ts[time] = struct_equiv->lhs;
    ad->cst = struct_equiv->lhs;
    ad->cst_ref = struct_equiv;
    //dll_remove_elem(list_ands[time], ad->dllade);
    dll_remove_elem(ad->dllade);


    stats_struct_eq++;
    return 1;
  }

  return 0;
}

bool struct_diff_global(SimpSolver* solver, ands_def *ad, unsigned hash_not, int time) {
  ands_def *struct_diff = check_struct_diff_global(ad->rhs_tr_fl, hash_not);

  if (struct_diff != NULL) {
    assert(struct_diff->cst == -1);
    assert(struct_diff != ad);
    assert(ad->d->lhs_ts[time] == ad->lhs);
	    
    if (fb2solver_OPT && ad->added2solver) {
      stats_fb2solver++;
      if (aiger_sign(ad->d->lhs_ts[time]) == aiger_sign(struct_diff->lhs)) { //
	bcl(solver,   ad->d->lhs_ts[time] >> 1 ,   struct_diff->lhs >> 1);
	bcl(solver, -(ad->d->lhs_ts[time] >> 1), -(struct_diff->lhs >> 1));
      } else { //
	bcl(solver,   ad->d->lhs_ts[time] >> 1 , -(struct_diff->lhs >> 1)); //
	bcl(solver, -(ad->d->lhs_ts[time] >> 1),   struct_diff->lhs >> 1); //
      } //
    }
	    
    ad->d->lhs_ts[time] = aiger_not(struct_diff->lhs);
    ad->cst = aiger_not(struct_diff->lhs);
    ad->cst_ref = struct_diff;
    //dll_remove_elem(list_ands[time], ad->dllade);
    dll_remove_elem(ad->dllade);
	    
    stats_struct_diff_global++;
    return 1;
  }

  return 0;
}


bool propagate_def_cst(SimpSolver* solver, ands_def* ad, unsigned cst, bool fb) {
  assert(cst <= 1);
  //printf("%d %d\n", ad->lhs, cst);
  if (ad->cst == -1) {
    int index2prop = -1;
    for (int j = 0; j < ad->d->lhs_ts.size() && index2prop == -1; j++)
      if (ad->d->lhs_ts[j] == ad->lhs)
  	index2prop = j;
    
    assert(index2prop != -1);
    
    ad->d->lhs_ts[index2prop] = cst;
    ad->cst = cst;
    ad->cst_ref = NULL;
    
    dll_remove_elem(ad->dllade);
    
    if (fb && fb2solver_OPT && ad->added2solver) {
      aiger_sign(ad->lhs) ?
  	cst ? ucl(solver, -(ad->lhs >> 1)) : ucl(solver, ad->lhs >> 1) :
  	cst ? ucl(solver, ad->lhs >> 1) : ucl(solver, -(ad->lhs >> 1));
      stats_fb2solver++;
    }
    
    return 1;

  } else if (ad->cst <= 1) {
    // Def to propagate is already a cst
    // Check consistency
    assert(ad->cst == cst);
    
  } else {
    // Def to propagate is already an alias
    if (ad->cst_ref == NULL) {
      // TODO: Propagate the alias which is an input or a latch..
      // How to retrieve the idef ?

    } else {

      return aiger_sign(ad->cst) == aiger_sign(ad->cst_ref->lhs) ?
	propagate_def_cst(solver, ad->cst_ref, cst, 1) :
	propagate_def_cst(solver, ad->cst_ref, 1 - cst, 1);
    }
  }
  return 0;
}

bool propagate_def_alias(SimpSolver* solver, std::map<unsigned, ands_def*> &map_ands, ands_def* ad, unsigned cst, bool fb) {
  assert(cst > 1);
  //printf("B: %d %d %d\n", ad->lhs, ad->cst, cst);  
  
  std::map<unsigned, ands_def*>::iterator it_cst_def = map_ands.find(aiger_lit2var(cst));
  
  //if (it_cst_def != map_ands.end() && it_cst_def->second->cst_ref != NULL) {
  if (it_cst_def != map_ands.end() && it_cst_def->second->cst != -1) {
    //printf("TEST C : %d %d\n", ad->cycle, it_cst_def->second->cycle);
    cst = (aiger_sign(it_cst_def->second->lhs) == aiger_sign(cst)) ?
      it_cst_def->second->cst : aiger_not(it_cst_def->second->cst);
    it_cst_def = map_ands.find(aiger_lit2var(cst));
  }
  

  //printf("A: %d %d %d\n", ad->lhs, ad->cst, cst);
  if (ad->lhs == cst || ad->cst == cst)
    return 0;
  if (it_cst_def != map_ands.end())
    if (aiger_lit2var(it_cst_def->second->cst) == aiger_lit2var(ad->lhs)) {
      aiger_sign(it_cst_def->second->lhs) == aiger_sign(cst) ?
	assert(it_cst_def->second->cst == ad->lhs) :
	assert(aiger_not(it_cst_def->second->cst) == ad->lhs);
      return 0;

    } else {
      //printf("TEST C : %d %d\n", ad->cycle, it_cst_def->second->cycle);
      //assert(ad->cycle <= it_cst_def->second->cycle);
    }
  assert(aiger_lit2var(ad->lhs) != aiger_lit2var(cst));
  assert(aiger_lit2var(ad->cst) != aiger_lit2var(cst));

  if (ad->cst == -1) {
    int index2prop = -1;
    for (int j = 0; j < ad->d->lhs_ts.size() && index2prop == -1; j++)
      if (ad->d->lhs_ts[j] == ad->lhs)
	index2prop = j;

    assert(index2prop != -1);
      
    ad->d->lhs_ts[index2prop] = cst;
    ad->cst = cst;
      
    if(it_cst_def != map_ands.end())
      ad->cst_ref = it_cst_def->second;
    else
      ad->cst_ref = NULL;

    dll_remove_elem(ad->dllade);

    if (fb && fb2solver_OPT && ad->added2solver) {
      aiger_sign(ad->lhs) != aiger_sign(cst) ? bcl(solver,   ad->lhs >> 1,    cst >> 1)  : bcl(solver,   ad->lhs >> 1, -(cst >> 1));
      aiger_sign(ad->lhs) != aiger_sign(cst) ? bcl(solver, -(ad->lhs >> 1), -(cst >> 1)) : bcl(solver, -(ad->lhs >> 1),  cst >> 1);
      stats_fb2solver++;
    }

    return 1;
    
  } else if (ad->cst <= 1) {
    // Def to propagate is already a cst
    if (it_cst_def != map_ands.end()) {
      if (it_cst_def->second->cst != -1) {
	if (it_cst_def->second->cst <= 1) {
	  // Check consistency
	  if (aiger_sign(it_cst_def->second->lhs) == aiger_sign(cst)) {
	    assert(ad->cst == it_cst_def->second->cst);
	  } else {
	    assert(ad->cst == aiger_not(it_cst_def->second->cst));
	  }
      
	} else {
	  return aiger_sign(it_cst_def->second->lhs) == aiger_sign(cst) ?
	    propagate_def_cst(solver, it_cst_def->second, ad->cst, 1) :
	    propagate_def_cst(solver, it_cst_def->second, 1 - ad->cst, 1);
	}
      } else {
	assert(0); // it_cst_def is always a constant (?)
      }
    } else {
      // Do nothing
    }

  } else { 
    // Def to propagate is already an alias
    if (ad->cst_ref != NULL && it_cst_def != map_ands.end()) {
      if (ad->cst_ref->cycle == it_cst_def->second->cycle) {
	//if (ad->cst_ref->pos > it_cst_def->second->pos) {
	if (ad->cst_ref->lhs > it_cst_def->second->lhs) {
	  return aiger_sign(ad->cst) == aiger_sign(ad->cst_ref->lhs) ? 
	    propagate_def_alias(solver, map_ands, ad->cst_ref, cst, 1) :
	    propagate_def_alias(solver, map_ands, ad->cst_ref, aiger_not(cst), 1);
	} else {

	  return aiger_sign(cst) == aiger_sign(it_cst_def->second->lhs) ? 
	    propagate_def_alias(solver, map_ands, it_cst_def->second, ad->cst, 1) :
	    propagate_def_alias(solver, map_ands, it_cst_def->second, aiger_not(ad->cst), 1);
	  //assert(0);
	}
      } else if (ad->cst_ref->cycle < it_cst_def->second->cycle) {
	return aiger_sign(ad->cst) == aiger_sign(ad->cst_ref->lhs) ? 
	  propagate_def_alias(solver, map_ands, ad->cst_ref, cst, 1) :
	  propagate_def_alias(solver, map_ands, ad->cst_ref, aiger_not(cst), 1);

      } else {
	// Do nothing

	// return aiger_sign(cst) == aiger_sign(it_cst_def->second->lhs) ? 
	//   propagate_def_alias(it_cst_def->second, ad->cst, 1) :
	//   propagate_def_alias(it_cst_def->second, aiger_not(ad->cst), 1);
      }
    }

  }
  
  return 0;
}



bool struct_diff(SimpSolver* solver, ands_def *ad, unsigned hash_not, int time) {
  ands_def *struct_diff = check_struct_diff(ad->rhs_tr_fl, hash_not);

  if (struct_diff != NULL) {
    assert(struct_diff->cst == -1);
    assert(struct_diff != ad);
    assert(ad->d->lhs_ts[time] == ad->lhs);
	    
    if (fb2solver_OPT && ad->added2solver) {
      stats_fb2solver++;
      if (aiger_sign(ad->d->lhs_ts[time]) == aiger_sign(struct_diff->lhs)) { //
	bcl(solver,   ad->d->lhs_ts[time] >> 1 ,   struct_diff->lhs >> 1);
	bcl(solver, -(ad->d->lhs_ts[time] >> 1), -(struct_diff->lhs >> 1));
      } else { //
	bcl(solver,   ad->d->lhs_ts[time] >> 1 , -(struct_diff->lhs >> 1)); //
	bcl(solver, -(ad->d->lhs_ts[time] >> 1),   struct_diff->lhs >> 1); //
      } //
    }
	    
    ad->d->lhs_ts[time] = aiger_not(struct_diff->lhs);
    ad->cst = aiger_not(struct_diff->lhs);
    ad->cst_ref = struct_diff;
    //dll_remove_elem(list_ands[time], ad->dllade);
    dll_remove_elem(ad->dllade);

    stats_struct_diff++;
    return 1;
  }

  return 0;
}


void simp_ands(SimpSolver* solver, std::vector<dll_list<ands_def>*> &list_ands, std::vector<idef*> &mapdef, std::map<unsigned, ands_def*> &map_ands, int time, unsigned bad_lit, bool bwd, bool verbose, bool elim_ll) {
  ands_def* tmp_def;
  dll_elem<ands_def> *tmp_e;

  if (elim_ll) {
    if (verbose) printf("[simp]  Warning : Global structural is disabled because of varelim!\n");
    struct_equiv_global_OPT  = 0;
    struct_diff_global_OPT   = 0;
  }
  
  bool cont;
  bool loop_defs = 1;
  int cpt_pass = 1;
  int nands = dll_size(list_ands[time]);

  while(loop_defs) {
    // Ands def
    loop_defs = 0;
    hash_struct_equiv.clear();

    tmp_e = list_ands[time]->head;
    while(tmp_e != NULL) {
      tmp_def = tmp_e->def;
      cont = 0;

      assert(tmp_def->lhs > 1);
      //assert(!aiger_sign(tmp_def->lhs));

      //if (tmp_def->lhs == )
      //print_ad_ptuf(tmp_def);

#ifndef NDEBUG
      check_def_consistency(tmp_def->rhs_tr_fl);
#endif

      cont |= propagate_cst(tmp_def->rhs_tr_fl);

      assert(tmp_def->cst == -1);

      cont |= check_cst(solver, tmp_def, time);

      if (tmp_def->cst == -1) 
	if (simp_prop(list_ands[time], tmp_def, time)) {
	  cont = 1;
	  stats_simp_prop++;
	  check_cst(solver, tmp_def, time);
	}
      
      //print_ad_ptuf(tmp_def);
      if (tmp_def->cst == -1 && simp_adv_OPT) {
	cont |= simp_adv(solver, tmp_def, time);
      }


      /*          STRUCT EQUIV/DIFF         */
      unsigned hash = hash_ptuf(tmp_def->rhs_tr_fl);
      unsigned hash_not = hash_not_ptuf(tmp_def->rhs_tr_fl);
      // Struct equiv global
      if (tmp_def->cst == -1 && struct_equiv_global_OPT && struct_equiv_global(solver, tmp_def, hash, time)) {
	cont = 1;
      }
      
      // Struct diff global
      if (tmp_def->cst == -1 && struct_diff_global_OPT && struct_diff_global(solver, tmp_def, hash_not, time)) {
	cont = 1;
      }

      // Struct equiv local
      if (tmp_def->cst == -1 && struct_equiv_OPT && struct_equiv(solver, tmp_def, hash, time)) {
	cont = 1;
      }

      // Struct diff local
      if (tmp_def->cst == -1 && struct_diff_OPT && struct_diff(solver, tmp_def, hash_not, time)) {
	cont = 1;
      }

      /****************************************/

      if (tmp_def->cst == -1 && !cont) {
	unsigned hash = hash_ptuf(tmp_def->rhs_tr_fl);
	hash_struct_equiv.insert(std::pair< unsigned, ands_def* >(hash, tmp_def));
      }

      //print_ad_ptuf_bis(tmp_def);
      //if (tmp_def->lhs == )
      //print_ad_ptuf(tmp_def);

      loop_defs |= cont;
      if (!cont || tmp_def->cst != -1)
	tmp_e = tmp_e->next;
    }
  } 


  
  // Propagate bad state
  if (!bwd) {
    if (mapdef[aiger_lit2var(bad_lit)]->lhs_ts[time] > 1) {
      std::map<unsigned, ands_def*>::iterator it_lit_ref = 
	map_ands.find(aiger_lit2var(mapdef[aiger_lit2var(bad_lit)]->lhs_ts[time]));
      
      if (it_lit_ref != map_ands.end()) {
	assert(!aiger_sign(it_lit_ref->second->lhs));
	
	if (it_lit_ref->second->cst != -1) {
	
	  //print_ad_ptuf(it_lit_ref->second);

	  if (it_lit_ref->second->cst_ref != NULL)
	    assert(it_lit_ref->second->cst_ref->cst == -1); // TODO: add while
	
	  //printf("%d => ", mapdef[aiger_lit2var(bad_lit)]->lhs_ts[time]);
	  if (aiger_sign(mapdef[aiger_lit2var(bad_lit)]->lhs_ts[time]))
	    mapdef[aiger_lit2var(bad_lit)]->lhs_ts[time] = aiger_not(it_lit_ref->second->cst);
	  else
	    mapdef[aiger_lit2var(bad_lit)]->lhs_ts[time] = it_lit_ref->second->cst;
	  //printf("%d (%d)\n", mapdef[aiger_lit2var(bad_lit)]->lhs_ts[time], time);
	}
      
     
      
      } else {
	// po <=> input
      }
    }
  }

  
  // Add to global struct equiv
  tmp_e = list_ands[time]->head;
  while(tmp_e != NULL) {
    tmp_def = tmp_e->def;
    
    //assert(!aiger_sign(tmp_def->lhs));
    assert(tmp_def->lhs > 1);
    assert(tmp_def->cst == -1);

    unsigned hash = hash_ptuf(tmp_def->rhs_tr_fl);
    
    hash_struct_equiv_global.insert(std::pair< unsigned, ands_def* >(hash, tmp_def));
    
    tmp_e = tmp_e->next;
  }
  
  
  if (verbose) printf("[simp]  Simp : %d | dom %d\n", stats_simp_unroll, stats_simp_prop);
  if (verbose) printf("[simp]  Simp adv : cst %d | contra %d | tauto %d | subs (A) %d (O) %d\n",
		      stats_simp_adv_cst,
		      stats_simp_adv_contra, stats_simp_adv_tauto,
		      stats_simp_adv_funcA_subs, stats_simp_adv_funcO_subs);
  if (verbose) printf("[simp]  Struct equiv : local %d | global %d\n",
			     stats_struct_eq, stats_struct_eq_global);
  if (verbose) printf("[simp]  Struct diff  : local %d | global %d\n",
			     stats_struct_diff, stats_struct_diff_global);
  if (verbose) printf("[simp]  Ands removed : %d/%d\n",
			     nands - dll_size(list_ands[time]), nands);
  if (verbose) printf("[simp]  FB : %d\n", stats_fb2solver);

}
