/***************************************************************************
Copyright (c) 2016-2017, Guillaume Baud-Berthier, SafeRiver/LaBRI

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
IN THE SOFTWARE.
***************************************************************************/

extern "C" {
#include "aiger.h"
}

#include "aigprep.h"
#include "bmc.h"
#include "zigzag.h"
#include "zigzag_bwd.h"
#include "dual.h"
#include "pdr.h"

#include <assert.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>

int         simple_p   = 0;
bool        prep_opt   = 0;
bool        zz_opt     = 0;
bool        zzb_opt    = 0;
bool        dual_opt   = 0;
bool        bmc_opt    = 0;
bool        pdr_opt    = 0;
bool        cex        = 0;
bool        inprep_hl  = 0;
clock_t     clk_diff, clk;

const char * name;
aiger * model, *model_coi;

int verbose_aigMC, bad_output;

static void SIG_handler(int signum) {
  printf("-1\nb0\n.\n");
  fflush(stdout);
  exit(0);
}

void init_aigMC() {
  model = aiger_init ();
}

void destroy_aigMC() {
  aiger_reset (model);
}

bool isnumeric (const char *str) {
  for (int i = 0; i < strlen(str); i++)
    if (!isdigit(str[i]))
      return 0;
  return 1;
}

const char * usage =
  "usage           : mc [option] <model> [<start> <stop>]\n"
  "-h              : help\n"
  "-v              : verbose\n"
  "-prep           : perform AIG preprocessing\n"
  "-simple_path_od : add simple path (loop free) constraints on demand\n"
  "-bmc            : perform BMC\n"
  "-zz             : perform ZigZag\n"
  "-zzb            : perform ZigZag backward\n"
  "-dual           : perform Dual\n"
  "-pdr            : perform PDR\n"
  "-cex            : print cex\n"
  "-ip             : perform high level inprocessing (BMC)\n"
  "start           : starting depth (Default: 0) (!! Must always be equal to 0 in this version !!)\n"
  "stop            : max depth      (Default: INT_MAX)\n";

int main (int argc, char ** argv) {
  int i, j, k, start, stop;
  const char * err;
  start = -1;
  stop = -1;
  if (argc == 1) {
    printf ("%s", usage);
    exit(-1);
  }
  for (i = 1; i < argc; i++) {
    if (!strcmp (argv[i], "-h")) {
      printf ("%s", usage);
      exit (0);
    } else if (!strcmp (argv[i], "-v")) {
      verbose_aigMC++;
    } else if (!strcmp (argv[i], "-prep")) {
      prep_opt = 1;
    } else if (!strcmp (argv[i], "-bmc")) {
      bmc_opt = 1;
    } else if (!strcmp (argv[i], "-zz")) {
      zz_opt = 1;
    } else if (!strcmp (argv[i], "-zzb")) {
      zzb_opt = 1;
    } else if (!strcmp (argv[i], "-dual")) {
      dual_opt = 1;
    } else if (!strcmp (argv[i], "-pdr")) {
      pdr_opt = 1;
    } else if (!strcmp (argv[i], "-cex")) {
      cex = 1;
    } else if (!strcmp (argv[i], "-ip")) {
      inprep_hl = 1;
    } else if (!strcmp (argv[i], "-simple_path_od")) {
      assert(!simple_p);
      simple_p = 2;
    } else if (argv[i][0] == '-') {
      printf ("%s", usage);
      printf("Unknown option : '%s'\n", argv[i]);
      exit(-1);
    } else if (name && stop >= 0) {
      printf ("%s", usage);
      printf("Unknown option : '%s'\n", argv[i]);
      exit(-1);
    } else if (name && !isnumeric (argv[i])) {
      printf ("%s", usage);
      printf("Unknown option : '%s'\n", argv[i]);
      exit(-1);
    } else if (start < 0 && isnumeric (argv[i])) {
      start = atoi (argv[i]);
    } else if (stop < 0 && isnumeric (argv[i])) {
      stop = atoi (argv[i]);
    } else {
      name = argv[i];
    }
  }

  if (start < 0) start = 0;
  if (stop  < 0) stop = INT_MAX;
  
  signal(SIGINT,  SIG_handler);
  signal(SIGTERM, SIG_handler);

  if (verbose_aigMC)
    printf("[MC]  AIGER MC from %d to %d\n", start, stop);
  init_aigMC();

  if (verbose_aigMC)
    printf("[MC]  Checking model : '%s'\n", name);

  
  if (name) err = aiger_open_and_read_from_file (model, name);
  if (err) {
    printf("Parsing error '%s': %s\n", name, err);
    destroy_aigMC();
    return -1;
  }

  if (model->num_justice) {
    printf("Justice properties are not supported yet!\n");
    exit(-1);
  }
  if (model->num_constraints) { 
    printf("Constraints are not supported yet!\n");
    exit(-1);
  }
  if (model->num_fairness) {
    printf("Fairness constraints are not supported yet!\n");
    exit(-1);
  }
  if (!model->num_bad) {
    if (verbose_aigMC) printf("[MC]  Using output as property\n");
    if (model->num_outputs > 1) {
      printf("Multiple properties are not supported yet!\n");
      exit(-1);
      
    } else {
      aiger_add_bad (model, model->outputs[0].lit, 0);
    }
  } else if (model->num_bad) {
    printf("Multiple properties are not supported yet!\n");
    exit(-1);
  }

  int saved_nb_latches = -1;
  if (cex)
    saved_nb_latches = model->num_latches;

  aiger_reencode (model);

  if (prep_opt) {
    if (verbose_aigMC)
      printf("[MC]  MILOA = %u %u %u %u %u\n", model->maxvar, model->num_inputs,
	     model->num_latches, model->num_outputs, model->num_ands);
  
    if (verbose_aigMC) printf("[aigprep]  Preprocessing\n");
    destroy_prep();
    init_prep(model, cex, verbose_aigMC);
    iter_defs(cex);
    model = rebuild_aig(true);
    destroy_prep();
  }

  if (verbose_aigMC) {
    printf("[MC]  MILOA = %u %u %u %u %u\n",
	   model->maxvar,
	   model->num_inputs,
	   model->num_latches,
	   model->num_outputs,
	   model->num_ands);
    
    // printf("[MC]  BCJF = %u %u %u %u\n",
    // 	   model->num_bad,
    // 	   model->num_constraints,
    // 	   model->num_justice,
    // 	   model->num_fairness);
  }
  
  if(model->bad[0].lit < 2) {
    if (!model->bad[0].lit) {
      if (verbose_aigMC) printf("[MC]  Bad state is equal to FALSE\n");
      printf("0\nb0\n.\n");
    } else {
      if (verbose_aigMC) printf("[MC]  Bad state is equal to TRUE\n");
      printf("1\nb0\n.\n");
    }

  } else {
    try {
      int status_bmc = -1;
      int status_zz = -1;
      int status_zzb = -1;
      int status_dual = -1;
      int status_pdr = -1;

      /*  BMC */
      if (bmc_opt) {
	if (verbose_aigMC) printf("[MC]  Starting BMC\n");
	bmc(start, stop, model, /*elim_ll 0,*/ /*elim_hl*/ 1, inprep_hl, cex,
	    saved_nb_latches, verbose_aigMC, &status_bmc);
	printf("%d\nb0\n.\n", status_bmc);
      }

      /* ZIGZAG */
      if (zz_opt) {
	if (verbose_aigMC) printf("[MC]  Starting ZigZag\n");
	zigzag(start, stop, model, /*elim_hl*/ 1, simple_p, verbose_aigMC, &status_zz);
	printf("%d\nb0\n.\n", status_zz);
      }

      /* ZIGZAG BWD */      
      if (zzb_opt) {
	if (verbose_aigMC) printf("[MC]  Starting ZigZag backward\n");
	zigzag_bwd(start, stop, model, /*elim_hl*/ 1, simple_p, verbose_aigMC, &status_zzb);
	printf("%d\nb0\n.\n", status_zzb);
      }

      /* DUAL */
      if (dual_opt) {
	if (verbose_aigMC) printf("[MC]  Starting Dual\n");
	dual(start, stop, model, /*elim_hl*/ 1, simple_p, verbose_aigMC, &status_dual);
	printf("%d\nb0\n.\n", status_dual);
      }

      /* PDR */
      if (pdr_opt) {
	if (verbose_aigMC) printf("[MC]  Starting PDR\n");
	pdr(model, /*elim_ll*/ 0, /*elim_hl*/ 1, verbose_aigMC, &status_pdr);
	printf("%d\nb0\n.\n", status_pdr);
      }
      
      fflush(stdout);

    } catch(OutOfMemoryException&) {
      printf("Out of Memory Exception..\n");
      printf("-1\nb0\n.\n");
    }
  }

  

  return 0;
}
