/***************************************************************************
Copyright (c) 2016-2017, Guillaume Baud-Berthier, SafeRiver/LaBRI

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
IN THE SOFTWARE.
***************************************************************************/

#include "pdr.h"

#define TARGET_ENLARGEMENT  1
#define GENERALIZE_PRED     1
#define GENERALIZE_LEMMA_P1 1
#define GENERALIZE_LEMMA_P2 1
#define MULTIPLE_LEMMAS     1
#define PRINT_CEX           0 // TODO

dll_list<idef>* dll_ands_pdr;
dll_list<idef>* dll_inputs_pdr;
dll_list<idef>* dll_latches_pdr;

SimpSolver* solver_pdr;
vec<Lit>    assumptions_pdr;

std::map<unsigned, ands_def*> map_ands_pdr;
std::vector<idef*> mapdef_pdr;
std::vector<idef*> map_latches_pdr;
std::vector<idef*> map_latches2_pdr;
std::vector<dll_list<ands_def>*> list_ands_pdr;

unsigned vars_pdr = 0;

int verbose_pdr = 0;
int stats_pdr_cti = 0;
int act_I_pdr;

int current_level_pdr = 0;

int lit_bad_pdr = -1;
int act_target = -1;

std::vector<Frame> Frames;

bool PDR_DEBUG = 0;

std::stack<std::pair<Cube, Cube>> path_cex;

void print_cube(Cube c) {
  for (int i = 0; i < c.ref_val.size(); i++) {
    printf("%d ", c.ref_val[i].second ? c.ref_val[i].first->lhs_ts[0] : -c.ref_val[i].first->lhs_ts[0]);
  }
  printf("\n");
}

void print_tcube(TCube tc) {
  printf("Frame %d : ", tc.frame_ind);
  print_cube(tc.cube);
}

Cube copy_cube(Cube c) {
  Cube cc;
  for (int i = 0; i < c.ref_val.size(); i++)
    cc.ref_val.push_back(c.ref_val[i]);
  cc.act_lem = c.act_lem;
  return cc;
}
dll_list<ands_def>* unroll_pdr(int time, bool elim_ll) {
  dll_list<ands_def> *tmp_list_ands;
  tmp_list_ands = new dll_list<ands_def>();
  dll_init<ands_def>(tmp_list_ands);
  
  idef* tmp_def;
  dll_elem<idef> *tmp_e;

  // Inputs
  tmp_e = dll_inputs_pdr->head;
  while(tmp_e != NULL) {
    tmp_def = tmp_e->def;
    
    tmp_def->lhs_ts.push_back(new_var(&vars_pdr));

    map_latches_pdr.push_back(NULL);
    map_latches2_pdr.push_back(NULL);

    tmp_e = tmp_e->next;
  }  

  // Latches
  tmp_e = dll_latches_pdr->head;
  while(tmp_e != NULL) {
    tmp_def = tmp_e->def;

    if (!time) { // Init
      tmp_def->lhs_ts.push_back(new_var(&vars_pdr));

      map_latches_pdr.push_back(tmp_def);
      map_latches2_pdr.push_back(NULL);

      assert(map_latches_pdr[tmp_def->lhs_ts[0] >> 1] == tmp_def);

      bcl(solver_pdr, -(tmp_def->lhs_ts[0] >> 1), -act_I_pdr);
      if (elim_ll) {
	solver_pdr->frozen[tmp_def->lhs_ts[0] >> 1] = true;
      }
    } else {
      assert(tmp_def->rhs.size() == 1);
      
      if (tmp_def->rhs.front() > 1) {
	assert(tmp_def->lhs_ts[time-1] != -1);

	if (map_latches2_pdr[mapdef_pdr[aiger_lit2var(tmp_def->rhs.front())]->lhs_ts[time-1] >> 1] != NULL) {
	  tmp_def->lhs_ts.push_back(new_var(&vars_pdr));
	  if (aiger_sign(tmp_def->rhs.front())) {
	    bcl(solver_pdr, (mapdef_pdr[aiger_lit2var(tmp_def->rhs.front())]->lhs_ts[time-1] >> 1),
		(tmp_def->lhs_ts.back() >> 1));
	    bcl(solver_pdr, -(mapdef_pdr[aiger_lit2var(tmp_def->rhs.front())]->lhs_ts[time-1] >> 1),
		-(tmp_def->lhs_ts.back() >> 1));
	  } else {
	    bcl(solver_pdr, -(mapdef_pdr[aiger_lit2var(tmp_def->rhs.front())]->lhs_ts[time-1] >> 1),
		(tmp_def->lhs_ts.back() >> 1));
	    bcl(solver_pdr, (mapdef_pdr[aiger_lit2var(tmp_def->rhs.front())]->lhs_ts[time-1] >> 1),
		-(tmp_def->lhs_ts.back() >> 1));
	  }
	  map_latches2_pdr.push_back(tmp_def);
	  assert(map_latches2_pdr[tmp_def->lhs_ts.back() >> 1] == tmp_def);
	} else {
	  if (aiger_sign(tmp_def->rhs.front()))
	    tmp_def->lhs_ts.push_back
	      (aiger_not(mapdef_pdr[aiger_lit2var(tmp_def->rhs.front())]->lhs_ts[time-1]));
	  else
	    tmp_def->lhs_ts.push_back
	      (mapdef_pdr[aiger_lit2var(tmp_def->rhs.front())]->lhs_ts[time-1]);
	  map_latches2_pdr[mapdef_pdr[aiger_lit2var(tmp_def->rhs.front())]->lhs_ts[time-1] >> 1] = tmp_def;
	}
	
      } else { // Cst latch
	tmp_def->lhs_ts.push_back(new_var(&vars_pdr));
	ucl(solver_pdr, tmp_def->lhs_ts.back() >> 1);
	map_latches2_pdr.push_back(tmp_def);
	assert(map_latches2_pdr[tmp_def->lhs_ts.back() >> 1] == tmp_def);
      }
    }
    tmp_e = tmp_e->next;
  }

  // Ands
  tmp_e = dll_ands_pdr->head;
  while(tmp_e != NULL) {
    tmp_def = tmp_e->def;

    prop_tr_un *tpu = unroll_prop_tr(mapdef_pdr, map_ands_pdr, tmp_def->rhs_tr, time, 0);

    if (tpu->lit == UNDEF_LIT) {
      assert(tpu->op != VAR);

      tmp_def->lhs_ts.push_back(new_var(&vars_pdr));

      map_latches_pdr.push_back(NULL);
      map_latches2_pdr.push_back(NULL);

      ands_def *tmp_ands = new ands_def(tmp_def->lhs_ts[time], tpu, tmp_def, time);

      map_ands_pdr.insert(std::pair<unsigned, ands_def*>(aiger_lit2var(tmp_ands->lhs), tmp_ands));
      
      tmp_ands->dllade = dll_insert_last(tmp_list_ands, aiger_lit2var(tmp_def->lhs_ts[time]), tmp_ands);

    } else {
      assert(tpu->op == VAR);

      tmp_def->lhs_ts.push_back(tpu->lit);
    }

    tmp_e = tmp_e->next;
  }

  return tmp_list_ands;
}

void insert_tc_Q(std::list<TCube> &l, TCube tc) {
  for (std::list<TCube>::iterator it = l.begin(); it != l.end(); ++it)
    if ((*it).frame_ind >= tc.frame_ind) {
      l.emplace(it, tc);
      return;
    }
  l.push_back(tc);
}


Cube getBadCube() {
  if (PDR_DEBUG) printf("Get bad cube\n");
 
#if TARGET_ENLARGEMENT
  assume(solver_pdr, assumptions_pdr, act_target);  // Assume: -P || -P'
#else
  assume(solver_pdr, assumptions_pdr, lit_bad_pdr); // Assume: -P
#endif

  /* Activate the current frame */
  for (int i = 0; i < Frames[current_level_pdr].lemmas.size(); i++) {
    assert(Frames[current_level_pdr].lemmas[i].act_lem != -1);
    assert(solver_pdr->value(Frames[current_level_pdr].lemmas[i].act_lem) == l_Undef);
    assume(solver_pdr, assumptions_pdr, Frames[current_level_pdr].lemmas[i].act_lem);
  }
  /* Activate invariants frame */
  for (int i = 0; i < Frames[current_level_pdr + 1].lemmas.size(); i++) {
    assert(Frames[current_level_pdr + 1].lemmas[i].act_lem != -1);
    assert(solver_pdr->value(Frames[current_level_pdr + 1].lemmas[i].act_lem) == l_Undef);
    assume(solver_pdr, assumptions_pdr, Frames[current_level_pdr + 1].lemmas[i].act_lem);
  }

  lbool res_check = check_sat(solver_pdr, assumptions_pdr, 0, 0);
  if (res_check == l_False)
    return Cube();
  
  Cube c;
  idef* tmp_def;
  dll_elem<idef> *tmp_e = dll_latches_pdr->head;
  while(tmp_e != NULL) {
    tmp_def = tmp_e->def;

    assert(tmp_def->lhs_ts[0] > 1);
    assert(!aiger_sign(tmp_def->lhs_ts[0]));

    if (solver_pdr->modelValue(tmp_def->lhs_ts[0] >> 1) == l_True) {
      c.ref_val.push_back(std::pair<idef*, bool>(tmp_def, 1));
 
    } else if (solver_pdr->modelValue(tmp_def->lhs_ts[0] >> 1) == l_False) {
      c.ref_val.push_back(std::pair<idef*, bool>(tmp_def, 0));
      
    } else {
      assert(0);
    }

    tmp_e = tmp_e->next;
  }
  c.act_lem = -1;

  return c;
}

bool isSorted(Cube c) {
  for (int i = 1; i < c.ref_val.size(); i++) {
    if (c.ref_val[i-1].first->lhs >= c.ref_val[i].first->lhs)
      return 0;
  }
  return 1;
}

bool subsumes(Cube c1, Cube c2) {
  assert(isSorted(c1));
  assert(isSorted(c2));
  // TODO: Add hash ?
  if (c1.ref_val.size() > c2.ref_val.size())
    return 0;
  
  int j = 0;
  for (int i = 0; i < c1.ref_val.size();) {
    if (j == c2.ref_val.size() || c1.ref_val[i].first->lhs < c2.ref_val[j].first->lhs) {
      return 0;

    } else if (c1.ref_val[i].first->lhs > c2.ref_val[j].first->lhs) {
      j++;

    } else {
      assert(c1.ref_val[i].first->lhs == c2.ref_val[j].first->lhs);
      if (c1.ref_val[i].second != c2.ref_val[j].second)
	return 0;
      i++;
      j++;
    }
  }

  return 1;
}

bool isBlocked(TCube tc) {
  assert(tc.frame_ind > 0);
  for (int i = tc.frame_ind; i < Frames.size(); i++) {
    for (int j = 0; j < Frames[i].lemmas.size(); j++)
      if (subsumes(Frames[i].lemmas[j], tc.cube))
  	return 1;
  }

  /* Assume: F_i */
  for (int k = tc.frame_ind; k < Frames.size(); k++) // delta-encoded trace
    for (int i = 0; i < Frames[k].lemmas.size(); i++) {
      assert(Frames[k].lemmas[i].act_lem != -1);
      assert(solver_pdr->value(Frames[k].lemmas[i].act_lem) == l_Undef);
      assume(solver_pdr, assumptions_pdr, Frames[k].lemmas[i].act_lem);
    }
  /* Assume: cube */
  for (int i = 0; i < tc.cube.ref_val.size(); i++) {
    if (tc.cube.ref_val[i].second)
      assume(solver_pdr, assumptions_pdr, tc.cube.ref_val[i].first->lhs_ts[0] >> 1);
    else
      assume(solver_pdr, assumptions_pdr, -(tc.cube.ref_val[i].first->lhs_ts[0] >> 1));
  }

  lbool res_check = check_sat(solver_pdr, assumptions_pdr, 0, 0);
  return res_check == l_False;
}

bool isInitial(Cube c) {
  for (int i = 0; i < c.ref_val.size(); i++)
    if (c.ref_val[i].second) 
      return 0;
  return 1;
}

bool isInitial(Cube c, int ind) {
  for (int i = 0; i < c.ref_val.size(); i++)
    if (i != ind && c.ref_val[i].second) 
      return 0;
  return 1;
}


Cube generalize_pred(TCube tc) {
  std::vector<int> inputs_assumptions;
  std::vector<int> latches_assumptions;

  idef* tmp_def;
  dll_elem<idef> *tmp_e = dll_inputs_pdr->head;
  while(tmp_e != NULL) {
    tmp_def = tmp_e->def;

    if (solver_pdr->modelValue(tmp_def->lhs_ts[0] >> 1) == l_True) {
      inputs_assumptions.push_back(tmp_def->lhs_ts[0] >> 1);

    } else if (solver_pdr->modelValue(tmp_def->lhs_ts[0] >> 1) == l_False) {
      inputs_assumptions.push_back(-(tmp_def->lhs_ts[0] >> 1));
      
    } else {
      assert(0);
    }
    tmp_e = tmp_e->next;
  }

  tmp_e = dll_latches_pdr->head;
  while(tmp_e != NULL) {
    tmp_def = tmp_e->def;

    if (solver_pdr->modelValue(tmp_def->lhs_ts[0] >> 1) == l_True) {
      latches_assumptions.push_back(tmp_def->lhs_ts[0] >> 1);
      
    } else if (solver_pdr->modelValue(tmp_def->lhs_ts[0] >> 1) == l_False) {
      latches_assumptions.push_back(-(tmp_def->lhs_ts[0] >> 1));

    } else {
      assert(0);
    }
    tmp_e = tmp_e->next;
  }

  vec<Lit> lits;
  for(int k = 0; k < tc.cube.ref_val.size(); k++) {
    if (tc.cube.ref_val[k].second) {
      if (tc.cube.ref_val[k].first->lhs_ts[1] > 1)
	aiger_sign(tc.cube.ref_val[k].first->lhs_ts[1]) ?
	  lits.push( mkLit(tc.cube.ref_val[k].first->lhs_ts[1] >> 1)) :
	  lits.push(~mkLit(tc.cube.ref_val[k].first->lhs_ts[1] >> 1));      
    } else {
      if (tc.cube.ref_val[k].first->lhs_ts[1] > 1)
	aiger_sign(tc.cube.ref_val[k].first->lhs_ts[1]) ?
	  lits.push(~mkLit(tc.cube.ref_val[k].first->lhs_ts[1] >> 1)) :
	  lits.push( mkLit(tc.cube.ref_val[k].first->lhs_ts[1] >> 1));
    }
  }
  int act_lit_tmp = new_var(&vars_pdr) >> 1;
  while (act_lit_tmp >= solver_pdr->nVars()) solver_pdr->newVar();
  lits.push(~mkLit(act_lit_tmp));
  solver_pdr->addClause_(lits);

  lbool res_check;
  int old_latches_assumptions_size = -1;
  while (latches_assumptions.size() != old_latches_assumptions_size) {
    assert(assumptions_pdr.size() == 0);
    
    for (int i = 0; i < inputs_assumptions.size(); i++)
      assume(solver_pdr, assumptions_pdr, inputs_assumptions[i]);

    assume(solver_pdr, assumptions_pdr, act_lit_tmp);

    for (int i = 0; i < latches_assumptions.size(); i++)
      assume(solver_pdr, assumptions_pdr, latches_assumptions[i]);

    old_latches_assumptions_size = latches_assumptions.size();
    latches_assumptions.clear();

    res_check = check_sat(solver_pdr, assumptions_pdr, 0, 0);
    assert(res_check == l_False);

    for (int i = solver_pdr->conflict.size() - 1; i >= 0 ; i--) {
      if ((solver_pdr->conflict[i].x >> 1) < map_latches_pdr.size()) {
	tmp_def = map_latches_pdr[solver_pdr->conflict[i].x >> 1];
	if (tmp_def != NULL) {
	  assert(tmp_def->lhs_ts[0] == (solver_pdr->conflict[i].x & ~1));
	  (solver_pdr->conflict[i].x & 1) ?
	    latches_assumptions.push_back(  solver_pdr->conflict[i].x >> 1) :
	    latches_assumptions.push_back(-(solver_pdr->conflict[i].x >> 1));
	}
      }
    }
  }

  ucl(solver_pdr, -act_lit_tmp);
  
  Cube c;
  for (int i = 0; i < latches_assumptions.size(); i++) {
    if (latches_assumptions[i] > 0) {
      tmp_def = map_latches_pdr[latches_assumptions[i]];
      assert(tmp_def->lhs_ts[0] == (latches_assumptions[i] << 1));
      c.ref_val.push_back(std::pair<idef*, bool>(tmp_def, 1));
 
    } else if (latches_assumptions[i] < 0) {
      tmp_def = map_latches_pdr[-latches_assumptions[i]];
      assert(tmp_def->lhs_ts[0] == (-latches_assumptions[i] << 1));
      c.ref_val.push_back(std::pair<idef*, bool>(tmp_def, 0));
      
    } else {
      assert(0);
    }
  }

  //print_cube(c);

  return c;
}


/* F_(i-1)  &  P  &  ~c  &  T  &  c' */
std::vector<TCube> solveRelative(TCube tc, bool extract_model, bool noInd, bool condA) {
  if (PDR_DEBUG) printf("Solve relative\n");
  std::vector<TCube> tcs;
  /* Assume: F_(i-1) */
  for (int k = tc.frame_ind - 1; k < Frames.size(); k++) // delta-encoded trace
    for (int i = 0; i < Frames[k].lemmas.size(); i++) {
      assert(Frames[k].lemmas[i].act_lem != -1);
      assert(solver_pdr->value(Frames[k].lemmas[i].act_lem) == l_Undef);
      assume(solver_pdr, assumptions_pdr, Frames[k].lemmas[i].act_lem);
    }

  /* Assume: P */
  assume(solver_pdr, assumptions_pdr, -lit_bad_pdr);
  
  /* Assume: ~c  */
  /* Assume:  c' */
  vec<Lit> lits;
  for (int i = 0; i < tc.cube.ref_val.size() ; i++) {
    assert(tc.cube.ref_val[i].first->lhs_ts[0] > 1);
    assert(!aiger_sign(tc.cube.ref_val[i].first->lhs_ts[0]));
    
    if (tc.cube.ref_val[i].second) {
      if (!noInd)
	lits.push(~mkLit(tc.cube.ref_val[i].first->lhs_ts[0] >> 1));
      
      if (tc.cube.ref_val[i].first->lhs_ts[1] > 1)
	assume(solver_pdr, assumptions_pdr, aiger_sign(tc.cube.ref_val[i].first->lhs_ts[1]) ?
	       -(tc.cube.ref_val[i].first->lhs_ts[1] >> 1) : tc.cube.ref_val[i].first->lhs_ts[1] >> 1);

    } else {
      if (!noInd)
	lits.push( mkLit(tc.cube.ref_val[i].first->lhs_ts[0] >> 1));

      if (tc.cube.ref_val[i].first->lhs_ts[1] > 1)
	assume(solver_pdr, assumptions_pdr, aiger_sign(tc.cube.ref_val[i].first->lhs_ts[1]) ?
	       tc.cube.ref_val[i].first->lhs_ts[1] >> 1 : -(tc.cube.ref_val[i].first->lhs_ts[1] >> 1));
    }
  }
  int act_c;
  if (!noInd) {
    act_c = new_var(&vars_pdr) >> 1;
    while (act_c >= solver_pdr->nVars()) solver_pdr->newVar();
    lits.push(~mkLit(act_c));
    solver_pdr->addClause_(lits);
    assume(solver_pdr, assumptions_pdr, act_c);
  }

  
  lbool res_check = check_sat(solver_pdr, assumptions_pdr, 0, 0);
  if (res_check == l_False) {
    if (noInd) {
      tcs.push_back(tc);
      return tcs;
    }
    
    if (PDR_DEBUG) printf("Generalize lemma p1\n");

#if GENERALIZE_LEMMA_P1 // generalize lemma p1
    Cube gen_c;
    idef* tmp_def;
    for (int i = solver_pdr->conflict.size() - 1; i >= 0 ; i--) {
      if ((solver_pdr->conflict[i].x >> 1) < map_latches2_pdr.size()) {
    	tmp_def = map_latches2_pdr[solver_pdr->conflict[i].x >> 1];
	if (tmp_def != NULL) {
	  assert((tmp_def->lhs_ts[1] & ~1) == (solver_pdr->conflict[i].x & ~1));
	    
	  if (aiger_sign(tmp_def->lhs_ts[1]))
	    gen_c.ref_val.push_back(std::pair<idef*, bool>(tmp_def, 1 - solver_pdr->conflict[i].x & 1));
	  else
	    gen_c.ref_val.push_back(std::pair<idef*, bool>(tmp_def, solver_pdr->conflict[i].x & 1));
	}
      }
    }    

    if (isInitial(gen_c)) {
      bool done = 0;
      for (int i = 0; i < tc.cube.ref_val.size() && !done; i++) {
      	if (tc.cube.ref_val[i].second) {
      	  done = 1;
      	  gen_c.ref_val.push_back(gen_c.ref_val.back());
      	  for (int j = gen_c.ref_val.size() - 1; j > 0; j--) {
      	    assert(tc.cube.ref_val[i].first->lhs != gen_c.ref_val[j].first->lhs);
      	    if (tc.cube.ref_val[i].first->lhs > gen_c.ref_val[j - 1].first->lhs) {
      	      gen_c.ref_val[j] = std::pair<idef*, bool>(tc.cube.ref_val[i].first, tc.cube.ref_val[i].second);
      	      j = -1;
      	    } else {
      	      gen_c.ref_val[j] = gen_c.ref_val[j - 1];
      	      if (!(j - 1))
      		gen_c.ref_val[0] = std::pair<idef*, bool>(tc.cube.ref_val[i].first, tc.cube.ref_val[i].second);
      	    }  
      	  }
      	}
      }
    }
    assert(isSorted(gen_c));
    assert(!isInitial(gen_c));

#if GENERALIZE_LEMMA_P2 // generalize lemma p2
    if (PDR_DEBUG) printf("Generalize lemma p2 (sz : %lu)\n", gen_c.ref_val.size());


#if MULTIPLE_LEMMAS
    if (!condA) {
      int old_size = gen_c.ref_val.size();
      Cube gen_c_bis = copy_cube(gen_c);

      for (int i = 0; i < gen_c.ref_val.size();) {
	if (!isInitial(gen_c, i)) {

	  /* Assume: F_(i-1) */
	  for (int k = tc.frame_ind - 1; k < Frames.size(); k++) // delta-encoded trace
	    for (int j = 0; j < Frames[k].lemmas.size(); j++) {
	      assert(Frames[k].lemmas[j].act_lem != -1);
	      assert(solver_pdr->value(Frames[k].lemmas[j].act_lem) == l_Undef);
	      assume(solver_pdr, assumptions_pdr, Frames[k].lemmas[j].act_lem);
	    }
	  
	  /* Assume: P */
	  assume(solver_pdr, assumptions_pdr, -lit_bad_pdr);

	  lits.clear();
	  for (int j = 0; j < gen_c.ref_val.size(); j++) {
	    if (i != j) {
	      /* Assume: ~c  */
	      /* Assume:  c' */
	      assert(gen_c.ref_val[j].first->lhs_ts[0] > 1);
	      assert(!aiger_sign(gen_c.ref_val[j].first->lhs_ts[0]));
		
	      if (gen_c.ref_val[j].second) {
		lits.push(~mkLit(gen_c.ref_val[j].first->lhs_ts[0] >> 1));
		
		if (gen_c.ref_val[j].first->lhs_ts[1] > 1)
		  assume(solver_pdr, assumptions_pdr, aiger_sign(gen_c.ref_val[j].first->lhs_ts[1]) ?
			 -(gen_c.ref_val[j].first->lhs_ts[1] >> 1) : gen_c.ref_val[j].first->lhs_ts[1] >> 1);
		
	      } else {
		lits.push( mkLit(gen_c.ref_val[j].first->lhs_ts[0] >> 1));
		
		if (gen_c.ref_val[j].first->lhs_ts[1] > 1)
		  assume(solver_pdr, assumptions_pdr, aiger_sign(gen_c.ref_val[j].first->lhs_ts[1]) ?
			 gen_c.ref_val[j].first->lhs_ts[1] >> 1 : -(gen_c.ref_val[j].first->lhs_ts[1] >> 1));
	      }
	    }
	  }
	  int act_lit_tmp = new_var(&vars_pdr) >> 1;
	  while (act_lit_tmp >= solver_pdr->nVars()) solver_pdr->newVar();
	  lits.push(~mkLit(act_lit_tmp));
	  solver_pdr->addClause_(lits);
	  assume(solver_pdr, assumptions_pdr, act_lit_tmp);
	  
	  if (check_sat(solver_pdr, assumptions_pdr, 0, 0) == l_False) {
	    for (int j = i; j < gen_c.ref_val.size() - 1; j++)
	      gen_c.ref_val[j] = gen_c.ref_val[j + 1];
	    gen_c.ref_val.pop_back();
	  } else {
	    i++;
	  } 
	  ucl(solver_pdr, -act_lit_tmp);
	} else {
	  i++;
	}
      }

      ucl(solver_pdr, -act_c);
      lits.clear();
      for (int i = 0; i < gen_c.ref_val.size(); i++) {
	if (gen_c.ref_val[i].second)
	  lits.push(~mkLit(gen_c.ref_val[i].first->lhs_ts[0] >> 1));
	else
	  lits.push( mkLit(gen_c.ref_val[i].first->lhs_ts[0] >> 1));
      }
      
      int act_gen_c_ = new_var(&vars_pdr) >> 1;
      while (act_gen_c_ >= solver_pdr->nVars()) solver_pdr->newVar();
      lits.push(~mkLit(act_gen_c_));
      solver_pdr->addClause_(lits);
      gen_c.act_lem = act_gen_c_;
    
      tcs.push_back(TCube(gen_c, tc.frame_ind));
      if (old_size == gen_c.ref_val.size())
	return tcs;
    
      for (int i = gen_c_bis.ref_val.size()-1; i >= 0 ; i--) {
	if (!isInitial(gen_c_bis, i)) {
	
	  /* Assume: F_(i-1) */
	  for (int k = tc.frame_ind - 1; k < Frames.size(); k++) // delta-encoded trace
	    for (int j = 0; j < Frames[k].lemmas.size(); j++) {
	      assert(Frames[k].lemmas[j].act_lem != -1);
	      assert(solver_pdr->value(Frames[k].lemmas[j].act_lem) == l_Undef);
	      assume(solver_pdr, assumptions_pdr, Frames[k].lemmas[j].act_lem);
	    }
	  
	  /* Assume: P */
	  assume(solver_pdr, assumptions_pdr, -lit_bad_pdr);

	  lits.clear();
	  for (int j = 0; j < gen_c_bis.ref_val.size(); j++) {
	    if (i != j) {
	      /* Assume: ~c  */
	      /* Assume:  c' */
	      assert(gen_c_bis.ref_val[j].first->lhs_ts[0] > 1);
	      assert(!aiger_sign(gen_c_bis.ref_val[j].first->lhs_ts[0]));
		
	      if (gen_c_bis.ref_val[j].second) {
		lits.push(~mkLit(gen_c_bis.ref_val[j].first->lhs_ts[0] >> 1));
		
		if (gen_c_bis.ref_val[j].first->lhs_ts[1] > 1)
		  assume(solver_pdr, assumptions_pdr, aiger_sign(gen_c_bis.ref_val[j].first->lhs_ts[1]) ?
			 -(gen_c_bis.ref_val[j].first->lhs_ts[1] >> 1) : gen_c_bis.ref_val[j].first->lhs_ts[1] >> 1);
		
	      } else {
		lits.push( mkLit(gen_c_bis.ref_val[j].first->lhs_ts[0] >> 1));
		
		if (gen_c_bis.ref_val[j].first->lhs_ts[1] > 1)
		  assume(solver_pdr, assumptions_pdr, aiger_sign(gen_c_bis.ref_val[j].first->lhs_ts[1]) ?
			 gen_c_bis.ref_val[j].first->lhs_ts[1] >> 1 : -(gen_c_bis.ref_val[j].first->lhs_ts[1] >> 1));
	      }
	    }
	  }
	  int act_lit_tmp = new_var(&vars_pdr) >> 1;
	  while (act_lit_tmp >= solver_pdr->nVars()) solver_pdr->newVar();
	  lits.push(~mkLit(act_lit_tmp));
	  solver_pdr->addClause_(lits);
	  assume(solver_pdr, assumptions_pdr, act_lit_tmp);
	  
	  if (check_sat(solver_pdr, assumptions_pdr, 0, 0) == l_False) {
	    for (int j = i; j < gen_c_bis.ref_val.size() - 1; j++)
	      gen_c_bis.ref_val[j] = gen_c_bis.ref_val[j + 1];
	    gen_c_bis.ref_val.pop_back();
	  }
	  ucl(solver_pdr, -act_lit_tmp);
	}
      }
    
      if (gen_c == gen_c_bis)
	return tcs;

      lits.clear();
      for (int i = 0; i < gen_c_bis.ref_val.size(); i++) {
	if (gen_c_bis.ref_val[i].second)
	  lits.push(~mkLit(gen_c_bis.ref_val[i].first->lhs_ts[0] >> 1));
	else
	  lits.push( mkLit(gen_c_bis.ref_val[i].first->lhs_ts[0] >> 1));
      }
      int act_gen_c_bis = new_var(&vars_pdr) >> 1;
      while (act_gen_c_bis >= solver_pdr->nVars()) solver_pdr->newVar();
      lits.push(~mkLit(act_gen_c_bis));
      solver_pdr->addClause_(lits);
      gen_c_bis.act_lem = act_gen_c_bis;
  
      tcs.push_back(TCube(gen_c_bis, tc.frame_ind));
      return tcs;
    }
#endif


    bool order_low = 0;

    if (order_low) {
      for (int i = 0; i < gen_c.ref_val.size();) {
	if (!isInitial(gen_c, i)) {

	  /* Assume: F_(i-1) */
	  for (int k = tc.frame_ind - 1; k < Frames.size(); k++) // delta-encoded trace
	    for (int j = 0; j < Frames[k].lemmas.size(); j++) {
	      assert(Frames[k].lemmas[j].act_lem != -1);
	      assert(solver_pdr->value(Frames[k].lemmas[j].act_lem) == l_Undef);
	      assume(solver_pdr, assumptions_pdr, Frames[k].lemmas[j].act_lem);
	    }
	  
	  /* Assume: P */
	  assume(solver_pdr, assumptions_pdr, -lit_bad_pdr);

	  lits.clear();
	  for (int j = 0; j < gen_c.ref_val.size(); j++) {
	    if (i != j) {
	      /* Assume: ~c  */
	      /* Assume:  c' */
	      assert(gen_c.ref_val[j].first->lhs_ts[0] > 1);
	      assert(!aiger_sign(gen_c.ref_val[j].first->lhs_ts[0]));
		
	      if (gen_c.ref_val[j].second) {
		lits.push(~mkLit(gen_c.ref_val[j].first->lhs_ts[0] >> 1));
		
		if (gen_c.ref_val[j].first->lhs_ts[1] > 1)
		  assume(solver_pdr, assumptions_pdr, aiger_sign(gen_c.ref_val[j].first->lhs_ts[1]) ?
			 -(gen_c.ref_val[j].first->lhs_ts[1] >> 1) : gen_c.ref_val[j].first->lhs_ts[1] >> 1);
		
	      } else {
		lits.push( mkLit(gen_c.ref_val[j].first->lhs_ts[0] >> 1));
		
		if (gen_c.ref_val[j].first->lhs_ts[1] > 1)
		  assume(solver_pdr, assumptions_pdr, aiger_sign(gen_c.ref_val[j].first->lhs_ts[1]) ?
			 gen_c.ref_val[j].first->lhs_ts[1] >> 1 : -(gen_c.ref_val[j].first->lhs_ts[1] >> 1));
	      }
	    }
	  }
	  int act_lit_tmp = new_var(&vars_pdr) >> 1;
	  while (act_lit_tmp >= solver_pdr->nVars()) solver_pdr->newVar();
	  lits.push(~mkLit(act_lit_tmp));
	  solver_pdr->addClause_(lits);
	  assume(solver_pdr, assumptions_pdr, act_lit_tmp);
	  
	  if (check_sat(solver_pdr, assumptions_pdr, 0, 0) == l_False) {
	    for (int j = i; j < gen_c.ref_val.size() - 1; j++)
	      gen_c.ref_val[j] = gen_c.ref_val[j + 1];
	    gen_c.ref_val.pop_back();
	  } else {
	    i++;
	  } 
	  ucl(solver_pdr, -act_lit_tmp);
	} else {
	  i++;
	}
      }

    } else {
      for (int i = gen_c.ref_val.size()-1; i >= 0 ; i--) {
	if (!isInitial(gen_c, i)) {

	  /* Assume: F_(i-1) */
	  for (int k = tc.frame_ind - 1; k < Frames.size(); k++) // delta-encoded trace
	    for (int j = 0; j < Frames[k].lemmas.size(); j++) {
	      assert(Frames[k].lemmas[j].act_lem != -1);
	      assert(solver_pdr->value(Frames[k].lemmas[j].act_lem) == l_Undef);
	      assume(solver_pdr, assumptions_pdr, Frames[k].lemmas[j].act_lem);
	    }
	  
	  /* Assume: P */
	  assume(solver_pdr, assumptions_pdr, -lit_bad_pdr);

	  lits.clear();
	  for (int j = 0; j < gen_c.ref_val.size(); j++) {
	    if (i != j) {
	      /* Assume: ~c  */
	      /* Assume:  c' */
	      assert(gen_c.ref_val[j].first->lhs_ts[0] > 1);
	      assert(!aiger_sign(gen_c.ref_val[j].first->lhs_ts[0]));
		
	      if (gen_c.ref_val[j].second) {
		lits.push(~mkLit(gen_c.ref_val[j].first->lhs_ts[0] >> 1));
		
		if (gen_c.ref_val[j].first->lhs_ts[1] > 1)
		  assume(solver_pdr, assumptions_pdr, aiger_sign(gen_c.ref_val[j].first->lhs_ts[1]) ?
			 -(gen_c.ref_val[j].first->lhs_ts[1] >> 1) : gen_c.ref_val[j].first->lhs_ts[1] >> 1);
		
	      } else {
		lits.push( mkLit(gen_c.ref_val[j].first->lhs_ts[0] >> 1));
		
		if (gen_c.ref_val[j].first->lhs_ts[1] > 1)
		  assume(solver_pdr, assumptions_pdr, aiger_sign(gen_c.ref_val[j].first->lhs_ts[1]) ?
			 gen_c.ref_val[j].first->lhs_ts[1] >> 1 : -(gen_c.ref_val[j].first->lhs_ts[1] >> 1));
	      }
	    }
	  }
	  int act_lit_tmp = new_var(&vars_pdr) >> 1;
	  while (act_lit_tmp >= solver_pdr->nVars()) solver_pdr->newVar();
	  lits.push(~mkLit(act_lit_tmp));
	  solver_pdr->addClause_(lits);
	  assume(solver_pdr, assumptions_pdr, act_lit_tmp);
	  
	  if (check_sat(solver_pdr, assumptions_pdr, 0, 0) == l_False) {
	    for (int j = i; j < gen_c.ref_val.size() - 1; j++)
	      gen_c.ref_val[j] = gen_c.ref_val[j + 1];
	    gen_c.ref_val.pop_back();
	  }
	  ucl(solver_pdr, -act_lit_tmp);
	}
      }
    }

#endif

    ucl(solver_pdr, -act_c);
    lits.clear();
    for (int i = 0; i < gen_c.ref_val.size(); i++) {
      if (gen_c.ref_val[i].second)
	lits.push(~mkLit(gen_c.ref_val[i].first->lhs_ts[0] >> 1));
      else
	lits.push( mkLit(gen_c.ref_val[i].first->lhs_ts[0] >> 1));
    }
      
    int act_gen_c = new_var(&vars_pdr) >> 1;
    while (act_gen_c >= solver_pdr->nVars()) solver_pdr->newVar();
    lits.push(~mkLit(act_gen_c));
    solver_pdr->addClause_(lits);
    gen_c.act_lem = act_gen_c;

    if (PDR_DEBUG) printf("lemma sz : %lu\n", gen_c.ref_val.size());

    tcs.push_back(TCube(gen_c, tc.frame_ind));
    return tcs;
    
#else
    tc.cube.act_lem = act_c;
    tcs.push_back(tc);
    return tcs;
#endif
    
    
    
  } else { // CTI
    ucl(solver_pdr, -act_c);
    tc.cube.act_lem = -1;
    if (extract_model) {
      Cube c;
      idef* tmp_def;
      dll_elem<idef> *tmp_e = dll_latches_pdr->head;
      while(tmp_e != NULL) {
	tmp_def = tmp_e->def;

	assert(tmp_def->lhs_ts[0] > 1);
	assert(!aiger_sign(tmp_def->lhs_ts[0]));
	
	if (solver_pdr->modelValue(tmp_def->lhs_ts[0] >> 1) == l_True) {
	  c.ref_val.push_back(std::pair<idef*, bool>(tmp_def, 1));
	  
	} else if (solver_pdr->modelValue(tmp_def->lhs_ts[0] >> 1) == l_False) {
	  c.ref_val.push_back(std::pair<idef*, bool>(tmp_def, 0));
	
	} else {
	  assert(0);
	}
      
	tmp_e = tmp_e->next;
      }
      c.act_lem = -1;

      //if (PDR_DEBUG) {
      //printf("\nPred     : ");
      //print_cube(c);
      //}
      
#if GENERALIZE_PRED // generalize predecessor
      if (tc.frame_ind - 1) {
	if (PDR_DEBUG) printf("Generalize predecessors");
	Cube c_gen;
	c_gen = generalize_pred(tc);
	c_gen.act_lem = -1;

	if (PDR_DEBUG) printf("Predecessors generalization removed %lu lits\n", c.ref_val.size() - c_gen.ref_val.size());
	assert(subsumes(c_gen, c));
	assert(!isInitial(c_gen));
	
	
	tcs.push_back(TCube(c_gen, -1));
	return tcs;
      }
#endif
      tcs.push_back(TCube(c, -1));
      return tcs;
    }

    tcs.push_back(TCube());
    return tcs;
  }
}


inline TCube TCnext(TCube tc) {
  return TCube(tc.cube, tc.frame_ind + 1);
}

bool condAssign(TCube &s, std::vector<TCube> tcs) {
  assert(tcs.size() == 1);
  TCube tc = tcs.back();
  if (tc.frame_ind != -1) {
    s = tc;
    return 1;
  }
  return 0;
}

void addLemma(TCube tc) {
  assert(isSorted(tc.cube));

  for (int i = 1; i < tc.frame_ind; i++) {
    for(int j = 0; j < Frames[i].lemmas.size();) {
      if (subsumes(tc.cube, Frames[i].lemmas[j])) {
	Frames[i].lemmas[j] = Frames[i].lemmas.back();
	Frames[i].lemmas.pop_back();
      } else {
	j++;
      }
    }
  }
  if (PDR_DEBUG) {
    printf("Add lemma to F%d\n", tc.frame_ind); //print_tcube(tc);
  }

  Frames[tc.frame_ind].lemmas.push_back(tc.cube);
}


bool propagateLemmas() {
  if (PDR_DEBUG) printf("Propagate Lemmas :\n");
  for (int i = 1; i < current_level_pdr; i++) {
    for(int j = 0; j < Frames[i].lemmas.size();) {

      std::vector<TCube> tcs = solveRelative(TCube(Frames[i].lemmas[j], i + 1), 0, 1, 0);
      assert(tcs.size() == 1);
      TCube tc = tcs.back();
      if (tc.frame_ind != -1)
	addLemma(tc);
      else
	j++;
    }

    if (Frames[i].lemmas.empty()) return 1;
  }

  // Propagate to Invariants Frame
  for(int j = 0; j < Frames[current_level_pdr].lemmas.size();) {
    TCube tc = TCube(Frames[current_level_pdr].lemmas[j], current_level_pdr + 1);
    for (int i = 0; i < tc.cube.ref_val.size() ; i++) {
      assert(tc.cube.ref_val[i].first->lhs_ts[0] > 1);
      assert(!aiger_sign(tc.cube.ref_val[i].first->lhs_ts[0]));
    
      if (tc.cube.ref_val[i].second) {
	if (tc.cube.ref_val[i].first->lhs_ts[1] > 1)
	  assume(solver_pdr, assumptions_pdr, aiger_sign(tc.cube.ref_val[i].first->lhs_ts[1]) ?
		 -(tc.cube.ref_val[i].first->lhs_ts[1] >> 1) : tc.cube.ref_val[i].first->lhs_ts[1] >> 1);
	
      } else {
	if (tc.cube.ref_val[i].first->lhs_ts[1] > 1)
	  assume(solver_pdr, assumptions_pdr, aiger_sign(tc.cube.ref_val[i].first->lhs_ts[1]) ?
		 tc.cube.ref_val[i].first->lhs_ts[1] >> 1 : -(tc.cube.ref_val[i].first->lhs_ts[1] >> 1));
      }
    }
    assume(solver_pdr, assumptions_pdr, tc.cube.act_lem);

    /* Add Invariants Frame */
    for (int i = 0; i < Frames.back().lemmas.size(); i++)
      assume(solver_pdr, assumptions_pdr, Frames.back().lemmas[i].act_lem);

    lbool res_check = check_sat(solver_pdr, assumptions_pdr, 0, 0);
    if (res_check == l_False)
      addLemma(tc);
    else
      j++;
  }

  if (verbose_pdr) {
    printf("[pdr] <%d> ", current_level_pdr);
    for (int i = 1; i < current_level_pdr; i++)
      printf("%lu ", Frames[i].lemmas.size());
    printf("| %lu || %lu\n", Frames[current_level_pdr].lemmas.size(),
	   Frames[current_level_pdr+1].lemmas.size());
  }
    
  return 0;
}

void newFrame() {
  Frames.push_back(Frames.back());
  assert(Frames.back().level == current_level_pdr + 1);
  current_level_pdr++;
  if (!verbose_pdr) printf("u%d\n", current_level_pdr);
  Frames[current_level_pdr] = Frame(current_level_pdr);
  Frames.back().level = current_level_pdr + 1;
  assert(Frames[current_level_pdr].level == current_level_pdr);
  assert(Frames.size() == current_level_pdr+2);
}

bool recBlockCube(TCube tc) {
  std::list<TCube> tcubeQ;
  insert_tc_Q(tcubeQ, tc);
  
  while (!tcubeQ.empty()) {

    if (verbose_pdr) {
      printf("[pdr] ");
      for (int i = 1; i < current_level_pdr; i++)
	printf("%lu ", Frames[i].lemmas.size());
      printf("| %lu || %lu\r", Frames[current_level_pdr].lemmas.size(),
	     Frames[current_level_pdr+1].lemmas.size());
    }
    
    TCube tc = tcubeQ.front();
    tcubeQ.pop_front();

    if (PDR_DEBUG) {
      printf("CTI to block at F%d\n", tc.frame_ind);
      //printf("To block : (frame %d) : ", tc.frame_ind);
      //print_cube(tc.cube);
    }

    if (tc.frame_ind == 0)
      return 0; // CEX

    if (!isBlocked(tc)) {
      assert(!isInitial(tc.cube));
      
      std::vector<TCube> zs = solveRelative(tc, 1, 0, 0);
      TCube z = zs.back();

      if (z.frame_ind != -1) {
	if (PDR_DEBUG) printf("=> Blocked %d\n", z.frame_ind);

	for (int i = 0; i < zs.size(); i++) {
	  z = zs[i];
	  while((z.frame_ind < (current_level_pdr - 1)) && condAssign(z, solveRelative(TCnext(z), 0, 0, 1))) {
	    if (PDR_DEBUG) printf("%d\n", z.frame_ind);
	  }
	  
	  addLemma(z);
	  
	  if (tc.frame_ind < current_level_pdr && (z.frame_ind != (current_level_pdr + 1)))
	    insert_tc_Q(tcubeQ, TCnext(tc));
	}
      } else {
	assert(zs.size() == 1);
	if (PDR_DEBUG) printf("=> CTI\n");
	z.frame_ind = tc.frame_ind - 1;
	insert_tc_Q(tcubeQ, tc);
	insert_tc_Q(tcubeQ, z);
	path_cex.push(std::pair<Cube, Cube>(z.cube, tc.cube));

      }
    } else {
      if (PDR_DEBUG) printf("=> Already blocked!\n");
    }
  }

  return 1;
}

int pdr(aiger *model, bool elim_ll, bool elim_hl, bool verbose_,  int *status) {
  int i, j;
  
  verbose_pdr = verbose_;


  dll_inputs_pdr = new dll_list<idef>();
  dll_latches_pdr = new dll_list<idef>();
  dll_ands_pdr = new dll_list<idef>();
  unsigned bad_def = init_model(model, dll_ands_pdr, dll_inputs_pdr, dll_latches_pdr, mapdef_pdr, elim_ll, verbose_);
  //aiger_reset(model);

  solver_pdr = new SimpSolver();
  init_solver(solver_pdr, elim_ll, verbose_);

  if (elim_hl)
    hl_ve(mapdef_pdr, dll_ands_pdr, verbose_pdr);

  act_I_pdr = new_var(&vars_pdr) >> 1;

  map_latches_pdr.push_back(NULL);
  map_latches_pdr.push_back(NULL);
  map_latches2_pdr.push_back(NULL);
  map_latches2_pdr.push_back(NULL);

  list_ands_pdr.push_back(unroll_pdr(0, elim_ll));
  iter_flatten_ptu(list_ands_pdr, 0);
  ands2clauses(solver_pdr, list_ands_pdr, 0, elim_ll);

  lit_bad_pdr = mapdef_pdr[aiger_lit2var(bad_def)]->lhs_ts[0];
  if (aiger_sign(lit_bad_pdr))
    lit_bad_pdr = aiger_sign(bad_def) ?  (lit_bad_pdr >> 1) : -(lit_bad_pdr >> 1);
  else
    lit_bad_pdr = aiger_sign(bad_def) ? -(lit_bad_pdr >> 1) :  (lit_bad_pdr >> 1);


  list_ands_pdr.push_back(unroll_pdr(1, elim_ll));
  iter_flatten_ptu(list_ands_pdr, 1);
  ands2clauses(solver_pdr, list_ands_pdr, 1, elim_ll); // Add a special COI to remove property related logic 

#if TARGET_ENLARGEMENT
  act_target = new_var(&vars_pdr) >> 1;

  map_latches_pdr.push_back(NULL);
  map_latches2_pdr.push_back(NULL);

  int lit_bad_pdr_prime = mapdef_pdr[aiger_lit2var(bad_def)]->lhs_ts[1];
  if (aiger_sign(lit_bad_pdr_prime))
    lit_bad_pdr_prime = aiger_sign(bad_def) ?  (lit_bad_pdr_prime >> 1) : -(lit_bad_pdr_prime >> 1);
  else
    lit_bad_pdr_prime = aiger_sign(bad_def) ? -(lit_bad_pdr_prime >> 1) :  (lit_bad_pdr_prime >> 1);
  tcl(solver_pdr, lit_bad_pdr, lit_bad_pdr_prime, -act_target);
#endif

  check_sat(solver_pdr, assumptions_pdr, 0); // Dummy sat call to perform bwd subsumption

  current_level_pdr = -1;
  Frames.push_back(Frame(0)); // Invariants Frame
  newFrame();
  Frames[current_level_pdr].lemmas.push_back(Cube(act_I_pdr));

  while(*status == -1) {
    while(!path_cex.empty()) path_cex.pop();

    Cube c = getBadCube();
    if (c.ref_val.size()) {
      if (!recBlockCube(TCube(c, current_level_pdr))) {
	*status = 1;
      }

    } else {
      newFrame();
      
      if (propagateLemmas()) {
	*status = 0;
      }
      
    }

  }  

  if (verbose_pdr) {
    printf("[pdr] ");
    for (int i = 1; i < current_level_pdr; i++)
      printf("%lu ", Frames[i].lemmas.size());
    printf("| %lu || %lu\n", Frames[current_level_pdr].lemmas.size(),
	   Frames[current_level_pdr+1].lemmas.size());
  }

#if PRINT_CEX
  /* CEX */
  if (*status) {
    //printf("TEST CEX (%lu):\n", path_cex.size());
    
    std::pair<Cube, Cube> p_c = path_cex.top();
    path_cex.pop();
    assert(isInitial(p_c.first));
    Cube current = p_c.first;
    int size = 0;
    bool done = path_cex.empty();
    while(!done) {
      if (current.ref_val == p_c.first.ref_val) {
	current = p_c.second;
	size++;

	//print_cube(p_c.first);
	//print_cube(p_c.second);


	for (int i = 0; i < p_c.first.ref_val.size() ; i++) {
	  if (p_c.first.ref_val[i].second) {
	    assume(solver_pdr, assumptions_pdr, p_c.first.ref_val[i].first->lhs_ts[0] >> 1);
	
	  } else {
	    assume(solver_pdr, assumptions_pdr, -(p_c.first.ref_val[i].first->lhs_ts[0] >> 1));
	  }
	  //printf("%d", p_c.first.ref_val[i].second);
	}
	//printf("\n");

	for (int i = 0; i < p_c.second.ref_val.size() ; i++) {
	  if (p_c.second.ref_val[i].second) {
	    assume(solver_pdr, assumptions_pdr, aiger_sign(p_c.second.ref_val[i].first->lhs_ts[1]) ?
		   -(p_c.second.ref_val[i].first->lhs_ts[1] >> 1) :
		   p_c.second.ref_val[i].first->lhs_ts[1] >> 1);
	
	  } else {
	    assume(solver_pdr, assumptions_pdr, aiger_sign(p_c.second.ref_val[i].first->lhs_ts[1]) ?
		   p_c.second.ref_val[i].first->lhs_ts[1] >> 1 :
		   -(p_c.second.ref_val[i].first->lhs_ts[1] >> 1));
	  }
	}

	lbool res_check = check_sat(solver_pdr, assumptions_pdr, 0, 0);
	assert(res_check == l_True);
      
	/* PRINT CEX */
	// idef* tmp_def;
	// dll_elem<idef> *tmp_e = dll_inputs_pdr->head;
	// while(tmp_e != NULL) {
	// 	tmp_def = tmp_e->def;
	
	// 	if (solver_pdr->modelValue(tmp_def->lhs_ts[0] >> 1) == l_True) {
	// 	  printf("1");
	  
	// 	} else if (solver_pdr->modelValue(tmp_def->lhs_ts[0] >> 1) == l_False) {
	// 	  printf("0");
	  
	// 	} else {
	// 	  assert(0);
	// 	}
	// 	tmp_e = tmp_e->next;
	// }
	// printf("\n");
	/*************/

      }
      p_c = path_cex.top();
      path_cex.pop();
      if (path_cex.empty())
	done = 1;
    }

    if (PDR_DEBUG) printf("CEX SIZE : %d\n", size);
    // print_cube(current);
    // print_cube(p_c.second);

    current = p_c.second;
    for (int i = 0; i < current.ref_val.size() ; i++) {
      if (current.ref_val[i].second) {
	assume(solver_pdr, assumptions_pdr, current.ref_val[i].first->lhs_ts[0] >> 1);
	
      } else {
	assume(solver_pdr, assumptions_pdr, -(current.ref_val[i].first->lhs_ts[0] >> 1));
      }
    }
    
    assume(solver_pdr, assumptions_pdr, lit_bad_pdr);
    assert(check_sat(solver_pdr, assumptions_pdr, 0, 0) == l_True);
    
    /* PRINT CEX */
    // idef* tmp_def;
    // dll_elem<idef> *tmp_e = dll_inputs_pdr->head;
    // while(tmp_e != NULL) {
    //   tmp_def = tmp_e->def;
	
    //   if (solver_pdr->modelValue(tmp_def->lhs_ts[0] >> 1) == l_True) {
    // 	printf("1");
	
    //   } else if (solver_pdr->modelValue(tmp_def->lhs_ts[0] >> 1) == l_False) {
    // 	printf("0");
	
    //   } else {
    // 	assert(0);
    //   }
    //   tmp_e = tmp_e->next;
    // }
    // printf("\n");
    /**************/

  }
  /********/

#endif

  //printf("%d\nb0\n.\n", *status);
  
  destroy_model(dll_ands_pdr, dll_inputs_pdr, dll_latches_pdr, list_ands_pdr, mapdef_pdr, map_ands_pdr, verbose_);


  return *status;
}
