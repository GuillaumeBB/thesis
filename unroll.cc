/***************************************************************************
Copyright (c) 2016-2017, Guillaume Baud-Berthier, SafeRiver/LaBRI

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
IN THE SOFTWARE.
***************************************************************************/
#include "unroll.h"

unsigned init_model(aiger* model, dll_list<idef>* dll_ands, dll_list<idef>* dll_inputs,
		    dll_list<idef>* dll_latches, std::vector<idef*> &mapdef, bool elim_ll, bool verbose) {
  idef *tmp_idef;
  prop_tr *tmp_prop_tr;
  int i;

  mapdef = std::vector< idef* >(model->maxvar+1);
  dll_init(dll_inputs);
  dll_init(dll_latches);
  dll_init(dll_ands);

  // Inputs
  for (i = 0; i < model->num_inputs; i++) {
    tmp_idef = new idef();
    tmp_idef->type = idef_input;
    tmp_idef->hl_type = UNK;
    tmp_idef->lhs = aiger_lit2var(model->inputs[i].lit);
    tmp_idef->lhs_ts = std::vector<int>();
    tmp_idef->dlle = dll_insert_last(dll_inputs, aiger_lit2var(model->inputs[i].lit), tmp_idef);
    //tmp_idef->apply_ve = 0;

    tmp_idef->rhs_tr = new prop_tr(tmp_idef->lhs);
    //tmp_idef->pt_comp = 0;

    tmp_idef->update_elim = 0;
    tmp_idef->cost = 0;

    mapdef[aiger_lit2var(model->inputs[i].lit)] = tmp_idef;

  }

  // Latches
  for (i = 0; i < model->num_latches; i++) {
    tmp_idef = new idef();
    tmp_idef->type = idef_latch;
    tmp_idef->hl_type = UNK;
    tmp_idef->lhs = aiger_lit2var(model->latches[i].lit);
    tmp_idef->lhs_ts = std::vector<int>();
    tmp_idef->rhs.push_back(model->latches[i].next);
    tmp_idef->dlle = dll_insert_last(dll_latches, aiger_lit2var(model->latches[i].lit), tmp_idef);
    //tmp_idef->apply_ve = 0;
    
    tmp_idef->rhs_tr = new prop_tr(tmp_idef->lhs);
    //tmp_idef->pt_comp = 0;

    tmp_idef->update_elim = 0;
    tmp_idef->cost = 0;

    mapdef[aiger_lit2var(model->latches[i].lit)] = tmp_idef;
  }

  // Ands
  for (i = 0; i < model->num_ands; i++) {
    tmp_idef = new idef();
    tmp_idef->type = idef_and;
    tmp_idef->hl_type = UNK;
    tmp_idef->lhs = aiger_lit2var(model->ands[i].lhs);
    tmp_idef->lhs_ts = std::vector<int>();
    tmp_idef->rhs.push_back(model->ands[i].rhs0);
    tmp_idef->rhs.push_back(model->ands[i].rhs1);
    tmp_idef->dlle = dll_insert_last(dll_ands, aiger_lit2var(model->ands[i].lhs), tmp_idef);
    tmp_idef->apply_ve = 1;

    tmp_prop_tr = new prop_tr();
    tmp_prop_tr->op = AND;
    tmp_prop_tr->var = tmp_idef->lhs;
    tmp_prop_tr->l_sign = aiger_sign(model->ands[i].rhs0);
    tmp_prop_tr->r_sign = aiger_sign(model->ands[i].rhs1);
    tmp_prop_tr->l = new prop_tr(aiger_lit2var(model->ands[i].rhs0));
    tmp_prop_tr->r = new prop_tr(aiger_lit2var(model->ands[i].rhs1));
    tmp_idef->rhs_tr = tmp_prop_tr;
    tmp_idef->pt_comp = 1;
    tmp_idef->pt_var = 2;

    tmp_idef->update_elim = 0;
    tmp_idef->cost = 0;

    mapdef[aiger_lit2var(model->ands[i].lhs)] = tmp_idef;

    if (model->ands[i].rhs0 > 1) {
      mapdef[aiger_lit2var(model->ands[i].rhs0)]->refBy.insert(tmp_idef);
    }
    if (model->ands[i].rhs1 > 1) {
      mapdef[aiger_lit2var(model->ands[i].rhs1)]->refBy.insert(tmp_idef);
    }
  }

  // Latches
  for (i = 0; i < model->num_latches; i++) {
    assert(!model->latches[i].reset);

    if (model->latches[i].next > 1) {
      mapdef[aiger_lit2var(model->latches[i].next)]->refBy.
	insert(mapdef[aiger_lit2var(model->latches[i].lit)]);
      mapdef[aiger_lit2var(model->latches[i].next)]->apply_ve = 0;
    }
  }

  // Disable VE for bad def
  mapdef[aiger_lit2var(model->bad[0].lit)]->apply_ve = 0;

  // Set bad_lit
  unsigned bad_lit = -1;
  if (model->num_bad > 1) {
    printf("Multiples properties are not supported yet!\n");
    exit(-1);
    
  } else {
    bad_lit = model->bad[0].lit;
  }

  // Assume 
  tmp_idef = new idef();
  tmp_idef->type = idef_assume;
  tmp_idef->hl_type = UNK;
  tmp_idef->lhs = 0;
  tmp_idef->lhs_ts = std::vector<int>();
  tmp_idef->rhs_tr = NULL;

  mapdef[0] = tmp_idef;

  //aiger_reset(model);

  return bad_lit;
}

void destroy_model(dll_list<idef>* dll_ands, dll_list<idef>* dll_inputs,
		   dll_list<idef>* dll_latches, std::vector<dll_list<ands_def>*> &list_ands,
		   std::vector<idef*> &mapdef, std::map<unsigned, ands_def*> &map_ands, bool verbose) {
  if (verbose) printf("[MC]  Terminating..");
  fflush(stdout);
  clock_t start = clock();

  for (int i = 0; i < mapdef.size(); i++) {
    delete mapdef[i]->dlle;
    delete mapdef[i];
  }
  dll_delete(dll_ands);
  dll_delete(dll_inputs);
  dll_delete(dll_latches);

  std::map< unsigned, ands_def* >::iterator it;
  for (it = map_ands.begin(); it != map_ands.end(); it++) {
    assert(it->second != NULL);
    assert(it->second->rhs_tr == NULL);
    delete it->second->rhs_tr;
    destroy_ptuf(it->second->rhs_tr_fl);
    delete it->second->dllade;
    delete it->second;
  }

  for(int i = 0; i < list_ands.size(); i++) {
    dll_delete(list_ands[i]);
  }

  map_ands.clear();

  if (verbose) printf(" done (%.2fs)\n", ((double) (clock() - start)) / CLOCKS_PER_SEC);
}

dll_list<ands_def>* unroll(dll_list<idef>* dll_ands, dll_list<idef>* dll_inputs, dll_list<idef>* dll_latches, std::vector<idef*> &mapdef, std::map<unsigned, ands_def*> &map_ands, unsigned *v, int time) {
  int j;
  unsigned tmp_rhs;
  unsigned tmp_rhs0, tmp_rhs1;

  dll_list<ands_def> *tmp_list_ands;
  tmp_list_ands = new dll_list<ands_def>();
  dll_init<ands_def>(tmp_list_ands);
  
  idef* tmp_def;
  dll_elem<idef> *tmp_e;
  std::list<unsigned>::iterator it_rhs;
  std::set<unsigned>::iterator it_tmp_rhs;

  // Inputs
  tmp_e = dll_inputs->head;
  while(tmp_e != NULL) {
    tmp_def = tmp_e->def;
    
    tmp_def->lhs_ts.push_back(new_var(v));

    tmp_e = tmp_e->next;
  }
  
  // Latches
  tmp_e = dll_latches->head;
  while(tmp_e != NULL) {
    tmp_def = tmp_e->def;

    if (!time) { // Init
      tmp_def->lhs_ts.push_back(0);

    } else {
      assert(tmp_def->rhs.size() == 1);
      
      if (tmp_def->rhs.front() > 1) {
	assert(tmp_def->lhs_ts[time-1] != -1);

	if (aiger_sign(tmp_def->rhs.front()))
	  tmp_def->lhs_ts.push_back
	    (aiger_not(mapdef[aiger_lit2var(tmp_def->rhs.front())]->lhs_ts[time-1]));
	else
	  tmp_def->lhs_ts.push_back(mapdef[aiger_lit2var(tmp_def->rhs.front())]->lhs_ts[time-1]);
	
      } else { // Cst latch
	tmp_def->lhs_ts.push_back(tmp_def->rhs.front());
      }
    }
    tmp_e = tmp_e->next;
  }

  // Ands
  tmp_e = dll_ands->head;
  while(tmp_e != NULL) {
    tmp_def = tmp_e->def;

    prop_tr_un *ptu = unroll_prop_tr(mapdef, map_ands, tmp_def->rhs_tr, time, 0);

    if (ptu->lit == UNDEF_LIT) {
      assert(ptu->op != VAR);
      tmp_def->lhs_ts.push_back(new_var(v));

      ands_def *tmp_ands = new ands_def(tmp_def->lhs_ts[time], ptu, tmp_def, time);

      map_ands.insert(std::pair<unsigned, ands_def*>(aiger_lit2var(tmp_ands->lhs), tmp_ands));
      
      tmp_ands->dllade = dll_insert_last(tmp_list_ands,
					 aiger_lit2var(tmp_def->lhs_ts[time]), tmp_ands);

    } else {
      assert(ptu->op == VAR);
      tmp_def->lhs_ts.push_back(ptu->lit);

    }

    assert(tmp_def->lhs_ts.size() == (time + 1));
    tmp_e = tmp_e->next;
  }

  return tmp_list_ands;
}

// int addPO(SimpSolver* solver, std::vector<idef*> &mapdef, unsigned *v, unsigned po, int time) {
//   mapdef[0]->lhs_ts.push_back(new_var(v));

//   int lit_po;
  
//   lit_po = mapdef[aiger_lit2var(po)]->lhs_ts[time];

//   if (aiger_sign(lit_po))
//     lit_po = aiger_sign(po) ?  (lit_po >> 1) : -(lit_po >> 1);
//   else
//     lit_po = aiger_sign(po) ? -(lit_po >> 1) :  (lit_po >> 1);

//   bcl(solver, -(mapdef[0]->lhs_ts.back() >> 1), lit_po);

//   return lit_po;
// }

prop_tr_un* unroll_prop_tr(std::vector<idef*> &mapdef, std::map<unsigned, ands_def*> &map_ands, prop_tr *pt, int time, bool _not) {
  prop_tr_un *ptu = new prop_tr_un();

  if (pt->op == VAR) {
    if (pt->var) {
      ptu->lit = _not ? aiger_not(mapdef[pt->var]->lhs_ts[time]) : mapdef[pt->var]->lhs_ts[time];

      if (ptu->lit > 1) {
	std::map<unsigned, ands_def*>::iterator it_lit_ref;
	it_lit_ref = map_ands.find(aiger_lit2var(ptu->lit));

	if (it_lit_ref != map_ands.end())
	  ptu->lit_ref = it_lit_ref->second;

      }
    } else {
      ptu->lit = _not;
    }

  } else {
    assert(!_not);
    assert(!pt->l_sign || pt->l->op == VAR);
    assert(!pt->r_sign || pt->r->op == VAR);

    ptu->l = unroll_prop_tr(mapdef, map_ands, pt->l, time, pt->l_sign);
    ptu->r = unroll_prop_tr(mapdef, map_ands, pt->r, time, pt->r_sign);

    unsigned ll = ptu->l->lit;
    unsigned lr = ptu->r->lit;
    
    if (pt->op == AND) {
      if (ll == 0 || lr == 0 || ll == aiger_not(lr)) {
	ptu->lit = 0;
	delete ptu->l;
	ptu->l = NULL;
	delete ptu->r;
	ptu->r = NULL;
	ptu->op = VAR;

      } else if (lr == 1) {
	if (ll == UNDEF_LIT) {
	  delete ptu->r;
	  ptu->op = ptu->l->op;
	  ptu->lit = ptu->l->lit;
	  ptu->lit_ref = ptu->l->lit_ref;
	  ptu->r = ptu->l->r;
	  prop_tr_un *toDestr = ptu->l;
	  ptu->l = ptu->l->l;
	  toDestr->l = NULL;
	  toDestr->r = NULL;
	  delete toDestr;
	  //ptu = ptu->l;

	} else {
	  ptu->lit = ll;
	  ptu->lit_ref = ptu->l->lit_ref;
	  delete ptu->l;
	  ptu->l = NULL;
	  delete ptu->r;
	  ptu->r = NULL;
	  ptu->op = VAR;
	}

      } else if (ll == 1) {
	if (lr == UNDEF_LIT) {
	  delete ptu->l;
	  ptu->op = ptu->r->op;
	  ptu->lit = ptu->r->lit;
	  ptu->lit_ref = ptu->r->lit_ref;
	  ptu->l = ptu->r->l;
	  prop_tr_un *toDestr = ptu->r;
	  ptu->r = ptu->r->r;
	  toDestr->l = NULL;
	  toDestr->r = NULL;
	  delete toDestr;
	  //ptu = ptu->r;

	} else {
	  ptu->lit = lr;
	  ptu->lit_ref = ptu->r->lit_ref;
	  delete ptu->l;
	  ptu->l = NULL;
	  delete ptu->r;
	  ptu->r = NULL;
	  ptu->op = VAR;
	}

      } else if (ll == lr && ll != UNDEF_LIT) {
	  ptu->lit = ll;
	  ptu->lit_ref = ptu->l->lit_ref;
	  delete ptu->l;
	  ptu->l = NULL;
	  delete ptu->r;
	  ptu->r = NULL;
	  ptu->op = VAR;
	

      } else {
	ptu->lit = UNDEF_LIT;//new_var();
	ptu->op = pt->op;
      }

    } else {
      if (ll == 1 || lr == 1 || ll == aiger_not(lr)) {
	ptu->lit = 1;
	delete ptu->l;
	ptu->l = NULL;
	delete ptu->r;
	ptu->r = NULL;
	ptu->op = VAR;

      } else if (lr == 0) {
	if (ll == UNDEF_LIT) {
	  delete ptu->r;
	  ptu->op = ptu->l->op;
	  ptu->lit = ptu->l->lit;
	  ptu->lit_ref = ptu->l->lit_ref;
	  ptu->r = ptu->l->r;
	  prop_tr_un *toDestr = ptu->l;
	  ptu->l = ptu->l->l;
	  toDestr->l = NULL;
	  toDestr->r = NULL;
	  delete toDestr;
	  //ptu = ptu->l;

	} else {
	  ptu->lit = ll;
	  ptu->lit_ref = ptu->l->lit_ref;
	  delete ptu->l;
	  ptu->l = NULL;
	  delete ptu->r;
	  ptu->r = NULL;
	  ptu->op = VAR;
	}

      } else if (ll == 0) {
	if (lr == UNDEF_LIT) {
	  delete ptu->l;
	  ptu->op = ptu->r->op;
	  ptu->lit = ptu->r->lit;
	  ptu->lit_ref = ptu->r->lit_ref;
	  ptu->l = ptu->r->l;
	  prop_tr_un *toDestr = ptu->r;
	  ptu->r = ptu->r->r;
	  toDestr->l = NULL;
	  toDestr->r = NULL;
	  delete toDestr;
	  //ptu = ptu->r;

	} else {
	  ptu->lit = lr;
	  ptu->lit_ref = ptu->r->lit_ref;
	  delete ptu->l;
	  ptu->l = NULL;
	  delete ptu->r;
	  ptu->r = NULL;
	  ptu->op = VAR;
	}

      } else if (ll == lr && ll != UNDEF_LIT) {
	ptu->lit = ll;
	ptu->lit_ref = ptu->l->lit_ref;
	delete ptu->l;
	ptu->l = NULL;
	delete ptu->r;
	ptu->r = NULL;
	ptu->op = VAR;
	
      } else {
	ptu->lit = UNDEF_LIT;//new_var();
	ptu->op = pt->op;
      }
    }
  }

  return ptu;
}


void iter_flatten_ptu(std::vector<dll_list<ands_def>*> &list_ands, int time) {
  ands_def* tmp_def;
  dll_elem<ands_def> *tmp_e;
  
  // Ands def
  tmp_e = list_ands[time]->head;
  while(tmp_e != NULL) {
    tmp_def = tmp_e->def;

    tmp_def->rhs_tr_fl = flatten_ptu(tmp_def, tmp_def->rhs_tr);

    delete tmp_def->rhs_tr;
    tmp_def->rhs_tr = NULL;

    tmp_e = tmp_e->next;
  }
}

prop_tr_un_fl* flatten_ptu(ands_def *ad, prop_tr_un *ptu) {
  prop_tr_un_fl *ptuf;

  if (ptu->op == VAR) {
    assert(ptu->lit != UNDEF_LIT);
    ptuf = new prop_tr_un_fl();
    ptuf->lit = ptu->lit;
    ptuf->op = ptu->op;
    ptuf->lit_ref = ptu->lit_ref;
    //if (ptuf->lit_ref != NULL)
    //ptuf->lit_ref->refBy.insert(ad);

  } else {
    assert(ptu->lit == UNDEF_LIT);
    ptuf = new prop_tr_un_fl();
    ptuf->lit = ptu->lit;
    ptuf->op = ptu->op;

    list_ptuf *lp;
    prop_tr_un_fl *ptuf_l = flatten_ptu(ad, ptu->l);
    prop_tr_un_fl *ptuf_r = flatten_ptu(ad, ptu->r);
    
    if (ptu->op == ptuf_l->op) {
      assert(ptuf_l->lit == UNDEF_LIT);
      assert(ptuf_l->lp->size > 1);
      lp = ptuf_l->lp;
      //delete ptuf_l;
      
    } else {
      lp = new list_ptuf();
      //insert_last(lp, ptuf_l);
      insert_ordered(lp, ptuf_l);
    }

    if (ptu->op == ptuf_r->op) {
      assert(ptuf_r->lit == UNDEF_LIT);
      assert(ptuf_r->lp->size > 1);
      list_ptuf_e* it = ptuf_r->lp->head;
      while(it != NULL) {
	//insert_last(lp, it->ptuf);
	insert_ordered(lp, it->ptuf);
	it = it->next;
	//if (it != NULL)
	//   delete it->prev;
	
      }
      //delete ptuf_r->lp;
      //delete ptuf_r;
      
    } else {
      //insert_last(lp, ptuf_r);
      insert_ordered(lp, ptuf_r);
    }

    ptuf->lp = lp;
  }

  //delete ptu;
  //ptu = NULL;

  return ptuf;
}

