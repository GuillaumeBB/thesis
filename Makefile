CC=g++ -std=c++11 -O3
DEBUG=#-DNDEBUG
LDFLAGS=$(DEBUG) -L./glucose-syrup_mod/simp
CFLAGS=$(DEBUG) -I./glucose-syrup_mod -I./glucose-syrup_mod/simp -I./aiger-1.9.4
LIBS=-l_release -ldl -lreadline -static-libstdc++
.c.o:
	$(CC) $(CFLAGS) -g -c $<
.cc.o:
	$(CC) $(CFLAGS) -g -c $<

mc: main.o utils.o unroll.o solver.o hl_ve.o zigzag.o zigzag_bwd.o aigprep.o bmc.o un_simp.o dual.o pdr.o
	$(CC) $(LDFLAGS) -g -o $@ main.o utils.o unroll.o solver.o hl_ve.o zigzag.o zigzag_bwd.o aigprep.o bmc.o un_simp.o dual.o pdr.o aiger-1.9.4/aiger.o $(LIBS)

clean:
	rm -f mc
	rm -f utils.o unroll.o solver.o hl_ve.o zigzag.o zigzag_bwd.o main.o aigprep.o bmc.o un_simp.o dual.o pdr.o
