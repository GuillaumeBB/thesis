/***************************************************************************
Copyright (c) 2016-2017, Guillaume Baud-Berthier, SafeRiver/LaBRI

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
IN THE SOFTWARE.
***************************************************************************/

#include "utils.h"

bool answer_found = 0;

int new_var(unsigned *v) {
  return (*v = (*v)+2);
}

bool terminate_flag() {
  return answer_found;
}

void set_terminate_flag() {
  answer_found = 1;
}
/* ========================================================================== */
/*                               Debug functions                              */
/* ========================================================================== */
void print_idef(idef *d) {
  /*
  std::list<unsigned>::iterator it_rhs;
  
  printf("%d =", aiger_var2lit(d->lhs));
  for (it_rhs = d->rhs.begin(); it_rhs != d->rhs.end(); ++it_rhs) {
    if (std::next(it_rhs) == d->rhs.end())
      printf(" %d", *it_rhs);
    else
      printf(" %d &", *it_rhs);
      }
  */
  if (d->type == idef_and) {
    printf("%d = ", d->lhs);
    print_pt(d->rhs_tr);
    printf("(ref: %lu)\n", d->refBy.size());
  } else if (d->type == idef_latch) {
    printf("%d = 0, %d ", d->lhs, d->rhs.front());
    printf("(ref: %lu)\n", d->refBy.size());
  } else if (d->type == idef_input) {
    printf("IN : %d ", d->lhs);
    printf("(ref: %lu)\n", d->refBy.size());
  }
}

void print_pt(prop_tr *pt) {
  if (pt->op == VAR) {
    printf("%d", pt->var);
  } else {
    if (pt->l_sign) {
      printf("(~");
      print_pt(pt->l);
    } else {
      printf("(");
      print_pt(pt->l);
    }
    if (pt->op == AND)
      printf(" && ");
    else
      printf(" || ");
    
    if (pt->r_sign) {
      printf("~");
      print_pt(pt->r);
    } else {
      print_pt(pt->r);
    }
    printf(")");
  }
}

void print_ptu(prop_tr_un *pt) {
  if (pt->op == VAR) {
    printf("%d", pt->lit);
  } else {
    printf("(");
    print_ptu(pt->l);
    
    if (pt->op == AND)
      printf(" && ");
    else
      printf(" || ");

    print_ptu(pt->r);
    printf(")");
  }
}

void print_lp(list_ptuf *lp) {
  list_ptuf_e *it = lp->head;
  printf("| ");
  while(it != NULL) {
    if (it->ptuf->op == VAR) {
      printf("VAR: %d | ", it->ptuf->lit);
    } else if (it->ptuf->op == AND) 
      printf("&& | ");
    else
      printf("|| | ");

    it = it->next;
  }
  printf("\n");
}

void print_ptuf(prop_tr_un_fl *pt) {
  if (pt->op == VAR) {
    printf("%d", pt->lit);
  } else {
    printf("(");
    list_ptuf_e *it = pt->lp->head;
    while(it != NULL) {
      print_ptuf(it->ptuf);
      if (it->next == NULL)
	printf(")");
      else if (pt->op == AND)
	printf(" && ");
      else
	printf(" || ");
      it = it->next;
    }
  }
}

void print_ptuf_bis(prop_tr_un_fl *pt) {
  if (pt->op == VAR) {
    aiger_sign(pt->lit) ?
      printf("~v%d", aiger_lit2var(pt->lit)) : 
      printf(" v%d", aiger_lit2var(pt->lit));
  } else {
    printf("(");
    list_ptuf_e *it = pt->lp->head;
    while(it != NULL) {
      print_ptuf_bis(it->ptuf);
      if (it->next == NULL)
	printf(")");
      else if (pt->op == AND)
	printf(" & ");
      else
	printf(" # ");
      it = it->next;
    }
  }
}


void print_ad_ptu(ands_def *ad) {
  if (ad->cst != -1) {
    printf("CST : %d -> %d\n", ad->lhs, ad->cst);

  } else {
    printf("%d -> ", ad->lhs);
    print_ptu(ad->rhs_tr);
    printf("\n");
  }
}

void print_ad_ptuf(ands_def *ad) {
  if (ad->cst != -1) {
    printf("CST : %d -> %d\n", ad->lhs, ad->cst);

  } else {
    printf("%d -> ", ad->lhs);
    print_ptuf(ad->rhs_tr_fl);
    printf("\n");
  }
}

void print_ad_type(ands_def *ad) {
  switch(ad->type) {
  case undef_t:
    {
      printf("UNDEF");
      break;
    }
  case and_t:
    {
      printf("AND");
      break;
    }
  case or_t:
    {
      printf("OR");
      break;
    }
  case xor_t:
    {
      printf("XOR");
      break;
    }
  case equiv_t:
    {
      printf("EQUIV");
      break;
    }
  case ite_t:
    {
      printf("ITE");
      break;
    }
  default:
    {
      printf("ERROR");
      break;
    }
  }
}

void print_ad_ptuf_bis(ands_def *ad) {
  if (ad->cst != -1) {
    aiger_sign(ad->cst) ?
      printf("CST : %d -> ~%d\n", aiger_lit2var(ad->lhs), aiger_lit2var(ad->cst)) :
      printf("CST : %d ->  %d\n", aiger_lit2var(ad->lhs), aiger_lit2var(ad->cst));
  } else {
    printf("v%d := ", aiger_lit2var(ad->lhs));
    print_ptuf_bis(ad->rhs_tr_fl);
    printf("; // (");
    print_ad_type(ad);
    printf(")\n");
  }
}

void print_ad_ptuf_f(ands_def *ad) {
  if (ad->cst != -1) {
    printf("CST : %d -> %d\n", ad->lhs, ad->cst);
    printf("%d -> ", ad->lhs);
    print_ptuf(ad->rhs_tr_fl);
    printf("\n");
  } else {
    printf("%d -> ", ad->lhs);
    print_ptuf(ad->rhs_tr_fl);
    printf("\n");
  }
}

void print_ad_ite(ands_def *ad) {
  if (ad->I != 0) {
    printf("%d := ", aiger_lit2var(ad->lhs));
    aiger_sign(ad->I) ? printf("ITE( ~%d, ", aiger_lit2var(ad->I)) :
      printf("ITE(  %d, ", aiger_lit2var(ad->I));
    aiger_sign(ad->T) ? printf("~%d, ", aiger_lit2var(ad->T)) : printf(" %d, ", aiger_lit2var(ad->T));
    aiger_sign(ad->E) ? printf("~%d)\n", aiger_lit2var(ad->E)) : printf(" %d)\n", aiger_lit2var(ad->E));
  }
}

int get_mem() {
  int tSize = 0, resident = 0, share = 0;
  std::ifstream buffer("/proc/self/statm");
  buffer >> tSize >> resident >> share;
  buffer.close();

  long page_size_kb = sysconf(_SC_PAGE_SIZE) / 1024; // in case x86-64 is configured to use 2MB pages
  double rss = resident * page_size_kb;
  return (int)rss/1000;
}

/* ========================================================================== */
/*                      List prop_tr_un_fl function                           */
/* ========================================================================== */
void insert_last(list_ptuf *lp, prop_tr_un_fl *ptuf) {
  if (!lp->size) {
    list_ptuf_e *lpe = new list_ptuf_e();
    lpe->ptuf = ptuf;
    lp->size = 1;
    lp->head = lpe;
    lp->tail = lpe;

  } else {
    list_ptuf_e *lpe = new list_ptuf_e();
    lpe->ptuf = ptuf;
    lpe->prev = lp->tail;
    lp->tail->next = lpe;
    lp->tail = lpe;
    lp->size++;
  }
}


int compare_ptuf(prop_tr_un_fl *ptuf1, prop_tr_un_fl *ptuf2) {
  if (ptuf1->op == ptuf2->op) {
    if (ptuf1->op == VAR) {
      return ptuf1->lit < ptuf2->lit;
	
    } else {
      return ptuf1->lp->size < ptuf2->lp->size;
      //return hash_ptuf(ptuf1) < hash_ptuf(ptuf2);
    }
    
  } else {
    if (ptuf1->op == VAR)
      return 1;
    if (ptuf2->op == VAR)
      return 0;
    if (ptuf1->op == AND)
      return 1;
    if (ptuf2->op == AND)
      return 0;
    assert(0);
    
  }
  return 0;
}

void insert_ordered(list_ptuf *lp, prop_tr_un_fl *ptuf) {
  if (!lp->size) {
    list_ptuf_e *lpe = new list_ptuf_e();
    lpe->ptuf = ptuf;
    lp->size = 1;
    lp->head = lpe;
    lp->tail = lpe;

  } else {
    bool inserted = 0;
    list_ptuf_e *e = lp->head;
    while(!inserted) {
      if (compare_ptuf(ptuf, e->ptuf)) { 
	list_ptuf_e *lpe = new list_ptuf_e();
	lpe->ptuf = ptuf;
	lpe->prev = e->prev;
	lpe->next = e;
	if (lpe->prev == NULL)
	  lp->head = lpe;
	else
	  e->prev->next = lpe;
	e->prev = lpe;
	lp->size++;
	inserted = 1;
      } else {
	if (e->next == NULL) {
	  list_ptuf_e *lpe = new list_ptuf_e();
	  lpe->ptuf = ptuf;
	  lpe->prev = lp->tail;
	  lp->tail->next = lpe;
	  lp->tail = lpe;
	  lp->size++;
	  inserted = 1;
	}
	e = e->next;
      }
    }
  }
}

void remove(list_ptuf *lp, list_ptuf_e *e) { 
  assert(lp->size);
  if (lp->head == e) {
    if (lp->size == 1) {
      lp->head = NULL;
      lp->tail = NULL;
    } else {
      lp->head = e->next;
      e->next->prev = NULL;
    }
  } else if (lp->tail == e) {
    lp->tail = e->prev;
    e->prev->next = NULL;    
  } else {
    e->prev->next = e->next;
    e->next->prev = e->prev;
  }
  lp->size--;
}


void reorder_lp(list_ptuf* lp, list_ptuf_e *e) {

  if (e->prev != NULL && e->ptuf->lit < e->prev->ptuf->lit) {

    list_ptuf_e *tmp = e->prev;
    while (tmp != NULL && e->ptuf->lit < tmp->ptuf->lit) {
      e->prev   = tmp->prev;
      tmp->next = e->next;      
      e->next   = tmp;
      tmp->prev = e;
      if (e->prev == NULL)
	lp->head = e;
      else
	e->prev->next = e;
      if (tmp->next == NULL)
	lp->tail = tmp;
      else
	tmp->next->prev = tmp;

      tmp = e->prev;
    }

  } else if (e->next != NULL && e->ptuf->lit > e->next->ptuf->lit) {

    list_ptuf_e *tmp = e->next;
    while (tmp != NULL && e->ptuf->lit > tmp->ptuf->lit) {
      e->next   = tmp->next;
      tmp->prev = e->prev;
      e->prev   = tmp;
      tmp->next = e;
      if (e->next == NULL)
	lp->tail = e;
      else
	e->next->prev = e;
      if (tmp->prev == NULL)
	lp->head = tmp;
      else
	tmp->prev->next = tmp;

      tmp = e->next;
    }
  }
}

void check_def_consistency(prop_tr_un_fl *ptuf) {
  list_ptuf_e *it = ptuf->lp->head;
  while(it != NULL) {
    if (it->ptuf->op == VAR) {
      if (it->ptuf->lit_ref != NULL) {
	assert(aiger_lit2var(it->ptuf->lit) == aiger_lit2var(it->ptuf->lit_ref->lhs));
      }
    } else {
      check_def_consistency(it->ptuf);
    }
    it = it->next;
  }
}

void destroy_ptuf(prop_tr_un_fl *ptuf) {
  if (ptuf != NULL) {
    if (ptuf->lp != NULL) {
      list_ptuf_e *it = ptuf->lp->head;
      list_ptuf_e *it_prev;
    
      while(it != NULL) {
	destroy_ptuf(it->ptuf);
	it_prev = it;
	it = it->next;
	delete it_prev;
      }
      delete ptuf->lp;
      ptuf->lp = NULL;
    }
    delete ptuf;
    ptuf = NULL;
  }
}
