/***************************************************************************
Copyright (c) 2016-2017, Guillaume Baud-Berthier, SafeRiver/LaBRI

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
IN THE SOFTWARE.
***************************************************************************/
extern "C" {
#include "aiger.h"
}

#include "aigprep.h"
#include <stdlib.h> 
#include <cassert>
#include <stack>
#include <queue>
#include <unordered_map>
#include <ctime>
#include <math.h> 
#include <algorithm>

unsigned bad_state;

int verbose;

bool balance_aig_OPT = 0;
bool preprocess_constraints_OPT = 0;

unsigned stats_struct_equiv = 0;
unsigned stats_func_equiv = 0;
unsigned stats_func_equiv_diff1 = 0;
unsigned stats_func_subsume = 0;
unsigned stats_unref = 0;
unsigned stats_cst_prop = 0;
unsigned stats_simp = 0;
unsigned stats_trivia = 0;
unsigned stats_balance = 0;

unsigned *reencode_;

unsigned MAX_ITER_TS = 1000;

unsigned *level;
unsigned GAP_LEVEL_CTR_s = 5;
unsigned GAP_LEVEL_CTR_sb = 10;
unsigned GAP_LEVEL_CTR_d = 15;
std::list<std::pair<unsigned, unsigned>> simple_ctr;
std::list<std::tuple<unsigned, unsigned, unsigned>> double_ctr;
unsigned stats_simple_ctr = 0;
unsigned stats_simple_ctr_bis = 0;
unsigned stats_double_ctr = 0;

unsigned stats_size_func_cst = 0;
unsigned stats_size_func = 0;

std::map<unsigned, aiger_def*> hash;
std::unordered_multimap<unsigned, aiger_def*> hash_combi_struct_equiv;
std::unordered_multimap<unsigned, aiger_def*> hash_combi_func_equiv;
std::unordered_multimap<unsigned, aiger_def*> hash_latch_struct_equiv;

dll_list<aiger_def> *l_ands;
dll_list<aiger_def> *l_inputs;
dll_list<aiger_def> *l_latches;

void destroy_prep() {
  if (verbose) printf("[aigprep]  Terminating..");
  fflush(stdout);
  clock_t start = clock();
  for (std::map<unsigned,aiger_def*>::iterator it = hash.begin(); it != hash.end(); ++it)
    delete it->second;
  hash.clear();
  if (l_ands != NULL)
    dll_delete(l_ands);
  if (l_inputs != NULL)
    dll_delete(l_inputs);
  if (l_latches != NULL)
    dll_delete(l_latches);
  
  delete[] level;
  delete[] reencode_;
  
  hash_combi_struct_equiv.clear();
  hash_latch_struct_equiv.clear();
  hash_combi_func_equiv.clear();

  stats_struct_equiv = 0;
  stats_func_equiv = 0;
  stats_unref = 0;
  stats_cst_prop = 0;
  if (verbose) printf(" done (%.2fs)\n", ((double) (clock() - start)) / CLOCKS_PER_SEC);
}


void init_prep(aiger * model, bool cex, int verb) {
  int i;
  aiger_def *def, *def_tmp;
  verbose = verb;

  if (verbose) printf("[aigprep]  Initialization..");
  fflush(stdout);
  clock_t start = clock();

  l_inputs = new dll_list<aiger_def>();
  dll_init(l_inputs);
  l_latches = new dll_list<aiger_def>();
  dll_init(l_latches);
  l_ands = new dll_list<aiger_def>();
  dll_init(l_ands);

  level = new unsigned[model->maxvar+1];
  reencode_ = new unsigned[2 * (model->maxvar+1)];
  

  /* Add inputs into hashmap */
  for (i = 0; i < model->num_inputs; i++) {
    assert(!aiger_sign(model->inputs[i].lit));

    def = new aiger_def();
    def->type = aiger_def_input;
    def->var_lhs = aiger_lit2var(model->inputs[i].lit);
    def->op1  = 0;
    def->op2  = 0;
    hash.insert(std::pair<unsigned, aiger_def*>(aiger_lit2var(model->inputs[i].lit), def));
    def->dle = dll_insert_last(l_inputs, aiger_lit2var(model->inputs[i].lit), def);
    level[def->var_lhs] = 0;
  }
  assert(dll_size(l_inputs) == hash.size());
  assert(hash.size() == model->num_inputs);

  /* Add latches into hashmap */
  for (i = 0; i < model->num_latches; i++) {
    if (model->latches[i].reset != 0) { 
      printf("[aigprep] Only handle uninitialized reset!"); 
      exit(-1); 
    }
    assert(!aiger_sign(model->latches[i].lit));

    def = new aiger_def();
    def->type = aiger_def_latch;
    def->var_lhs = aiger_lit2var(model->latches[i].lit);
    def->op1  = model->latches[i].reset;
    def->op2  = model->latches[i].next;
    hash.insert(std::pair<unsigned, aiger_def*>(aiger_lit2var(model->latches[i].lit), def));
    def->dle = dll_insert_last(l_latches, aiger_lit2var(model->latches[i].lit), def);
    level[def->var_lhs] = 0;
  }

  assert(hash.size() == (model->num_inputs + model->num_latches));

  /* Add ands into hashmap and update refBy */
  for (i = 0; i < model->num_ands; i++) {
    assert(!aiger_sign(model->ands[i].lhs));

    def = new aiger_def();
    def->type = aiger_def_and;
    def->var_lhs = aiger_lit2var(model->ands[i].lhs);

    /* Update rhs0 refBy */
    if (model->ands[i].rhs0 > 1) {
      def_tmp = hash[aiger_lit2var(model->ands[i].rhs0)];
      def_tmp->refBy.insert(aiger_lit2var(model->ands[i].lhs));
      def->op1_ref = def_tmp;
    }
    
    /* Update rhs1 refBy */
    if (model->ands[i].rhs1 > 1) {
      def_tmp = hash[aiger_lit2var(model->ands[i].rhs1)];
      def_tmp->refBy.insert(aiger_lit2var(model->ands[i].lhs));
      def->op2_ref = def_tmp;
    }
    
    if (model->ands[i].rhs0 < model->ands[i].rhs1) {
      def->op1  = model->ands[i].rhs0;
      def->op2  = model->ands[i].rhs1;

    } else {
      def->op1  = model->ands[i].rhs1;
      def->op2  = model->ands[i].rhs0;

      def_tmp = def->op1_ref;
      def->op1_ref = def->op2_ref;
      def->op2_ref = def_tmp;
    }
    hash.insert(std::pair<unsigned, aiger_def*>(aiger_lit2var(model->ands[i].lhs), def));
    def->dle = dll_insert_last(l_ands, aiger_lit2var(model->ands[i].lhs), def);
  }
  
  assert(hash.size() == (model->num_inputs + model->num_latches + model->num_ands));
  /* Update latches refBy */
  /* Version 1 */
  // for (i = 0; i < model->num_latches; i++) {
  //   if (model->latches[i].next > 1){
  //     def_tmp = hash[aiger_lit2var(model->latches[i].next)];
  //     def_tmp->refBy.insert(aiger_lit2var(model->latches[i].lit));
  //     def = hash[aiger_lit2var(model->latches[i].lit)]; // OPT
  //     def->op2_ref = def_tmp; // OPT
  //   }
  // }
  
  /* Version 2 */
  dll_elem<aiger_def> * e_tmp;
  e_tmp = l_latches->head;
  while(e_tmp != NULL) {
    def = e_tmp->def;

    if (def->op2 > 1 ) {
      def_tmp = hash[aiger_lit2var(def->op2)];
      def_tmp->refBy.insert(e_tmp->id);
      def->op2_ref = def_tmp;
    }
    e_tmp = e_tmp->next;
  }

  /* Update bad state */
  if (model->num_bad == 1) {
    bad_state = model->bad[0].lit;
  } else if (model->num_outputs == 1) {
    bad_state = model->outputs[0].lit;
  } else {
    printf("TODO handle multiple properties\n"); exit(-1);
  }
  if (bad_state < 2) {
    if (!bad_state) {
      if (verbose) printf("\n[aigprep]  Bad state is equal to FALSE\n");
      printf("0\nb0\n.\n");
    } else {
      if (verbose) printf("\n[aigprep]  Bad state is equal to TRUE\n");
      printf("1\nb0\n.\n");
    }
    exit(0);
  } else {
    hash[aiger_lit2var(bad_state)]->refBy.insert(0);
  }
    
   
  if (verbose) printf(" done (%.2fs)\n", ((double) (clock() - start)) / CLOCKS_PER_SEC);

  /* Compute COI */
  compute_coi(cex);

  aiger_reset(model);
}

void compute_coi(bool cex) {
  if (verbose) printf("[aigprep]  Computing COI..\n");
  clock_t start = clock();
  reset_coi();
  coi(cex);
  reset_coi();
  if (verbose) printf("[aigprep]  COI  | Time: %.2fs\n", ((double) (clock() - start)) / CLOCKS_PER_SEC);
}

void coi(bool cex) {
  aiger_def *def_tmp, *def_refby;
  
  /* VERSION 1 */
  // std::stack<unsigned> refs;
  // refs.push(aiger_lit2var(bad_state));
  // unsigned tmp_id;
  // std::map<unsigned,unsigned>::iterator iter;

  // while(!refs.empty()) {
    
  //   tmp_id = refs.top();
  //   def_tmp = hash[tmp_id];
  //   refs.pop();

  //   if (!def_tmp->coi) {
  //     def_tmp->coi = 1;

  //     if (def_tmp->type == aiger_def_latch) {
  // 	/* latch */
  // 	if (def_tmp->op2 > 1) refs.push(aiger_lit2var(def_tmp->op2));
	
  //     } else if (def_tmp->type == aiger_def_and) {
  // 	/* and */
  // 	if (def_tmp->op1 > 1) refs.push(aiger_lit2var(def_tmp->op1));
  // 	if (def_tmp->op2 > 1) refs.push(aiger_lit2var(def_tmp->op2));
  //     }
  //   } 
  // }

  /* VERSION 2 */
  // std::stack<aiger_def*> refs;
  // aiger_def *tmp = hash[aiger_lit2var(bad_state)];
  // tmp->coi = 1;
  // refs.push(tmp);

  // while(!refs.empty()) {
  //   def_tmp = refs.top();
  //   refs.pop();

  //   if (!def_tmp->coi) {
  //     printf("ERROR COI\n");
  //     exit(-1);
  //   }

  //   if (def_tmp->type == aiger_def_latch) {
  //     /* latch */
  //     if (def_tmp->op2 > 1) {
  // 	tmp = hash[aiger_lit2var(def_tmp->op2)];
  // 	if (!tmp->coi) {
  // 	  tmp->coi = 1;
  // 	  refs.push(tmp);
  // 	}
  //     }
	
  //   } else if (def_tmp->type == aiger_def_and) {
  //     /* and */
  //     if (def_tmp->op1 > 1) {
  // 	tmp = hash[aiger_lit2var(def_tmp->op1)];
  // 	if (!tmp->coi) {
  // 	  tmp->coi = 1;
  // 	  refs.push(tmp);
  // 	}
  //     }
  //     if (def_tmp->op2 > 1) {
  // 	tmp = hash[aiger_lit2var(def_tmp->op2)];
  // 	if (!tmp->coi) {
  // 	  tmp->coi = 1;
  // 	  refs.push(tmp);
  // 	}
  //     }
  //   }
  // }
  
  /* VERSION 3 */
  std::stack<aiger_def*> refs;
  aiger_def *tmp = hash[aiger_lit2var(bad_state)];
  tmp->coi = 1;
  refs.push(tmp);

  while(!refs.empty()) {
    def_tmp = refs.top();
    refs.pop();

    if (def_tmp->type == aiger_def_latch) {
      /* latch */
      if (def_tmp->op2 > 1) {
  	if (!def_tmp->op2_ref->coi) {
  	  def_tmp->op2_ref->coi = 1;
	  if (def_tmp->op2_ref->type != aiger_def_input)
	    refs.push(def_tmp->op2_ref);
  	}
      }
	
    } else if (def_tmp->type == aiger_def_and) {
      /* and */
      if (def_tmp->op1 > 1) {
  	if (!def_tmp->op1_ref->coi) {
  	  def_tmp->op1_ref->coi = 1;
	  if (def_tmp->op1_ref->type != aiger_def_input)
	    refs.push(def_tmp->op1_ref);
  	}
      }
      if (def_tmp->op2 > 1) {
  	if (!def_tmp->op2_ref->coi) {
  	  def_tmp->op2_ref->coi = 1;
	  if (def_tmp->op2_ref->type != aiger_def_input)
	    refs.push(def_tmp->op2_ref);
  	}
      }
    }
  }


  dll_elem<aiger_def> *e_tmp;
  int nb_del_input = 0;
  int nb_del_latch = 0;
  int nb_del_and = 0;
  /* Remove all defs that aren't inside the COI */
  /* Remove latches */
  e_tmp = l_latches->head;
  while(e_tmp != NULL) {
    def_tmp = e_tmp->def;

    if (!def_tmp->coi) {
      def_tmp->mark_del = 1;
      dll_remove_elem(e_tmp);
      nb_del_latch++;
    }
    e_tmp = e_tmp->next;
      
  }
  
  /* Remove ands */
  e_tmp = l_ands->head;
  while(e_tmp != NULL) {
    def_tmp = e_tmp->def;

    if (!def_tmp->coi) {
      def_tmp->mark_del = 1;
      dll_remove_elem(e_tmp);
      nb_del_and++;
    } 
    e_tmp = e_tmp->next;
  }

  if (!cex) {
    /* Remove inputs */
    e_tmp = l_inputs->head;
    while(e_tmp != NULL) {
      def_tmp = e_tmp->def;

      if (!def_tmp->coi) {
	def_tmp->mark_del = 1;
	dll_remove_elem(e_tmp);
	nb_del_input++;
      }
      e_tmp = e_tmp->next;
    }
  }

  if (verbose) printf("[aigprep]  COI  | Removed latches : %d\n", nb_del_latch);
  if (verbose) printf("[aigprep]  COI  | Removed ands : %d\n", nb_del_and);
  if (cex) {
    if (verbose) printf("[aigprep]  COI  | Inputs are not removed to print CEX\n");
  } else {
    if (verbose) printf("[aigprep]  COI  | Removed inputs : %d\n", nb_del_input);
  }

}


void reset_coi() {
  reset_coi_latches();
  reset_coi_ands();
  reset_coi_inputs();
}

void reset_coi_latches() {
  dll_elem<aiger_def> * e_tmp;
  aiger_def *def_tmp;

  /* Reset latches */
  e_tmp = l_latches->head;
  while(e_tmp != NULL) {
    def_tmp = e_tmp->def;
    def_tmp->coi = 0;
    e_tmp = e_tmp->next;
  }
}

void reset_coi_ands(){
  dll_elem<aiger_def> * e_tmp;
  aiger_def *def_tmp;

  /* Reset ands */
  e_tmp = l_ands->head;
  while(e_tmp != NULL) {
    def_tmp = e_tmp->def;
    def_tmp->coi = 0;
    e_tmp = e_tmp->next;
  }
}

void reset_coi_inputs(){
  dll_elem<aiger_def> * e_tmp;
  aiger_def *def_tmp;
 
  /* Reset inputs */
  e_tmp = l_inputs->head;
  while(e_tmp != NULL) {
    def_tmp = e_tmp->def;
    def_tmp->coi = 0;
    e_tmp = e_tmp->next;
  }
}


void reset_update_flag() {
  dll_elem<aiger_def> * e_tmp;
  aiger_def *def_tmp;

  /* Reset latches */
  e_tmp = l_latches->head;
  while(e_tmp != NULL) {
    def_tmp = e_tmp->def;
    def_tmp->update = 1;
    e_tmp = e_tmp->next;
  }
  /* Reset ands */
  e_tmp = l_ands->head;
  while(e_tmp != NULL) {
    def_tmp = e_tmp->def;
    def_tmp->update = 1;
    e_tmp = e_tmp->next;
  }
  /* Reset inputs */
  e_tmp = l_inputs->head;
  while(e_tmp != NULL) {
    def_tmp = e_tmp->def;
    def_tmp->update = 1;
    e_tmp = e_tmp->next;
  }
}


dll_list<aiger_def>* order_list_latch() {
  // Not really efficient but this will do for now..
  // (aiger_reencode doesn't reorder latches)
  dll_list<aiger_def> *new_l_latches;
  new_l_latches = new dll_list<aiger_def>();
  dll_init(new_l_latches);
  
  dll_elem<aiger_def> *e_tmp = l_latches->head;
  aiger_def *def_tmp, *def_op2;
  while(e_tmp != NULL) {
    def_tmp = e_tmp->def;
    def_tmp->update = 0;

    if (def_tmp->op2 > 1) {
      def_op2 = hash[aiger_lit2var(def_tmp->op2)];

      if (def_op2->type != aiger_def_latch) {
	dll_insert_last(new_l_latches, def_tmp->var_lhs, def_tmp);
	def_tmp->update = 1;
      } else if (def_tmp->var_lhs == aiger_lit2var(def_tmp->op2)) {
	dll_insert_last(new_l_latches, def_tmp->var_lhs, def_tmp);
	def_tmp->update = 1;
      }
    } else {
      dll_insert_last(new_l_latches, def_tmp->var_lhs, def_tmp);
      def_tmp->update = 1;
    }


    e_tmp = e_tmp->next;
  }

  int old_size = -1;
  while (new_l_latches->sz != l_latches->sz) { 
    if (old_size == new_l_latches->sz) {
      // Due to cross reference between latches, sometimes we have to make a choice..
      e_tmp = l_latches->head;
      while(e_tmp != NULL) {
	def_tmp = e_tmp->def;
	if (!def_tmp->update) {
	  dll_insert_last(new_l_latches, def_tmp->var_lhs, def_tmp);
	  def_tmp->update = 1;
	} else {
	  e_tmp = e_tmp->next;
	}
      }
    }
    old_size = new_l_latches->sz;

    e_tmp = l_latches->head;
    while(e_tmp != NULL) {
      def_tmp = e_tmp->def;

      if (def_tmp->op2 > 1 && !def_tmp->update) {
	def_op2 = hash[aiger_lit2var(def_tmp->op2)];
	if (def_op2->type == aiger_def_latch) {
	  if (def_op2->update == 1) {
	    dll_insert_last(new_l_latches, def_tmp->var_lhs, def_tmp);
	    def_tmp->update = 1;
	  }
	} else {
	  assert(def_tmp->update = 1);
	}
      }

      e_tmp = e_tmp->next;
    }
  }

  return new_l_latches;
}

aiger* rebuild_aig(bool bad) {
  dll_elem<aiger_def> * e_tmp;
  aiger_def * def_tmp;
  aiger * n_model;
  n_model = aiger_init();

  if (verbose) printf("[aigprep]  Building optimized model..");
  fflush(stdout);
  clock_t start = clock();

  unsigned new_v = 2;
  
  reencode_[0] = 0;
  reencode_[1] = 1;

  /* Add inputs */
  e_tmp = l_inputs->head;
  while(e_tmp != NULL) {
    
    reencode_[aiger_var2lit(e_tmp->id)] = new_v;
    reencode_[aiger_var2lit(e_tmp->id) + 1] = new_v + 1;

    aiger_add_input(n_model, new_v, 0);
    
    new_v += 2;

    e_tmp = e_tmp->next;
  }
  
  dll_list<aiger_def>* ordered_l_latches = order_list_latch();

  /* Prepare latches */
  e_tmp = ordered_l_latches->head;
  while(e_tmp != NULL) {
    def_tmp = e_tmp->def;
    
    reencode_[aiger_var2lit(e_tmp->id)] = new_v;
    reencode_[aiger_var2lit(e_tmp->id) + 1] = new_v + 1;
    
    new_v += 2;    

    e_tmp = e_tmp->next;
  }

  /* Prepare ands */
  e_tmp = l_ands->head;
  while(e_tmp != NULL) {
    def_tmp = e_tmp->def;

    reencode_[aiger_var2lit(e_tmp->id)] = new_v;
    reencode_[aiger_var2lit(e_tmp->id) + 1] = new_v + 1;
    
    new_v += 2;

    e_tmp = e_tmp->next;
  }

  /* Add latches */
  e_tmp = ordered_l_latches->head;
  while(e_tmp != NULL) {
    def_tmp = e_tmp->def;

    aiger_add_latch(n_model, reencode_[aiger_var2lit(e_tmp->id)], reencode_[def_tmp->op2], 0);
      
    aiger_add_reset(n_model, reencode_[aiger_var2lit(e_tmp->id)], reencode_[def_tmp->op1]);
    
    e_tmp = e_tmp->next;
  }

  /* Add ands */
  e_tmp = l_ands->head;
  while(e_tmp != NULL) {
    def_tmp = e_tmp->def;

    aiger_add_and(n_model, reencode_[aiger_var2lit(e_tmp->id)],
		  reencode_[def_tmp->op1],
		  reencode_[def_tmp->op2]);
    
    e_tmp = e_tmp->next;
  }


  if (bad) {
    /* Add bad */
    aiger_add_bad(n_model, reencode_[bad_state], 0);
  } else {
    /* Add output */
    aiger_add_output(n_model, reencode_[bad_state], 0);
  }


  /* Reencode constraints */
  std::list<std::pair<unsigned, unsigned>>::iterator it_list_ctr;
  for (it_list_ctr = simple_ctr.begin(); it_list_ctr != simple_ctr.end(); ++it_list_ctr) {
    it_list_ctr->first  = reencode_[it_list_ctr->first];
    it_list_ctr->second = reencode_[it_list_ctr->second];
  }

  std::list<std::tuple<unsigned, unsigned, unsigned>>::iterator it_list_ctr_d;
  for (it_list_ctr_d = double_ctr.begin(); it_list_ctr_d != double_ctr.end(); ++it_list_ctr_d) {
    std::get<0>(*it_list_ctr_d) = reencode_[std::get<0>(*it_list_ctr_d)];
    std::get<1>(*it_list_ctr_d) = reencode_[std::get<1>(*it_list_ctr_d)];
    std::get<2>(*it_list_ctr_d) = reencode_[std::get<2>(*it_list_ctr_d)];
  }

  if (verbose) printf(" done (%.2fs)\n", ((double) (clock() - start)) / CLOCKS_PER_SEC);

  const char* s = aiger_check(n_model);
  if(verbose) s == NULL ? printf("[aigprep]  Preprocessed model OK\n") :
		          printf("[aigprep]  Preprocessed model error: %s\n", s);

  // Cannot perform reencoding because the constraints computed while preprocessing could become inconsistent
  // !! Not anymore !!
  //if (balance_aig_OPT) {
  aiger_reencode(n_model);
  //}
  return n_model;
}

void check_refBy_consistency();

void iter_defs(bool cex) {
  ternary_simulation();
  iter_defs_simplifications();
  check_refBy_consistency();
  if (ternary_simulation()) {
    reset_update_flag();
    check_refBy_consistency();
    iter_defs_simplifications();
  }
  check_refBy_consistency();
  compute_coi(cex);
}

void iter_defs_simplifications() {
  dll_elem<aiger_def> *e_tmp;
  aiger_def *def_tmp;
  unsigned stats_iter = 0;
  bool continue_iter = 1, modif_diff1;
  clock_t start;
  unsigned key_struct, key_func;
  if (verbose) printf("[aigprep]  Optimization..\n");

  while(continue_iter) {
    continue_iter = 0;
    stats_iter++;
    start = clock();

    if (verbose) {
      printf("[aigprep]  OPT  | <Iteration %d> [#ANDs %d | #Latches %d]\n",
	     stats_iter, l_ands->sz, l_latches->sz);
      printf("[aigprep]  OPT  | <Stats>  cst: %7d | struct: %7d |  func: %7d | unref: %7d\n",
	     stats_cst_prop, stats_struct_equiv, stats_func_equiv, stats_unref);
      printf("[aigprep]  OPT  | <Stats> triv: %7d |   simp: %7d |  subs: %7d | diff1: %7d\n",
	     stats_trivia, stats_simp, stats_func_subsume, stats_func_equiv_diff1);
    }

    hash_combi_struct_equiv.clear();
    hash_combi_func_equiv.clear();
    hash_latch_struct_equiv.clear();

    // Iterate over ANDs
    e_tmp = l_ands->head;
    while(e_tmp != NULL) {
      def_tmp = e_tmp->def;

      assert(!def_tmp->mark_del);
      if (def_tmp->refBy.empty()) {
	remove_def(def_tmp);

      } else if (def_tmp->update) {
	def_tmp->update = 0;
	iter_combi_propagate_cst(e_tmp->id, def_tmp);
	continue_iter |= iter_combi_check_cst(e_tmp->id, def_tmp);

	if (!def_tmp->mark_del) {
	  continue_iter |= iter_combi_trivia(e_tmp->id, def_tmp);
	
	  if (!def_tmp->mark_del) {
	    continue_iter |= iter_combi_simp(e_tmp->id, def_tmp);

	    if (!def_tmp->mark_del) {
	      order_ops(def_tmp);
	      
	      assert(def_tmp->op1 < def_tmp->op2);
	      key_struct = hash_def(def_tmp->op1, def_tmp->op2);
	      continue_iter |= iter_combi_struct_equiv(e_tmp->id, def_tmp, key_struct);

	      if (!def_tmp->mark_del) {
		iter_combi_build_func(e_tmp->id, def_tmp);
		continue_iter |= iter_combi_func_equiv_cst(e_tmp->id, def_tmp);

		if (!def_tmp->mark_del) {
		  continue_iter |= iter_combi_func_subsume(e_tmp->id, def_tmp);
		  
		  if (!def_tmp->mark_del) {
		    key_func = hash_set(def_tmp->func);
		    continue_iter |= iter_combi_func_equiv(e_tmp->id, def_tmp, key_func);
		
		    if (!def_tmp->mark_del) {
		  
		      if (def_tmp->func.size() > 2) {
		      
			if (def_tmp->func.size() == 3 ? 
			    iter_combi_func_equiv_diff1_s3(e_tmp->id, def_tmp) :
			    iter_combi_func_equiv_diff1   (e_tmp->id, def_tmp) ) {
			
			  continue_iter = 1;

			  assert(def_tmp->op1 < def_tmp->op2);

			  key_struct = hash_def(def_tmp->op1, def_tmp->op2);
			  iter_combi_struct_equiv(e_tmp->id, def_tmp, key_struct);
			}
		      
		      }
		      if (!def_tmp->mark_del) {
			order_ops(def_tmp);
			assert(def_tmp->op1 < def_tmp->op2);
			hash_combi_struct_equiv.insert(std::pair<unsigned, aiger_def*>(key_struct, def_tmp));
		      }
		    }
		  }
		}
	      }
	    }
	  }
	}
      } else {
	key_struct = hash_def(def_tmp->op1, def_tmp->op2);
	if (iter_combi_struct_equiv(e_tmp->id, def_tmp, key_struct))
	  continue_iter = 1;
	else {
	  hash_combi_struct_equiv.insert(std::pair<unsigned, aiger_def*>(key_struct, def_tmp));

	  assert(!def_tmp->mark_del);
	  key_func = hash_set(def_tmp->func);
	  continue_iter |= iter_combi_func_equiv(e_tmp->id, def_tmp, key_func);
	}
      }

      e_tmp = e_tmp->next;
    }

    // Iterate over Latches
    e_tmp = l_latches->head;
    while(e_tmp != NULL) {
      def_tmp = e_tmp->def;

      
      assert(!def_tmp->mark_del);
      if (def_tmp->refBy.empty()) {
	remove_def(def_tmp);

      }	else if (def_tmp->update) {
	def_tmp->update = 0;
	iter_latch_propagate_cst(e_tmp->id, def_tmp);
	
	continue_iter |= iter_latch_check_cst(e_tmp->id, def_tmp);
	if (!def_tmp->mark_del)
	  continue_iter |= iter_latch_struct_equiv(e_tmp->id, def_tmp);
	
      } else {
	key_struct = hash_uint(def_tmp->op2);
	hash_latch_struct_equiv.insert(std::pair<unsigned, aiger_def*>(key_struct, def_tmp));
      }
    
      e_tmp = e_tmp->next;
    }

    update_bad_state();

    if (verbose) printf("[aigprep]  OPT  | Time: %.2fs\n", ((double) (clock() - start)) / CLOCKS_PER_SEC);

    if (!continue_iter && balance_aig_OPT/* && max_aig_lvl() > 300*/) {

      bool continue_balance = 1;
      while(continue_balance) {

	if (verbose) printf("[aigprep]  BAL  | lvl : %8d | <Stats> rw : %8d |\r",
			    max_aig_lvl(), stats_balance);

	continue_balance = 0;
	printf("\n%d\n", l_ands->sz);
	e_tmp = l_ands->head;
	while(e_tmp != NULL) {
	  def_tmp = e_tmp->def;

	  assert(!def_tmp->mark_del);
	  if (def_tmp->refBy.empty())
	    print_def(def_tmp);
	  assert(!def_tmp->refBy.empty());
	  update_level(def_tmp);
	  //iter_combi_build_func(e_tmp->id, def_tmp);
	  continue_balance |= iter_balance_def(def_tmp);
	  //printf("\t"); print_def(hash[105]);
	  continue_iter |= continue_balance;
	    
	  e_tmp = e_tmp->next;
	}
      }
      if (verbose) printf("\n");
      e_tmp = l_ands->head;
      while(e_tmp != NULL) {
	def_tmp = e_tmp->def;
	
	assert(!def_tmp->mark_del);
	assert(!def_tmp->refBy.empty());
	//update_level(def_tmp);
	//iter_combi_build_func(e_tmp->id, def_tmp);

	e_tmp = e_tmp->next;
      }
    }

  }
}
/********************************************************************************
Balance AIG def
********************************************************************************/
unsigned max_aig_lvl() {
  dll_elem<aiger_def> *e_tmp;
  aiger_def *def_tmp;
  unsigned max_lvl = 0;

  // Iterate over ANDs
  e_tmp = l_ands->head;
  while(e_tmp != NULL) {
    def_tmp = e_tmp->def;

    assert(!def_tmp->mark_del);
    assert(e_tmp->id == def_tmp->var_lhs);
    
    update_level(def_tmp);
    max_lvl = max_lvl < level[def_tmp->var_lhs] ? level[def_tmp->var_lhs] : max_lvl;

    e_tmp = e_tmp->next;
  }
  return max_lvl;
}

bool iter_balance_def(aiger_def *def) {
  if (!aiger_sign(def->op1) &&
      def->op1_ref->refBy.size() == 1 &&
      def->op1_ref->type == aiger_def_and) {

    if (level[aiger_lit2var(def->op2)] < level[aiger_lit2var(def->op1_ref->op1)] &&
	level[aiger_lit2var(def->op1_ref->op2)] < level[aiger_lit2var(def->op1_ref->op1)]) {
      print_def(def->op1_ref);
      print_def(def->op2_ref);
      print_def(def);
      // Update RefBy
      def->op1_ref->op1_ref->refBy.erase(def->op1_ref->var_lhs);
      def->op1_ref->op1_ref->refBy.insert(def->var_lhs);
      def->op2_ref->refBy.erase(def->var_lhs);
      def->op2_ref->refBy.insert(def->op1_ref->var_lhs);

      //
      unsigned old_op1_ref_op1 = def->op1_ref->op1;
      aiger_def *old_op1_ref_op1_ref = def->op1_ref->op1_ref;
      def->op1_ref->op1 = def->op2;
      def->op1_ref->op1_ref = def->op2_ref;
	    
      def->op2 = old_op1_ref_op1;
      def->op2_ref = old_op1_ref_op1_ref;
	    
      // Update func
      //iter_combi_build_func(aiger_lit2var(def->op1), def->op1_ref);

      //
      order_ops(def->op1_ref);
      order_ops(def);
	    
      // Set update flag
      def->op1_ref->update = 1;
      set_update_flag(def->op1_ref->refBy);
      def->op2_ref->update = 1;
      set_update_flag(def->op2_ref->refBy);
      def->update = 1;
      set_update_flag(def->refBy);

      update_level(def->op1_ref);
      update_level(def->op2_ref);
      update_level(def);
      print_def(def->op1_ref);
      print_def(def->op2_ref);
      print_def(def);
      printf("\n");
      stats_balance++;
      return 1;


    } else if (level[aiger_lit2var(def->op2)] < level[aiger_lit2var(def->op1_ref->op2)] &&
	       level[aiger_lit2var(def->op1_ref->op1)] < level[aiger_lit2var(def->op1_ref->op2)]) {
      print_def(def->op1_ref);
      print_def(def->op2_ref);
      print_def(def);
      // Update RefBy
      def->op1_ref->op2_ref->refBy.erase(def->op1_ref->var_lhs);
      def->op1_ref->op2_ref->refBy.insert(def->var_lhs);
      def->op2_ref->refBy.erase(def->var_lhs);
      def->op2_ref->refBy.insert(def->op1_ref->var_lhs);
      
      //
      unsigned old_op1_ref_op2 = def->op1_ref->op2;
      aiger_def *old_op1_ref_op2_ref = def->op1_ref->op2_ref;
      def->op1_ref->op2 = def->op2;
      def->op1_ref->op2_ref = def->op2_ref;
	  
      def->op2 = old_op1_ref_op2;
      def->op2_ref = old_op1_ref_op2_ref;

      // Update func
      //iter_combi_build_func(aiger_lit2var(def->op1), def->op1_ref);

      //
      order_ops(def->op1_ref);
      order_ops(def);

      // Set update flag
      def->op1_ref->update = 1;
      set_update_flag(def->op1_ref->refBy);
      def->op2_ref->update = 1;
      set_update_flag(def->op2_ref->refBy);
      def->update = 1;
      set_update_flag(def->refBy);

      update_level(def->op1_ref);
      update_level(def->op2_ref);
      update_level(def);
      print_def(def->op1_ref);
      print_def(def->op2_ref);
      print_def(def);
      printf("\n");
      stats_balance++;
      return 1;
 
    }

  } else if (!aiger_sign(def->op2) &&
	     def->op2_ref->refBy.size() == 1 &&
	     def->op2_ref->type == aiger_def_and) {
    
    if (level[aiger_lit2var(def->op1)] < level[aiger_lit2var(def->op2_ref->op1)] &&
	level[aiger_lit2var(def->op2_ref->op2)] < level[aiger_lit2var(def->op2_ref->op1)]) {
      print_def(def->op1_ref);
      print_def(def->op2_ref);
      print_def(def);
      // Update RefBy
      def->op2_ref->op1_ref->refBy.erase(def->op2_ref->var_lhs);
      def->op2_ref->op1_ref->refBy.insert(def->var_lhs);
      def->op1_ref->refBy.erase(def->var_lhs);
      def->op1_ref->refBy.insert(def->op2_ref->var_lhs);

      //
      unsigned old_op2_ref_op1 = def->op2_ref->op1;
      aiger_def *old_op2_ref_op1_ref = def->op2_ref->op1_ref;
      def->op2_ref->op1 = def->op1;
      def->op2_ref->op1_ref = def->op1_ref;
	    
      def->op1 = old_op2_ref_op1;
      def->op1_ref = old_op2_ref_op1_ref;

      // Update func
      //iter_combi_build_func(aiger_lit2var(def->op2), def->op2_ref);

      //
      order_ops(def->op2_ref);
      order_ops(def);

      // Set update flag
      def->op1_ref->update = 1;
      set_update_flag(def->op1_ref->refBy);
      def->op2_ref->update = 1;
      set_update_flag(def->op2_ref->refBy);
      def->update = 1;
      set_update_flag(def->refBy);

      update_level(def->op1_ref);
      update_level(def->op2_ref);
      update_level(def);
      print_def(def->op1_ref);
      print_def(def->op2_ref);
      print_def(def);
      printf("\n");
      stats_balance++;
      return 1;

    } else if (level[aiger_lit2var(def->op1)] < level[aiger_lit2var(def->op2_ref->op2)] &&
	       level[aiger_lit2var(def->op2_ref->op1)] < level[aiger_lit2var(def->op2_ref->op2)]) {
      print_def(def->op1_ref);
      print_def(def->op2_ref);
      print_def(def);
      // Update RefBy
      def->op2_ref->op2_ref->refBy.erase(def->op2_ref->var_lhs);
      def->op2_ref->op2_ref->refBy.insert(def->var_lhs);
      def->op1_ref->refBy.erase(def->var_lhs);
      def->op1_ref->refBy.insert(def->op2_ref->var_lhs);

      //
      unsigned old_op2_ref_op2 = def->op2_ref->op2;
      aiger_def *old_op2_ref_op2_ref = def->op2_ref->op2_ref;
      def->op2_ref->op2 = def->op1;
      def->op2_ref->op2_ref = def->op1_ref;
      
      def->op1 = old_op2_ref_op2;
      def->op1_ref = old_op2_ref_op2_ref;
      
      // Update func
      //iter_combi_build_func(aiger_lit2var(def->op2), def->op2_ref);

      //
      order_ops(def->op2_ref);
      order_ops(def);

      // Set update flag
      def->op1_ref->update = 1;
      set_update_flag(def->op1_ref->refBy);
      def->op2_ref->update = 1;
      set_update_flag(def->op2_ref->refBy);
      def->update = 1;
      set_update_flag(def->refBy);

      update_level(def->op1_ref);
      update_level(def->op2_ref);
      update_level(def);
      print_def(def->op1_ref);
      print_def(def->op2_ref);
      print_def(def);
      printf("\n");
      stats_balance++;
      return 1;

    }
  }
  return 0;
}

/********************************************************************************
AND simplification helper functions
********************************************************************************/
void iter_combi_propagate_cst(unsigned id, aiger_def *def) {
  assert(def->type == aiger_def_and);

  std::map<unsigned,unsigned>::iterator it;
  aiger_def *def_refby;

  unsigned old_op1 = def->op1;
  unsigned old_op2 = def->op2;
  aiger_def *old_op1_ref = def->op1_ref;
  aiger_def *old_op2_ref = def->op2_ref;
  
  /* Check if op1 = cst */
  while (def->op1 > 1 && def->op1_ref->type == aiger_def_cst) {
    if (aiger_sign(def->op1))
      def->op1 = aiger_not(def->op1_ref->op2);
    else
      def->op1 = def->op1_ref->op2;

    def->op1_ref = def->op1_ref->op2_ref;
  }
  

  /* Check if op2 = cst */
  while (def->op2 > 1 && def->op2_ref->type == aiger_def_cst) {
    if (aiger_sign(def->op2))
      def->op2 = aiger_not(def->op2_ref->op2);
    else
      def->op2 = def->op2_ref->op2;

    def->op2_ref = def->op2_ref->op2_ref;
  }


  // Update refBy
  if (def->op1 != old_op1) {
    assert(old_op1_ref != def->op1_ref);
    assert(old_op1 > 1);
    assert(old_op1_ref->refBy.size() != 0);

    set_update_flag(def->refBy);

    old_op1_ref->refBy.erase(id);
    if (def->op1 > 1)
      def->op1_ref->refBy.insert(id);

  }
  if (def->op2 != old_op2) {
    assert(old_op2_ref != def->op2_ref);
    assert(old_op2 > 1);
    assert(old_op2_ref->refBy.size() != 0 || aiger_lit2var(old_op1) == aiger_lit2var(old_op2));

    if (def->op1 == old_op1)
      set_update_flag(def->refBy);

    old_op2_ref->refBy.erase(id);
    if (def->op2 > 1)
      def->op2_ref->refBy.insert(id);

  }

  order_ops(def);
}

bool iter_combi_check_cst(unsigned id, aiger_def *def) {
  assert(def->type == aiger_def_and);

  bool continue_propa = 0;
  aiger_def *def_refby;
  
  /* Check if def = cst */
  if (aiger_lit2var(def->op1) == aiger_lit2var(def->op2)) {

    set_update_flag(def->refBy);

    if (def->op1 == def->op2) {
      // lhs = a & a   =>  lhs = a
      def->type = aiger_def_cst;
      // def->op2     is already equal to "a"
      // def->op2_ref is already equal to "a" def

    } else {
      // lhs = a & ~a   =>  lhs = 0
      def->type = aiger_def_cst;
      def->op2 = 0;
      def->op2_ref = NULL;

      // Update refBy
      if (def->op1 > 1) {
  	assert(def->op1_ref->refBy.size() != 0);
  	def->op1_ref->refBy.erase(id);
  	// if (def->op1_ref->refBy.size() == 0) {
  	//   remove_def(aiger_lit2var(def->op1), def->op1_ref);
  	// }
      }	
    }

    def->mark_del = 1;
    dll_remove_elem(def->dle);
    continue_propa = 1;
    stats_cst_prop++;
	
  } else if (def->op1 <= 1) { 
    set_update_flag(def->refBy);

    if (def->op1 == 1) {
      // lhs = 1 & a   =>  lhs = a
      def->type = aiger_def_cst;
      // def->op2     is already equal to "a"
      // def->op2_ref is already equal to "a" def

    } else {
      // lhs = 0 & a   =>  lhs = 0
      // Update refBy
      assert(def->op2 > 1); // If op2 <= 1 then previous condition
      assert(def->op2_ref->refBy.size() != 0);
      def->op2_ref->refBy.erase(id);
      // if (def->op2_ref->refBy.size() == 0) {
      // 	remove_def(aiger_lit2var(def->op2), def->op2_ref);
      // }
      def->type = aiger_def_cst;
      def->op2 = 0;
      def->op2_ref = NULL;
    }

    def->mark_del = 1;
    dll_remove_elem(def->dle);
    continue_propa = 1;
    stats_cst_prop++;

  } else if (def->op2 <= 1) {
    set_update_flag(def->refBy);

    if (def->op2 == 1) {
      // lhs = a & 1   =>  lhs = a
      def->type = aiger_def_cst;
      def->op2 = def->op1;
      def->op2_ref = def->op1_ref;

    } else {
      // lhs = a & 0   =>  lhs = 0
      def->type = aiger_def_cst;
      // def->op2     is already equal to 0
      // def->op2_ref is already equal to NULL

      // Update refBy
      assert(def->op1 > 1); // If op1 <= 1 then previous condition
      assert(def->op1_ref->refBy.size() != 0);
      def->op1_ref->refBy.erase(id);
      // if (def->op1_ref->refBy.size() == 0) {
      // 	remove_def(aiger_lit2var(def->op1), def->op1_ref);
      // }
      
    }

    def->mark_del = 1;
    dll_remove_elem(def->dle);
    continue_propa = 1;
    stats_cst_prop++;
  }

  return continue_propa;
}


bool iter_combi_trivia(unsigned id, aiger_def *def) {
  assert(def->type == aiger_def_and);
  

  if ( def->op1_ref->type == aiger_def_and && !aiger_sign(def->op1) &&
       (def->op1_ref->op1 == aiger_not(def->op2) || def->op1_ref->op2 == aiger_not(def->op2))) {
    // printf("Contra 1.1 :\n");
    set_update_flag(def->refBy);

    // Update refBy
    assert(def->op1 > 1);
    assert(def->op1_ref->refBy.size() != 0);
    def->op1_ref->refBy.erase(id);
    // if (def->op1_ref->refBy.size() == 0) {
    //   remove_def(aiger_lit2var(def->op1), def->op1_ref);
    // }
    
    assert(def->op2 > 1);
    assert(def->op2_ref->refBy.size() != 0);
    def->op2_ref->refBy.erase(id);
    // if (def->op2_ref->refBy.size() == 0) {
    //   remove_def(aiger_lit2var(def->op2), def->op2_ref);
    // }
   
    def->type = aiger_def_cst;
    def->op2 = 0;
    def->op2_ref = NULL;
    assert(!def->mark_del);
    def->mark_del = 1;
    dll_remove_elem(def->dle);
    
    stats_trivia++;
    return 1;

  } else if ( def->op2_ref->type == aiger_def_and && !aiger_sign(def->op2) &&
	      (def->op2_ref->op1 == aiger_not(def->op1) || def->op2_ref->op2 == aiger_not(def->op1))) {
    // printf("Contra 1.2 :\n");
    set_update_flag(def->refBy);

    // Update refBy
    assert(def->op1 > 1);
    assert(def->op1_ref->refBy.size() != 0);
    def->op1_ref->refBy.erase(id);
    // if (def->op1_ref->refBy.size() == 0) {
    //   remove_def(aiger_lit2var(def->op1), def->op1_ref);
    // }
    
    assert(def->op2 > 1);
    assert(def->op2_ref->refBy.size() != 0);
    def->op2_ref->refBy.erase(id);
    // if (def->op2_ref->refBy.size() == 0) {
    //   remove_def(aiger_lit2var(def->op2), def->op2_ref);
    // }		
    
    def->type = aiger_def_cst;
    def->op2 = 0;
    def->op2_ref = NULL;
    assert(!def->mark_del);
    def->mark_del = 1;
    dll_remove_elem(def->dle);
    
    stats_trivia++;
    return 1;

  } else if ( def->op1_ref->type == aiger_def_and && !aiger_sign(def->op1) &&
	      def->op2_ref->type == aiger_def_and && !aiger_sign(def->op2) &&
	      (def->op1_ref->op1 == aiger_not(def->op2_ref->op1) ||
	       def->op1_ref->op1 == aiger_not(def->op2_ref->op2) ||
	       def->op1_ref->op2 == aiger_not(def->op2_ref->op1) ||
	       def->op1_ref->op2 == aiger_not(def->op2_ref->op2) ) ) {
    // printf("Contra 2 :\n");
    set_update_flag(def->refBy);

    // Update refBy
    assert(def->op1 > 1);
    assert(def->op1_ref->refBy.size() != 0);
    def->op1_ref->refBy.erase(id);
    // if (def->op1_ref->refBy.size() == 0) {
    //   remove_def(aiger_lit2var(def->op1), def->op1_ref);
    // }
    
    assert(def->op2 > 1);
    assert(def->op2_ref->refBy.size() != 0);
    def->op2_ref->refBy.erase(id);
    // if (def->op2_ref->refBy.size() == 0) {
    //   remove_def(aiger_lit2var(def->op2), def->op2_ref);
    // }	
    
    def->type = aiger_def_cst;
    def->op2 = 0;
    def->op2_ref = NULL;
    assert(!def->mark_del);
    def->mark_del = 1;
    dll_remove_elem(def->dle);
    
    stats_trivia++;
    return 1;

  } else if (def->op1_ref->type == aiger_def_and && aiger_sign(def->op1) &&
	     (def->op1_ref->op1 == aiger_not(def->op2) ||
	      def->op1_ref->op2 == aiger_not(def->op2) )) {
    // printf("Subsum 1.1 :\n");
    set_update_flag(def->refBy);

    // Update refBy
    assert(def->op1 > 1);
    assert(def->op1_ref->refBy.size() != 0);
    def->op1_ref->refBy.erase(id);
    // if (def->op1_ref->refBy.size() == 0) {
    //   remove_def(aiger_lit2var(def->op1), def->op1_ref);
    // }
    
    def->type = aiger_def_cst;
    // def->op2     OK
    // def->op2_ref OK
    assert(!def->mark_del);
    def->mark_del = 1;
    dll_remove_elem(def->dle);
    
    stats_trivia++;
    return 1;


  } else if (def->op2_ref->type == aiger_def_and && aiger_sign(def->op2) &&
	     (def->op2_ref->op1 == aiger_not(def->op1) ||
	      def->op2_ref->op2 == aiger_not(def->op1) )) {
    // printf("Subsum 1.2 :\n");
    set_update_flag(def->refBy);

    // Update refBy
    assert(def->op2 > 1);
    assert(def->op2_ref->refBy.size() != 0);
    def->op2_ref->refBy.erase(id);
    // if (def->op2_ref->refBy.size() == 0) {
    //   remove_def(aiger_lit2var(def->op2), def->op2_ref);
    // }
    
    def->type = aiger_def_cst;
    def->op2 = def->op1;
    def->op2_ref = def->op1_ref;
    assert(!def->mark_del);
    def->mark_del = 1;
    dll_remove_elem(def->dle);
    
    stats_trivia++;
    return 1;

  } else if (def->op1_ref->type == aiger_def_and && aiger_sign(def->op1) &&
	     def->op2_ref->type == aiger_def_and && !aiger_sign(def->op2) &&
	     (def->op1_ref->op1 == aiger_not(def->op2_ref->op1) ||
	      def->op1_ref->op1 == aiger_not(def->op2_ref->op2) ||
	      def->op1_ref->op2 == aiger_not(def->op2_ref->op1) ||
	      def->op1_ref->op2 == aiger_not(def->op2_ref->op2)
	      )) {
    // printf("Subsum 2.1 :\n");
    set_update_flag(def->refBy);

    // Update refBy
    assert(def->op1 > 1);
    assert(def->op1_ref->refBy.size() != 0);
    def->op1_ref->refBy.erase(id);
    // if (def->op1_ref->refBy.size() == 0) {
    //   remove_def(aiger_lit2var(def->op1), def->op1_ref);
    // }
    
    def->type = aiger_def_cst;
    // def->op2     OK
    // def->op2_ref OK
    assert(!def->mark_del);
    def->mark_del = 1;
    dll_remove_elem(def->dle);
    
    stats_trivia++;
    return 1;

  } else if (def->op1_ref->type == aiger_def_and && !aiger_sign(def->op1) &&
	     def->op2_ref->type == aiger_def_and && aiger_sign(def->op2) &&
	     (def->op1_ref->op1 == aiger_not(def->op2_ref->op1) ||
	      def->op1_ref->op1 == aiger_not(def->op2_ref->op2) ||
	      def->op1_ref->op2 == aiger_not(def->op2_ref->op1) ||
	      def->op1_ref->op2 == aiger_not(def->op2_ref->op2)
	      )) {
    // printf("Subsum 2.2 :\n");
    set_update_flag(def->refBy);

    // Update refBy
    assert(def->op2 > 1);
    assert(def->op2_ref->refBy.size() != 0);
    def->op2_ref->refBy.erase(id);
    // if (def->op2_ref->refBy.size() == 0) {
    //   remove_def(aiger_lit2var(def->op2), def->op2_ref);
    // }
    
    def->type = aiger_def_cst;
    def->op2 = def->op1;
    def->op2_ref = def->op1_ref;
    assert(!def->mark_del);
    def->mark_del = 1;
    dll_remove_elem(def->dle);
    
    stats_trivia++;
    return 1;

  } else if ( def->op2_ref->type == aiger_def_and && !aiger_sign(def->op2) &&
       (def->op1 == def->op2_ref->op1 || def->op1 == def->op2_ref->op2)) {
    // printf("Idempot 1.1 :\n");
    set_update_flag(def->refBy);

    // Update refBy
    assert(def->op1 > 1);
    assert(def->op1_ref->refBy.size() != 0);
    def->op1_ref->refBy.erase(id);
    assert(def->op1_ref->refBy.size() != 0);

    def->type = aiger_def_cst;
    //def->op2     OK
    //def->op2_ref OK
    assert(!def->mark_del);
    def->mark_del = 1;
    dll_remove_elem(def->dle);
    
    stats_trivia++;
    return 1;

  } else if ( def->op1_ref->type == aiger_def_and && !aiger_sign(def->op1) &&
       (def->op2 == def->op1_ref->op1 || def->op2 == def->op1_ref->op2)) {
    // printf("Idempot 1.2 :\n");
    set_update_flag(def->refBy);

    // Update refBy
    assert(def->op2 > 1);
    assert(def->op2_ref->refBy.size() != 0);
    def->op2_ref->refBy.erase(id);
    assert(def->op2_ref->refBy.size() != 0);

    def->type = aiger_def_cst;
    def->op2 = def->op1;
    def->op2_ref = def->op1_ref;
    assert(!def->mark_del);
    def->mark_del = 1;
    dll_remove_elem(def->dle);
    
    stats_trivia++;
    return 1;

  } else if (def->op1_ref->type == aiger_def_and && aiger_sign(def->op1) &&
	     def->op2_ref->type == aiger_def_and && aiger_sign(def->op2) &&
	     ((def->op1_ref->op1 == def->op2_ref->op1 && 
	       def->op1_ref->op2 == aiger_not(def->op2_ref->op2)) ||
	      (def->op1_ref->op1 == def->op2_ref->op2 && 
	       def->op1_ref->op2 == aiger_not(def->op2_ref->op1))
	      )) {
    // printf("Resolution 1.1 :\n");
    set_update_flag(def->refBy);

    // Update refBy
    assert(def->op1 > 1);
    assert(def->op1_ref->refBy.size() != 0);
    def->op1_ref->refBy.erase(id);
    
    assert(def->op2 > 1);
    assert(def->op2_ref->refBy.size() != 0);
    def->op2_ref->refBy.erase(id);

    def->op1_ref->op1_ref->refBy.insert(id);

    // if (def->op1_ref->refBy.size() == 0) {
    //   remove_def(aiger_lit2var(def->op1), def->op1_ref);
    // }
    // if (def->op2_ref->refBy.size() == 0) {
    //   remove_def(aiger_lit2var(def->op2), def->op2_ref);
    // }	
    

    def->type = aiger_def_cst;
    def->op2 = aiger_not(def->op1_ref->op1);
    def->op2_ref = def->op1_ref->op1_ref;
    assert(!def->mark_del);
    def->mark_del = 1;
    dll_remove_elem(def->dle);
    
    stats_trivia++;
    return 1;

  } else if (def->op1_ref->type == aiger_def_and && aiger_sign(def->op1) &&
	     def->op2_ref->type == aiger_def_and && aiger_sign(def->op2) &&
	     ((def->op1_ref->op2 == def->op2_ref->op1 && 
	       def->op1_ref->op1 == aiger_not(def->op2_ref->op2)) ||
	      (def->op1_ref->op2 == def->op2_ref->op2 && 
	       def->op1_ref->op1 == aiger_not(def->op2_ref->op1))
	      )) {
    // printf("Resolution 1.2 :\n");
    set_update_flag(def->refBy);
    def->update = 1;

    // Update refBy
    assert(def->op1 > 1);
    assert(def->op1_ref->refBy.size() != 0);
    def->op1_ref->refBy.erase(id);
    
    assert(def->op2 > 1);
    assert(def->op2_ref->refBy.size() != 0);
    def->op2_ref->refBy.erase(id);

    def->op1_ref->op2_ref->refBy.insert(id);

    // if (def->op1_ref->refBy.size() == 0) {
    //   remove_def(aiger_lit2var(def->op1), def->op1_ref);
    // }
    // if (def->op2_ref->refBy.size() == 0) {
    //   remove_def(aiger_lit2var(def->op2), def->op2_ref);
    // }


    def->type = aiger_def_cst;
    def->op2 = aiger_not(def->op1_ref->op2);
    def->op2_ref = def->op1_ref->op2_ref;
    assert(!def->mark_del);
    def->mark_del = 1;
    dll_remove_elem(def->dle);
    
    stats_trivia++;
    return 1;

  } else if (def->op1_ref->type == aiger_def_and && aiger_sign(def->op1) && 
	     def->op1_ref->op1 == def->op2) {
    // printf("Substitution 1.1 :\n");
    set_update_flag(def->refBy);
    def->update = 1;
    
    // Update refBy
    assert(def->op1 > 1);
    assert(def->op1_ref->refBy.size() != 0);
    def->op1_ref->refBy.erase(id);
    
    def->op1_ref->op2_ref->refBy.insert(id);
    
    // if (def->op1_ref->refBy.size() == 0) {
    //   remove_def(aiger_lit2var(def->op1), def->op1_ref);
    // }

    def->op1 = aiger_not(def->op1_ref->op2);
    def->op1_ref = def->op1_ref->op2_ref;

    stats_trivia++;
    return 1;

  } else if (def->op1_ref->type == aiger_def_and && aiger_sign(def->op1) && 
	     def->op1_ref->op2 == def->op2) {
    // printf("Substitution 1.2 :\n");
    set_update_flag(def->refBy);
    def->update = 1;

    // Update refBy
    assert(def->op1 > 1);
    assert(def->op1_ref->refBy.size() != 0);
    def->op1_ref->refBy.erase(id);

    def->op1_ref->op1_ref->refBy.insert(id);
    
    // if (def->op1_ref->refBy.size() == 0) {
    //   remove_def(aiger_lit2var(def->op1), def->op1_ref);
    // }

    def->op1 = aiger_not(def->op1_ref->op1);
    def->op1_ref = def->op1_ref->op1_ref;

    stats_trivia++;
    return 1;

  } else if (def->op2_ref->type == aiger_def_and && aiger_sign(def->op2) && 
	     def->op2_ref->op1 == def->op1) {
    // printf("Substitution 1.3 :\n");
    set_update_flag(def->refBy);
    def->update = 1;

    // Update refBy
    assert(def->op2 > 1);
    assert(def->op2_ref->refBy.size() != 0);
    def->op2_ref->refBy.erase(id);
    
    def->op2_ref->op2_ref->refBy.insert(id);

    // if (def->op2_ref->refBy.size() == 0) {
    //   remove_def(aiger_lit2var(def->op2), def->op2_ref);
    // }

    def->op2 = aiger_not(def->op2_ref->op2);
    def->op2_ref = def->op2_ref->op2_ref;

    stats_trivia++;
    return 1;

  } else if (def->op2_ref->type == aiger_def_and && aiger_sign(def->op2) && 
	     def->op2_ref->op2 == def->op1) {
    // printf("Substitution 1.4 :\n");
    set_update_flag(def->refBy);
    def->update = 1;

    // Update refBy
    assert(def->op2 > 1);
    assert(def->op2_ref->refBy.size() != 0);
    def->op2_ref->refBy.erase(id);
    
    def->op2_ref->op1_ref->refBy.insert(id);

    // if (def->op2_ref->refBy.size() == 0) {
    //   remove_def(aiger_lit2var(def->op2), def->op2_ref);
    // }

    def->op2 = aiger_not(def->op2_ref->op1);
    def->op2_ref = def->op2_ref->op1_ref;

    stats_trivia++;
    return 1;

  } else if (def->op1_ref->type == aiger_def_and && aiger_sign(def->op1) &&
	     def->op2_ref->type == aiger_def_and && !aiger_sign(def->op2) &&
	     (def->op1_ref->op1 == def->op2_ref->op1 ||
	      def->op1_ref->op1 == def->op2_ref->op2 )) {
    // printf("Substitution 2.1 :\n");
    set_update_flag(def->refBy);
    def->update = 1;

    // Update refBy
    assert(def->op1 > 1);
    assert(def->op1_ref->refBy.size() != 0);
    def->op1_ref->refBy.erase(id);
    
    def->op1_ref->op2_ref->refBy.insert(id);
    
    // if (def->op1_ref->refBy.size() == 0) {
    //   remove_def(aiger_lit2var(def->op1), def->op1_ref);
    // }

    def->op1 = aiger_not(def->op1_ref->op2);
    def->op1_ref = def->op1_ref->op2_ref;

    stats_trivia++;
    return 1;

  } else if (def->op1_ref->type == aiger_def_and && aiger_sign(def->op1) &&
	     def->op2_ref->type == aiger_def_and && !aiger_sign(def->op2) &&
	     (def->op1_ref->op2 == def->op2_ref->op1 ||
	      def->op1_ref->op2 == def->op2_ref->op2 )) {
    // printf("Substitution 2.2 :\n");
    set_update_flag(def->refBy);
    def->update = 1;

    // Update refBy
    assert(def->op1 > 1);
    assert(def->op1_ref->refBy.size() != 0);
    def->op1_ref->refBy.erase(id);

    def->op1_ref->op1_ref->refBy.insert(id);

    // if (def->op1_ref->refBy.size() == 0) {
    //   remove_def(aiger_lit2var(def->op1), def->op1_ref);
    // }

    def->op1 = aiger_not(def->op1_ref->op1);
    def->op1_ref = def->op1_ref->op1_ref;

    stats_trivia++;
    return 1;

  } else if (def->op1_ref->type == aiger_def_and && !aiger_sign(def->op1) &&
	     def->op2_ref->type == aiger_def_and && aiger_sign(def->op2) &&
	     (def->op2_ref->op1 == def->op1_ref->op1 ||
	      def->op2_ref->op1 == def->op1_ref->op2 )) {
    // printf("Substitution 2.3 :\n");
    set_update_flag(def->refBy);
    def->update = 1;

    // Update refBy
    assert(def->op2 > 1);
    assert(def->op2_ref->refBy.size() != 0);
    def->op2_ref->refBy.erase(id);

    def->op2_ref->op2_ref->refBy.insert(id);

    // if (def->op2_ref->refBy.size() == 0) {
    //   remove_def(aiger_lit2var(def->op2), def->op2_ref);
    // }

    def->op2 = aiger_not(def->op2_ref->op2);
    def->op2_ref = def->op2_ref->op2_ref;

    stats_trivia++;
    return 1;

  } else if (def->op1_ref->type == aiger_def_and && !aiger_sign(def->op1) &&
	     def->op2_ref->type == aiger_def_and && aiger_sign(def->op2) &&
	     (def->op2_ref->op2 == def->op1_ref->op1 ||
	      def->op2_ref->op2 == def->op1_ref->op2 )) {
    // printf("Substitution 2.4 :\n");
    set_update_flag(def->refBy);
    def->update = 1;

    // Update refBy
    assert(def->op2 > 1);
    assert(def->op2_ref->refBy.size() != 0);
    def->op2_ref->refBy.erase(id);

    def->op2_ref->op1_ref->refBy.insert(id);

    // if (def->op2_ref->refBy.size() == 0) {
    //   remove_def(aiger_lit2var(def->op2), def->op2_ref);
    // }

    def->op2 = aiger_not(def->op2_ref->op1);
    def->op2_ref = def->op2_ref->op1_ref;

    stats_trivia++;
    return 1;

  }

  return 0;
}


bool iter_combi_simp(unsigned id, aiger_def *def) {
  assert(def->type == aiger_def_and);

  std::set<unsigned>::iterator it_set;

  /* rule 1 */
  /* x :=  op1 & ~op2  with op2 := { .., ~op1, .. }      ==>      x :=  op1 */
  /* x := ~op1 & ~op2  with op2 := { ..,  op1, .. }      ==>      x := ~op1 */
  if (def->op2_ref->func.size() >= 2 && aiger_sign(def->op2)) {
    // (def->op2_ref->func.size() >= 2)   <=>   op2 is not an input nor a latch 
    for(it_set = def->op2_ref->func.begin(); it_set != def->op2_ref->func.end(); it_set++)
      if (aiger_not(*it_set) == def->op1) {

	set_update_flag(def->refBy);

	// Update refBy
	assert(def->op2 > 1);
	assert(def->op2_ref->refBy.size() != 0);
	def->op2_ref->refBy.erase(id);
	// if (def->op2_ref->refBy.size() == 0)
	//   remove_def(aiger_lit2var(def->op2), def->op2_ref);
	
	def->type = aiger_def_cst;
	def->op2 = def->op1;
	def->op2_ref = def->op1_ref;
	assert(!def->mark_del);
	def->mark_del = 1;
	dll_remove_elem(def->dle);
	
	stats_simp++;
	return 1;
      }	
  }

  /* rule 2 */
  /* x := ~op1 &  op2  with op1 := { .., ~op2, .. }      ==>      x :=  op2 */
  /* x := ~op1 & ~op2  with op1 := { ..,  op2, .. }      ==>      x := ~op2 */
  if (def->op1_ref->func.size() >= 2 && aiger_sign(def->op1)) {
    // (def->op1_ref->func.size() >= 2)   <=>   op1 is not an input nor a latch 
    for(it_set = def->op1_ref->func.begin(); it_set != def->op1_ref->func.end(); it_set++)
      if (aiger_not(*it_set) == def->op2) {

	set_update_flag(def->refBy);

	// Update refBy
	assert(def->op1 > 1);
	assert(def->op1_ref->refBy.size() != 0);
	def->op1_ref->refBy.erase(id);
	// if (def->op1_ref->refBy.size() == 0)
	//   remove_def(aiger_lit2var(def->op1), def->op1_ref);
	
	def->type = aiger_def_cst;
	// def->op2     (nothing to do)
	// def->op2_ref (nothing to do)
	assert(!def->mark_del);
	def->mark_del = 1;
	dll_remove_elem(def->dle);
	
	stats_simp++;
	return 1;
      }	
  }

  /* rule 3 */
  /* x :=  op1 & op2  with  op2 := ~op3 &  op4  with  op3 := { .., ~op1, .. }    ==>    x :=  op1 &  op4 */
  /* x := ~op1 & op2  with  op2 := ~op3 &  op4  with  op3 := { ..,  op1, .. }    ==>    x := ~op1 &  op4 */
  /* x :=  op1 & op2  with  op2 := ~op3 & ~op4  with  op3 := { .., ~op1, .. }    ==>    x :=  op1 & ~op4 */
  /* x := ~op1 & op2  with  op2 := ~op3 & ~op4  with  op3 := { ..,  op1, .. }    ==>    x := ~op1 & ~op4 */
  if (def->op2_ref->func.size() >= 2 && !aiger_sign(def->op2)) {
    //(def->op2_ref->func.size() >= 2)   <=>   op2 is not an input nor a latch 
    if (def->op2_ref->op1_ref->func.size() >= 2 && aiger_sign(def->op2_ref->op1)) {
      //(def->op2_ref->op1_ref->func.size() >= 2)   <=>   op3 is not an input nor a latch 
      for(it_set = def->op2_ref->op1_ref->func.begin();
	  it_set != def->op2_ref->op1_ref->func.end(); it_set++)
	if (aiger_not(*it_set) == def->op1) {
	  
	  set_update_flag(def->refBy);
	  def->update = 1;

	  // Update refBy
	  assert(def->op2 > 1);
	  assert(def->op2_ref->refBy.size() != 0);
	  
	  def->op2_ref->op2_ref->refBy.insert(id);

	  def->op2_ref->refBy.erase(id);
	  // if (def->op2_ref->refBy.size() == 0)
	  //   remove_def(aiger_lit2var(def->op2), def->op2_ref);

	  def->op2 = def->op2_ref->op2;
	  def->op2_ref = def->op2_ref->op2_ref;

	  order_ops(def);
	  stats_simp++;
	  return 1;
	}
    } 
  }

  /* rule 4 */
  /* x :=  op1 & op2  with  op2 :=  op3 & ~op4  with  op4 := { .., ~op1, .. }    ==>    x :=  op1 &  op3 */
  /* x := ~op1 & op2  with  op2 :=  op3 & ~op4  with  op4 := { ..,  op1, .. }    ==>    x := ~op1 &  op3 */
  /* x :=  op1 & op2  with  op2 := ~op3 & ~op4  with  op4 := { .., ~op1, .. }    ==>    x :=  op1 & ~op3 */
  /* x := ~op1 & op2  with  op2 := ~op3 & ~op4  with  op4 := { ..,  op1, .. }    ==>    x := ~op1 & ~op3 */
  if (def->op2_ref->func.size() >= 2 && !aiger_sign(def->op2)) {
    //(def->op2_ref->func.size() >= 2)   <=>   op2 is not an input nor a latch 
    if (def->op2_ref->op2_ref->func.size() >= 2 && aiger_sign(def->op2_ref->op2)) {
      //(def->op2_ref->op2_ref->func.size() >= 2)   <=>   op4 is not an input nor a latch 
      for(it_set = def->op2_ref->op2_ref->func.begin();
	  it_set != def->op2_ref->op2_ref->func.end(); it_set++)
	if (aiger_not(*it_set) == def->op1) {
	  
	  set_update_flag(def->refBy);
	  def->update = 1;

	  // Update refBy
	  assert(def->op2 > 1);
	  assert(def->op2_ref->refBy.size() != 0);
	  
	  def->op2_ref->op1_ref->refBy.insert(id);

	  def->op2_ref->refBy.erase(id);
	  // if (def->op2_ref->refBy.size() == 0)
	  //   remove_def(aiger_lit2var(def->op2), def->op2_ref);

	  def->op2 = def->op2_ref->op1;
	  def->op2_ref = def->op2_ref->op1_ref;

	  order_ops(def);
	  stats_simp++;
	  return 1;
	}
    } 
  }

  /* rule 5 */
  /* x := op1 &  op2  with  op1 := ~op3 &  op4  with  op3 := { .., ~op2, .. }    ==>    x :=  op2 &  op4 */
  /* x := op1 & ~op2  with  op1 := ~op3 &  op4  with  op3 := { ..,  op2, .. }    ==>    x := ~op2 &  op4 */
  /* x := op1 &  op2  with  op1 := ~op3 & ~op4  with  op3 := { .., ~op2, .. }    ==>    x :=  op2 & ~op4 */
  /* x := op1 & ~op2  with  op1 := ~op3 & ~op4  with  op3 := { ..,  op2, .. }    ==>    x := ~op2 & ~op4 */
  if (def->op1_ref->func.size() >= 2 && !aiger_sign(def->op1)) {
    //(def->op1_ref->func.size() >= 2)   <=>   op1 is not an input nor a latch 
    if (def->op1_ref->op1_ref->func.size() >= 2 && aiger_sign(def->op1_ref->op1)) {
      //(def->op1_ref->op1_ref->func.size() >= 2)   <=>   op3 is not an input nor a latch 
      for(it_set = def->op1_ref->op1_ref->func.begin();
	  it_set != def->op1_ref->op1_ref->func.end(); it_set++)
	if (aiger_not(*it_set) == def->op2) {

	  set_update_flag(def->refBy);
	  def->update = 1;

	  // Update refBy
	  assert(def->op1 > 1);
	  assert(def->op1_ref->refBy.size() != 0);
	  
	  def->op1_ref->op2_ref->refBy.insert(id);

	  def->op1_ref->refBy.erase(id);
	  // if (def->op1_ref->refBy.size() == 0)
	  //   remove_def(aiger_lit2var(def->op1), def->op1_ref);

	  def->op1 = def->op1_ref->op2;
	  def->op1_ref = def->op1_ref->op2_ref;

	  order_ops(def);
	  stats_simp++;
	  return 1;
	}
    } 
  }

  /* rule 6 */
  /* x := op1 &  op2  with  op1 :=  op3 & ~op4  with  op4 := { .., ~op2, .. }    ==>    x :=  op2 &  op3 */
  /* x := op1 & ~op2  with  op1 :=  op3 & ~op4  with  op4 := { ..,  op2, .. }    ==>    x := ~op2 &  op3 */
  /* x := op1 &  op2  with  op1 := ~op3 & ~op4  with  op4 := { .., ~op2, .. }    ==>    x :=  op2 & ~op3 */
  /* x := op1 & ~op2  with  op1 := ~op3 & ~op4  with  op4 := { ..,  op2, .. }    ==>    x := ~op2 & ~op3 */
  if (def->op1_ref->func.size() >= 2 && !aiger_sign(def->op1)) {
    //(def->op1_ref->func.size() >= 2)   <=>   op1 is not an input nor a latch 
    if (def->op1_ref->op2_ref->func.size() >= 2 && aiger_sign(def->op1_ref->op2)) {
      //(def->op1_ref->op2_ref->func.size() >= 2)   <=>   op4 is not an input nor a latch 
      for(it_set = def->op1_ref->op2_ref->func.begin();
	  it_set != def->op1_ref->op2_ref->func.end(); it_set++)
	if (aiger_not(*it_set) == def->op2) {

	  set_update_flag(def->refBy);
	  def->update = 1;

	  // Update refBy
	  assert(def->op1 > 1);
	  assert(def->op1_ref->refBy.size() != 0);
	  
	  def->op1_ref->op1_ref->refBy.insert(id);

	  def->op1_ref->refBy.erase(id);
	  // if (def->op1_ref->refBy.size() == 0)
	  //   remove_def(aiger_lit2var(def->op1), def->op1_ref);

	  def->op1 = def->op1_ref->op1;
	  def->op1_ref = def->op1_ref->op1_ref;
	  
	  order_ops(def);
	  stats_simp++;
	  return 1;
	}
    } 
  }

  return 0;
}

bool iter_combi_struct_equiv(unsigned id, aiger_def *def, unsigned key) {
  assert(!def->mark_del);
  if(def->mark_del)
    return 0;

  assert(def->type == aiger_def_and);

  std::unordered_multimap<unsigned,aiger_def*>::iterator elem;
  aiger_def *def_refby;
  bool struct_equiv = 0;

  std::pair<std::unordered_multimap<unsigned,aiger_def*>::iterator,
	    std::unordered_multimap<unsigned,aiger_def*>::iterator> range;
  range = hash_combi_struct_equiv.equal_range(key);

  if(range.first != hash_combi_struct_equiv.end()) {
    // Conflict or structurally equivalent!
    if (hash_combi_struct_equiv.count(key) > 150)
      printf("(Conflict size = %lu) WARNING lots of conflicts ->"
	     "Change combi struct_equiv hash function)\n",
             hash_combi_struct_equiv.count(key));

    for(elem = range.first; elem != range.second && !struct_equiv; elem++) {
      def_refby = (*elem).second;
      assert(!def_refby->mark_del);
      if (equal_def(def, def_refby)) {
	// Structurally equivalent
	
	set_update_flag(def->refBy);

	// Update refBy
	assert(def->op1 > 1);
	def->op1_ref->refBy.erase(id);
	assert(def->op1_ref->refBy.size() != 0);
	
	assert(def->op2 > 1);
	def->op2_ref->refBy.erase(id);
	assert(def->op2_ref->refBy.size() != 0);
	
	def_refby->refBy.insert(id);

	def->type = aiger_def_cst;
	def->op2 = aiger_var2lit(((*elem).second)->var_lhs);
	def->op2_ref = def_refby;
	assert(!def->mark_del);
	def->mark_del = 1;
	dll_remove_elem(def->dle);
	struct_equiv = 1;
	
	stats_struct_equiv++;
      }
    }
  }
  return struct_equiv;
}

void iter_combi_build_func(unsigned id, aiger_def *def) {

  assert(def->type == aiger_def_and);

  std::set<unsigned> old_func_cst(def->func_cst);

  def->func_cst.clear();
  def->func.clear();

  aiger_def *def_op1, *def_op2;
  def_op1 = def->op1_ref; // OPT
  def_op2 = def->op2_ref; // OPT
  
  assert(def_op1->type != aiger_def_cst);
  assert(def_op2->type != aiger_def_cst);
  assert(def_op1->type != aiger_def_and || def_op1->func.size());
  assert(def_op1->type != aiger_def_and || def_op1->func_cst.size());
  assert(def_op2->type != aiger_def_and || def_op2->func.size());
  assert(def_op2->type != aiger_def_and || def_op2->func_cst.size());

  if (aiger_sign(def->op1) || (def_op1->type != aiger_def_and)
      || def_op1->func_cst.size() > 1000) {
    def->func_cst.insert(def->op1);
    def->func.insert(def->op1);

  } else {
    def->func_cst.insert(def->op1);
    add_set2set(def_op1->func_cst, def->func_cst);
    add_set2set(def_op1->func, def->func);
  }

  if (aiger_sign(def->op2) || (def_op2->type != aiger_def_and)
      || def_op2->func_cst.size() > 1000) {
    def->func_cst.insert(def->op2);
    def->func.insert(def->op2);
    
  } else {
    def->func_cst.insert(def->op2);
    add_set2set(def_op2->func_cst, def->func_cst);
    add_set2set(def_op2->func, def->func);
  }

  if (!old_func_cst.empty()) {
    if(!equal_set(old_func_cst, def->func_cst)) {
      set_update_flag(def->refBy);
    }
  }

  /* Check if functional <=> 0 */
  assert(def->func_cst.size() >= 2);
  assert(def->func.size() >= 2);
}

bool iter_combi_func_equiv_cst(unsigned id, aiger_def *def) {
  std::set<unsigned>::iterator it_set;
  unsigned current;
  bool is_cst = 0;
  
  if (def->func.size() == 2)
    return 0;

  for (it_set = def->func_cst.begin(); !is_cst && it_set != (--def->func_cst.end());) {
    current = *it_set;
    it_set++;

    if (aiger_lit2var(*it_set) == aiger_lit2var(current)) {
      set_update_flag(def->refBy);
      
      // Update refBy
      if (def->op1 > 1) {
	assert(def->op1_ref->refBy.size() != 0);
	def->op1_ref->refBy.erase(id);
	// if (def->op1_ref->refBy.size() == 0)
	//   remove_def(aiger_lit2var(def->op1), def->op1_ref);
      }
      if (def->op2 > 1) {
	assert(def->op2_ref->refBy.size() != 0);
	def->op2_ref->refBy.erase(id);
	// if (def->op2_ref->refBy.size() == 0)
	//   remove_def(aiger_lit2var(def->op2), def->op2_ref);
      }
      
      def->type = aiger_def_cst;
      def->op2 = 0;
      def->op2_ref = NULL;
      assert(!def->mark_del);
      def->mark_del = 1;
      dll_remove_elem(def->dle);
      is_cst = 1;
      
      stats_func_equiv++;
    }
  }
  return is_cst;
}

bool iter_combi_func_subsume(unsigned id, aiger_def *def) {
  bool is_cst = 0;
  
  if (def->func.size() == 2)
    return 0;


  if (def->op1_ref->func_cst.find(def->op2) != def->op1_ref->func_cst.end()) {
    assert(0); // Should not happen because of operands order

  } else if (def->op2_ref->func_cst.find(def->op1) != def->op2_ref->func_cst.end() && !aiger_sign(def->op2)) {
    // This case is handled in functional equivalence too.. (Should be removed)

    // Update refBy
    assert(def->op1 > 1);
    assert(def->op1_ref->refBy.size() != 0);
    def->op1_ref->refBy.erase(id);
    // if (def->op1_ref->refBy.size() == 0)
    //   remove_def(aiger_lit2var(def->op1), def->op1_ref);
    
    def->type = aiger_def_cst;
    // def->op2 = 
    // def->op2_ref = 
    assert(!def->mark_del);
    def->mark_del = 1;
    dll_remove_elem(def->dle);
    is_cst = 1;

    stats_func_subsume++;

  } else if (!aiger_sign(def->op1) && !aiger_sign(def->op2)) {
    if (def->op1_ref->func.size() > 0 &&
	def->op1_ref->func.size() < def->op2_ref->func.size() &&
	std::includes(def->op2_ref->func.begin(), def->op2_ref->func.end(),
		      def->op1_ref->func.begin(), def->op1_ref->func.end())) {
      // Update refBy
      assert(def->op1 > 1);
      assert(def->op1_ref->refBy.size() != 0);
      def->op1_ref->refBy.erase(id);
      // if (def->op1_ref->refBy.size() == 0)
      // 	remove_def(aiger_lit2var(def->op1), def->op1_ref);
    
      def->type = aiger_def_cst;
      def->op2 = def->op2;
      def->op2_ref = def->op2_ref;
      assert(!def->mark_del);
      def->mark_del = 1;
      dll_remove_elem(def->dle);
      is_cst = 1;
    
      stats_func_subsume++;

    } else if (def->op2_ref->func.size() > 0 &&
	       def->op1_ref->func.size() > def->op2_ref->func.size() &&
	       std::includes(def->op1_ref->func.begin(), def->op1_ref->func.end(),
			     def->op2_ref->func.begin(), def->op2_ref->func.end())) {
      // Update refBy
      assert(def->op2 > 1);
      assert(def->op2_ref->refBy.size() != 0);
      def->op2_ref->refBy.erase(id);
      // if (def->op2_ref->refBy.size() == 0)
      // 	remove_def(aiger_lit2var(def->op2), def->op2_ref);
    
      def->type = aiger_def_cst;
      def->op2 = def->op1;
      def->op2_ref = def->op1_ref;
      assert(!def->mark_del);
      def->mark_del = 1;
      dll_remove_elem(def->dle);
      is_cst = 1;
    
      stats_func_subsume++;
    }

  } else if (aiger_sign(def->op1) && aiger_sign(def->op2)) {
    assert(0); // This case should not happen, assert is kept to check consistency
  }
  return is_cst;
}

bool iter_combi_func_equiv(unsigned id, aiger_def *def, unsigned key) {
  assert(!def->mark_del);
  /* TODO: Optimize functional diff of size 3 */
  std::unordered_multimap<unsigned,aiger_def*>::iterator elem;
  aiger_def *def_refby;
  bool func_equiv = 0;

  std::pair< std::unordered_multimap<unsigned,aiger_def*>::iterator,
  	     std::unordered_multimap<unsigned,aiger_def*>::iterator> range;
  range = hash_combi_func_equiv.equal_range(key);

  if(range.first == hash_combi_func_equiv.end()) {
    // Add to hash
    hash_combi_func_equiv.insert(std::pair<unsigned, aiger_def*>(key, def));

  } else {
    // Conflict or functionally equivalent!
    if (hash_combi_func_equiv.count(key) > 150)
      printf("(Conflict size = %lu) WARNING lots of conflicts -> Change func_equiv hash function)\n",
             hash_combi_func_equiv.count(key));

    for(elem = range.first; elem != range.second && !func_equiv; elem++) {
      def_refby = (*elem).second;
      assert(!def_refby->mark_del);
      if (equal_set(def->func, def_refby->func)) {
	// Functionally equivalent
	set_update_flag(def->refBy);

	// Update refBy
	assert(def->op1 > 1);
	assert(def->op1_ref->refBy.size() != 0);
	def->op1_ref->refBy.erase(id);
	// if (def->op1_ref->refBy.size() == 0 && aiger_lit2var(def->op1) != def_refby->var_lhs)
	//   remove_def(aiger_lit2var(def->op1), def->op1_ref);
	
	assert(def->op2 > 1);
	assert(def->op2_ref->refBy.size() != 0);
	def->op2_ref->refBy.erase(id);
	// if (def->op2_ref->refBy.size() == 0 && aiger_lit2var(def->op2) != def_refby->var_lhs)
	//   remove_def(aiger_lit2var(def->op2), def->op2_ref);

	def_refby->refBy.insert(id);

	def->type = aiger_def_cst;
	def->op2 = aiger_var2lit(def_refby->var_lhs);
	def->op2_ref = def_refby;
	assert(!def->mark_del);
	def->mark_del = 1;
	dll_remove_elem(def->dle);
	func_equiv = 1;

	stats_func_equiv++;
      }
    }
    if (!func_equiv) // Conflict
      hash_combi_func_equiv.insert(std::pair<unsigned, aiger_def*>(key, def));
  }

  return func_equiv;
}

bool iter_combi_func_equiv_diff1(unsigned id, aiger_def *def) {
  std::unordered_multimap<unsigned,aiger_def*>::iterator elem;
  aiger_def *def_refby, *def_elem;
  bool func_equiv = 0;
  unsigned key;
  unsigned elem_to_add;
  std::pair< std::unordered_multimap<unsigned,aiger_def*>::iterator,
  	     std::unordered_multimap<unsigned,aiger_def*>::iterator> range;
  
  assert(def->func.size() > 2);

  /* Heuristic */
  if (def->func.size() > 1000)
    return 0;

  for(int i = 0; i < def->func.size(); i++) {
    key = hash_set_diff1(def->func, i);
    range = hash_combi_func_equiv.equal_range(key);
    
    if(range.first != hash_combi_func_equiv.end()) {
      for(elem = range.first; elem != range.second && !func_equiv; elem++) {
	def_refby = (*elem).second;
	assert(!def_refby->mark_del);
	if (def->op1 != aiger_var2lit(def_refby->var_lhs) &&
	    def->op2 != aiger_var2lit(def_refby->var_lhs)) {
	  if (def_refby->func.size() == def->op1_ref->func.size() ||
	      def_refby->func.size() == def->op2_ref->func.size()) {
	    return 0;
	  
	  } else if (elem_to_add = equal_set_diff1(def->func, def_refby->func, i)) {
	    def_elem = hash[aiger_lit2var(elem_to_add)];

	    if (def->op1_ref->refBy.size() == 1 ||
		def->op2_ref->refBy.size() == 1 ||
		// Heuristic
		aiger_var2lit(def_refby->var_lhs) < def->op1 ||
		aiger_var2lit(def_refby->var_lhs) < def->op2 ) {

	      set_update_flag(def->refBy);
	      def->update = 1;
	      
	      // Update refBy
	      assert(def->op1 > 1);
	      assert(def->op1_ref->refBy.size() != 0);
	      assert(aiger_lit2var(def->op1) != def_refby->var_lhs);
	      def->op1_ref->refBy.erase(id);

	      assert(def->op2 > 1);
	      assert(def->op2_ref->refBy.size() != 0);
	      assert(aiger_lit2var(def->op2) != def_refby->var_lhs);
	      def->op2_ref->refBy.erase(id);

	      def_refby->refBy.insert(id);
	      def_elem->refBy.insert(id);
	  
	      // if (def->op1_ref->refBy.size() == 0)
	      // 	remove_def(aiger_lit2var(def->op1), def->op1_ref);
	      // if (def->op2_ref->refBy.size() == 0)
	      // 	remove_def(aiger_lit2var(def->op2), def->op2_ref);

	      def->op1 = elem_to_add;
	      def->op1_ref = def_elem;
	      def->op2 = aiger_var2lit(def_refby->var_lhs);
	      def->op2_ref = def_refby;
	  
	      order_ops(def);

	      func_equiv = 1;
	      stats_func_equiv_diff1++;
	      return 1;
	    }
	  }
	}
      }
    }
  }
  return 0;
}
  
bool iter_combi_func_equiv_diff1_s3(unsigned id, aiger_def *def) {
  /* TODO: Can be optimized using struct_equiv hash        */
  /* Do not add into func_equiv_hash when func.size() == 2 */
  return iter_combi_func_equiv_diff1(id, def);
}

/********************************************************************************
Latch simplification helper functions
********************************************************************************/
void iter_latch_propagate_cst(unsigned id, aiger_def *def) {
  assert(def->type == aiger_def_latch);

  std::map<unsigned,unsigned>::iterator it;
  aiger_def *def_refby;

  if (def->op1 != 0)
    printf("WARNING, handle only uninitialized model\n");
  assert(def->op1 == 0);

  unsigned old_op2 = def->op2;
  aiger_def *old_op2_ref = def->op2_ref;
  /* Check if op2 = cst */
  while (def->op2 > 1 && def->op2_ref->type == aiger_def_cst) {
    if (aiger_sign(def->op2))
      def->op2 = aiger_not(def->op2_ref->op2);
    else
      def->op2 = def->op2_ref->op2;

    def->op2_ref = def->op2_ref->op2_ref;
  }

  // Update refBy
  if (def->op2 != old_op2) {
    assert(old_op2_ref != def->op2_ref);
    assert(old_op2 > 1);
    assert(old_op2_ref->refBy.size() != 0);

    set_update_flag(def->refBy);

    old_op2_ref->refBy.erase(id);
    
    if (def->op2 > 1)
      def->op2_ref->refBy.insert(id);

  }

}

bool iter_latch_check_cst(unsigned id, aiger_def *def) {
  assert(def->type == aiger_def_latch);

  bool continue_propa = 0;
  if (def->op1 != 0)
    printf("WARNING, handle only uninitialized model\n");
  assert(def->op1 == 0);

  if (def->op2 == 0 || def->op2 == aiger_var2lit(def->var_lhs)) {

    set_update_flag(def->refBy);

    def->type = aiger_def_cst;
    def->op2 = 0;
    def->op2_ref = NULL;

    assert(!def->mark_del);
    def->mark_del = 1;
    dll_remove_elem(def->dle);
    continue_propa = 1;
    stats_cst_prop++;

  } else {
    // Check constant chain :
    // a := 0, b;
    // b := 0, c;
    // ...
    // z := 0, a;
    // =>
    // a, b, .., z = cst 0

    // OR :
    // a := 0, ~b;
    // b := 0, ~c;
    // ...
    // z := 0, ~a;
    // =>
    // a := 0, ~a;   and   b, .., z = cst a
    
  }
  
  return continue_propa;
}

bool iter_latch_struct_equiv(unsigned id, aiger_def *def) {
  assert(!def->mark_del);
  if (def->mark_del)
    return 0;

  assert(def->type == aiger_def_latch);
  assert(def->op1 == 0);

  std::unordered_multimap<unsigned,aiger_def*>::iterator elem;
  aiger_def *def_refby;
  bool struct_equiv = 0;
  bool continue_propa;
  unsigned key = hash_uint(def->op2);
  std::pair< std::unordered_multimap<unsigned,aiger_def*>::iterator,
  	     std::unordered_multimap<unsigned,aiger_def*>::iterator> range;
  range = hash_latch_struct_equiv.equal_range(key);

  if(range.first == hash_latch_struct_equiv.end()) {
    // Add to hash
    hash_latch_struct_equiv.insert(std::pair<unsigned, aiger_def*>(key, def));
    
  } else {
    // Conflict or structurally equivalent!
    if (hash_latch_struct_equiv.count(key) > 100)
      printf("(Conflict size = %lu) WARNING lots of conflicts -> Change latch struct_equiv hash function)\n",
	     hash_latch_struct_equiv.count(key));

    for(elem = range.first; elem != range.second && !struct_equiv; elem++) {
      def_refby = (*elem).second;
      assert(!def_refby->mark_del);
      if (equal_def(def, def_refby)) {
	// Structurally equivalent

	set_update_flag(def->refBy);

	// Update refBy
	if (def->op2 > 1) {
	  def->op2_ref->refBy.erase(id);
	  assert(def->op2_ref->refBy.size() != 0);
	}
	
	def_refby->refBy.insert(id);

	def->type = aiger_def_cst;
	def->op2 = aiger_var2lit(def_refby->var_lhs);
	def->op2_ref = def_refby;
	
	assert(!def->mark_del);
	def->mark_del = 1;
	dll_remove_elem(def->dle);
	struct_equiv = 1;
	
	stats_struct_equiv++;
      }
    }
    if (!struct_equiv) // Conflict
      hash_latch_struct_equiv.insert(std::pair<unsigned, aiger_def*>(key, def));
  }

  return struct_equiv;
}


/********************************************************************************
Ternary Simulation
********************************************************************************/
void reset_latches() {
  aiger_def *def;
  dll_elem<aiger_def> *e_tmp = l_latches->head;
  while(e_tmp != NULL) {
    def = e_tmp->def;

    def->val = v0;

    e_tmp = e_tmp->next;
  }
}

void inputs_to_X() {
  aiger_def *def;
  dll_elem<aiger_def> *e_tmp = l_inputs->head;
  while(e_tmp != NULL) {
    def = e_tmp->def;

    def->val = vX;

    e_tmp = e_tmp->next;
  }
}

void compute_current_val(aiger_def *def) {
  if (def->type == aiger_def_latch) {
    def->prev_val = def->val;
    if (def->op2 > 1) {
      assert(def->op2_ref != NULL);
      sim_val val_op2;
      if (def->op2_ref->type == aiger_def_latch) {
	if (def->op2_ref->var_lhs < def->var_lhs)
	  val_op2 = def->op2_ref->prev_val;
	else
	  val_op2 = def->op2_ref->val;
      } else {
	val_op2 = def->op2_ref->val;
      }
      if (val_op2 == vX)
	def->val = vX;
      else {
	if (aiger_sign(def->op2))
	  def->val = val_op2 == v0 ? v1 : v0;
	else
	  def->val = val_op2;
      }
    } else {
      def->val = def->op2 ? v1 : v0;
    }

  } else if (def->type == aiger_def_and) {
    sim_val val1;
    sim_val val2;
    if (def->op1 > 1) {
      val1 = def->op1_ref->val;
      if (val1 != vX) {
	if (aiger_sign(def->op1))
	  val1 = val1 == v0 ? v1 : v0;
      }
    } else {
      val1 = def->op1 ? v1 : v0;
    }
    if (def->op2 > 1) {
      val2 = def->op2_ref->val;
      if (val2 != vX) {
	if (aiger_sign(def->op2))
	  val2 = val2 == v0 ? v1 : v0;
      }
    } else {
      val2 = def->op2 ? v1 : v0;
    }
    if (val1 == v0 || val2 == v0)
      def->val = v0;
    else if (val1 == vX || val2 == vX)
      def->val = vX;
    else
      def->val = v1;

  } else {
    assert(0);
  }
}

void iter_compute_val(dll_list<aiger_def> *l) {
  aiger_def *def;
  dll_elem<aiger_def> *e_tmp = l->head;
  while(e_tmp != NULL) {
    def = e_tmp->def;

    assert(!def->mark_del);
    compute_current_val(def);

    e_tmp = e_tmp->next;
  }
}

int ternary_simulation() {
  if (verbose) printf("[aigprep]  Starting ternary simulation\n");  
  reset_latches();

  std::vector<std::pair<std::vector<sim_val>, int>> prev_states;
  prev_states.push_back(std::pair<std::vector<sim_val>, int>(std::vector<sim_val>(), 0));
  std::vector<bool> latch_to_check;
  aiger_def *def;
  dll_elem<aiger_def> *e_tmp = l_latches->head;
  int cpt_test = 0;
  while(e_tmp != NULL) {
    def = e_tmp->def;

    assert(!def->mark_del);
    cpt_test++;
    prev_states.back().first.push_back(def->val);
    latch_to_check.push_back(1);

    e_tmp = e_tmp->next;
  }  
  inputs_to_X();
  bool done = 0;
  int stats_iter_tsim = 0;
  std::vector<sim_val> curr_state;
  curr_state.reserve(l_latches->sz);
  while(!done) {
    stats_iter_tsim++;
    iter_compute_val(l_ands);
    iter_compute_val(l_latches);

    curr_state.clear();
    int hash = 0;
    e_tmp = l_latches->head;
    while(e_tmp != NULL) {
      def = e_tmp->def;
      
      curr_state.push_back(def->val);
      hash += (int)def->val;
    
      e_tmp = e_tmp->next;
    }

    for (int i = 0; i < prev_states.size() && !done; i++)
      if (prev_states[i].second == hash) {
	assert(curr_state.size() == prev_states[i].first.size());
	bool diff = 0;
	for (int j = 0; j < curr_state.size() && !diff; j++) {
	  if (prev_states[i].first[j] != curr_state[j]) {
	    diff = 1;
	    if (latch_to_check[j])
	      latch_to_check[j] = 0;
	  }
	}
	if (!diff)
	  done = 1;
      }
    if (!done)
      prev_states.push_back(std::pair<std::vector<sim_val>, int>(curr_state, hash));
    if (verbose) printf("[aigprep]  Ternary simulation : %d iterations\r", stats_iter_tsim);
    if (stats_iter_tsim > MAX_ITER_TS) {
      if (verbose) printf("\n[aigprep]  Ternary simulation cancelled (too many iterations required)\n");
      return 0;
    }
  }

  int nb_stuck_at = 0;
  if (prev_states.size() > 1) {
    for (int i = 0; i < latch_to_check.size(); i++) {
      bool done = 0;
      if (latch_to_check[i] && prev_states[1].first[i] != vX) {
	int val = prev_states[1].first[i];
	for (int j = 2; j < prev_states.size() && !done; j++) {
	  if (prev_states[j].first[i] != val) {
	    done = 1;
	    latch_to_check[i] = 0;
	  } 
	}
      } else {
	latch_to_check[i] = 0;
      }
    }

    e_tmp = l_latches->head;
    for (int i = 0; i < latch_to_check.size(); i++) {
      def = e_tmp->def;
      if (latch_to_check[i]) {
	nb_stuck_at++;

	if (def->op2 > 1) {
	  def->op2_ref->refBy.erase(def->var_lhs);
	  def->op2 = prev_states[1].first[i];
	  def->op2_ref = NULL;

	} else {
	  assert(def->op2 == prev_states[1].first[i]);
	  assert(def->op2_ref == NULL);
	}
      }
      e_tmp = e_tmp->next;
    }
    assert(e_tmp == NULL);
  }
  if (verbose)
    printf("[aigprep]  Found %d stuck-at latches (%d iterations)\n", nb_stuck_at, stats_iter_tsim);

  return nb_stuck_at;
}

/********************************************************************************
Helper functions
********************************************************************************/
void remove_def(aiger_def* def) {
  int id = def->var_lhs;
  assert(def->refBy.size() == 0);
  assert(!def->mark_del);
  if (id != aiger_lit2var(bad_state)) {  
    dll_remove_elem(def->dle);
    def->mark_del = 1;
  
    // Update refBy
    if (def->op1 > 1)
      def->op1_ref->refBy.erase(id);
    if (def->op2 > 1)
      def->op2_ref->refBy.erase(id);
  
    stats_unref++;
  }
}

unsigned hash_uint(int x) {
  unsigned ret = ((x >> 16) ^ x) * 0x45d9f3b;
  ret = ((ret >> 16) ^ ret) * 0x45d9f3b;
  ret = (ret >> 16) ^ ret;
  return ret;
}

inline unsigned hash_def(unsigned op1, unsigned op2) {
  return op1 + op2 * 111;
}

inline bool equal_def(aiger_def* def1, aiger_def* def2) {
  return (def1->op1 == def2->op1) && (def1->op2 == def2->op2);
}


unsigned hash_set(std::set<unsigned> &f) {
  std::set<unsigned>::iterator elem;
  unsigned hash = 0;
  unsigned cpt = 1;
  for(elem = f.begin(); elem != f.end(); elem++) {
    hash += hash_uint(*elem) * cpt++;
  }
  return hash;
}

unsigned hash_set_diff1(std::set<unsigned> &f, unsigned ind) {
  std::set<unsigned>::iterator elem;
  unsigned hash = 0;
  unsigned cpt = 1;
  unsigned cpt_ind = 0;
  for(elem = f.begin(); elem != f.end(); elem++) {
    if (cpt_ind++ != ind) {
      hash += hash_uint(*elem) * cpt++;
    }
  }
  return hash;
}

unsigned equal_set_diff1(std::set<unsigned> &f1, std::set<unsigned> &f2, unsigned ind){
  if ((f1.size()-1) != f2.size())
    return 0;
  
  std::set<unsigned>::iterator elem2 = f2.begin();
  unsigned cpt_ind = 0;
  unsigned elem_to_add;
  for(std::set<unsigned>::iterator elem1 = f1.begin(); elem1 != f1.end(); elem1++) {
    if (cpt_ind++ != ind) {
      if (*elem1 != *elem2)
	return 0;
      elem2++;
    } else {
      elem_to_add = *elem1;
    }
  }
  return elem_to_add;
}
 
inline bool equal_set(std::set<unsigned> &f1, std::set<unsigned> &f2) {
  return f1.size() == f2.size() && f1 == f2;
}

void update_bad_state() {
  std::map<unsigned,unsigned>::iterator it;
  aiger_def *def;

  while ((def = hash[aiger_lit2var(bad_state)])->type == aiger_def_cst) {
    if (aiger_sign(bad_state))
      bad_state = aiger_not(def->op2);
    else
      bad_state = def->op2;

    if (bad_state == 1) {
      if (verbose) printf("[aigprep]  Bad state evaluates to TRUE by preprocessing\n");
      printf("1\nb0\n.\n");
      exit(0);
    } else if (bad_state == 0) {
      if (verbose) printf("[aigprep]  Bad state evaluates to FALSE by preprocessing\n");
      printf("0\nb0\n.\n");
      exit(0);
    }

  }
}

void set_update_flag(std::set<unsigned> &refBy) {  
  aiger_def *def;
  for(std::set<unsigned>::iterator it_set = refBy.begin(); it_set != refBy.end(); it_set++) {
    if (*it_set != 0) {
      def = hash[*it_set];
      if (!def->update) {
	def->update = 1;
      }
    }
  }
}

void add_set2set(std::set<unsigned> &src, std::set<unsigned> &dest) {
  for (std::set<unsigned>::iterator it_set = src.begin(); it_set != src.end(); ++it_set) {
    dest.insert(*it_set);
  }
}

void update_level(aiger_def* def) {
  assert(def->type == aiger_def_and);
  level[def->var_lhs] = level[def->op1_ref->var_lhs] < level[def->op2_ref->var_lhs] ?
    level[def->op2_ref->var_lhs] + 1 : level[def->op1_ref->var_lhs] + 1;
}

inline void order_ops(aiger_def* def) {
  if(def->op1 > def->op2) {
    unsigned op_tmp = def->op1;
    aiger_def* ref_tmp = def->op1_ref;
    def->op1 = def->op2;
    def->op1_ref = def->op2_ref;
    def->op2 = op_tmp;
    def->op2_ref = ref_tmp;
  }
}

void print_set(std::set<unsigned> &s) {
  printf("{ ");
  for (std::set<unsigned>::iterator it_set = s.begin(); it_set != s.end(); ++it_set)
    printf("%d ", *it_set);
  printf("}\n");
}

void print_def(aiger_def* def) {
  if (def->type == aiger_def_input) {
    printf("%d (Input) (refBy : %lu) (coi: %d) (del: %d)\n",
	   def->var_lhs, def->refBy.size(), def->coi, def->mark_del);
  } else if (def->type == aiger_def_latch) {
    printf("%d := 0, %d; (refBy : %lu) (coi: %d) (del: %d)\n",
	   def->var_lhs, aiger_sign(def->op2) ? -aiger_lit2var(def->op2) : aiger_lit2var(def->op2),
	   def->refBy.size(), def->coi, def->mark_del);

  } else if (def->type == aiger_def_and) {
    //printf("%d := %d & %d; (refBy : %lu) (coi: %d) (del: %d)\n",
    //def->var_lhs, def->op1, def->op2, def->refBy.size(), def->coi, def->mark_del);
    printf("%d := %d & %d", def->var_lhs, 
	   aiger_sign(def->op1) ? -aiger_lit2var(def->op1) : aiger_lit2var(def->op1),
	   aiger_sign(def->op2) ? -aiger_lit2var(def->op2) : aiger_lit2var(def->op2));
    printf("; (refBy : %lu) (coi: %d) (del: %d) ",def->refBy.size(), def->coi, def->mark_del);
    print_set(def->refBy);
  } else if (def->type == aiger_def_cst) {
    printf("%d := %d; (refBy : %lu) (coi: %d) (del: %d)\n",
	   def->var_lhs, def->op2, def->refBy.size(), def->coi, def->mark_del);
  } else {
    assert(0);
  }
}


/* Doubly linked list for def id  */
// void dll_init(dll_list* l) {
//   l->sz = 0;
//   l->head = NULL;
//   l->last = NULL;
// }

// void dll_delete(dll_list* l) {
//   delete l;
// }

// dll_elem* dll_insert_last(dll_list* l, unsigned ind, aiger_def *def) { // OPT
//   dll_elem* e = new dll_elem();
//   e->id = ind;
//   e->def = def;
//   e->next = NULL;
//   e->prev = l->last;
  
//   if (!l->sz)
//     l->head = e;
//   else 
//     l->last->next = e;
  
//   l->last = e;
//   l->sz++;
//   return e;
// }

// void dll_remove_elem(dll_list* l, dll_elem* e) {
//   if (e == NULL) {
//     printf("Trying to remove a NULL element!\n"); exit(-1);
//   }
//   if (!l->sz) {
//     printf("Trying to remove an element of an empty dl list!\n"); exit(-1);
//   } else if (l->sz == 1) {
//     l->sz = 0;
//     l->head = NULL;
//     l->last = NULL;
//   } else {
//     l->sz--;
//     if (e->prev == NULL)
//       l->head = e->next;
//     else
//       e->prev->next = e->next;

//     if (e->next == NULL)
//       l->last = e->prev;
//     else
//       e->next->prev = e->prev;
//   }
  
// }

// unsigned dll_size(dll_list* l) {
//   return l->sz;
// }



void check_refBy_consistency() {
  aiger_def *def;
  dll_elem<aiger_def> * e_tmp;
  e_tmp = l_inputs->head;
  while(e_tmp != NULL) {
    def = e_tmp->def;
    
    if (!def->mark_del) {
      for (std::set<unsigned>::iterator it = def->refBy.begin(); it != def->refBy.end(); it++) {
	if (*it != 0) {
	  aiger_def *def_rb = hash[*it];
	  if (def_rb->type == aiger_def_and) {
	    assert((def_rb->op1 >> 1) == def->var_lhs || (def_rb->op2 >> 1) == def->var_lhs);

	  } else if (def_rb->type == aiger_def_latch) {
	    assert((def_rb->op2 >> 1) == def->var_lhs);

	  } else if (def_rb->type == aiger_def_cst) {
	    assert((def_rb->op2 >> 1) == def->var_lhs);

	  } else {
	    assert(0);
	  }
	}
      }
    }
    e_tmp = e_tmp->next;
  }

  e_tmp = l_latches->head;
  while(e_tmp != NULL) {
    def = e_tmp->def;

    if (!def->mark_del) {
      for (std::set<unsigned>::iterator it = def->refBy.begin(); it != def->refBy.end(); it++) {
	if (*it != 0) {
	  aiger_def *def_rb = hash[*it];
	  if (def_rb->type == aiger_def_and) {
	    assert((def_rb->op1 >> 1) == def->var_lhs || (def_rb->op2 >> 1) == def->var_lhs);

	  } else if (def_rb->type == aiger_def_latch) {
	    assert((def_rb->op2 >> 1) == def->var_lhs);

	  } else if (def_rb->type == aiger_def_cst) {
	    assert((def_rb->op2 >> 1) == def->var_lhs);

	  } else {
	    assert(0);
	  }
	}
      }

      if (def->op2_ref != NULL) {
	assert(def->op2_ref->refBy.find(def->var_lhs) != def->op2_ref->refBy.end());
      }
    }
    e_tmp = e_tmp->next;
  }

  e_tmp = l_ands->head;
  while(e_tmp != NULL) {
    def = e_tmp->def;

    if (!def->mark_del) {
      for (std::set<unsigned>::iterator it = def->refBy.begin(); it != def->refBy.end(); it++) {
	if (*it != 0) {
	  aiger_def *def_rb = hash[*it];
	  if (def_rb->type == aiger_def_and) {
	    assert((def_rb->op1 >> 1) == def->var_lhs || (def_rb->op2 >> 1) == def->var_lhs);

	  } else if (def_rb->type == aiger_def_latch) {
	    assert((def_rb->op2 >> 1) == def->var_lhs);

	  } else if (def_rb->type == aiger_def_cst) {
	    assert((def_rb->op2 >> 1) == def->var_lhs);

	  } else {
	    assert(0);
	  }
	}
      }
      if (def->op1_ref != NULL) {
	assert(def->op1_ref->refBy.find(def->var_lhs) != def->op1_ref->refBy.end());
      }
      if (def->op2_ref != NULL) {
	assert(def->op2_ref->refBy.find(def->var_lhs) != def->op2_ref->refBy.end());
      }
    }
    e_tmp = e_tmp->next;
  }
}
