/***************************************************************************
Copyright (c) 2016-2017, Guillaume Baud-Berthier, SafeRiver/LaBRI

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
IN THE SOFTWARE.
***************************************************************************/
#ifndef unroll_h
#define unroll_h

#include "utils.h"
#include "solver.h"

//int addPO(SimpSolver* solver, std::vector<idef*> &mapdef, unsigned *v, unsigned po, int time);

unsigned init_model(aiger* model, dll_list<idef>* dll_ands, dll_list<idef>* dll_inputs, dll_list<idef>* dll_latches, std::vector<idef*> &mapdef, bool elim_ll, bool verbose);

void destroy_model(dll_list<idef>* dll_ands, dll_list<idef>* dll_inputs, dll_list<idef>* dll_latches, std::vector<dll_list<ands_def>*> &list_ands, std::vector<idef*> &mapdef, std::map<unsigned, ands_def*> &map_ands, bool verbose);

dll_list<ands_def>* unroll(dll_list<idef>* dll_ands, dll_list<idef>* dll_inputs, dll_list<idef>* dll_latches, std::vector<idef*> &mapdef, std::map<unsigned, ands_def*> &map_ands, unsigned *v, int time);

prop_tr_un* unroll_prop_tr(std::vector<idef*> &mapdef, std::map<unsigned, ands_def*> &map_ands, prop_tr *pt, int time, bool _not);

void iter_flatten_ptu(std::vector<dll_list<ands_def>*> &list_ands, int time);
prop_tr_un_fl* flatten_ptu(ands_def *ad, prop_tr_un *ptu);


#endif
