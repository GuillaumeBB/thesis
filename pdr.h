/***************************************************************************
Copyright (c) 2016-2017, Guillaume Baud-Berthier, SafeRiver/LaBRI

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
IN THE SOFTWARE.
***************************************************************************/

#ifndef pdr_h
#define pdr_h

#include "utils.h"
#include "hl_ve.h"
#include "solver.h"
#include "unroll.h"

struct Cube {
  std::vector<std::pair<idef*, bool>> ref_val;
  int act_lem;

bool operator <(const Cube& c) const {
  return std::tie(ref_val, act_lem) < std::tie(c.ref_val, c.act_lem);
  //return act_lem < c.act_lem;
}

bool operator ==(const Cube& c) const {
  if (ref_val.size() != c.ref_val.size())
    return 0;
  for (int i = 0; i < ref_val.size(); i++)
    if (ref_val[i].first != c.ref_val[i].first || ref_val[i].second != c.ref_val[i].second)
      return 0;
  return 1;
}


Cube(): ref_val(), act_lem(-1) {};
Cube(int act_lit): ref_val(), act_lem(act_lit) {};
};

struct TCube {
  Cube cube;
  unsigned frame_ind;

TCube(Cube c, unsigned i): cube(c), frame_ind(i) {};
TCube(): cube(), frame_ind(-1) {};
};

struct Frame {
  std::vector<Cube> lemmas;
  int level;

Frame(): lemmas(), level() {};
Frame(int l): lemmas(), level(l) {};
};

int pdr(aiger *model, bool elim_ll, bool elim_hl, bool verbose_, int *status);

#endif
