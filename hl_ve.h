/***************************************************************************
Copyright (c) 2016-2017, Guillaume Baud-Berthier, SafeRiver/LaBRI

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
IN THE SOFTWARE.
***************************************************************************/

#ifndef hl_ve_h
#define hl_ve_h

#include "utils.h"


int compute_comp(prop_tr *pt, prop_op previous_op);
int compute_comp_with_ve(prop_tr *pt, unsigned var_to_replace, prop_op op_rep, int comp);
int compute_comp_with_ve_pass8(prop_tr *pt, prop_op previous_op, unsigned var_to_replace,
			       prop_op op_rep, int comp, bool sign);
//std::pair<int, int> 
std::pair<std::pair<int, int>, std::pair<int, int>> compute_nb_cls_gen(prop_tr *pt);
std::pair<std::pair<int, int>, std::pair<int, int>> compute_nb_cls_gen_ve(prop_tr *pt, int var_to_elim,
									  std::pair<int, int> cl_gen_var, bool sign);
void update_refby_hl_ve(std::vector<idef*> &mapdef, prop_tr *pt, idef *idef_to_del, idef *idef_to_add);
int nb_of_occurrence(prop_tr *pt, prop_tr *pt2elim);
bool only_pos_occurrence(prop_tr *pt, prop_tr *pt2elim);

void hl_ve(std::vector<idef*> &mapdef, dll_list<idef>* dll_ands, bool verbose);
#endif
