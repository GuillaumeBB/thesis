/***************************************************************************
Copyright (c) 2016-2017, Guillaume Baud-Berthier, SafeRiver/LaBRI

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
IN THE SOFTWARE.
***************************************************************************/

#include "dual.h"

SimpSolver* solver_dual_bmc;
SimpSolver* solver_dual_kind;

vec<Lit>    assumptions_dual_bmc;
vec<Lit>    assumptions_dual_kind;

std::map<unsigned, ands_def*> map_ands_dual_bmc;
std::map<unsigned, ands_def*> map_ands_dual_kind;
std::vector<idef*> mapdef_dual_bmc;
std::vector<idef*> mapdef_dual_kind;

std::vector<dll_list<ands_def>*> list_ands_dual_bmc;
std::vector<dll_list<ands_def>*> list_ands_dual_kind;

dll_list<idef>* dll_ands_dual_bmc;
dll_list<idef>* dll_inputs_dual_bmc;
dll_list<idef>* dll_latches_dual_bmc;
dll_list<idef>* dll_ands_dual_kind;
dll_list<idef>* dll_inputs_dual_kind;
dll_list<idef>* dll_latches_dual_kind;

unsigned vars_dual_bmc  = 0;
unsigned vars_dual_kind = 0;

// Simple path constraints on demand
bool simple_path_od_dual(int time, bool verbose) {
  vec<Lit> lits;
  idef* tmp_def;
  dll_elem<idef> *tmp_e;
  bool same_state;
  bool run_solver_again = 1;
  
  while (run_solver_again) {
    run_solver_again = 0;
     
    for(int i = 0; i < time && !run_solver_again; i++) {
      for(int j = i + 1; j <= time && !run_solver_again; j++) {
	
	same_state = 1;
	
	tmp_e = dll_latches_dual_kind->head;
	while(tmp_e != NULL && same_state) {
	  tmp_def = tmp_e->def;

	  assert(tmp_def->lhs_ts[i] > 1);
	  assert(tmp_def->lhs_ts[j] > 1);
	  assert(solver_dual_kind->model[tmp_def->lhs_ts[i] >> 1] != l_Undef);
	  assert(solver_dual_kind->model[tmp_def->lhs_ts[j] >> 1] != l_Undef);

	  if (solver_dual_kind->model[tmp_def->lhs_ts[i] >> 1] == l_Undef ||
	      solver_dual_kind->model[tmp_def->lhs_ts[j] >> 1] == l_Undef ||
	      solver_dual_kind->model[tmp_def->lhs_ts[i] >> 1] != solver_dual_kind->model[tmp_def->lhs_ts[j] >> 1] ||
	      ((tmp_def->lhs_ts[i] >> 1 == tmp_def->lhs_ts[j] >> 1) &&
	       (tmp_def->lhs_ts[i] >> 1 != tmp_def->lhs_ts[j])) )
	    same_state = 0;
	  
	  tmp_e = tmp_e->next;
	}
	
	if (same_state) {
	  // Add constraints
	  lits.clear();
	  tmp_e = dll_latches_dual_kind->head;
	  while(tmp_e != NULL && same_state) {
	    tmp_def = tmp_e->def;

	    assert(tmp_def->lhs_ts[i] > 1);
	    assert(tmp_def->lhs_ts[j] > 1);
	    
	    unsigned new_v = new_var(&vars_dual_kind) >> 1;

	    while (new_v >= solver_dual_kind->nVars()) solver_dual_kind->newVar();

	    assert(aiger_sign(tmp_def->lhs_ts[i]) == aiger_sign(tmp_def->lhs_ts[j]));
	    lits.push(mkLit(new_v));

	    tcl(solver_dual_kind, -new_v, -(tmp_def->lhs_ts[i] >> 1), -(tmp_def->lhs_ts[j] >> 1));
	    tcl(solver_dual_kind, -new_v,  (tmp_def->lhs_ts[i] >> 1),  (tmp_def->lhs_ts[j] >> 1));
	    tcl(solver_dual_kind,  new_v,  (tmp_def->lhs_ts[i] >> 1), -(tmp_def->lhs_ts[j] >> 1));
	    tcl(solver_dual_kind,  new_v, -(tmp_def->lhs_ts[i] >> 1),  (tmp_def->lhs_ts[j] >> 1));

	    tmp_e = tmp_e->next;
	  }
	  
	  solver_dual_kind->addClause_(lits);
	  run_solver_again = 1;
	}
      }
    }

    if (run_solver_again) {
      if (check_sat(solver_dual_kind, assumptions_dual_kind, verbose) == l_False)
	return 1;
    }

  }

  return 0;
}

dll_list<ands_def>* unroll_kind_dual(int time) {
  dll_list<ands_def> *tmp_list_ands;
  tmp_list_ands = new dll_list<ands_def>();
  dll_init<ands_def>(tmp_list_ands);
  
  idef* tmp_def;
  dll_elem<idef> *tmp_e;

  // Latches
  tmp_e = dll_latches_dual_kind->last;
  while(tmp_e != NULL) {
    tmp_def = tmp_e->def;
    
    if (tmp_def->lhs == aiger_lit2var(tmp_def->rhs.front())) {
      // Special case x := 0, ~x
      assert(aiger_sign(tmp_def->rhs.front()));

      if (!time) {
	assert(!tmp_def->lhs_ts.size());
	tmp_def->lhs_ts.push_back(new_var(&vars_dual_kind));

	//bcl(-(tmp_def->lhs_ts[time] >> 1), -act_init_bwd[time]);

      } else {
	if (tmp_def->lhs_ts.size() == time) {
	  tmp_def->lhs_ts.push_back(aiger_not(tmp_def->lhs_ts[time-1]));
	  if (aiger_sign(tmp_def->lhs_ts[time]))
	    ;//bcl((tmp_def->lhs_ts[time] >> 1), -act_init_bwd[time]);
	  else
	    ;//bcl(-(tmp_def->lhs_ts[time] >> 1), -act_init_bwd[time]);

	} else {
	  int lit_latch  = tmp_def->lhs_ts[time-1];
	  int lit_latch_ = tmp_def->lhs_ts[time];
	  if (aiger_sign(lit_latch_)) {
	    bcl(solver_dual_kind,   lit_latch >> 1,  -(lit_latch_ >> 1));
	    bcl(solver_dual_kind, -(lit_latch >> 1),   lit_latch_ >> 1);
	    ;//bcl(  lit_latch_ >> 1, -act_init_bwd[time]);
	  } else {
	    bcl(solver_dual_kind,   lit_latch >> 1,    lit_latch_ >> 1);
	    bcl(solver_dual_kind, -(lit_latch >> 1), -(lit_latch_ >> 1));
	    ;//bcl(-(lit_latch_ >> 1), -act_init_bwd[time]);
	  }
	}
      }

    } else {

      if (tmp_def->lhs_ts.size() == time) {
	tmp_def->lhs_ts.push_back(new_var(&vars_dual_kind));
      } else {
	assert(tmp_def->lhs_ts.size() == time + 1);
      }

      if (aiger_sign(tmp_def->lhs_ts[time]))
	;//bcl((tmp_def->lhs_ts[time] >> 1), -act_init_bwd[time]);
      else
	;//bcl(-(tmp_def->lhs_ts[time] >> 1), -act_init_bwd[time]);
    
      if (time) {
	assert(tmp_def->rhs.size() == 1);
      
	if (tmp_def->rhs.front() > 1) {

	  if (mapdef_dual_kind[aiger_lit2var(tmp_def->rhs.front())]->lhs_ts.size() > time) {
	    // Special case to consider => Cross reference between latches, e.g. :
	    // x := 0, ~y;
	    // y := 0,  x;
	    // Or 
	    // x := 0,  a;
	    // y := 0,  x;
	    // z := 0, ~x;
	    // We have to add constraints..
	    int lit_latch = tmp_def->lhs_ts[time-1];
	    int lit_def   = mapdef_dual_kind[aiger_lit2var(tmp_def->rhs.front())]->lhs_ts[time];
	    if ((!aiger_sign(lit_latch) && !aiger_sign(lit_def)) ||
		( aiger_sign(lit_latch) &&  aiger_sign(lit_def))) {
	      if (aiger_sign(tmp_def->rhs.front())) {
		bcl(solver_dual_kind,   lit_latch >> 1,    lit_def >> 1);
		bcl(solver_dual_kind, -(lit_latch >> 1), -(lit_def >> 1));
	      } else {
		bcl(solver_dual_kind,   lit_latch >> 1,  -(lit_def >> 1));
		bcl(solver_dual_kind, -(lit_latch >> 1),   lit_def >> 1);
	      }
	    } else {
	      if (aiger_sign(tmp_def->rhs.front())) {
		bcl(solver_dual_kind,   lit_latch >> 1,  -(lit_def >> 1));
		bcl(solver_dual_kind, -(lit_latch >> 1),   lit_def >> 1);
	      } else {
		bcl(solver_dual_kind,   lit_latch >> 1,    lit_def >> 1);
		bcl(solver_dual_kind, -(lit_latch >> 1), -(lit_def >> 1));
	      }
	    }

	  } else {
	    if (aiger_sign(tmp_def->rhs.front()))
	      mapdef_dual_kind[aiger_lit2var(tmp_def->rhs.front())]->lhs_ts.push_back(aiger_not(tmp_def->lhs_ts[time-1]));
	    else
	      mapdef_dual_kind[aiger_lit2var(tmp_def->rhs.front())]->lhs_ts.push_back(tmp_def->lhs_ts[time-1]);
	  }

	} else { // Cst latch
	  if ((tmp_def->rhs.front() && !aiger_sign(tmp_def->lhs_ts[time-1])) ||
	      (!tmp_def->rhs.front() && aiger_sign(tmp_def->lhs_ts[time-1])))
	    ucl(solver_dual_kind, tmp_def->lhs_ts[time-1] >> 1);
	  else
	    ucl(solver_dual_kind, -(tmp_def->lhs_ts[time-1] >> 1));
	}
      }
    }

    tmp_e = tmp_e->prev;
  }

  // Inputs
  tmp_e = dll_inputs_dual_kind->head;
  while(tmp_e != NULL) {
    tmp_def = tmp_e->def;

    if (tmp_def->lhs_ts.size() <= time)
      tmp_def->lhs_ts.push_back(new_var(&vars_dual_kind));
    else
      assert(tmp_def->lhs_ts.size() == time + 1);

    tmp_e = tmp_e->next;
  }

  // Ands
  tmp_e = dll_ands_dual_kind->head;
  while(tmp_e != NULL) {
    tmp_def = tmp_e->def;

    prop_tr_un *tpu = unroll_prop_tr(mapdef_dual_kind, map_ands_dual_kind, tmp_def->rhs_tr, time, 0);

    assert(tpu->lit == UNDEF_LIT);
    assert(tpu->op != VAR);

    if (tmp_def->lhs_ts.size() <= time)
      tmp_def->lhs_ts.push_back(new_var(&vars_dual_kind));
    else
      assert(tmp_def->lhs_ts.size() == time + 1);

    ands_def *tmp_ands = new ands_def(tmp_def->lhs_ts[time], tpu, tmp_def, time);
    map_ands_dual_kind.insert(std::pair<unsigned, ands_def*>(aiger_lit2var(tmp_ands->lhs), tmp_ands));
      
    tmp_ands->dllade = dll_insert_last(tmp_list_ands, aiger_lit2var(tmp_def->lhs_ts[time]), tmp_ands);


    tmp_e = tmp_e->next;
  }

  return tmp_list_ands;
}


/**************************************************************************************/
int dual(int start, int stop, aiger *model, bool elim_hl, int simple, bool verbose_, int *status) {
  int i, j;

  dll_inputs_dual_kind = new dll_list<idef>();
  dll_latches_dual_kind = new dll_list<idef>();
  dll_ands_dual_kind = new dll_list<idef>();

  unsigned bad_lit_kind = init_model(model, dll_ands_dual_kind, dll_inputs_dual_kind, dll_latches_dual_kind, mapdef_dual_kind, 0, verbose_);

  dll_inputs_dual_bmc = new dll_list<idef>();
  dll_latches_dual_bmc = new dll_list<idef>();
  dll_ands_dual_bmc = new dll_list<idef>();

  unsigned bad_def_bmc = init_model(model, dll_ands_dual_bmc, dll_inputs_dual_bmc, dll_latches_dual_bmc, mapdef_dual_bmc, 0, verbose_);

  aiger_reset(model);

  solver_dual_kind = new SimpSolver();
  solver_dual_bmc = new SimpSolver();
  init_solver(solver_dual_kind, 0, verbose_);
  init_solver(solver_dual_bmc, 0, verbose_);

  if (elim_hl) {
    hl_ve(mapdef_dual_kind, dll_ands_dual_kind, verbose_);
    hl_ve(mapdef_dual_bmc, dll_ands_dual_bmc, verbose_);
  }

  for(i = 0; i <= stop; i++) {
    //act_init_bwd.push_back(new_var(&vars_dual_kind) >> 1);

    if (verbose_) printf("[zigzag]  Encoding %d-induction..\n", i);

    list_ands_dual_kind.push_back(unroll_kind_dual(i));

    iter_flatten_ptu(list_ands_dual_kind, i);

    ands2clauses(solver_dual_kind, list_ands_dual_kind, i, 0);

    assert(mapdef_dual_kind[aiger_lit2var(bad_lit_kind)]->lhs_ts[i] > 1);
    
    // Add Bad cls
    int lit_bad;
    lit_bad = mapdef_dual_kind[aiger_lit2var(bad_lit_kind)]->lhs_ts[i];
    std::map<unsigned, ands_def*>::iterator lit_bad_ref = map_ands_dual_kind.find(aiger_lit2var(lit_bad));
    if (lit_bad_ref != map_ands_dual_kind.end())
      assert(lit_bad_ref->second->cst == -1);
      
    if (aiger_sign(lit_bad))
      lit_bad = aiger_sign(bad_lit_kind) ?  (lit_bad >> 1) : -(lit_bad >> 1);
    else
      lit_bad = aiger_sign(bad_lit_kind) ? -(lit_bad >> 1) :  (lit_bad >> 1);

    if (i) 
      ucl(solver_dual_kind, -lit_bad); // Assert:  P(x_i);
    else
      ucl(solver_dual_kind, lit_bad);  // Assert: -P(x_0);

    if (verbose_) printf("[zigzag]  Solving %d-induction..\n", i);

    lbool res_check = check_sat(solver_dual_kind, assumptions_dual_kind, verbose_); 
    if (res_check == l_False) {
      *status = 0;
      break;
    } else if (i > 1 && simple == 2) {
      if (simple_path_od_dual(i, verbose_)) {
	*status = 0;
	break;
      }
    }

    if (verbose_) printf("[zigzag]  Encoding BMC %d..\n", i);
    bool elim_ll = 0;

    list_ands_dual_bmc.push_back(unroll(dll_ands_dual_bmc, dll_inputs_dual_bmc, dll_latches_dual_bmc, mapdef_dual_bmc, map_ands_dual_bmc, &vars_dual_bmc, i));
    
    iter_flatten_ptu(list_ands_dual_bmc, i);
    
    simp_ands(solver_dual_bmc, list_ands_dual_bmc, mapdef_dual_bmc, map_ands_dual_bmc, i, bad_def_bmc, 0, verbose_, elim_ll);

    if (elim_ll) {
      if (verbose_) printf("[bmc]  Warning : k-COI is disabled because of varelim!\n");
      ands2clauses(solver_dual_bmc, list_ands_dual_bmc, i, elim_ll);
    } else {
      ands2clauses_coi(solver_dual_bmc, list_ands_dual_bmc, mapdef_dual_bmc, map_ands_dual_bmc, bad_def_bmc, i, verbose_);
    }

    if (verbose_) printf("[zigzag]  Solving BMC %d..\n", i);

    if ((!mapdef_dual_bmc[aiger_lit2var(bad_def_bmc)]->lhs_ts[i] &&
	 !aiger_sign(bad_def_bmc)) ||
	(mapdef_dual_bmc[aiger_lit2var(bad_def_bmc)]->lhs_ts[i] == 1 &&
	 aiger_sign(bad_def_bmc))) {
      
      if (verbose_) printf("[zigzag]  UNSAT (Solved by simplification)\n");
      printf("u%d\n", i);
      fflush(stdout);

      if (!dll_size(dll_latches_dual_bmc)) {
	*status = 0;
	break;
      }

    } else if ((!mapdef_dual_bmc[aiger_lit2var(bad_def_bmc)]->lhs_ts[i] &&
		aiger_sign(bad_def_bmc)) ||
	       (mapdef_dual_bmc[aiger_lit2var(bad_def_bmc)]->lhs_ts[i] == 1 &&
		!aiger_sign(bad_def_bmc))) {

      if (verbose_) printf("[zigzag]  CEX (Solved by simplification)\n");
      *status = 1;
      break;
	
    } else {

      unsigned var_po = mapdef_dual_bmc[aiger_lit2var(bad_def_bmc)]->lhs_ts[i];
      assert(var_po > 1);
      std::map<unsigned, ands_def*>::iterator it_lit_ref = map_ands_dual_bmc.find(aiger_lit2var(var_po));

      assert(it_lit_ref != map_ands_dual_bmc.end()); // Might be false if po <=> input
      assert(it_lit_ref->second->cst == -1);
      
      //int lit_bad = addPO(solver_dual_bmc, mapdef_dual_bmc, &vars_dual_bmc, bad_def_bmc, i);
      int lit_bad = mapdef_dual_bmc[aiger_lit2var(bad_def_bmc)]->lhs_ts[i];
      if (aiger_sign(lit_bad))
	lit_bad = aiger_sign(bad_def_bmc) ?  (lit_bad >> 1) : -(lit_bad >> 1);
      else
	lit_bad = aiger_sign(bad_def_bmc) ? -(lit_bad >> 1) :  (lit_bad >> 1);

      assume(solver_dual_bmc, assumptions_dual_bmc, lit_bad);
      //assume(solver_dual_bmc, assumptions_dual_bmc, mapdef_dual_bmc[0]->lhs_ts.back() >> 1);

      lbool res_check;
      res_check = check_sat(solver_dual_bmc, assumptions_dual_bmc, verbose_);
      if (res_check == l_True) {
	*status = 1;
	break;
      } else if (res_check == l_Undef) {
	assert(0);
      }


      printf("u%d\n", i);
      fflush(stdout);

      if (!dll_size(dll_latches_dual_bmc)) {
	*status = 0;
	break;
      }         
      
      ucl(solver_dual_bmc, -lit_bad);
      //ucl(solver_dual_bmc, -(mapdef_dual_bmc[0]->lhs_ts.back() >> 1));
    }
  
  }

  //printf("%d\nb0\n.\n", *status);
  //fflush(stdout);

  if (verbose_)
    printf("Vars %d (%d - %d)| Clauses %d\n",
	   solver_dual_kind->nVars() - solver_dual_kind->eliminated_vars, solver_dual_kind->nVars(), solver_dual_kind->eliminated_vars, solver_dual_kind->nClauses());
  destroy_model(dll_ands_dual_kind, dll_inputs_dual_kind, dll_latches_dual_kind, list_ands_dual_kind, mapdef_dual_kind, map_ands_dual_kind, verbose_);
  destroy_model(dll_ands_dual_bmc, dll_inputs_dual_bmc, dll_latches_dual_bmc, list_ands_dual_bmc, mapdef_dual_bmc, map_ands_dual_bmc, verbose_);
  return 0;
}

